﻿jQuery(document).ready(function () {
    LoadGrid();

    jQuery('#tableAuth_Permissions').dataTable();

    $("#btnCreate").click(function () {
        CreateRow();
    });
});

function LoadGrid(e) {
    if (e) {
        e.preventDefault();
    }
    BlockScreen();

    jQuery.ajax({
        url: "/Auth_Permissions/IndexGrid",
        data: null,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length == 0) {
                Notifications("information", GetResource("NoSeEncontraronDatos"))
            } else {
                var tblAuth_Permissions = jQuery('#tableAuth_Permissions').DataTable();

                //Data Table
                $.each(data, function (i, data) {
                    tblAuth_Permissions.row.add([
                        data.PermissionID,
                        data.Name,
                        data.Icon,
                        data.FlagActive,
                    ]).node().id = "rowId" + intSensors;

                    intSensors++;
                });
                tblSensorsList.draw(false);
            //Data Table

            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#ddlBusinessUnit")).text());
        },
        async: false
    });

};

function CreateRow() {
    var url = "/Auth_Permissions/Create";
    window.location = url;
};

function EditRow(arrKey) {
    var PermissionID = arrKey[0];
    var url = "/Auth_Permissions/Edit?PermissionID=" + ID;

    window.location = url;
};
