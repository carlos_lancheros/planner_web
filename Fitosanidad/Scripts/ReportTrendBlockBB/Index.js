﻿$("document").ready(function () {

    CargaProductos();

    $("#BusinessUnitID").change(function () {
        CargaProductos();
    });
});


function CargaProductos() {
    var BusinessUnitID = $("#BusinessUnitID").val();

    var Datos = {
        "GreenHouseID": BusinessUnitID,
    }

    $.ajax({
        DataType: "JSON",
        type: "POST",
        data: Datos,
        url: "/ReportTrendBlockBB/CargaProductos",
        success: function (r) {
            var $html = "<option>TODOS</option>";
            for (x in r) {
                $html += "<option value=" + r[x]['PlantProductID'] + ">" + r[x]['PlantProductName'] + "</option>";
            }
            $("#PlantProductID").html($html);
        }
    });
}

function CreaReporte() {
    var WeekIni = $("#WeekIni").val();
    var WeekFin = $("#WeekFin").val();
    var BusinessUnitID = $("#BusinessUnitID").val();
    var PlantProductID = $("#PlantProductID").val();
    var PestID = $("#PestID").val();

    if (!PestID) {
        Notifications("error", "Debe seleccionar al menos una plaga");
        return;
    }
    PestID = PestID.toString();
    //PestID = PestID.replace(",", "/");
    //console.log("exec InformeTendenciaBloqueBB " + GreenHouseID + ", '" + PestId + "' , " + ProductId);


    Datos = {
        "WeekIni": WeekIni,
        "WeekFin": WeekFin,
        "BusinessUnitID": BusinessUnitID,
        "PlantProductID": PlantProductID,
        "PlagaIdM": PestID,
        "ReportType": 2,
    };
    BlockScreen();
    $.ajax({
        DataType: "JSON",
        type: "POST",
        data: Datos,
        url: "/ReportTrendBlockBB/CreaReporte",
        success: function (r) {

            //Semanas
            var Weeks = [];
            for (i in r) {
                Weeks.push(r[i].Semana);
            }
            $.unique(Weeks);

            //Bloque
            var Blocks = [];
            for (i in r) {
                Blocks.push(r[i].Bloque);
            }
            $.unique(Blocks.sort());

            //Plagas
            var Plagas = [];
            for (i in r) {
                Plagas.push(r[i].Plaga);
            }
            $.unique(Plagas.sort());

            //PlagasID
            var PlagasID = [];
            for (i in r) {
                PlagasID.push(r[i].PlagaId);
            }
            $.unique(PlagasID.sort());

            var html = "";

            for (i in Blocks) {
                if (Weeks.length >= 10) {
                    html += '<div class="col-md-12" id="Div' + Blocks[i] + '"><div class="col-sm-12 text-center text-bold">Incidencia Bloque ' + Blocks[i] + '<h4></h4></div><canvas id="Chart_' + Blocks[i] + '"></canvas></div>';
                }
                else {
                    html += '<div class="col-md-6" id="Div' + Blocks[i] + '"><div class="col-sm-12 text-center text-bold">Incidencia Bloque ' + Blocks[i] + '<h4></h4></div><canvas id="Chart_' + Blocks[i] + '"></canvas></div>';
                }
               
            }
            $("#graficas").html(html);

            //Armado de data
            var Color = [];
            var BloqueSinData = [];
            var DatosPorPlagas = [];
            for (p in Plagas) {
                var Data = [];
                Color[Plagas[p]] = [];
                for (a in Blocks) {
                    var dataTemp = 0;
                    Data[Blocks[a]] = [];
                    for (b in Weeks) {
                        for (c in r) {
                            if (r[c]['Semana'] === Weeks[b] && r[c]['Bloque'] === Blocks[a] && r[c]['Plaga'] === Plagas[p]) {

                                Color[Plagas[p]].push(r[c]['Color']);

                                if (r[c]['Monitoreo'] === -1) {
                                    Data[Blocks[a]].push("NaN");
                                }
                                else {
                                    Data[Blocks[a]].push(r[c]['PPIncidencia']);
                                    dataTemp++;
                                }
                            }
                        }
                    }
                }
                DatosPorPlagas.push(Data);

            }

            for (i in Blocks) {
                var Sets = [];
                for (d in DatosPorPlagas) {
                    var color = random_rgba();
                    var Label = [];
                    //console.log(DatosPorPlagas[d][Blocks[i]]);
                    console.log(Plagas[d]);
                    Sets.push({
                        label: Plagas[d],
                        backgroundColor: Color[Plagas[d]][0],
                        data: DatosPorPlagas[d][Blocks[i]],
                        borderColor: Color[Plagas[d]][0],
                        fill: false,
                    });
                }
                var ctx = document.getElementById('Chart_' + Blocks[i]);
                var color = random_rgba();
                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: Weeks,
                        //datasets: [{
                        //    //label: 'Bloque ' + Blocks[i],
                        //    label: Plagas,
                        //    data: Data[Blocks[i]],
                        //    borderColor: color,
                        //    fill: false,
                        //    backgroundColor: color,
                        //}],

                        datasets: Sets,
                    },
                    options: {
                        "hover": {
                            "animationDuration": 0,
                            onHover: function () {
                                return false;
                            }
                        },
                        legend: {
                            "display": false
                        },
                        tooltips: {
                            "enabled": false
                        },
                        legend: {
                            onClick: (e) => e.stopPropagation()
                        },
                        "animation": {
                            "hover": {
                                "animationDuration": 0
                            },
                            "duration": 1,
                            "onComplete": function () {
                                var chartInstance = this.chart,
                                    ctx = chartInstance.ctx;

                                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                ctx.textBaseline = 'bottom';
                                ctx.textAlign = 'rigth';

                                this.data.datasets.forEach(function (dataset, i) {
                                    var meta = chartInstance.controller.getDatasetMeta(i);
                                    meta.data.forEach(function (bar, index) {
                                        var data = dataset.data[index];
                                        ctx.save();
                                        var random = Math.floor(Math.random() * 16);
                                        ctx.translate(bar._model.x + 7, bar._model.y);
                                        //ctx.rotate(-0.5 * Math.PI);
                                        ctx.fillText(data, 0, 0);
                                        ctx.restore();
                                    });
                                });
                            }
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    min: 0,
                                    max: 100,
                                },
                                scaleLabel: {
                                    display: true,
                                    labelString: '% INCIDENCIA'
                                }
                            }],
                            xAxes: [{
                                scaleLabel: {
                                    display: true,
                                    labelString: 'SEMANA'
                                }
                            }]
                        },
                    }
                });
            }

            //Borra Graficas sin información

            //console.log($.unique(BloqueSinData));

            for (x in BloqueSinData) {
                //$("#" + BloqueSinData[x]).remove();
            }
        },
        complete: function () {
            UnlockScreen();
        }
    })
}

function random_rgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ', 1)';
}