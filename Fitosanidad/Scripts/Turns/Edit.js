﻿$(document).ready(function () {
    $("#btnHome").click(function () {
        BackToList();
    });
    $("#btnSave").click(function () {
        CallSubmit();
    });

    LoadIniData();
});

function BackToList() {
    var Url = "/Auth_FormsActions";
    window.location.href = Url;
};

function CallSubmit() {
    if (jQuery('#Name').val() == null) {
        Notifications("error", GetResource("DebeDigitarDescripcion"))
        jQuery('#Name').focus();
        return false;
    } else {
        if (jQuery('#Name').val().length == 0) {
            Notifications("error", GetResource("DebeDigitarDescripcion"))
            jQuery('#Name').focus();
            return false;
        };
    }

    jQuery("#Edit").submit();
};

function LoadIniData() {
    JSONGetCompanies();

};

function JSONGetCompanies() {
    var prmCodigoTurno = getParameterByName("ActionID");

    BlockScreen();
    jQuery.ajax({
        url: "/Turns/JSONGetCompanies",
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });
};