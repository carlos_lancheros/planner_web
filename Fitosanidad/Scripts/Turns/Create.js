﻿$(document).ready(function () {

    $('#Almuerzo').val('30');
    $('#Refrigerio1').val('0');
    $('#Refrigerio2').val('0');
    $('#Cena').val('0');

    $('#startTime').timepicker({
        minuteStep: 15,
        showSeconds: false,
        showMeridian: false,
        defaultTime: false
    });
    $('#endTime').timepicker({
        minuteStep: 15,
        showSeconds: false,
        showMeridian: false,
        defaultTime: false
    });

    jQuery("#startDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#startDate').focus();
            }
        }
    });
    jQuery('#startDate').datepicker('setDate', 'today');
    jQuery('#startDate').attr("autocomplete", "off");

    $("#ActionID").val(0);


    $("#btnHome").click(function () {
        BackToList();
    });
    $("#btnSave").click(function () {
        CallSubmit();
    });
});

function BackToList() {
    var Url = "/Turns";
    window.location.href = Url;
};

function CallSubmit() {
    if (jQuery('#Name').val() == null) {
        Notifications("error", GetResource("DebeDigitarDescripcion"))
        jQuery('#Name').focus();
        return false;
    } else {
        if (jQuery('#Name').val().length == 0) {
            Notifications("error", GetResource("DebeDigitarDescripcion"))
            jQuery('#Name').focus();
            return false;
        };
    }    
    if (jQuery('#ID').val().length != 0) {
        Notifications("error", GetResource("Ya se guardo el registro"))
        jQuery('#Name').focus();
        return false;
    };
    
    Save();
};
function Save() {

    //var Fecha = jQuery("#startDate").val();
    var strDate = $("#startDate").val().split("-")
    var srtDateIni = new Date(strDate[2], strDate[1] - 1, strDate[0])
    var strDate = $("#startDate").val().split("-")
    var srtDateFin = new Date(strDate[2], strDate[1] - 1, strDate[0])

    //var values = { "CentroOperacionID": CentroOperacionID, "FechaIni": srtDateIni, "FechaFin": srtDateFin }

    var Name = jQuery("#Name").val();
    var starTime = jQuery("#startTime").val();
    var endTime = jQuery("#endTime").val();
    var CompaniaID = jQuery("#ddlCompania").val();
    var UnidadNegocioID = jQuery("#ddlUnidadNegocio").val();
    var CentrosOperacionID = jQuery("#ddlCentrosOperacion").val();
    var CentrosCostoID = jQuery("#ddlCentrosCosto").val();
    var Almuerzo = jQuery("#Almuerzo").val();    
    var Refrigerio1 = jQuery("#Refrigerio1").val();
    var Refrigerio2 = jQuery("#Refrigerio2").val();
    var Cena = jQuery("#Cena").val();
    var Cena = jQuery("#Cena").val();
    var Observaciones = jQuery("#Observaciones").val();
    ///var PasaotroDia = jQuery("#PasaotroDia").val();
    var PasaotroDia = $('#PasaotroDia').prop('checked');   
    PasaotroDia = (PasaotroDia ? 1 : 0);
    var IDAppUsrs = 34;
    var EmpleadosID = jQuery("#ddlEmpleados").val();

    var values = {
        "Nombre": Name,"FechaIni": srtDateIni, "FechaFin": srtDateFin, "CompaniaID": CompaniaID, "UnidadNegocioID": UnidadNegocioID,
        "CentrosOperacionID": CentrosOperacionID, "CentrosCostoID": CentrosCostoID, "HInicial": starTime, "HFin": endTime, "Almuerzo": Almuerzo,
        "Refrigerio1": Refrigerio1, "Refrigerio2": Refrigerio2, "Cena": Cena, "TurnoOtroDia": PasaotroDia, "Observaciones": Observaciones,
        "IDAppUsrs": IDAppUsrs, "EmpleadosID": EmpleadosID
    }

    BlockScreen();
    jQuery.ajax({
        url: "/Turns/Save",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length == 0) {
                Notifications("error", "No se puedo crear Turno. Por favor verifique")
            } else {
                $("#ID").val(data);
            }

            console.log(data);
            //jQuery('#TurnsDataTable').html(data);
            //var table = jQuery('#tableTurns').DataTable({
            //    "bDestroy": true,
            //    "searching": true
            //    , "info": true
            //    , paging: true
            //    , responsive: true
                
            //});

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });
}
function AgregarEmpleados() {

    var EmpleadosID = jQuery("#ddlEmpleados").val();
    var ArrCodes = [];
    jQuery("#tableLabelPrint").find("input[type='checkbox']").each(function () {
        if (jQuery(this).is(':checked')) {
            var g = jQuery(this).attr('id');
            ArrCodes.push(g);
        };
    });

    var ArrNames = [];
    jQuery("#tableLabelPrint").find("input[type='checkbox']").each(function () {
        if (jQuery(this).is(':checked')) {
            var valores = $(this).parents("tr").find("td")[2].innerHTML;
            ArrNames.push(valores);
        };
    });


    var Quantity = jQuery('#LabelQuantity').val();
    var Week = jQuery('#lstWeek').find(":selected").text();
    var Printer = jQuery('#lstPrinter').find(":selected").val();


    var CentrosOperacion = jQuery("#lstCentrosOperacion").find(":selected").text();
    var Abrr;
    if (CentrosOperacion == "POSTCOSECHA EL SENDERO") {
        Abrr = CentrosOperacion.substring(0, 1) + "-" + CentrosOperacion.substring(15, 18);
    } else {
        Abrr = CentrosOperacion.substring(0, 1) + "-" + CentrosOperacion.substring(12, 15);
    }

    var a = Week.replace(' ', '');
    var a = a.replace(' ', '');
    var parameters = { "Quantity": Quantity, "Week": a, "ArrayCodes": ArrCodes, "ArrNames": ArrNames, "LabelUnit": Abrr, "Printer": Printer }

    jQuery.ajax({
        url: "/LabelPrint/GenerateCode",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);
            location = "/LabelPrint/Download/?nombre=" + data;
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(errorThrown);

            //UnlockScreen();
            //Notifications("error", errorThrown)
        },
    });
}
