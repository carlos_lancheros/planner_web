﻿var tblTurns

jQuery(document).ready(function () {
    jQuery("#startDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#startDate').focus();
            }
        }
    });
    jQuery('#startDate').datepicker('setDate', 'today');
    jQuery('#startDate').attr("autocomplete", "off");

    jQuery("#endDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#startDate').focus();
            }
        }
    });
    jQuery('#endDate').datepicker('setDate', 'today');
    jQuery('#endDate').attr("autocomplete", "off");

    tblTurns = jQuery('#tableTurns').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , orderCellsTop: true
        , scrollX: true
        , scrollCollapse: true
        , fixedHeader: true
        , fixedColumns: {
            leftColumns: 2
        }
        , "columnDefs": [
            {
                "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
                "className": "text-left",
            },
            { className: "dt-right", "width": "8%", "targets": 0 }
            , { "width": "8%", "targets": 1 }
            , { "width": "6%", "targets": 2 }
            , { "width": "6%", "targets": 3 }
            , { "width": "4%", "targets": 4 }
            , { "width": "4%", "targets": 5 }
            , { "width": "4%", "targets": 6 }
            , { "width": "4%", "targets": 7 }
            , { "width": "4%", "targets": 8 }
            , { "width": "4%", "targets": 9 }
            , { "width": "4%", "targets": 10 }
            , { "width": "4%", "targets": 11 }
            , { "width": "4%", "targets": 12 }
            , { "width": "4%", "targets": 13 }
            , { "width": "4%", "targets": 14 }
            , { "width": "4%", "targets": 15 }
            , { "width": "4%", "targets": 16 }
            , { "width": "4%", "targets": 17 }
            , { "width": "4%", "targets": 18 }
            , { "width": "4%", "targets": 19 }
            , { "width": "4%", "targets": 20 }
        ],
        rowsGroup: [
            0,
            1,
            2,
        ],
        rowGroup: {
            dataSrc: 'group'
        }
    });

    jQuery("#ddlCompania").change(function () {
        //Search();

        //var CompaniaID = jQuery("#ddlCompania").find(":selected").val();
        //var UnidadNegocioID = 0;
        //var CentrosOperacionID = 0;
        //var CentrosCostoID = 0;        
        //EmployeesEnable = (EmployeesEnable ? 1 : 0);

        //var values = { "CompaniaID": CompaniaID, "UnidadNegocioID": UnidadNegocioID, "CentrosOperacionID": CentrosOperacionID, "CentrosCostoID": CentrosCostoID, "FlagActivo": EmployeesEnable }

        //jQuery("#tableEmployees").empty()

        //jQuery.ajax({
        //    url: "/Employees/Employees",
        //    data: JSON.stringify(values),
        //    type: 'POST',
        //    contentType: 'application/json; charset=utf-8',
        //    success: function (data) {
        //        var table = jQuery('#tableEmployees').DataTable();
        //        table.destroy();
        //        jQuery('#tableEmployees').html(data);
        //        jQuery('#tableEmployees').DataTable();
        //    },
        //    error: function () {

        //    }
        //});
    });

    jQuery("#ddlUnidadNegocio").change(function () {
        //Search();

        //var CompaniaID = jQuery("#ddlCompania").find(":selected").val();
        //var UnidadNegocioID = jQuery("#ddlUnidadNegocio").find(":selected").val();
        //var CentrosOperacionID = 0;
        //var CentrosCostoID = 0;        
        //EmployeesEnable = (EmployeesEnable ? 1 : 0);

        //var values = { "CompaniaID": CompaniaID, "UnidadNegocioID": UnidadNegocioID, "CentrosOperacionID": CentrosOperacionID, "CentrosCostoID": CentrosCostoID, "FlagActivo": EmployeesEnable }


        //jQuery("#tableEmployees").empty()

        //jQuery.ajax({
        //    url: "/Employees/Employees",
        //    data: JSON.stringify(values),
        //    type: 'POST',
        //    contentType: 'application/json; charset=utf-8',
        //    success: function (data) {
        //        var table = jQuery('#tableEmployees').DataTable();
        //        table.destroy();
        //        jQuery('#tableEmployees').html(data);
        //        jQuery('#tableEmployees').DataTable();
        //    },
        //    error: function () {

        //    }
        //});
    });

    jQuery("#ddlCentrosOperacion").change(function () {
        //Search();

        //var CompaniaID = jQuery("#ddlCompania").find(":selected").val();
        //var UnidadNegocioID = jQuery("#ddlUnidadNegocio").find(":selected").val();
        //var CentrosOperacionID = jQuery("#ddlCentrosOperacion").find(":selected").val();
        //var CentrosCostoID = 0;
        //EmployeesEnable = (EmployeesEnable ? 1 : 0);

        //var values = { "CompaniaID": CompaniaID, "UnidadNegocioID": UnidadNegocioID, "CentrosOperacionID": CentrosOperacionID, "CentrosCostoID": CentrosCostoID, "FlagActivo": EmployeesEnable }

        //jQuery("#tableEmployees").empty()

        //jQuery.ajax({
        //    url: "/Employees/Employees",
        //    data: JSON.stringify(values),
        //    type: 'POST',
        //    contentType: 'application/json; charset=utf-8',
        //    success: function (data) {
        //        var table = jQuery('#tableEmployees').DataTable();
        //        table.destroy();
        //        jQuery('#tableEmployees').html(data);
        //        jQuery('#tableEmployees').DataTable();
        //    },
        //    error: function () {

        //    }
        //});
    });

    jQuery("#ddlCentrosCosto").change(function () {
        //Search();

        //var CompaniaID = jQuery("#ddlCompania").find(":selected").val();
        //var UnidadNegocioID = jQuery("#ddlUnidadNegocio").find(":selected").val();
        //var CentrosOperacionID = jQuery("#ddlCentrosOperacion").find(":selected").val();
        //var CentrosCostoID = jQuery("#ddlCentrosCosto").find(":selected").val();
        //EmployeesEnable = (EmployeesEnable ? 1 : 0);

        //var values = { "CompaniaID": CompaniaID, "UnidadNegocioID": UnidadNegocioID, "CentrosOperacionID": CentrosOperacionID, "CentrosCostoID": CentrosCostoID, "FlagActivo": EmployeesEnable }


        //jQuery("#tableEmployees").empty()

        //jQuery.ajax({
        //    url: "/Employees/Employees",
        //    data: JSON.stringify(values),
        //    type: 'POST',
        //    contentType: 'application/json; charset=utf-8',
        //    success: function (data) {
        //        var table = jQuery('#tableEmployees').DataTable();
        //        table.destroy();
        //        jQuery('#tableEmployees').html(data);
        //        jQuery('#tableEmployees').DataTable();
        //    },
        //    error: function () {

        //    }
        //});
    });

    jQuery("#dataExport").click(function () {
        jQuery('<table>')
            .append(
                jQuery("#tableTurns").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Turnos"
            });
    });

    //jQuery("#dataExport").click(function () {
    //    jQuery("#tableEmployees").table2excel({
    //        exclude: ".noExl",
    //        name: "Worksheet Name",
    //        filename: "Empleados" //do not include extension
    //    });
    //});
    $("#btnCreate").click(function () {
        CreateRow();
    });
});

function Search() {

    //var Fecha = jQuery("#startDate").val();
    var strDate = $("#startDate").val().split("-")
    var srtDateIni = new Date(strDate[2], strDate[1] - 1, strDate[0])
    var strDate = $("#endDate").val().split("-")
    var srtDateFin = new Date(strDate[2], strDate[1] - 1, strDate[0])

    //var values = { "CentroOperacionID": CentroOperacionID, "FechaIni": srtDateIni, "FechaFin": srtDateFin }

    var CompaniaID = jQuery("#ddlCompania").find(":selected").val();
    var UnidadNegocioID = jQuery("#ddlUnidadNegocio").find(":selected").val();
    var CentrosOperacionID = jQuery("#ddlCentrosOperacion").find(":selected").val();
    var CentrosCostoID = jQuery("#ddlCentrosCosto").find(":selected").val();    

    var values = { "FechaIni": srtDateIni, "FechaFin": srtDateFin, "CompaniaID": CompaniaID, "UnidadNegocioID": UnidadNegocioID, "CentrosOperacionID": CentrosOperacionID, "CentrosCostoID": CentrosCostoID, "FlagActivo": 34}

    BlockScreen();
    jQuery.ajax({
        url: "/Turns/GetTurns",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblTurns = jQuery('#tableTurns').DataTable();
            tblTurns.clear().draw();

            $.each(data, function (i, data) {
                tblTurns.row.add([
                    data.IDEntidadesProductivas_Nombre,
                    data.IDCentrosCosto_Nombre,
                    data.Dia,
                    data.IDTurnosHorarios,
                    jsonTimeFormat(data.HoraEntrada,true,true,false),
                    jsonTimeFormat(data.HoraSalida, true,true,false),
                    data.CantidadHoras,
                    data.HorasJornadasOrdinarias,
                    data.HorasExtrasDiurnas,
                    data.HorasExtrasNocturnas,
                    data.HorasFestivaDiurnas,
                    data.HorasFestivaDiurnas,
                    data.HorasFestivaDiurnasExtras,
                    data.HorasFestivaNocturnas,
                    data.HorasRecargoNocturno,
                    data.HorasRecargoNocturnoDominical,
                    data.Refrigerio1,
                    data.Refrigerio2,
                    data.Cena,
                    data.TotalHoras,
                    '<div class="actions">' +
                    '    <div class="btn-group">' +
                    '        <button class="btn btn-xs" type="button" data-toggle="tooltip" data-placement="top" title="' + GetResource("Editar") + '" onclick="location=\'/Turns/Edit?ActionID=' + data.IDTurnosHorarios + '\'">' +
                    '            <i class="fa fa-pencil text-blue"></i>' +
                    '        </button>' + 
                    '    </div>' +
                    '</div>',
                ]);

            });
            tblTurns.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });
}
function CreateRow() {
    var url = "/Turns/Create";
    window.location = url;
};


