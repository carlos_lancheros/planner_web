﻿var tblConfirm

jQuery(window).load(function () {
    BlockScreen();
});

jQuery(document).ready(function () {
    UnlockScreen();

    $('#btnView').on('click', function () {
        GetWebGuideKey()
    });
});

function GetWebGuideKey() {
    BlockScreen();
    jQuery.ajax({
        url: "/Orders/GetWebGuideKey",
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $("#modPin").val(data);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}