﻿var tblOrders

jQuery(window).load(function () {
    BlockScreen();
});

jQuery(document).ready(function () {
    UnlockScreen();

    $('#tableOrders thead tr').clone(true).appendTo('#tableOrders thead');
    $('#tableOrders thead tr:eq(1) th').each(function (i) {
        $(this).html('<input type="text" />');

        $('input', this).on('keyup change', function () {
            if (tblOrders.column(i).search() !== this.value) {
                tblOrders
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        });
    });

    tblOrders = jQuery('#tableOrders').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , orderCellsTop: true
        , scrollX: true
        , scrollCollapse: true
        , fixedHeader: true
        , fixedColumns: {
            leftColumns: 2
        }
        , "columnDefs": [
            {
                "targets": [2, 3, 8],
                "className": "text-right",
            }
            , { "width": "05%", "targets": 0 }
            , { "width": "05%", "targets": 1 }
            , { "width": "10%", "targets": 2 }
            , { "width": "10%", "targets": 3 }
            , { "width": "10%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
            , { "width": "05%", "targets": 6 }
            , { "width": "05%", "targets": 7 }
            , { "width": "05%", "targets": 8 }
            , { "width": "10%", "targets": 9 }
            , { "width": "10%", "targets": 10 }
        ]
        , 'order': [[2, 'asc']]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

});

function BackToList() {
    var Url = "/Orders/Confirm";
    window.location.href = Url;
}

function LoadGrid() {
    var prmIDordenesFinca = getParameterByName("ID");

    var parameters = { "prmIDordenesFinca": prmIDordenesFinca }

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/GetOrderDetailById",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblDetail = jQuery('#tableDetail').DataTable();
            tblDetail.clear().draw();

            $.each(data, function (i, data) {
                tblDetail.row.add([
                    '<div class="actions">' +
                    '    <div class="btn-group">' +
                    '        <button class="btn btn-xs" type="button" data-toggle="tooltip" data-placement="top" title="' + GetResource("Detalles") + '" onclick=GetBunchRow(' + data.ID + ')>' +
                    '            <i class="fa fa-search text-orange"></i>' +
                    '        </button>' +
                    '        <button class="btn btn-xs" type="button" data-toggle="tooltip" data-placement="top" title="' + GetResource("UPC") + '" onclick=GetUpcRow(' + data.ID + ')>' +
                    '            <i class="fa fa-barcode text-green"></i>' +
                    '        </button>' +
                    '        <button class="btn btn-xs" type="button" data-toggle="tooltip" data-placement="top" title="' + GetResource("ConsolidarPorRamo") + '" onclick=GetBouquetRow(' + data.ID + ')>' +
                    '            <i class="fa fa fa-cogs text-red"></i>' +
                    '        </button>' +
                    '    </div>' +
                    '</div>',
                    data.ID,
                    data.IDProductos_Nombre,
                    data.Pack,
                    data.IDCajas_Nombre,
                    data.Cantidad,
                    data.TotalTallos,
                    data.Label_Color,
                    data.Status_Detalle,
                ]);

            });
            tblDetail.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function GetBunchRow(ID) {
    var Url = "/Orders/Bunch?DetailID=" + getParameterByName("ID") + "&ID=" + ID;
    window.location.href = Url;
}

function GetUpcRow(ID) {
    var Url = "/Orders/UPC2Pdf?DetailID=" + getParameterByName("ID") + "&ID=" + ID;
    window.location.href = Url;

    //var Url = "/Orders/Detail2Pdf?DetailID=" + getParameterByName("ID") + "&ID=" + ID;
    //window.location.href = Url;
}

function GetBouquetRow(ID) {
    $("#BouquetId").val(ID);
    $("#modProducto").val();
    $("#modEmpaque").val();
    $("#modTipoDeCaja").val();
    $("#modCantidadDeCajas").val();
    $("#modCantidadDeTallos").val();
    $("#modColorEtiqueta").val();
    $("#modEstado").val();

    var tblData = tblDetail.rows().data();

    tblData.each(function (value, index) {
        if (ID == value[1]) {
            $("#modProducto").val(value[2]);
            $("#modEmpaque").val(value[3]);
            $("#modTipoDeCaja").val(value[4]);
            $("#modCantidadDeCajas").val(value[5]);
            $("#modCantidadDeTallos").val(value[6]);
            $("#modColorEtiqueta").val(value[7]);
            $("#modEstado").val(value[8]);
        }
    });

    $('#modalBouquet').modal('toggle');
    $('#modalBouquet').modal('show');

    return false;
}

function SetBouquetRow() {

    $('#modalBouquet').modal('hide');

    var parameters = { "prmIDDetalleOriginal": $("#BouquetId").val() };

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/SetBouquetRow",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data[0].IsWetBox == 0) {
                Notifications("warning", GetResource("ImposibleConsolidarParaElTipoDeCajaDry"));
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });

    LoadGrid();
}