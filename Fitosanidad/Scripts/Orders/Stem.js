﻿jQuery(document).ready(function () {
    $('#tableStem thead tr').clone(true).appendTo('#tableStem thead');
    $('#tableStem thead tr:eq(1) th').each(function (i) {
        $(this).html('<input type="text" />');

        $('input', this).on('keyup change', function () {
            if (tblStem.column(i).search() !== this.value) {
                tblStem
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        });
    });

    var tblStem = jQuery('#tableStem').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , orderCellsTop: true
        , scrollX: true
        , scrollCollapse: true
        , fixedHeader: true
        , "columnDefs": [
            {
                "targets": [0, 5],
                "className": "text-right",
            }
            , { "width": "05%", "targets": 0 }
            , { "width": "40%", "targets": 1 }
            , { "width": "05%", "targets": 2 }
            , { "width": "05%", "targets": 3 }
            , { "width": "05%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
        ]
        , "select": {
            'style': 'multi'
        }
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    LoadGrid();
});

function BackToList() {
    var Url = "/Orders/Bunch?DetailID=" + getParameterByName("DetailID") + "&ID=" + getParameterByName("BunchID");
    window.location.href = Url;
}

function LoadGrid() {
    var prmOrdenesEnsambleOriginalesDetalles = getParameterByName("BunchID");
    var prmIDOrdenesEnsambleOriginalesDetallesReceta = getParameterByName("ID");

    var parameters = { "prmOrdenesEnsambleOriginalesDetalles": prmOrdenesEnsambleOriginalesDetalles, "prmIDOrdenesEnsambleOriginalesDetallesReceta": prmIDOrdenesEnsambleOriginalesDetallesReceta }

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/GetOrderDetailRecipe",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblStem = jQuery('#tableStem').DataTable();
            tblStem.clear().draw();

            $.each(data, function (i, data) {
                tblStem.row.add([
                    data.ID,
                    data.ProductosComponentes,
                    data.PresentacionesMedida,
                    data.ProductosCalidades,
                    data.TemporadasBase,
                    data.Cantidad,
                    data.BreakLine,
                    '<div class="actions">' +
                    '    <div class="btn-group">' +
                    '        <button class="btn btn-xs" type="button" data-toggle="tooltip" data-placement="top" title="' + GetResource("Detalles") + '" onclick=GetStemRow(' + data.ID + ')>' +
                    '            <i class="fa fa-search text-orange"></i>' +
                    '        </button>' +
                    '    </div>' +
                    '</div>',
                ]);

            });
            tblStem.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}