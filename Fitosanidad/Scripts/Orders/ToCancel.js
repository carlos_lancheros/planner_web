﻿var tblToCancel

jQuery(window).load(function () {
    BlockScreen();
});

jQuery(document).ready(function () {
    UnlockScreen();

    $('#tableToCancel thead tr').clone(true).appendTo('#tableToCancel thead');
    $('#tableToCancel thead tr:eq(1) th').each(function (i) {
        if (i != 0) {
            $(this).html('<input type="text" />');

            $('input', this).on('keyup change', function () {
                if (tblToCancel.column(i).search() !== this.value) {
                    tblToCancel
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        }
    });

    tblToCancel = jQuery('#tableToCancel').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , orderCellsTop: true
        , scrollX: true
        , scrollCollapse: true
        , fixedHeader: true
        , fixedColumns: {
            leftColumns: 2
        }
        , "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
        , "columnDefs": [
            {
                targets: 0,
                orderable: false,
                className: 'select-checkbox',
                'checkboxes': {
                    'selectRow': true
                }
            }
            , {
                "targets": [8, 9, 10],
                "className": "text-right",
            }
            , { "width": "05%", "targets": 0 }
            , { "width": "05%", "targets": 1 }
            , { "width": "10%", "targets": 2 }
            , { "width": "10%", "targets": 3 }
            , { "width": "10%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
            , { "width": "05%", "targets": 6 }
            , { "width": "05%", "targets": 7 }
            , { "width": "05%", "targets": 8 }
            , { "width": "10%", "targets": 9 }
            , { "width": "10%", "targets": 10 }
        ]
        , 'select': {
            'style': 'multi'
        }
        , 'order': [[2, 'asc']]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    jQuery("#btnExcel").click(function () {
        jQuery('<table>')
            .append(
                jQuery("#tableToCancel").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "ToCancelOrders"
            });
    });

    LoadGrid();
});

function LoadGrid() {
    BlockScreen();

    var usrAuth_UserFarm = GetSessionVar("usrAuth_UserFarm")
    var prmIDFarm = [];
    $.each(usrAuth_UserFarm, function (index, rowId) {
        prmIDFarm.push(rowId.FarmID);
    });

    var parameters = { "prmIDFarm": prmIDFarm }

    jQuery.ajax({
        url: "/Orders/ToCancelGetData",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblToCancel = jQuery('#tableToCancel').DataTable();
            tblToCancel.clear().draw();

            $.each(data, function (i, data) {
                tblToCancel.row.add([
                    data.ID,
                    data.Cod_Cliente,
                    data.Cod_Customer,
                    data.Nombre_Finca,
                    data.IDProductos_Nombre,
                    data.Customer,
                    strDateTime2Date(data.FechaPedido, "dd-mm-yyyy"),
                    data.IDCajas_Nombre,
                    data.Pack,
                    data.Cantidad,
                    data.TotalTallos,
                    data.Status_Detalle,
                    data.Observaciones,
                ]);

            });
            tblToCancel.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}
