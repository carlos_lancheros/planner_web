﻿var tblBoxLabel;

jQuery(window).load(function () {
    BlockScreen();
});

jQuery(document).ready(function () {
    UnlockScreen();

    jQuery("#startDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#startDate').focus();
            }
        }
    });
    jQuery('#startDate').datepicker('setDate', 'today');
    jQuery('#startDate').attr("autocomplete", "off");

    jQuery("#endDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#endDate').focus();
            }
        }
    });
    jQuery('#endDate').datepicker('setDate', 'today');
    jQuery('#endDate').attr("autocomplete", "off");

    $('#tableBoxLabel thead tr').clone(true).appendTo('#tableBoxLabel thead');
    $('#tableBoxLabel thead tr:eq(1) th').each(function (i) {
        if (i != 0) {
            $(this).html('<input type="text" />');

            $('input', this).on('keyup change', function () {
                if (tblBoxLabel.column(i).search() !== this.value) {
                    tblBoxLabel
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        }
    });

    tblBoxLabel = jQuery('#tableBoxLabel').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , orderCellsTop: true
        , scrollX: true
        , scrollCollapse: true
        , fixedHeader: true
        , fixedColumns: {
            leftColumns: 2
        }
        , "columnDefs": [
            {
                targets: 0,
                orderable: false,
                className: 'select-checkbox',
                'checkboxes': {
                    'selectRow': true
                }
            }
            , {
                "targets": [2, 4, 5, 6],
                "className": "text-right",
            }
            , { "width": "05%", "targets": 0 }
            , { "width": "05%", "targets": 1 }
            , { "width": "10%", "targets": 2 }
            , { "width": "10%", "targets": 3 }
            , { "width": "10%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
            , { "width": "05%", "targets": 6 }
            , { "width": "05%", "targets": 7 }
            , { "width": "05%", "targets": 8 }
            , { "width": "10%", "targets": 9 }
        ]
        , 'select': {
            'style': 'multi'
        }
        , 'order': [[2, 'asc']]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    $('#btnDownload').on('click', function () {
        BlockScreen();

        var prmOrders = [];
        var rows_selected = tblBoxLabel.column(0).checkboxes.selected();
        $.each(rows_selected, function (index, rowId) {
            prmOrders.push(rowId);
        });

        if (prmOrders.length == 0) {
            UnlockScreen();
            Notifications("error", GetResource("NoHaSeleccionadoEtiquetasADescargar"));
            return;
        }

        var Url = "/Orders/DownloadLabels?prmOrders=" + prmOrders;
        window.location.href = Url;
        UnlockScreen();
    });

});

function LoadGrid() {
    var prmIDOrdenesFincas = $("#cmbOrdersToPrint").val();

    var parameters = { "prmIDOrdenesFincas": prmIDOrdenesFincas }

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/JsonGetDetailOrdersToPrint",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblBoxLabel = jQuery('#tableBoxLabel').DataTable();
            tblBoxLabel.clear().draw();

            $.each(data, function (i, data) {
                tblBoxLabel.row.add([
                    data.ID,
                    data.IDProductos_Nombre,
                    data.IDPresentacionesMedida_Cantidad,
                    data.TipoCaja,
                    data.Cajas,
                    data.Sequence,
                    data.LabelsToPrint,
                    data.LabelColor,
                    data.DMX,
                    data.IDOrdenesEnsambleOriginalesDetalles,
                ]);

            });
            tblBoxLabel.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function Refresh() {
    $("#cmbOrdersToPrint").empty();

    var usrAuth_UserFarm = GetSessionVar("usrAuth_UserFarm")
    var prmIDFarm = [];
    $.each(usrAuth_UserFarm, function (index, rowId) {
        prmIDFarm.push(rowId.FarmID);
    });

    var strDate = $("#startDate").val().split("-")
    var startDate = strDate[2] + strDate[1] + strDate[0]
    var strDate = $("#endDate").val().split("-")
    var endDate = strDate[2] + strDate[1] + strDate[0]
    var prmTipoCarga = $("#cmbTipoCarga").val();

    var prmOrder = "0";
    if ($("#Order").val() == null) {
        prmOrder = "0";
    } else {
        if ($("#Order").val().length == 0) {
            prmOrder = "0";
        };
    }

    var parameters = { "prmIDFarm": prmIDFarm, "prmFInicial": startDate, "prmFFinal": endDate, "prmTipoCarga": prmTipoCarga, "prmOrder": prmOrder };

    jQuery.ajax({
        url: "/Orders/JsonGetOrdersToPrint",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.each(data, function (i, data) {
                $("#cmbOrdersToPrint").append('<option value="'
                    + data.ID + '">'
                    + data.Clientecodigo + '</option>');

            });
        },
        error: function (xhr, textStatus, errorThrown) {
            Notifications("error", errorThrown);
        }
    });
}