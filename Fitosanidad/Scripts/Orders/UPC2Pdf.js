﻿var tblBunch
    , tableFlowers
    , tblMaterial

jQuery(document).ready(function () {
    tblBunch = jQuery('#tableBunch').DataTable({
        "searching": false
        , "info": false
        , paging: false
        , responsive: true
        , orderCellsTop: false
        , scrollX: false
        , scrollCollapse: false
        , fixedHeader: false
        , bSort: false
        , "columnDefs": [
            {
                "targets": [0, 1, 4, 8],
                "className": "text-left",
            },
            {
                "targets": [2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13],
                "className": "text-right",
            }
            //, { "width": "30%", "targets": 0 }
            //, { "width": "15%", "targets": 1 }
            //, { "width": "15%", "targets": 2 }
            //, { "width": "15%", "targets": 3 }
            //, { "width": "15%", "targets": 4 }
            //, { "width": "15%", "targets": 5 }
            //, { "width": "15%", "targets": 6 }
            //, { "width": "15%", "targets": 7 }
            //, { "width": "15%", "targets": 8 }
            //, { "width": "15%", "targets": 9 }
            //, { "width": "15%", "targets": 10 }
            //, { "width": "15%", "targets": 11 }
            //, { "width": "15%", "targets": 12 }
            //, { "width": "15%", "targets": 13 }
        ]
    });

    tblFlowers = jQuery('#tableFlowers').DataTable({
        "searching": false
        , "info": false
        , paging: false
        , responsive: true
        , orderCellsTop: false
        , scrollX: false
        , scrollCollapse: false
        , fixedHeader: false
        , bSort: false
        , "columnDefs": [
            {
                "targets": [0, 1],
                "className": "text-left",
            },
            {
                "targets": [2, 3, 4],
                "className": "text-right",
            }
            , { "width": "30%", "targets": 0 }
            , { "width": "15%", "targets": 1 }
            , { "width": "15%", "targets": 2 }
            , { "width": "15%", "targets": 3 }
            , { "width": "15%", "targets": 4 }
        ]
    });

    tblMaterial = jQuery('#tableMaterial').DataTable({
        "searching": false
        , "info": false
        , paging: false
        , responsive: true
        , orderCellsTop: false
        , scrollX: false
        , scrollCollapse: false
        , fixedHeader: false
        , bSort: false
        , "columnDefs": [
            {
                "targets": [0, 1],
                "className": "text-left",
            },
            {
                "targets": [2],
                "className": "text-right",
            }
            , { "width": "20%", "targets": 0 }
            , { "width": "70%", "targets": 1 }
            , { "width": "10%", "targets": 2 }
        ]
    });

    LoadGrid();

    $("#btnSubmit").click(function () {
        $("input[name='ExportData']").val($("#UPC2Pdf").html());
        //$("#ExportData").val($("#UPC2Pdf").html())
    });

});

function LoadGrid() {
    //Flower
    var prmIDSeccion = 1;
    var prmIDOrdenesEnsambleOriginalesDetalles = getParameterByName("ID");

    var parameters = { "prmIDSeccion": prmIDSeccion, "prmIDOrdenesEnsambleOriginalesDetalles": prmIDOrdenesEnsambleOriginalesDetalles }

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/GetUPCRecipe",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblFlowers = jQuery('#tableFlowers').DataTable();
            tblFlowers.clear().draw();

            $.each(data, function (i, data) {
                tblFlowers.row.add([
                    data.ProductoTallo,
                    data.CalidadTallo,
                    data.CantidadRecetaTallo,
                    data.CantidadTotalOrdenTallo,
                    data.CantidadFinca,
                ]);

            });
            tblFlowers.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });

    //Material
    var prmIDSeccion = 2;
    var prmIDOrdenesEnsambleOriginalesDetalles = getParameterByName("ID");

    var parameters = { "prmIDSeccion": prmIDSeccion, "prmIDOrdenesEnsambleOriginalesDetalles": prmIDOrdenesEnsambleOriginalesDetalles }

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/GetUPCRecipe",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblMaterial = jQuery('#tableMaterial').DataTable();
            tblMaterial.clear().draw();

            $.each(data, function (i, data) {
                tblMaterial.row.add([
                    data.IDProductosComponentes_IDClasesProductos_Nombre,
                    data.IDProductosComponentes_Nombre,
                    data.Cantidad,
                ]);

            });
            tblMaterial.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function BackToList() {
    var Url = "/Orders/Detail?ID=" + getParameterByName("DetailID");
    window.location.href = Url;
}

//function UPC2Pdf() {
//    $("#ExportData").val($("#UPC2Pdf").html());
//    //var Url = "/Orders/Export?ExportData=" + $("#ExportData").val();
//    //window.location.href = Url;

//    $.ajax({
//        type: 'POST',
//        url: '/Orders/Export',
//        data: { ExportData: $("#ExportData").val() },
//        success: function (data) { alert(data.result); }
//    });


//    //var element = document.getElementById('UPC2Pdf');
//    //let nbPages = 10;
//    //var opt = {
//    //    margin: [10, 10],
//    //    filename: 'myfile.pdf',
//    //    image: { type: 'png', quality: 1 },
//    //    html2canvas: { dpi: 193, letterRendering: true, width: 1250, heigth: 800 },
//    //    //jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' },
//    //    jsPDF: { unit: 'mm', format: 'letter', orientation: 'portrait' }
//    //};

//    //html2pdf().set(opt).from(element).toPdf().get('pdf').then(function (pdf) {
//    //    var number_of_pages = pdf.internal.getNumberOfPages()
//    //    var pdf_pages = pdf.internal.pages
//    //    var myFooter = "Footer info"
//    //    var tituloOutput = $("#tituloOutput").text();

//    //    for (var i = 1; i < pdf_pages.length; i++) {
//    //        pdf.setPage(i)
//    //        pdf.setFontSize(13);
//    //        pdf.text(tituloOutput, 10, 10);
//    //    }
//    //}).save();
//}