﻿var tblUpcPhotos,
    tblUpcImage

jQuery(window).load(function () {
    BlockScreen();
});

jQuery(document).ready(function () {
    UnlockScreen();

    jQuery("#startDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#startDate').focus();
            }
        }
    });
    jQuery('#startDate').datepicker('setDate', 'today');
    jQuery('#startDate').attr("autocomplete", "off");

    jQuery("#endDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#endDate').focus();
            }
        }
    });
    jQuery('#endDate').datepicker('setDate', 'today');
    jQuery('#endDate').attr("autocomplete", "off");

    tblUpcPhotos = jQuery('#tableUpcPhotos').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , orderCellsTop: true
        , scrollX: true
        , scrollCollapse: true
        , "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
        , "columnDefs": [
            {
                targets: 8,
                orderable: false,
                className: 'select-checkbox',
            }
            , {
                'targets': [8, 9],
                'orderable': false,
            }
            , {
                "targets": [8],
                "className": "text-center",
            }
            , { "width": "05%", "targets": 0 }
            , { "width": "05%", "targets": 1 }
            , { "width": "10%", "targets": 2 }
            , { "width": "10%", "targets": 3 }
            , { "width": "10%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
            , { "width": "05%", "targets": 6 }
            , { "width": "05%", "targets": 7 }
            , { "width": "05%", "targets": 8 }
            , { "width": "05%", "targets": 9 }
        ]
        , "columns": [
            { 'data': 0 },
            { 'data': 1 },
            { 'data': 2 },
            { 'data': 3 },
            { 'data': 4 },
            { 'data': 5 },
            { 'data': 6 },
            { 'data': 7 },
            {
                'data': 8,
                render: function (data, type, row) {
                    if (data === 1) {
                        return '<input type="checkbox" class="editor-active" onclick="return false;" checked>';
                    } else {
                        return '<input type="checkbox" onclick="return false;" class="editor-active">';
                    }
                    return data;
                }
            },
            { 'data': 9 },
        ]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    //tableUpcImage
    tblUpcImage = jQuery('#tableUpcImage').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , orderCellsTop: true
        , scrollX: true
        , scrollCollapse: true
        , "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
        , "columnDefs": [
            {
                'targets': [5],
                'orderable': false,
            }
            , {
                "targets": [4],
                "className": "text-right",
            }
            , { "width": "05%", "targets": 0 }
            , { "width": "05%", "targets": 1 }
            , { "width": "10%", "targets": 2 }
            , { "width": "10%", "targets": 3 }
            , { "width": "10%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
        ]
        , "columns": [
            { 'data': 0 },
            { 'data': 1 },
            { 'data': 2 },
            { 'data': 3 },
            { 'data': 4 },
            { 'data': 5 },
        ]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });
    //tableUpcImage

    //tableUpcPhoto
    tblUpcImage = jQuery('#tableUpcPhoto').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , orderCellsTop: true
        , scrollX: true
        , scrollCollapse: true
        , "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
        , "columnDefs": [
            {
                targets: [2, 3],
                orderable: false,
                className: 'select-checkbox',
            }
            , {
                'targets': [5],
                'orderable': false,
            }
            , { "width": "05%", "targets": 0 }
            , { "width": "05%", "targets": 1 }
            , { "width": "10%", "targets": 2 }
            , { "width": "10%", "targets": 3 }
            , { "width": "10%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
        ]
        , "columns": [
            { 'data': 0 },
            { 'data': 1 },
            {
                'data': 2,
                render: function (data, type, row) {
                    if (data === 1) {
                        return '<input type="checkbox" class="editor-active" onclick="return 1;" checked>';
                    } else {
                        return '<input type="checkbox" onclick="return 0;" class="editor-active">';
                    }
                    return data;
                }
            },
            {
                'data': 3,
                render: function (data, type, row) {
                    if (data === 1) {
                        return '<input type="checkbox" class="editor-active" onclick="return 1;" checked>';
                    } else {
                        return '<input type="checkbox" onclick="return 0;" class="editor-active">';
                    }
                    return data;
                }
            },
            { 'data': 4 },
            { 'data': 5 },
        ]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });
    //tableUpcPhoto

    LoadGrid();
});

function LoadGrid() {
    var strDate = $("#startDate").val().split("-")
    var startDate = new Date(strDate[2], strDate[1] - 1, strDate[0])
    var strDate = $("#endDate").val().split("-")
    var endDate = new Date(strDate[2], strDate[1] - 1, strDate[0])

    var prmIDFarm = $("#cmbFarms").val();

    var parameters = { "prmFechaIni": startDate, "prmFechaFin": endDate, "prmIDFarm": prmIDFarm }

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/GetUPCPhotosInformation",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblUpcPhotos = jQuery('#tableUpcPhotos').DataTable();
            tblUpcPhotos.clear().draw();

            $.each(data, function (i, data) {
                tblUpcPhotos.row.add([
                    data.IDOrdenesEnsambleOriginalesDetalles,
                    data.PO,
                    strDateTime2Date(data.FechaPedido, "dd-mm-yyyy"),
                    data.CustomerGroup,
                    data.Customer,
                    data.CustomerOrder,
                    data.FarmOrder,
                    data.Product,
                    data.Foto,
                    '<div class="actions">' +
                    '    <div class="btn-group">' +
                    '        <button class="btn btn-xs" type="button" data-toggle="tooltip" data-placement="top" title="' + GetResource("Imagen") + '" onclick=GetInformationToReviewUPC(' + data.IDOrdenesEnsambleOriginalesDetalles + ')>' +
                    '            <i class="fa fa-search text-orange"></i>' +
                    '        </button>' +
                    '    </div>' +
                    '</div>',
                ]);

            });
            tblUpcPhotos.draw(false);

            GetInformationToReviewUPC(0) 

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function GetInformationToReviewUPC(prmIDOrdenesEnsambleOriginalesDetalles) {
    var parameters = { "prmIDOrdenesEnsambleOriginalesDetalles": prmIDOrdenesEnsambleOriginalesDetalles }

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/GetInformationToReviewUPC",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblUpcImage = jQuery('#tableUpcImage').DataTable();
            tblUpcImage.clear().draw();

            $.each(data, function (i, data) {
                tblUpcImage.row.add([
                    data.Bunch,
                    data.UpcProductName,
                    data.UPCBarcode_Sleeve,
                    data.DateCode,
                    data.RetailPrice,
                    '<div class="actions">' +
                    '    <div class="btn-group">' +
                    '        <button class="btn btn-xs" type="button" data-toggle="tooltip" data-placement="top" title="' + GetResource("Tipo") + '" onclick="GetUPCTypeFile(\'' + data.UPCType + '.jpg\',\'' + data.Descripcion + '\')">' +
                    '            <i class="fa fa-barcode text-orange"></i>' +
                    '        </button>' +
                    '    </div>' +
                    '</div>',
                ]);

            });
            tblUpcImage.draw(false);

            GetUPCPhoto(prmIDOrdenesEnsambleOriginalesDetalles);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function GetUPCPhoto(prmIDOrdenesEnsmableOriginalesDetalles) {
    var parameters = { "prmIDOrdenesEnsmableOriginalesDetalles": prmIDOrdenesEnsmableOriginalesDetalles }

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/GetUPCPhoto",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblUpcPhoto = jQuery('#tableUpcPhoto').DataTable();
            tblUpcPhoto.clear().draw();

            $.each(data, function (i, data) {
                tblUpcPhoto.row.add([
                    data.ID,
                    data.NombreArchivo,
                    data.FlagVerificado,
                    data.FlagNoVerificado,
                    data.AprobadoPor,
                    '<div class="actions">' +
                    '    <div class="btn-group">' +
                    '        <button class="btn btn-xs" type="button" data-toggle="tooltip" data-placement="top" title="' + GetResource("Imagen") + '" onclick="GetUPCPhotoFile(\'' + data.NombreArchivo + '\')">' +
                    '            <i class="fa fa-search text-orange"></i>' +
                    '        </button>' +
                    '    </div>' +
                    '</div>',
                ]);

            });
            tblUpcPhoto.draw(false);

            GetUPCTypeFile();
            GetUPCPhotoFile();

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function GetUPCTypeFile(strUPCTypeFileName, strUPCTypeFileDescription) {
    if (!strUPCTypeFileName) {
        $("#imgUPCType").attr("src", "");
        $("#strUPCTypeFileDescription").empty();
        return false;
    }

    var parameters = { "strUPCTypeFileName": strUPCTypeFileName }

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/GetUPCTypeFile",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length == 0) {
                $("#imgUPCType").attr("src", "/Downloads/" + strUPCTypeFileName);
                $("#strUPCTypeFileDescription").empty();
                $("#strUPCTypeFileDescription").append(strUPCTypeFileDescription);
            } else {
                $("#imgUPCType").attr("src", "/Content/img/no_image.jpg");
                $("#strUPCTypeFileDescription").empty();
                $("#strUPCTypeFileDescription").append(data);
                $("#strUPCTypeFileDescription").append("<hr>");
                $("#strUPCTypeFileDescription").append(strUPCTypeFileDescription);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function GetUPCPhotoFile(strPhotoFileName) {
    if (!strPhotoFileName) {
        $("#imgUPCPhoto").attr("src", "");
        $("#strUPCTypeFileDescription").empty();
        return false;
    }

    var parameters = { "strPhotoFileName": strPhotoFileName }

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/GetUPCPhotoFile",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length == 0) {
                $("#imgUPCPhoto").attr("src", "/Downloads/" + strPhotoFileName);
                $("#imgUPCPhotoDescription").empty();
            } else {
                $("#imgUPCPhoto").attr("src", "/Content/img/no_image.jpg");
                $("#imgUPCPhotoDescription").empty();
                $("#imgUPCPhotoDescription").append(data);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}
