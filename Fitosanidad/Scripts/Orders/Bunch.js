﻿jQuery(document).ready(function () {
    $('#tableBunch thead tr').clone(true).appendTo('#tableBunch thead');
    $('#tableBunch thead tr:eq(1) th').each(function (i) {
        if (i != 0) {
            $(this).html('<input type="text" />');

            $('input', this).on('keyup change', function () {
                if (tblBunch.column(i).search() !== this.value) {
                    tblBunch
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        }
    });

    var tblBunch = jQuery('#tableBunch').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , orderCellsTop: true
        , scrollX: true
        , scrollCollapse: true
        , fixedHeader: true
        , "columnDefs": [
            {
                "targets": [0, 5, 6],
                "className": "text-right",
            }
            , { "width": "05%", "targets": 0 }
            , { "width": "40%", "targets": 1 }
            , { "width": "05%", "targets": 2 }
            , { "width": "05%", "targets": 3 }
            , { "width": "05%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
            , { "width": "05%", "targets": 6 }
            , { "width": "05%", "targets": 7 }
        ]
        , "select": {
            'style': 'multi'
        }
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    LoadGrid();
});

function BackToList() {
    var Url = "/Orders/Detail?ID=" + getParameterByName("DetailID");
    window.location.href = Url;
}

function LoadGrid() {
    var prmOrdenesEnsambleOriginalesDetalles = getParameterByName("ID");
    var prmIDOrdenesEnsambleOriginalesDetallesReceta = 0; // Se deja quemado porque así se está trabajando en la versión ASPX

    var parameters = { "prmOrdenesEnsambleOriginalesDetalles": prmOrdenesEnsambleOriginalesDetalles, "prmIDOrdenesEnsambleOriginalesDetallesReceta": prmIDOrdenesEnsambleOriginalesDetallesReceta }

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/GetOrderDetailRecipe",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblBunch = jQuery('#tableBunch').DataTable();
            tblBunch.clear().draw();

            $.each(data, function (i, data) {
                tblBunch.row.add([
                    '<div class="actions">' +
                    '    <div class="btn-group">' +
                    '        <button class="btn btn-xs" type="button" data-toggle="tooltip" data-placement="top" title="' + GetResource("Detalles") + '" onclick=GetStemRow(' + data.ID + ')>' +
                    '            <i class="fa fa-search text-orange"></i>' +
                    '        </button>' +
                    '    </div>' +
                    '</div>',
                    data.ID,
                    data.ProductosComponentes,
                    data.PresentacionesMedida,
                    data.ProductosCalidades,
                    data.TemporadasBase,
                    data.Cantidad,
                    data.BreakLine,
                ]);

            });
            tblBunch.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function GetStemRow(ID) {
    var Url = "/Orders/Stem?DetailID=" + getParameterByName("DetailID") + "&BunchID=" + getParameterByName("ID") + "&ID=" + ID;
    window.location.href = Url;
}