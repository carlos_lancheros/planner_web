﻿var tblConfirm
    , tblPending
    , tblToCancel

jQuery(window).load(function () {
    BlockScreen();
});

jQuery(document).ready(function () {
    UnlockScreen();

    jQuery("#startDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#startDate').focus();
            }
        }
    });
    jQuery('#startDate').datepicker('setDate', 'today');
    jQuery('#startDate').attr("autocomplete", "off");

    jQuery("#endDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#endDate').focus();
            }
        }
    });
    jQuery('#endDate').datepicker('setDate', 'today');
    jQuery('#endDate').attr("autocomplete", "off");

    $('#tableConfirm thead tr').clone(true).appendTo('#tableConfirm thead');
    $('#tableConfirm thead tr:eq(1) th').each(function (i) {
        if (i != 0 && i != 1 && i != 16) {
            $(this).html('<input type="text" />');

            $('input', this).on('keyup change', function () {
                if (tblConfirm.column(i).search() !== this.value) {
                    tblConfirm
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        }
    });

    tblConfirm = jQuery('#tableConfirm').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , orderCellsTop: true
        , scrollX: true
        , scrollCollapse: true
        , fixedHeader: true
        , fixedColumns: {
            leftColumns: 2
        }
        //, serverSide: true
        //, ajax: {
        //    "url": "http://getorderssyncfarms.sendero.api/api/NewSafex/GetOrdersSyncFarms",
        //    "data": function (d) {
        //        d.extra_search = $('#extra').val();

        //        "prmIDSeccion" = 1,
        //        "prmIDFarm" = 35,
        //        "prmFInicial" = "01-01-2019",
        //        "prmFFinal" = "07-24-2019",
        //        "prmTipoCarga" = 0,
        //        "prmFarmOrder" = 0
        //    }
        //    data: {
        //        "prmIDSeccion" : 1,
        //        "prmIDFarm" : 35,
        //        "prmFInicial" : "01-01-2019",
        //        "prmFFinal" : "07-24-2019",
        //        "prmTipoCarga" : 0,
        //        "prmFarmOrder" : 0
        //    }
        //}
        , "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
        , "columnDefs": [
            {
                targets: 0,
                orderable: false,
                className: 'select-checkbox',
                'checkboxes': {
                    'selectRow': true
                }
            }
            , {
                'targets': 1,
                'orderable': false,
            }
            , {
                "targets": [2, 3, 8],
                "className": "text-right",
            }
            , { "width": "05%", "targets": 0 }
            , { "width": "05%", "targets": 1 }
            , { "width": "10%", "targets": 2 }
            , { "width": "10%", "targets": 3 }
            , { "width": "10%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
            , { "width": "05%", "targets": 6 }
            , { "width": "05%", "targets": 7 }
            , { "width": "05%", "targets": 8 }
            , { "width": "10%", "targets": 9 }
            , { "width": "10%", "targets": 10 }
            , { "width": "05%", "targets": 11 }
            , { "width": "05%", "targets": 12 }
            , { "width": "05%", "targets": 13 }
            , { "width": "05%", "targets": 14 }
            , { "width": "05%", "targets": 15 }
        ]
        , 'select': {
            'style': 'multi'
        }
        , 'order': [[2, 'asc']]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    tblPending = jQuery('#tablePending').dataTable();

    tblToCancel = jQuery('#tableToCancel').dataTable();

    $('#btnPending').on('click', function () {
        $("#divConfirm").hide();
        $("#divPending").show();
        $("#divToCancel").hide();

        PendingGetData();
    });

    $('#btnToCancel').on('click', function () {
        $("#divConfirm").hide();
        $("#divPending").hide();
        $("#divToCancel").show();

        ToCancelGetData();
    });

    $('#btnDivPending').on('click', function () {
        $("#divConfirm").show();
        $("#divPending").hide();
        $("#divToCancel").hide();
    });

    $('#btnDivToCancel').on('click', function () {
        $("#divConfirm").show();
        $("#divPending").hide();
        $("#divToCancel").hide();
    });

    $('#btnFirmAtFarm').on('click', function () {
        BlockScreen();

        var usrAuth_UserFarm = GetSessionVar("usrAuth_UserFarm")
        var prmIDFarm = [];
        $.each(usrAuth_UserFarm, function (index, rowId) {
            prmIDFarm.push(rowId.FarmID);
        });

        var prmOrders = [];
        var rows_selected = tblConfirm.column(0).checkboxes.selected();
        $.each(rows_selected, function (index, rowId) {
            prmOrders.push(rowId);
        });

        if (prmOrders.length == 0) {
            UnlockScreen();
            Notifications("error", GetResource("NoHaSeleccionadoOrdenesAConfirmar"));
            return;
        }

        var parameters = { "prmIDFarm": prmIDFarm, "prmOrders": prmOrders };

        BlockScreen();
        jQuery.ajax({
            url: "/Orders/FirmAtFarm",
            data: JSON.stringify(parameters),
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {

                UnlockScreen();
            },
            error: function (xhr, textStatus, errorThrown) {
                UnlockScreen();
                Notifications("error", errorThrown);
            }
        });
    });

    $('#btnDownload').on('click', function () {
        BlockScreen();

        var prmIDSeccion = $("#cmbOrders").val();

        var usrAuth_UserFarm = GetSessionVar("usrAuth_UserFarm")
        var prmIDFarm = [];
        $.each(usrAuth_UserFarm, function (index, rowId) {
            prmIDFarm.push(rowId.FarmID);
        });

        var prmOrders = [];
        var rows_selected = tblConfirm.column(0).checkboxes.selected();
        $.each(rows_selected, function (index, rowId) {
            prmOrders.push(rowId);
        });

        if (prmOrders.length == 0) {
            UnlockScreen();
            Notifications("error", GetResource("NoHaSeleccionadoOrdenesADescargar"));
            return;
        }

        var Url = "/Orders/Download?prmIDSeccion=" + prmIDSeccion + "&prmIDFarm=" + prmIDFarm + "&prmOrders=" + prmOrders;
        window.location.href = Url;
        UnlockScreen();
    });

    GetVerifyGeneralPermits();
    GetVerifyPermissionsPinWeb();
    LoadGrid();

    PendingGetData();
    ToCancelGetData();

    $("#divConfirm").show();
    $("#divPending").hide();
    $("#divToCancel").hide();
});

function GetVerifyGeneralPermits() {
    var prmAppUserWeb = GetSessionVar("UserID");
    var parameters = { "prmAppUserWeb": prmAppUserWeb };

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/GetVerifyGeneralPermits",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function GetVerifyPermissionsPinWeb() {
    var prmAppUserWeb = GetSessionVar("UserID");
    var parameters = { "prmAppUserWeb": prmAppUserWeb };

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/GetVerifyPermissionsPinWeb",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data != 1) {
                $("#btnPin").attr("disabled", true);
                $("#btnPendingOrders").attr("disabled", true);
                $("#btnUpcPhotos").attr("disabled", true);
            }

            if (prmAppUserWeb == 65) {
                $("#btnPin").attr("disabled", false);
                $("#btnPendingOrders").attr("disabled", false);
                $("#btnUpcPhotos").attr("disabled", false);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function LoadGrid() {
    var strDate = $("#startDate").val().split("-")
    var startDate = strDate[2] + strDate[1] + strDate[0]
    var strDate = $("#endDate").val().split("-")
    var endDate = strDate[2] + strDate[1] + strDate[0]

    var prmIDSeccion = $("#cmbOrders").val();

    var usrAuth_UserFarm = GetSessionVar("usrAuth_UserFarm")
    var prmIDFarm = [];
    $.each(usrAuth_UserFarm, function (index, rowId) {
        prmIDFarm.push(rowId.FarmID);
    });

    var prmFInicial = startDate;
    var prmFFinal = endDate;
    var prmTipoCarga = $("#cmbTipoCarga").val();

    if (localStorage.getItem("Parameters")) {
        var arrParameters = JSON.parse(localStorage.getItem("Parameters"));

        prmIDSeccion = arrParameters[0];
        prmFInicial = arrParameters[1];
        prmFFinal = arrParameters[2];
        prmFarmOrder = arrParameters[3];

        $("#cmbTipoCarga").val(prmIDSeccion);
        $("#startDate").val(prmFInicial);
        $("#endDate").val(prmFFinal);

        localStorage.removeItem("Parameters");
    }

    var parameters = { "prmIDSeccion": prmIDSeccion, "prmIDFarm": prmIDFarm, "prmFInicial": prmFInicial, "prmFFinal": prmFFinal, "prmTipoCarga": prmTipoCarga }

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/CargarOrdenesSyncFincas",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblConfirm = jQuery('#tableConfirm').DataTable();
            tblConfirm.clear().draw();

            $.each(data, function (i, data) {
                tblConfirm.row.add([
                    data.ID,
                    '<div class="actions">' +
                    '    <div class="btn-group">' +
                    '        <button class="btn btn-xs" type="button" data-toggle="tooltip" data-placement="top" title="' + GetResource("Detalles") + '" onclick=GetDetailRow(' + data.ID + ')>' +
                    '            <i class="fa fa-search text-orange"></i>' +
                    '        </button>' +
                    '    </div>' +
                    '</div>',
                    data.IDOrdenesEnsamblePOCabezas_Codigo,
                    data.IDOrdenesEnsambleOriginales_Codigo,
                    data.IDOrdenesEnsamblePOCabezas_PO,
                    data.IDFarm_Nombre,
                    data.IDOrdenesEnsamblePOCabezas_IDClientesFinales_Nombre,
                    data.IDOrdenesEnsamblePOCabezas_IDClientesFinales_IDCustomerGroup_Nombre,
                    data.Qty_Boxes,
                    data.IDOrdenesEnsamblePOCabezas_IDTemporadasBase_Nombre,
                    strDateTime2Date(data.IDOrdenesEnsamblePOCabezas_FechaPedido, "dd-mm-yyyy"),
                    strDateTime2Date(data.IDOrdenesEnsamblePOCabezas_ShipDate, "dd-mm-yyyy"),
                    strDateTime2Date(data.IDOrdenesEnsamblePOCabezas_FechaEntrega, "dd-mm-yyyy"),
                    data.IDOrdenesEnsamblePOCabezas_IDEstadosPO_Nombre,
                    data.IDEstadosSincFinca_Nombre,
                    data.OrderType,
                ]);

            });
            tblConfirm.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function GetDetailRow(ID) {
    var tblData = tblConfirm.rows().data();

    var arrHeader = [];
    tblData.each(function (value, index) {
        if (ID == value[0]) {
            arrHeader.push(value[3]);
            arrHeader.push(value[4]);
            arrHeader.push(value[6]);
            arrHeader.push(value[10]);
            arrHeader.push(value[11]);
        }
    });
    localStorage.setItem('frmHeader', JSON.stringify(arrHeader));

    var arrParameters = [];
    arrParameters.push($("#cmbTipoCarga").val());
    arrParameters.push($("#startDate").val());
    arrParameters.push($("#endDate").val());
    arrParameters.push(0);
    localStorage.setItem('Parameters', JSON.stringify(arrParameters));

    var table = $('#tableConfirm').DataTable();
    var data = table.row(0).data();

    var Url = "/Orders/Detail?ID=" + ID;
    window.location.href = Url;
}

function PendingGetData() {
    BlockScreen();

    jQuery("#txtPending").empty();

    jQuery.ajax({
        url: "/Orders/PendingGetData",
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery("#txtPending").append(data.length);

            var tblPending = jQuery('#tablePending').DataTable();
            tblPending.clear().draw();

            $.each(data, function (i, data) {
                tblPending.row.add([
                    data.Cust_Order,
                    data.Farm_Order,
                    data.PO,
                    data.Farm,
                    data.Customer,
                    data.Cust_Group,
                    data.Qty_Boxes,
                    strDateTime2Date(data.Farm_Ship, "dd-mm-yyyy"),
                    strDateTime2Date(data.Miami_Ship, "dd-mm-yyyy"),
                    data.PO_Status,
                    data.Farm_Status,
                ]);

            });
            tblPending.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function ToCancelGetData() {
    BlockScreen();

    jQuery("#txtToCancel").empty();

    var usrAuth_UserFarm = GetSessionVar("usrAuth_UserFarm")
    var prmIDFarm = [];
    $.each(usrAuth_UserFarm, function (index, rowId) {
        prmIDFarm.push(rowId.FarmID);
    });

    var parameters = { "prmIDFarm": prmIDFarm }

    jQuery.ajax({
        url: "/Orders/ToCancelGetData",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery("#txtToCancel").append(data.length);

            var tblToCancel = jQuery('#tableToCancel').DataTable();
            tblToCancel.clear().draw();

            $.each(data, function (i, data) {
                tblToCancel.row.add([
                    data.Cod_Cliente,
                    data.Cod_Customer,
                    data.Nombre_Finca,
                    data.IDProductos_Nombre,
                    data.Customer,
                    strDateTime2Date(data.FechaPedido, "dd-mm-yyyy"),
                    data.IDCajas_Nombre,
                    data.Pack,
                    data.Cantidad,
                    data.TotalTallos,
                    data.Status_Detalle,
                    data.Observaciones,
                ]);

            });
            tblToCancel.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}