﻿var tblPending

jQuery(window).load(function () {
    BlockScreen();
});

jQuery(document).ready(function () {
    UnlockScreen();

    $('#tablePending thead tr').clone(true).appendTo('#tablePending thead');
    $('#tablePending thead tr:eq(1) th').each(function (i) {
        $(this).html('<input type="text" />');

        $('input', this).on('keyup change', function () {
            if (tblPending.column(i).search() !== this.value) {
                tblPending
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        });
    });

    tblPending = jQuery('#tablePending').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , orderCellsTop: true
        , scrollX: true
        , scrollCollapse: true
        , fixedHeader: true
        , fixedColumns: {
            leftColumns: 2
        }
        , "columnDefs": [
            {
                "targets": [6],
                "className": "text-right",
            }
            , { "width": "05%", "targets": 0 }
            , { "width": "05%", "targets": 1 }
            , { "width": "10%", "targets": 2 }
            , { "width": "10%", "targets": 3 }
            , { "width": "10%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
            , { "width": "05%", "targets": 6 }
            , { "width": "05%", "targets": 7 }
            , { "width": "05%", "targets": 8 }
            , { "width": "10%", "targets": 9 }
            , { "width": "10%", "targets": 10 }
        ]
        , 'order': [[2, 'asc']]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    jQuery("#btnExcel").click(function () {
        jQuery('<table>')
            .append(
                jQuery("#tablePending").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "PendingOrders"
            });
    });

    LoadGrid();
});

function LoadGrid() {
    BlockScreen();
    jQuery.ajax({
        url: "/Orders/PendingGetData",
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblPending = jQuery('#tablePending').DataTable();
            tblPending.clear().draw();

            $.each(data, function (i, data) {
                tblPending.row.add([
                    data.Cust_Order,
                    data.Farm_Order,
                    data.PO,
                    data.Farm,
                    data.Customer,
                    data.Cust_Group,
                    data.Qty_Boxes,
                    strDateTime2Date(data.Farm_Ship, "dd-mm-yyyy"),
                    strDateTime2Date(data.Miami_Ship, "dd-mm-yyyy"),
                    data.PO_Status,
                    data.Farm_Status,
                ]);

            });
            tblPending.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}
