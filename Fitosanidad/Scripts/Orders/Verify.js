﻿var tblOrders

jQuery(window).load(function () {
    BlockScreen();
});

jQuery(document).ready(function () {
    UnlockScreen();

    $('#tableOrders thead tr').clone(true).appendTo('#tableOrders thead');
    $('#tableOrders thead tr:eq(1) th').each(function (i) {
        $(this).html('<input type="text" />');

        $('input', this).on('keyup change', function () {
            if (tblOrders.column(i).search() !== this.value) {
                tblOrders
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        });
    });

    tblOrders = jQuery('#tableOrders').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , orderCellsTop: true
        , scrollX: true
        , scrollCollapse: true
        , fixedHeader: true
        , fixedColumns: {
            leftColumns: 2
        }
        , "columnDefs": [
            {
                "targets": [2, 3, 8],
                "className": "text-right",
            }
            , { "width": "05%", "targets": 0 }
            , { "width": "05%", "targets": 1 }
            , { "width": "10%", "targets": 2 }
            , { "width": "10%", "targets": 3 }
            , { "width": "10%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
            , { "width": "05%", "targets": 6 }
            , { "width": "05%", "targets": 7 }
            , { "width": "05%", "targets": 8 }
            , { "width": "10%", "targets": 9 }
            , { "width": "10%", "targets": 10 }
        ]
        , 'order': [[2, 'asc']]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });
});

function BackToList() {
    var Url = "/Orders/Confirm";
    window.location.href = Url;
}

function LoadGrid() {
    var prmCodigoCliente = $("#PONumber").val();
    var prmCodigoFinca = $("#FarmOrder").val();

    var prmIDCabezas = "";

    var parameters = { "prmCodigoCliente": prmCodigoCliente, "prmCodigoFinca": prmCodigoFinca }

    $("#ddlFincasAsociadas").empty();
    $("#OrderPONumber").val("");
    $("#OrderCustomerOrder").val("");
    $("#OrderStatus").val("");
    $("#OrderType").val("");
    $("#OrderCustomer").val("");

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/JsonGetAssociatedFarms",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.each(data, function (i, data) {
                $("#ddlFincasAsociadas").append('<option value="'
                    + data.Farm + '">'
                    + data.Farm + ' - ' + GetResource("OrdenEnFinca") + ': ' + data.Farm_Order + '</option>');

                $("#OrderPONumber").val(data.PO);
                $("#OrderCustomerOrder").val(data.Cust_Order);
                $("#OrderStatus").val(data.Estatus);
                $("#OrderType").val(data.Type_Order);
                $("#OrderCustomer").val(data.Customer);

                prmIDCabezas = $("#prmIDCabezas").val(data.ID);
            });

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });

    var prmFarm = Session("IDFarm");
    var prmFarmOrder = prmCodigoFinca;

    var parameters = { "prmIDCabezas": prmIDCabezas, "prmFarm": prmFarm, "prmFarmOrder": prmFarmOrder }

    BlockScreen();
    jQuery.ajax({
        url: "/Orders/JsonGetLoadDetailsOrders",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblOrders = jQuery('#tableOrders').DataTable();
            tblOrders.clear().draw();

            $.each(data, function (i, data) {
                tblOrders.row.add([
                    data.LN,
                    data.Farm,
                    data.FechaShipDateFarm,
                    data.Product,
                    data.Box,
                    data.Pack,
                    data.CantidadFinca,
                    data.Farm_Order,
                    data.Status_Detalle,
                    data.TotalTallos,
                    data.Status_Detalle,
                    data.Farm,
                ]);

            });
            tblOrders.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function Search() {
    LoadGrid();
}