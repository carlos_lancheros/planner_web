﻿jQuery(document).ready(function () {

    //jQuery("#dataPlanner_Sowing").click(function () {
    //    jQuery('<table>')
    //        .append(
    //            jQuery("#tablePlanner_Sowing").DataTable().$('tr').clone()
    //        )
    //        .table2excel({
    //            exclude: ".excludeThisClass",
    //            name: "Worksheet Name",
    //            filename: "Planner_Sowing_Data" //do not include extension
    //        });
    //});
    //jQuery("#tablePlanner_Sowing").dataTable();
});

function Search() {
    var prmSemanaINI = parseInt($("#ddlWeekFrom").find(":selected").val());
    var prmSemanaFIN = parseInt($("#ddlWeekTo").find(":selected").val());
    var prmUnidadNegocio = parseInt($("#ddlBusinessUnit").find(":selected").val());

    var parameters = { "prmSemanaINI": prmSemanaINI, "prmSemanaFIN": prmSemanaFIN, "prmUnidadNegocio": prmUnidadNegocio };

    BlockScreen();
    jQuery.ajax({
        url: "/Photometer/GetPhotometer",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery('#PhotometernDataTable').html(data);
            UnlockScreen();
        },
        error: function () {
            UnlockScreen();
        }
    });
}
