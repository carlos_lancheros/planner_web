﻿$(document).ready(function () {
    var bootstrapTooltip = $.fn.tooltip.noConflict();
    $.fn.bstooltip = bootstrapTooltip;
    $(this).find("button").each(function () {
        $(this).bstooltip();
    })

    $('.select2').select2();
});

jQuery(document).ready(function () {
    jQuery("#divNotifications").hide();
    jQuery('#sidebar').toggleClass('active');

    jQuery("#btnNotifications").click(function (e) {
        jQuery("#divNotifications").hide();
    });

    $.getBadge = function (hadler) {
        $.getJSON('/Json/GetBadge', function (data) {
            if (data == "undefined" || data == null) {
                Notifications("information", GetResource("NoSeEncontraronDatos"))
                return;
            }
            hadler(data);
        });
    };

    $.getCountry = function (hadler) {
        $.getJSON('/Json/GetCountry', function (data) {
            if (data == "undefined" || data == null) {
                Notifications("information", GetResource("NoSeEncontraronDatos"))
                return;
            }
            hadler(data);
        });
    }
});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function Notifications(_type, _message) {
    if (_message == undefined) {
        return;
    }
    jQuery("#divNotifications").hide();

    jQuery("#strMessage").empty();
    jQuery("#strMessage").append(_message);

    if (_type == "alert") {
        jQuery("#divNotifications").attr("class", "alert alert-info alert-dismissable");
    }
    if (_type == "success") {
        jQuery("#divNotifications").attr("class", "alert alert-success alert-dismissable");
    }
    if (_type == "error") {
        jQuery("#divNotifications").attr("class", "alert alert-danger alert-dismissable");
    }
    if (_type == "warning") {
        jQuery("#divNotifications").attr("class", "alert alert-warning alert-dismissable");
    }
    if (_type == "information") {
        jQuery("#divNotifications").attr("class", "alert alert-danger alert-dismissable");
    }
    jQuery("#divNotifications").show();
    setTimeout(function () {
        jQuery("#divNotifications").hide();
    }, 5000);
};

function GetResource(key, reemplazo) {
    var _key = "";
    var laArray = new Array();

    if (reemplazo !== null) {
        laArray = reemplazo;
    }

    var data = {
        "_Key": key,
        "_Parametros": laArray,
    }
    $.ajax({
        type: "POST",
        url: "/Base/GetResource",
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        success: function (d) {
            _key = d;
        },
        error: function (xhr, textStatus, errorThrown) {
            _key = errorThrown;
        },
        dataType: "Json",
        async: false
    });
    return _key
}

/******************
* Procedimiento que bloquea la pantalla mientras
* se hace un proceso
******************/
function BlockScreen() {
    jQuery.blockUI();
    jQuery.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#002744',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }, message: '<img src="/Image/ajax-loader.gif" />'
    });
};

/******************
*   Procedimiento que desbloquea la pantalla
*   después que termina un proceso
******************/
function UnlockScreen() {
    setTimeout(jQuery.unblockUI, 1000);
};

function soloNumeros(e) {
    var charCode = (e.which) ? e.which : e.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function soloNumerosDecimal(evt, input) {
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;
    var chark = String.fromCharCode(key);
    var tempValue = input.value + chark;
    if (key >= 48 && key <= 57) {
        if (filter(tempValue) === false) {
            return false;
        } else {
            return true;
        }
    } else {
        if (key == 8 || key == 13 || key == 0) {
            return true;
        } else if (key == 46) {
            if (filter(tempValue) === false) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}

function filter(__val__) {
    var preg = /^([0-9]+\.?[0-9]{0,2})$/;
    if (preg.test(__val__) === true) {
        return true;
    } else {
        return false;
    }

}

function getFormattedDate(date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return day + '/' + month + '/' + year;
}

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function NomMes(Mes) {
    var _NomMes = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

    return _NomMes[Mes];
}

function Meses() {
    var moment = require('moment');

    var count = 0;
    var months = [];
    while (count < 12) months.push(moment().month(count++).format("MMMM"));
}

function EMailValid(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

$(document).ajaxStart(function () {
    //BlockScreen();
});

$(document).ajaxStop(function () {
    //UnlockScreen();
});

function ReplaceNumberWithCommas(intNumberToConvert) {
    var components = intNumberToConvert.toString().split(".");
    components[0] = components[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return components.join(".");
}

function ValidateDate(datStartDate, datEndDate) {
    if (datStartDate != null && datEndDate != null) {
        if (datStartDate > datEndDate) {
            Notifications("alert", "The start date should not exceed the end date");
            return false;
        }
    }
    return true;
}

function addCommas(nStr) {
    if (nStr == null) {
        return '';
    }
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function strDateFormat(d) {
    if (d.indexOf('T') > 0) {
        d = d.substring(0, d.indexOf('T'));

        var dsplit = d.split("-");
        var d = new Date(dsplit[0], dsplit[2] - 1, dsplit[1]);
        var day = ('0' + d.getDate()).slice(-2);
        var month = ('0' + (d.getMonth() + 1)).slice(-2);
        d = (day) + "-" + (month) + "-" + d.getFullYear()
    }

    return (d);
};

function jsonDateFormat(d) {
    var date = new Date(parseInt(d.replace("/Date(", "").replace(")/", ""), 10));
    var day = ('0' + date.getDate()).slice(-2);
    var month = ('0' + (date.getMonth() + 1)).slice(-2);
    var parsedDate = date.getFullYear() + "-" + (month) + "-" + (day);

    return (parsedDate);
};

function strDateTime2Date(d, strFormat) {
    if (d.indexOf('T') > 0) {
        d = d.substring(0, d.indexOf('T'));
        var dsplit = d.split("-");

        switch (strFormat) {
            case "dd-mm-yyyy":
                var d = new Date(dsplit[0], dsplit[1] - 1, dsplit[2]);
                var day = ('0' + d.getDate()).slice(-2);
                var month = ('0' + (d.getMonth() + 1)).slice(-2);
                var year = d.getFullYear();
                break;
        };

        d = (day) + "-" + (month) + "-" + (year);
    }

    return (d);
};

function jsonDateTimeFormat(d) {
    if (!d) {
        return;
    }

    var date = new Date(parseInt(d.replace("/Date(", "").replace(")/", ""), 10));
    var day = ('0' + date.getDate()).slice(-2);
    var month = ('0' + (date.getMonth() + 1)).slice(-2);

    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var ampm = hours >= 12 ? 'p.m.' : 'a.m.';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    var parsedDate = (day) + "/" + (month) + "/" + date.getFullYear() + " " + hours + ":" + minutes + ":" + seconds + " " + ampm;

    return (parsedDate);
};

function jsonDateTimeFormat24(d) {
    if (!d) {
        return;
    }

    var date = new Date(parseInt(d.replace("/Date(", "").replace(")/", ""), 10));
    var day = ('0' + date.getDate()).slice(-2);
    var month = ('0' + (date.getMonth() + 1)).slice(-2);

    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();

    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    var parsedDate = (day) + "/" + (month) + "/" + date.getFullYear() + " " + hours + ":" + minutes + ":" + seconds;

    return (parsedDate);
};

function ValidaPorcentaje(strporc, id) {
    var isValid = strporc.match(/^((100(\.0{1,2})?)|(\d{1,2}(\.\d{1,2})?))$/) == null ? false : true;
    if (isValid == false) {
        $("#" + id).focus().select();
        $("#" + id).val("");
        Notifications("error", "El campo debe ser menor o igual a 100, velifique");
        location = "#divNotifications";
    }
}

function DecimalPlaces(value, decimalPlaces) {
    if (value) {
        return Number.parseFloat(value).toFixed(decimalPlaces);
    }
    else {
        return '';
    }
};

function Between(x, min, max) {
    return x >= min && x <= max;
};

function GoToUrl(strUrl, booTab) {
    if (booTab) {
        window.open(strUrl, '_blank');
    } else {
        var Url = strUrl;
        window.location.href = Url;
    }
};
function jsonTimeFormat(d, booDate) {
    if (!d) {
        return;
    }

    if (booDate) {
        var date = new Date(parseInt(d.replace("/Date(", "").replace(")/", ""), 10));
        d = date
    }


    var hours = d.getHours();
    var minutes = d.getMinutes();
    var seconds = d.getSeconds();
    hours = d.getHours();
    var ampm = hours >= 12 ? 'p.m.' : 'a.m.';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    var parsedDate = hours + ":" + minutes + ":" + seconds + " " + ampm;

    // Changed data format;
    return (parsedDate);
};

function GetSessionVar(strSession) {
    var strResult = "";

    var parameters = {
        "strSession": strSession
    }
    $.ajax({
        type: "POST",
        url: "/Helper/GetSessionVar",
        data: JSON.stringify(parameters),
        contentType: 'application/json; charset=utf-8',
        success: function (d) {
            strResult = d;
        },
        error: function (xhr, textStatus, errorThrown) {
            strResult = errorThrown;
        },
        dataType: "Json",
        async: false
    });
    return strResult
}

function getInvertColor(hex, bw) {
    if (hex.indexOf('#') === 0) {
        hex = hex.slice(1);
    }
    // convert 3-digit hex to 6-digits.
    if (hex.length === 3) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    if (hex.length !== 6) {
        return hex;
    }
    var r = parseInt(hex.slice(0, 2), 16),
        g = parseInt(hex.slice(2, 4), 16),
        b = parseInt(hex.slice(4, 6), 16);
    if (bw) {
        return (r * 0.299 + g * 0.587 + b * 0.114) > 186
            ? '#000000'
            : '#FFFFFF';
    }

    r = (255 - r).toString(16);
    g = (255 - g).toString(16);
    b = (255 - b).toString(16);

    return "#" + padZero(r) + padZero(g) + padZero(b);
}

function padZero(str, len) {
    len = len || 2;
    var zeros = new Array(len).join('0');
    return (zeros + str).slice(-len);
}