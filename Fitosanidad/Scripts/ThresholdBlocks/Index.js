﻿$("document").ready(function () {
    //alert("entro ... ");

    $("#GreeHouseID").on("change", function () {
        //ProductoPorBloque();
    });

    $("#DrawReport").on("click", function () {
        BlockScreen();
        $("#myChart").remove();
        $("#cargaCanvas").append('<canvas id="myChart"></canvas>');

        var ReportType = $('[name="ReportType"]:checked').val();

        var WeekID = $("#WeekID").val();
        var WeekIDF = $("#WeekIDF").val();

        if (parseInt(WeekIDF) < parseInt(WeekID)) {
            Notifications("error", "Semana final debe ser mayor a la final, verifique");
            UnlockScreen();
            return;
        }

        var GreeHouseID = $("#GreeHouseID").val();
        var ProductID = $("#ProductID").val();
        var PestID = $("#PestID").val();

        if (!WeekID || !GreeHouseID) {
            Notifications("error", "Debe seleccionar semana y finca, Verifique!");
            UnlockScreen();
            return false;
        }

        var datos = {
            "WeekID": WeekID,
            "WeekIDF": WeekIDF,
            "GreeHouseID": GreeHouseID,
            "ProductID": ProductID,
            "PestID": PestID,
        }

        $.ajax({
            DataType: "json",
            type: "post",
            url: "/ThresholdBlocks/DrawReport",
            data: datos,
            success: function (r) {
                DataTotal = r.DataTotal;

                if (DataTotal.length === 0) {
                    Notifications("error", "No se encontraron resultados, Verifique");
                    UnlockScreen();
                    return;
                }

                //Armado de arr de bloques
                var Labels = [];
                var Weeks = [];
                for (i in DataTotal) {
                    //if (i % 4 == 0) {
                    Labels.push(DataTotal[i].IDInvernaderos_Codigo);
                    //Cama = DataTotal[i].IDInvernaderos_Codigo;
                    Weeks.push(DataTotal[i].Semana);
                    //}
                }

                $.unique(Weeks.sort());
                $.unique(Labels.sort());

                console.log(Weeks);


                //Armado de arr de semanas
                //var Weeks = [];
                //for (i = 0; i < 4; i++) {
                //    Weeks.push(DataTotal[i].Semana);
                //}

                //Carga de datos por semana
                var Datos = [];


                for (S in Weeks) {
                    Datos[S] = [];
                    for (D in DataTotal) {
                        if (DataTotal[D]["Semana"] === Weeks[S]) {
                            Datos[S].push(DataTotal[D]["PorcIncidencia"]);
                        }
                    }
                }

                //Armado de arr de dataset
                var Sets = [];
                var cont = 0;
                for (S in Weeks) {
                    Sets.push({
                        label: "Semana " + Weeks[S],
                        backgroundColor: random_rgba(),
                        data: Datos[cont]
                    });
                    cont++;
                }
                var retorna = {
                    labels: Labels,
                    datasets: Sets
                }

                DrawChart(retorna);
            },
            complete: function () {
                $("#titulo").text("Bloques x Encima del Umbral de Acción");
                UnlockScreen();
            }
        })
    });

    $('#downloadPdf').click(function (event) {
        // get size of report page
        var reportPageHeight = $('#cargaCanvas').innerHeight();
        var reportPageWidth = $('#cargaCanvas').innerWidth();

        // create a new canvas object that we will populate with all other canvas objects
        var pdfCanvas = $('<canvas />').attr({
            id: "canvaspdf",
            width: reportPageWidth,
            height: reportPageHeight
        });

        // keep track canvas position
        var pdfctx = $(pdfCanvas)[0].getContext('2d');
        var pdfctxX = 0;
        var pdfctxY = 0;
        var buffer = 100;

        // for each chart.js chart
        $("canvas").each(function (index) {
            // get the chart height/width
            var canvasHeight = $(this).innerHeight();
            var canvasWidth = $(this).innerWidth();

            // draw the chart into the new canvas
            pdfctx.drawImage($(this)[0], pdfctxX, pdfctxY, canvasWidth, canvasHeight);
            pdfctxX += canvasWidth + buffer;

            // our report page is in a grid pattern so replicate that in the new canvas
            if (index % 2 === 1) {
                pdfctxX = 0;
                pdfctxY += canvasHeight + buffer;
            }
        });

        // create new pdf and add our new canvas as an image
        var pdf = new jsPDF('l', 'pt', [reportPageWidth, reportPageHeight]);
        pdf.addImage($(pdfCanvas)[0], 'PNG', 0, 0);

        // download the pdf
        pdf.save('filename.pdf');
    });

    //ProductoPorBloque();
});

function random_rgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ', 1)';
}

function DrawChart(datos) {
    //Grafica de prueba
    var data = datos;
    $("#myChart").empty();
    $("#myChart").html("");

    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            "hover": {
                "animationDuration": 0,
                onHover: function () {
                    return false;
                }
            },
            legend: {
                "text": "Semanas",
            },
            tooltips: {
                "enabled": false,
            },
            legend: {
                onClick: (e) => e.stopPropagation()
            },
            "animation": {
                "hover": {
                    "animationDuration": 0
                },
                "duration": 1,
                "onComplete": function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;

                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textBaseline = 'bottom';
                    ctx.textAlign = 'rigth';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.save();
                            var random = Math.floor(Math.random() * 16) + 5;
                            ctx.translate(bar._model.x + 7, bar._model.y - random);
                            ctx.rotate(-0.5 * Math.PI);
                            ctx.fillText(data, 0, 0);
                            ctx.restore();
                        });
                    });
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: 100,
                    },
                     scaleLabel: {
                        display: true,
                        labelString: '% INCIDENCIA'
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'BLOQUE'
                    }
                }],
            },
        }
    })
}

function ProductoPorBloque() {
    var GreeHouseID = $("#GreeHouseID").val();
    var datos = {
        "BlockID": GreeHouseID,
    };

    $.ajax({
        DataType: "json",
        type: "post",
        url: "/ThresholdBlocks/ProductByBlock",
        data: datos,
        success: function (r) {
            var html = "<option></option>";
            for (x in r) {
                html += "<option value='" + r[x].Value + "'>" + r[x].Text + "</option>";
            }
            $("#ProductID").html(html);
        }
    });
}