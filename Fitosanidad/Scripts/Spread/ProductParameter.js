﻿jQuery(document).ready(function () {
    jQuery("#dataSensor").click(function () {
        jQuery('<table>')
            .append(
                jQuery("#tableSensor").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Sensor_Data" //do not include extension
            });
    });

    $("#btnCreate").click(function () {
        CreateRow();
    });
});

function CreateRow() {
    var url = "/ProductParameter/Create";
    window.location = url;
}

function EditRow(arrKey) {
    var ProductParametersID = arrKey[0];
    var url = "/ProductParameter/Edit?ProductParametersID=" + ProductParametersID;

    window.location = url;
}

function DelRow(arrKey) {
    var ProductParametersID = arrKey[0];
    var url = "/ProductParameter/Delete?ProductParametersID=" + ProductParametersID;

    window.location = url;
}
