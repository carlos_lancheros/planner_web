﻿jQuery(document).ready(function () {
    //$('#ExecutionLaborsWorkforceDataTable').hide();
    jQuery('#tableProductParameter').dataTable({
        "info": false
        , paging: true
        , responsive: true
        , destroy: true
        , rowGroup: true
        , "columnDefs": [
            {
                "targets": [3, 4, 5, 6],
                "className": "text-right",
            }
            ,{ className: "dt-left", "width": "05%", "targets": 0 }
            , { "width": "30%", "targets": 1 }
            , { "width": "20%", "targets": 2 }
            , { "width": "10%", "targets": 3 }
            , { "width": "10%", "targets": 4 }
            , { "width": "10%", "targets": 5 }
            , { "width": "10%", "targets": 6 }
        ]
        , "columns": [
            { 'data': 0 },
            { 'data': 1 },
            { 'data': 2 },
            { 'data': 3 },
            { 'data': 4 },
            { 'data': 5 },
            { 'data': 6 },
        ]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });
});
