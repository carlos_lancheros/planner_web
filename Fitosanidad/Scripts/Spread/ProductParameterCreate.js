﻿$(document).ready(function () {
    $("#IOT_SensorListID").val(0);
    $("#ddlGreenhouse").empty();
    $("#ddlGreenhouse").append($("<option></option>").val('0').html('EXTERIOR - Cód. 0'));

    $("#ddlBusinessUnit").change(function () {
        var businessUnitID = $("#ddlBusinessUnit").find(":selected").val();
        var values = { "businessUnitID": businessUnitID }
        $("#ddlGreenhouse").empty()

        BlockScreen();
        $.ajax({
            url: "/IOT_SensorList/greenhouseListByBusinessUnitID",
            data: JSON.stringify(values),
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $.each(data, function (i, data) {
                    $("#ddlGreenhouse").append('<option value="'
                        + data.Value + '">'
                        + data.Text + '</option>');
                });
                $("#ddlGreenhouse").append($("<option></option>").val('0').html('EXTERIOR - Cód. 0'));
                UnlockScreen();
            },
            error: function (xhr, textStatus, errorThrown) {
                UnlockScreen();
                Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#lstBusinessUnit")).text());
            },
        });
    });
});

function BackToList() {
    var Url = "/ProductParameter";
    window.location.href = Url;
}

function greenhouseListOnChange() {
    var businessUnitID = $("#ddlGreenhouse").find(":selected").val();

    var parameters = { "businessUnitID": businessUnitID };

    $("#ddlDetails").empty();

    BlockScreen();
    jQuery.ajax({
        url: "/IOT_SensorList/greenhouseListByBusinessUnitID",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $("#ddlDetails").append('<option value="">' + GetResource("Seleccionar") + '</option>');

            $.each(data, function (i, data) {
                $("#ddlDetails").append('<option value="'
                    + data.Value + '">'
                    + data.Text + '</option>');
            });

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#lstBusinessUnit")).text());
        },
        async: false
    });



}

function CallSubmit() {
    if ($("#AddName").val() == "") {
        $('#AddName').focus();
        return false;
    }
    if ($("#AddService").val() == "") {
        $('#AddService').focus();
        return false;
    }
    if ($("#AddExternalID").val() == "") {
        $('#AddExternalID').focus();
        return false;
    }

    var IOT_SensorListID = $("#IOT_SensorListID").val();
    var businessUnitId = $("#ddlBusinessUnit").find(":selected").val();
    var greenHouseId = $("#ddlGreenhouse").find(":selected").val();
    var name = $("#AddName").val();
    var service = $("#AddService").val();
    var flagActive = $("#AddFlagActive").is(":checked");
    var externalId = $("#AddExternalID").val();
    var CRUD = "C";
    var values = { "IOT_SensorListID": IOT_SensorListID, "businessUnitId": businessUnitId, "greenHouseId": greenHouseId, "name": name, "service": service, "flagActive": flagActive, "externalId": externalId, "CRUD": CRUD };

    BlockScreen();
    jQuery.ajax({
        url: "/IOT_SensorList/IOT_SensorList_SaveChanges",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $("#AddName").val("");
            $("#AddService").val("");
            $("#AddExternalID").val("");
            $("#AddFlagActive").attr('checked', false);

            UnlockScreen();
        },
        error: function () {
            UnlockScreen();
        }
    });
    $('#modAdd').modal('toggle');

    var values = null;
    BlockScreen();
    jQuery.ajax({
        url: "/IOT_SensorList/GetSensorList",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //$('#SensorDataTable').html(data);
            BackToList();
            UnlockScreen();
        },
        error: function () {
            UnlockScreen();
        }
    });

};