﻿$("document").ready(function () {
    //alert("jadhasdh");
});

function TraeData() {
    BlockScreen();
    var WeekIDInic = $("#WeekIDInic").val();
    var WeekIDFina = $("#WeekIDFina").val();
    var GreeHouseID = $("#GreeHouseID").val();

    if (parseInt(WeekIDInic) > parseInt(WeekIDFina)) {
        Notifications("error", "Semana inicial debe ser menor que la final, verifique!");
        return;
    }

    var datos = {
        "WeekIDInic": WeekIDInic,
        "WeekIDFina": WeekIDFina,
        "GreeHouseID": GreeHouseID,
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/IncidenceDynamicViewer/TraeData",
        success: function (r) {

            var renderers = $.extend(
                $.pivotUtilities.renderers,
                $.pivotUtilities.plotly_renderers,
                $.pivotUtilities.d3_renderers,
                $.pivotUtilities.export_renderers
            );

            $("#PivotData").pivotUI(r, {
                rows: ["Bloque", "Plaga"],
                cols: ["Semana"],
                hiddenAttributes: [""],
                renderers: renderers,
                aggregatorName: "Sum",
                vals: ["PPIncidencia"],
                rendererName: "Table",
            }, true);
            UnlockScreen();
        }
    })
}

function TraeDataW() {
    BlockScreen();
    var WeekIDInic = $("#WeekIDInic").val();
    var WeekIDFina = $("#WeekIDFina").val();
    var GreeHouseID = $("#GreeHouseID").val();

    if (!GreeHouseID) {
        GreeHouseID = 0;
    }


    if (parseInt(WeekIDInic) > parseInt(WeekIDFina)) {
        UnlockScreen();
        Notifications("error", "Semana inicial debe ser menor que la final, verifique!");
        return;
    }

    var datos = {
        "WeekIDInic": WeekIDInic,
        "WeekIDFina": WeekIDFina,
        "GreeHouseID": GreeHouseID,
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/IncidenceDynamicViewer/TraeData",
        success: function (r) {
            var pivot = new Flexmonster({
                container: "#PivotWData",
                componentFolder: "https://cdn.flexmonster.com/",
                beforetoolbarcreated: customizeToolbar,
                toolbar: true,
                height: 600,
                global: {
                    localization: "https://cdn.webdatarocks.com/loc/es.json",
                },


                report: {
                    "formats": [
                        {
                            "name": "decimal",
                            "decimalPlaces": 2,
                            decimalSeparator: ",",
                            currencySymbol: "%",
                            nullValue: "",
                            infinityValue: "Over",
                            divideByZeroValue: ""
                        },
                        {
                            "name": "integer",
                            "decimalPlaces": 0,
                        },
                    ],

                    dataSource: {
                        data: r,
                    },
                    "slice": {
                        "rows": [
                            {
                                "uniqueName": "Finca"
                            },
                            {
                                "uniqueName": "Bloque"
                            },
                            {
                                "uniqueName": "Producto"
                            },
                            {
                                "uniqueName": "Plaga"
                            },
                        ],
                        "columns": [
                            {
                                "uniqueName": "Semana"
                            }
                        ],
                        "measures": [
                            {
                                "uniqueName": "TCamas",
                                "formula": "\"TCamas\"",
                                "individual": false,
                                "caption": "TCamas Semb.",
                                "format": "integer"
                            },
                            {
                                "uniqueName": "CamasAfect",
                                "formula": "\"CamasAfec\"",
                                "individual": true,
                                "caption": "CamasAfect",
                                "format": "integer"
                            },
                            {
                                "uniqueName": "CuadrosAfect",
                                "formula": "\"CuadrosAfec\"",
                                "individual": true,
                                "caption": "CuadrosAfect",
                                "format": "integer"
                            },
                            {
                                "uniqueName": "Incidencia",
                                "formula": '("CamasAfec" * 100) / "TCamas"',
                                "individual": false,
                                "caption": "% Incidencia",
                                "format": "decimal"
                            },
                            {
                                "uniqueName": "Severidad",
                                "formula": "(\"CuadrosAfec\" / (\"TCamas\" * 5))*100",
                                "individual": false,
                                "caption": "Severidad %",
                                "format": "decimal"
                            }
                        ],
                        "expands": {
                            "expandAll": true,
                        },
                    },
                    "options": {
                        "grid": {
                            "showTotals": "off",
                            "showGrandTotals": "off",
                        }
                    }
                },
                licenseKey: "Z792-XABH0H-1N670U-4I3D20",
            });
            UnlockScreen();
        },
        complete: function () {
            var scroll = $('html, body');
            scroll.animate({ scrollTop: scroll.prop("scrollHeight") });
        }
    });
}

function customizeToolbar(toolbar) {
    var tabs = toolbar.getTabs();
    toolbar.getTabs = function () {
        delete tabs[0];
        delete tabs[1];
        delete tabs[2];
        delete tabs[4];
        delete tabs[5];
        delete tabs[6];
        delete tabs[7];
        delete tabs[8];
        delete tabs[9];
        return tabs;
    }
}

function exportTableToExcel(tableID, filename = '') {
    $(".pvtTable").attr("id", "pvtTable");
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    filename = filename ? filename + '.xls' : 'excel_data.xls';
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob(blob, filename);
    } else {

        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

        downloadLink.download = filename;

        downloadLink.click();
    }
}