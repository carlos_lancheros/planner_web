﻿
$("document").ready(function () {
    //alert("jadhasdh");
});

function ShowData() {
    BlockScreen();
    var WeekIDInic = $("#WeekIDInic").val();
    var WeekIDFina = $("#WeekIDFina").val();
    var BusinessUnitID = $("#BusinessUnitID").val();

    if (parseInt(WeekIDInic) > parseInt(WeekIDFina)) {
        Notifications("error", "Semana inicial debe ser menor que la final, verifique!");
        return;
    }

    var datos = {
        "WeekIDInic": WeekIDInic,
        "WeekIDFina": WeekIDFina,
        "BusinessUnitID": BusinessUnitID,
    };

    $.ajax({
        type: "post",
        data: JSON.stringify(datos),
        url: "/InvIncTracing/ShowData",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {


            UnlockScreen();
        }
    })
}


function GetDocTracing(SowingID) {
    var Url = "/InvIncTracing/DocTracing?SowingID=" + SowingID;
    window.location.href = Url;

    
}

function changePhoto() {
 
    $('#tableInvTracingDetail tbody tr td').find('img').each(function (i) {
        strPhotoFileName = $(this).attr("id")

        var datos = { "strPhotoFileName": strPhotoFileName }
        BlockScreen();
        
        $.ajax({
            type: "post",
            data: JSON.stringify(datos),
            url: "/InvIncTracing/GetUPCPhotoFile",
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.length == 0) {
                    document.getElementById(strPhotoFileName).src = '/Downloads/' + strPhotoFileName; 
                } else {
                    $("#imgUPCPhoto").attr("src", "/Content/img/no_image.jpg");
                }

                UnlockScreen();
            },
            async:false
        })
    });
}


function BackToList() {
    location = "/InvIncTracing/Index";
}

function exportPdf() {

    var element = document.getElementById('content');

    var widthValue = 0
    var heigthValue = 0
    var orientationVAlue = "";
    var imp = 3;
    if (imp == 1 || imp == 2) {
        orientationVAlue = "portrait";
        widthValue = 740
        heigthValue = 540
    } else {
        orientationVAlue = "landscape"
        widthValue = 980
        heigthValue = 780
    }

    var opt = {
        margin: [15, 10, 15, 10],
        filename: 'inv.pdf',
        image: { type: 'png', quality: 0.1 },
        html2canvas: { dpi: 300, letterRendering: true, width: widthValue, heigth: heigthValue },
        //jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' },
        jsPDF: { unit: 'mm', format: 'letter', orientation: orientationVAlue }
    };

    html2pdf().set(opt).from(element).toPdf().get('pdf').then(function (pdf) {
        var number_of_pages = pdf.internal.getNumberOfPages()
        var pdf_pages = pdf.internal.pages
        var myFooter = "Footer info"
        var tituloOutput = $("#tituloPdf").text();

        for (var i = 1; i < pdf_pages.length; i++) {
            pdf.setPage(i)
            pdf.setFontSize(10);
            pdf.text(tituloOutput, 2, 10);
        }
    }).save();
    return;
}