﻿function Upload() {
    if ($("#UploadFile").val() == '') {
        Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
        $('#UploadFile').focus();
        return false;
    } else {
        if ($("#UploadFile").val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
            $('#UploadFile').focus();
            return false;
        };
    }

    var file = $('input[type="file"]').val();
    var exts = ['csv', 'xls', 'xlsx'];
    if (file) {
        var get_ext = file.split('.');
        get_ext = get_ext.reverse();
        if ($.inArray(get_ext[0].toLowerCase(), exts) > -1) {
            Notifications("warning", GetResource("CargandoInformacionDesdeElArchivo"))
        } else {
            Notifications("error", GetResource("TipoDeArchivoNoValido"))
            $('#UploadFile').focus();
            return false;
        }
    }

    var data = new FormData();
    var files = $("#UploadFile").get(0).files;

    if (files.length > 0) {
        data.append("HelpSectionImages", files[0]);
    }
    else {
        Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
        $('#UploadFile').focus();
        return false;
    }
    var extension = $("#UploadFile").val().split('.').pop().toUpperCase();
    if (extension != "CSV") {
        Notifications("error", GetResource("TipoDeArchivoNoValido"))
        $('#UploadFile').focus();
        return false;
    }

    $.ajaxSetup({ cache: false });
    BlockScreen();
    $.ajax({
        type: "POST",
        processData: false,
        url: "/plantingPlan/UploadFromFile",
        data: data,
        dataType: "Json",
        contentType: false,
        success: function (data) {
            //var tblSensorDataList = jQuery('#tableSensorDataList').DataTable();

            //$.each(data, function (i, data) {
            //    if (data.SensorID != $("#ExternalID").val()) {
            //        UnlockScreen();
            //        Notifications("error", GetResource("LaInformacionACargarNoCorrespondeAlSensorSeleccionado"))
            //        return false;
            //    }

            //    var add = tblSensorDataList.row.add([
            //        data.SensorID,
            //        data.Date,
            //        data.Humidity,
            //        data.Temperature,
            //        data.Luminosity,
            //    ]).node().id = "rowId" + i;
            //});

            //tblSensorDataList.draw(false);

            //$('input[type="file"]').val("");

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
        dataType: "Json",
        async: false
    });
}