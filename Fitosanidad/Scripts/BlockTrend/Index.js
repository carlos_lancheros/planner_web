﻿$(document).ready(function () {
    $("#DrawReport").click(function () {
        DrawReport();
    });
});

function DrawReport() {

    var GreeHouseID = $("#GreeHouseID").val();
    var ProductID = $("#ProductID").val();
    var PestID = $("#PestID").val();

    var datos = {
        "prmIDUnidadesNegocios": GreeHouseID,
        "prmBlanco": PestID,
        "prmClasesProductos": ProductID,
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/BlockTrend/CantidadBloques/",
        success: function (r) {
            for (x in r) {
                var html = "";
                html = "<div class='col-sm-6'><canvas id='myChart_" + r[x] + "'></canvas></div>";
                $("#cargaCanvas").append(html);
                cargaDatosporTabla(r[x]);
            }
        }
    });
}

function cargaDatosporTabla(Block) {
    $.ajax({
        DataType: "JSON",
        type: "post",
        data: {"Block": Block},
        url: "/BlockTrend/cargaDatosporTabla/",
        success: function (r) {
            console.log(r);
        }
    });
}