﻿jQuery(document).ready(function () {
    jQuery('#tableEmployees').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , "columnDefs": [
            {
                "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8],
                //"targets": [0, 1, 2],
                "className": "text-left",
            },
            { className: "dt-right", "width": "5%", "targets": 0 }
            , { "width": "7%", "targets": 1 }
            , { "width": "20%", "targets": 2 }
            , { "width": "5%", "targets": 3 }
            , { "width": "15%", "targets": 4 }
            , { "width": "15%", "targets": 5 }
            , { "width": "10%", "targets": 6 }
            , { "width": "13%", "targets": 7 }
            , { "width": "10%", "targets": 8 }
            //, { "width": "9%", "targets": 9 }
        ]
    });

    jQuery("#ddlCompania").change(function () {
        Search();

        //var CompaniaID = jQuery("#ddlCompania").find(":selected").val();
        //var UnidadNegocioID = 0;
        //var CentrosOperacionID = 0;
        //var CentrosCostoID = 0;        
        //EmployeesEnable = (EmployeesEnable ? 1 : 0);

        //var values = { "CompaniaID": CompaniaID, "UnidadNegocioID": UnidadNegocioID, "CentrosOperacionID": CentrosOperacionID, "CentrosCostoID": CentrosCostoID, "FlagActivo": EmployeesEnable }

        //jQuery("#tableEmployees").empty()

        //jQuery.ajax({
        //    url: "/Employees/Employees",
        //    data: JSON.stringify(values),
        //    type: 'POST',
        //    contentType: 'application/json; charset=utf-8',
        //    success: function (data) {
        //        var table = jQuery('#tableEmployees').DataTable();
        //        table.destroy();
        //        jQuery('#tableEmployees').html(data);
        //        jQuery('#tableEmployees').DataTable();
        //    },
        //    error: function () {

        //    }
        //});
    });

    jQuery("#ddlUnidadNegocio").change(function () {
        Search();

        //var CompaniaID = jQuery("#ddlCompania").find(":selected").val();
        //var UnidadNegocioID = jQuery("#ddlUnidadNegocio").find(":selected").val();
        //var CentrosOperacionID = 0;
        //var CentrosCostoID = 0;        
        //EmployeesEnable = (EmployeesEnable ? 1 : 0);

        //var values = { "CompaniaID": CompaniaID, "UnidadNegocioID": UnidadNegocioID, "CentrosOperacionID": CentrosOperacionID, "CentrosCostoID": CentrosCostoID, "FlagActivo": EmployeesEnable }


        //jQuery("#tableEmployees").empty()

        //jQuery.ajax({
        //    url: "/Employees/Employees",
        //    data: JSON.stringify(values),
        //    type: 'POST',
        //    contentType: 'application/json; charset=utf-8',
        //    success: function (data) {
        //        var table = jQuery('#tableEmployees').DataTable();
        //        table.destroy();
        //        jQuery('#tableEmployees').html(data);
        //        jQuery('#tableEmployees').DataTable();
        //    },
        //    error: function () {

        //    }
        //});
    });

    jQuery("#ddlCentrosOperacion").change(function () {
        Search();

        //var CompaniaID = jQuery("#ddlCompania").find(":selected").val();
        //var UnidadNegocioID = jQuery("#ddlUnidadNegocio").find(":selected").val();
        //var CentrosOperacionID = jQuery("#ddlCentrosOperacion").find(":selected").val();
        //var CentrosCostoID = 0;
        //EmployeesEnable = (EmployeesEnable ? 1 : 0);

        //var values = { "CompaniaID": CompaniaID, "UnidadNegocioID": UnidadNegocioID, "CentrosOperacionID": CentrosOperacionID, "CentrosCostoID": CentrosCostoID, "FlagActivo": EmployeesEnable }

        //jQuery("#tableEmployees").empty()

        //jQuery.ajax({
        //    url: "/Employees/Employees",
        //    data: JSON.stringify(values),
        //    type: 'POST',
        //    contentType: 'application/json; charset=utf-8',
        //    success: function (data) {
        //        var table = jQuery('#tableEmployees').DataTable();
        //        table.destroy();
        //        jQuery('#tableEmployees').html(data);
        //        jQuery('#tableEmployees').DataTable();
        //    },
        //    error: function () {

        //    }
        //});
    });

    jQuery("#ddlCentrosCosto").change(function () {
        Search();

        //var CompaniaID = jQuery("#ddlCompania").find(":selected").val();
        //var UnidadNegocioID = jQuery("#ddlUnidadNegocio").find(":selected").val();
        //var CentrosOperacionID = jQuery("#ddlCentrosOperacion").find(":selected").val();
        //var CentrosCostoID = jQuery("#ddlCentrosCosto").find(":selected").val();
        //EmployeesEnable = (EmployeesEnable ? 1 : 0);

        //var values = { "CompaniaID": CompaniaID, "UnidadNegocioID": UnidadNegocioID, "CentrosOperacionID": CentrosOperacionID, "CentrosCostoID": CentrosCostoID, "FlagActivo": EmployeesEnable }


        //jQuery("#tableEmployees").empty()

        //jQuery.ajax({
        //    url: "/Employees/Employees",
        //    data: JSON.stringify(values),
        //    type: 'POST',
        //    contentType: 'application/json; charset=utf-8',
        //    success: function (data) {
        //        var table = jQuery('#tableEmployees').DataTable();
        //        table.destroy();
        //        jQuery('#tableEmployees').html(data);
        //        jQuery('#tableEmployees').DataTable();
        //    },
        //    error: function () {

        //    }
        //});
    });

    jQuery("#dataExport").click(function () {
        jQuery('<table>')
            .append(
            jQuery("#tableEmployees").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Empleados"
            });
    });

    //jQuery("#dataExport").click(function () {
    //    jQuery("#tableEmployees").table2excel({
    //        exclude: ".noExl",
    //        name: "Worksheet Name",
    //        filename: "Empleados" //do not include extension
    //    });
    //});
});

function Search() {

    var CompaniaID = jQuery("#ddlCompania").find(":selected").val();
    var UnidadNegocioID = jQuery("#ddlUnidadNegocio").find(":selected").val();
    var CentrosOperacionID = jQuery("#ddlCentrosOperacion").find(":selected").val();
    var CentrosCostoID = jQuery("#ddlCentrosCosto").find(":selected").val();
    EmployeesEnable = (EmployeesEnable ? 1 : 0);

    var values = { "CompaniaID": CompaniaID, "UnidadNegocioID": UnidadNegocioID, "CentrosOperacionID": CentrosOperacionID, "CentrosCostoID": CentrosCostoID, "FlagActivo": EmployeesEnable }


    BlockScreen();
    jQuery.ajax({
        url: "/Employees/Employees",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var table = jQuery('#tableEmployees').DataTable();
            table.destroy();
            //jQuery('#tableEmployees').html(data);
            //jQuery('#tableEmployees').DataTable();

            ///////////////
            //if ($.fn.dataTable.isDataTable('#tableEmployees')) {
            //    table.destroy();
            //    table = $('#tableEmployees').DataTable({
            //        "rowsGroup": [0],
            //        "ordering": false,
            //        "bDestroy": true,
            //        "bFilter": false,
            //    }).draw();
            //}
            //else {
            //    table = $('#tableEmployees').DataTable({
            //        "rowsGroup": [0],
            //        "ordering": false,
            //        "bDestroy": true,
            //        "bFilter": false,
            //    }).draw();
            //}
            //////////////////////

            jQuery('#tableEmployees').html(data);
            jQuery('#tableEmployees').DataTable();
            //var total = 0;
            //$("#tableEmployees tbody tr").find("td:eq(6)").each(function () {
            //    valor = $(this).html();
            //    total += parseInt(valor)
            //});
            //jQuery("#Total").html(total);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });
}


