﻿$(document).ready(function () {

    var tituloBloque = null;

    $('.ReportType').iCheck({
        radioClass: 'iradio_flat-green'
    });

    $("#BusinessUnitID").on("change", function () {
        if ($("#BlockEnable").is(":checked")) {
            $("#GreenhouseID").removeAttr("disabled");
        }
        else {
            $("#GreenhouseID").attr("disabled", "disabled");
        }
        
        var BusinessUnitID = $("#BusinessUnitID").val();
        var datos = {
            "BusinessUnitID": BusinessUnitID,
        };

        $.ajax({
            DataType: "json",
            type: "post",
            url: "/IncidenceSeverity/GreeHousebyBusinessUnit",
            data: datos,
            success: function (r) {
                var html = "<option value='0'>Seleccione</option>";
                for (x in r) {
                    html += "<option value='" + r[x].Value + "'>" + r[x].Text + "</option>";
                }

                $("#GreenhouseID").html(html);
            }
        });
    });



    $("#BlockEnable").on("change", function () {
        if ($("#BlockEnable").is(":checked")) {
            $("#GreenhouseID").removeAttr("disabled");
        }
        else {
            $("#GreenhouseID").attr("disabled", "disabled");
        }


        var BusinessUnitID = $("#BusinessUnitID").val();
        var datos = {
            "BusinessUnitID": BusinessUnitID,
        };

        $.ajax({
            DataType: "json",
            type: "post",
            url: "/IncidenceSeverity/GreeHousebyBusinessUnit",
            data: datos,
            success: function (r) {
                var html = "<option value='0'>Seleccione</option>";
                for (x in r) {
                    html += "<option value='" + r[x].Value + "'>" + r[x].Text +"</option>";
                }

                $("#GreenhouseID").html(html);
            }
        });
    });

    $("#GreenhouseID").on("change", function () {
        var GreenhouseID = $("#GreenhouseID").val();
        var datos = {
            "BlockID": GreenhouseID,
        };

        $.ajax({
            DataType: "json",
            type: "post",
            url: "/IncidenceSeverity/ProductByBlock",
            data: datos,
            success: function (r) {
                var html = "<option value='0'>TODOS</option>";
                for (x in r) {
                    html += "<option value='" + r[x].Value + "'>" + r[x].Text +"</option>";
                }
                $("#ProductID").html(html);
            }
        });
    });

    $("#DrawReport").on("click", function () {
        $("#myChart").remove();
        var ckGreenhouse = 0;
        if ($("#BlockEnable").is(":checked")){
            ckGreenhouse = 1;
        }
        else {
            ckGreenhouse = 0;
        }

        $("#myChart").remove();
        $("#cargaCanvas").append('<canvas id="myChart" style="height:424px!important"></canvas>');

        var ReportType = $('[name="ReportType"]:checked').val();

        var BusinessUnitID = $("#BusinessUnitID").val();
        var GreeHouseID = $("#GreenhouseID").val();       
        var PlantProductID = $("#ProductID").val();
        var PestID = $("#PestID").val();
        var WeekIni = $("#WeekIDInic").val();
        var WeekFin = $("#WeekIDFina").val();
        var type = 0;
        var Grafica = 0;

        if (PlantProductID == 0) {
            PlantProductID = null;
        }

        if (ckGreenhouse == 0) {
            if (ReportType == 1 || ReportType == 2) {
                type = 2;
                Grafica = 1 //Barras
            }
            if (ReportType == 3) {
                type = 1;
                Grafica = 3 //Linea vs 
            }
        }
        else {
            if (ReportType == 1 || ReportType == 2) {
                type = 2;
                Grafica = 2 // Linea Bloque
            }
            if (ReportType == 3) {
                type = 2;
                Grafica = 3 //Linea vs 
            }           
        }
                
        var datos = {
            "BusinessUnitID": BusinessUnitID,
            "WeekIni": WeekIni,
            "WeekFin": WeekFin,
            "GreeHouseID": GreeHouseID,
            "PlantProductID": PlantProductID,
            "PestID": PestID,
            "ReportType": type,
        }

        if (Grafica == 1) {
            ConsultBarras(datos, ReportType);
            return
        }
        if (Grafica == 2) {
            ConsultaLinea(datos, ReportType);
            return
        }
        if (Grafica == 3) {
            ConsultaLineaVs(datos, ckGreenhouse);
            return
        }
                
    });

    $('#downloadPdf').click(function (event) {
        //// get size of report page
        //var reportPageHeight = $('.myChart').innerHeight();
        //var reportPageWidth = $('.myChart').innerWidth();

        //// create a new canvas object that we will populate with all other canvas objects
        //var pdfCanvas = $('<canvas />').attr({
        //    id: "canvaspdf",
        //    width: reportPageWidth,
        //    height: reportPageHeight
        //});

        //// keep track canvas position
        //var pdfctx = $(pdfCanvas)[0].getContext('2d');
        //var pdfctxX = 0;
        //var pdfctxY = 0;
        //var buffer = 100;

        //// for each chart.js chart
        //$("canvas").each(function (index) {
        //    // get the chart height/width
        //    var canvasHeight = $(this).innerHeight();
        //    var canvasWidth = $(this).innerWidth();

        //    // draw the chart into the new canvas
        //    pdfctx.drawImage($(this)[0], pdfctxX, pdfctxY, canvasWidth, canvasHeight);
        //    pdfctxX += canvasWidth + buffer;

        //    // our report page is in a grid pattern so replicate that in the new canvas
        //    if (index % 2 === 1) {
        //        pdfctxX = 0;
        //        pdfctxY += canvasHeight + buffer;
        //    }
        //});

        //// create new pdf and add our new canvas as an image
        //var pdf = new jsPDF('l', 'pt', [reportPageWidth, reportPageHeight]);
        //pdf.addImage($(pdfCanvas)[0], 'PNG', 0, 0); 

        //// download the pdf
        //pdf.save('filename.pdf');
        var element = document.getElementById('AllGraph');

        var opt = {
            margin: [12, 8, 8, 8],
            filename: 'IncidenciaSeveridad.pdf',
            image: { type: 'png', quality: 0.1 },
            html2canvas: { dpi: 193, letterRendering: true, width: 1250, heigth: 800 },
            //jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' },
            jsPDF: { unit: 'mm', format: 'A4', orientation: 'landscape' }
        };

        html2pdf().set(opt).from(element).toPdf().get('pdf').then(function (pdf) {
            var number_of_pages = pdf.internal.getNumberOfPages()
            var pdf_pages = pdf.internal.pages
            var myFooter = "Footer info"
            var tituloOutput = $("#titulo").text();

            for (var i = 1; i < pdf_pages.length; i++) {
                pdf.setPage(i)
                pdf.setFontSize(10);
                pdf.text(tituloOutput, 2, 5);
            }
        }).save();
    });
});

function ConsultBarras(datos, ReportType) {
    $("#CanvasMultiple").html("");
    $("#myChart").remove();
    var ejeX = "BLOQUE";
    $.ajax({
        DataType: "json",
        type: "post",
        url: "/IncidenceSeverity/DrawReportLines",
        data: datos,
        success: function (r) {

            //Armado de arr de bloques
            var Labels = [];
            var Color = [];
            var Labels = [];
            for (i in r) {
                Labels.push(r[i].Semana);
                Color.push(r[i].Color);
            }

            $.unique(Color.sort());
            $.unique(Labels.sort());

            var DataIncidencia = []
            var DataSeveridad = []


            for (x in r) {
                if (r[x].Monitoreo == "-1") {
                    DataIncidencia.push('NaN');
                    DataSeveridad.push('NaN');
                } else {
                    DataIncidencia.push(r[x].PPIncidencia);
                    DataSeveridad.push(r[x].PPSeveridad);
                }
            }

            //Armado de arr de semanas
            var Bloques = [];
            for (i in r) {
                Bloques.push(r[i].Bloque);
            }

            $.unique(Bloques.sort());


            //Armado de multiples gráficas caso "Todas los bloques"
            var $html = "";
            for (b in Bloques) {
                $html += "<div class='col-sm-5 col-md-5 col-xs-5 col-md-offset-1' text-left> " +
                    "<div class='col-sm-12'> " +
                    "<h5><b>Bloque " + Bloques[b] + "</b></h5>" +
                    "</div> " +
                    "<canvas id='canvas_" + b + "' ></canvas>" +
                    "</div> ";
            }

            $("#CanvasMultiple").html($html);

            //Carga de datos por bloque

            for (b in Bloques) {
                var DataIncidencia = []
                var DataSeveridad = []
                for (s in Labels) {
                    for (x in r) {
                        if (r[x].Bloque === Bloques[b] && r[x].Semana === Labels[s]) {
                            if (r[x].Monitoreo == "-1") {
                                DataIncidencia.push('NaN');
                                DataSeveridad.push('NaN');
                            } else {
                                DataIncidencia.push(r[x].PPIncidencia);
                                DataSeveridad.push(r[x].PPSeveridad);
                            }
                        }
                    }
                }
                if (ReportType == 1) {
                    ejeX = "SEMANA";
                    var titulo = "Informe Incidencia";
                    var ejeY = "% INCIDENCIA";
                    retorna = {
                        type: 'line',
                        labels: Labels,
                        datasets: [
                            {
                                label: "Incidencia",
                                backgroundColor: Color[0],
                                borderColor: Color[0],
                                data: DataIncidencia,
                                fill: false,
                            }
                        ],
                    }
                }

                else {
                    ejeX = "SEMANA";
                    var titulo = "Informe Severidad";
                    var ejeY = "% SEVERIDAD";
                    retorna = {
                        labels: Labels,
                        datasets: [
                            {
                                label: "Severidad",
                                backgroundColor: Color[0],
                                data: DataSeveridad,
                                fill: false,
                                borderColor: Color[0]
                            }
                        ]
                    }
                }

                var Div = "canvas_" + b;
                DrawChart(retorna, ejeX, ejeY, titulo, Div);
            }
        }
    });
}

function ConsultaLinea(datos, ReportType) {
    $("#CanvasMultiple").html("");
    $("#cargaCanvas").append('<canvas id="myChart"></canvas>');
    var ejeX = "";
    $.ajax({
        DataType: "json",
        type: "post",
        url: "/IncidenceSeverity/DrawReportLines",
        data: datos,
        success: function (r) {

            //Armado titulos eje x
            var Labels = [];
            var Color = []
            for (i in r) {
                Labels.push(r[i].Semana);
                Color.push(r[i].Color);
            }
            $.unique(Color.sort());

            var DataIncidencia = []
            var DataSeveridad = []

            for (x in r) { 
                if (r[x].Monitoreo == "-1") {                  
                    DataIncidencia.push('NaN');     
                    DataSeveridad.push('NaN');
                } else {
                    DataIncidencia.push(r[x].PPIncidencia);
                    DataSeveridad.push(r[x].PPSeveridad);
                }
            }
                        
            var retorna;
            var ejeX = "";
            if (ReportType == 1) {
                ejeX = "SEMANA";
                var titulo = "Informe Incidencia";
                var ejeY = "% INCIDENCIA";
                retorna = {
                    type: 'line',
                    labels: Labels,
                    datasets: [
                        {
                            label: "Incidencia",
                            backgroundColor: Color[0],
                            borderColor: Color[0],
                            data: DataIncidencia,
                            fill: false,
                            // borderColor: "rgba(0,0,0,0.3)"
                        }
                    ],
                }
            }
            else {
                ejeX = "SEMANA";
                var titulo = "Informe Severidad";
                var ejeY = "% SEVERIDAD";
                retorna = {
                    labels: Labels,
                    datasets: [
                        {
                            label: "Severidad",
                            backgroundColor: "red",
                            data: DataSeveridad,
                            fill: false,
                            borderColor: "red"
                        }
                    ]
                }
            }
            
            DrawChartVersus(retorna, ejeX, ejeY, titulo, "myChart");
        }
    })
}

function ConsultaLineaVs(datos, ckGreenhouse) {

    datos.ReportType = 2;

    //if (ckGreenhouse === 0) {
    //    $("#CanvasMultiple").html("");
    //    $("#myChart").remove();
    //}
    //else {
    //    $("#CanvasMultiple").html("");
    //    $("#cargaCanvas").append('<canvas id="myChart"></canvas>');
    //}

    $("#CanvasMultiple").html("");

    var ejeX = "SEMANA";
    var titulo = "Informe Incidencia vs Severidad";
    $.ajax({
        DataType: "json",
        type: "post",
        url: "/IncidenceSeverity/DrawReportLines",
        data: datos,
        success: function (r) {

            //Armado de arr de bloques
            var Labels = [];
            var Color = [];
            for (i in r) {
                Labels.push(r[i].Semana);
                Color.push(r[i].Color);
            }

            //Armado de arr de semanas
            var Bloques = [];
            for (i in r) {
                Bloques.push(r[i].Bloque);
            }

            $.unique(Bloques.sort());
            $.unique(Labels.sort());
            $.unique(Color.sort());

            //if (ckGreenhouse === 0) {
            //    //Armado de multiples gráficas caso "Todas los bloques"
            //    var $html = "";
            //    for (b in Bloques) {
            //        $html += "<div class='col-sm-6'> " +
            //            "<div class='col-sm-12 text-center'> " +
            //            "<h5><b>Bloque " + Bloques[b] + "</b></h5>" +
            //            "</div> " +
            //            "<canvas id='canvas_" + b + "'></canvas>" +
            //            "</div> ";
            //    }

            //    $("#CanvasMultiple").html($html);
            //}
            //console.log(Bloques);

            //for (b in Bloques) {
                var DataIncidencia = [];
                var DataSeveridad = [];
                for (s in Labels) {
                    for (x in r) {
                        //if (r[x].Bloque === Bloques[b] && r[x].Semana === Labels[s]) {
                            if (r[x].Monitoreo == "-1") {
                                DataIncidencia.push('NaN');
                                DataSeveridad.push('NaN');
                            } else {
                                DataIncidencia.push(r[x].PPIncidencia);
                                DataSeveridad.push(r[x].PPSeveridad);
                            }
                        //}
                    }
                }
                var retorna = {
                    labels: Labels,
                    datasets: [
                        {
                            label: "Incidencia",
                            backgroundColor: "blue",
                            data: DataIncidencia,
                            fill: false,
                            borderColor: "blue"
                        },
                        {
                            label: "Severidad",
                            backgroundColor: "red",
                            data: DataSeveridad,
                            fill: false,
                            borderColor: "red"
                        },
                    ]
                }
                var ejeY = "PORCENTAJE";
                //var Div = "";
                //if (Bloques.length <= 1) {
                //    Div = "myChart";
                //}
                //else {
                //    Div = "canvas_" + b;
                //}
                DrawChartVersus(retorna, ejeX, ejeY, titulo, "myChart");
            //}
            
        }
    })
}

function random_rgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ', 1)';
}

function DrawChart(datos, ejeX, ejeY, titulo, Div) {
    var data = datos;
    var ctx = document.getElementById(Div);
    var myChart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: {
            "hover": {
                "animationDuration": 0,
                onHover: function () {
                    return false;
                }
            },
            legend: {
                "display": true
            },
            tooltips: {
                "enabled": true
            },
            legend: {
                onClick: (e) => e.stopPropagation()
            },
            "animation": {
                "hover": {
                    "animationDuration": 0
                },
                "duration": 1,
                "onComplete": function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;

                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textBaseline = 'bottom';
                    ctx.textAlign = 'rigth';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.save();
                            var random = Math.floor(Math.random() * 16);
                            ctx.translate(bar._model.x + 7, bar._model.y);
                            //ctx.rotate(-0.5 * Math.PI);
                            ctx.fillText(data, 0, 0);
                            ctx.restore();
                        });
                    });
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: 100,
                    },
                    scaleLabel: {
                        display: true,
                        labelString: ejeY
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: ejeX
                    }
                }],
            },
        }
    })
    //var myChart = new Chart(ctx, {
    //    type: 'bar',
    //    data: data,
    //    options: {
    //        "hover": {
    //            "animationDuration": 0,
    //            onHover: function () {
    //                return false;
    //            }
    //        },
    //        legend: {
    //            "display": false
    //        },
    //        tooltips: {
    //            "enabled": false
    //        },
    //        legend: {
    //            onClick: (e) => e.stopPropagation()
    //        },
    //        "animation": {
    //            "hover": {
    //                "animationDuration": 0
    //            },
    //            "duration": 1,
    //            "onComplete": function () {
    //                var chartInstance = this.chart,
    //                    ctx = chartInstance.ctx;

    //                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
    //                ctx.textBaseline = 'bottom';
    //                ctx.textAlign = 'rigth';

    //                this.data.datasets.forEach(function (dataset, i) {
    //                    var meta = chartInstance.controller.getDatasetMeta(i);
    //                    meta.data.forEach(function (bar, index) {
    //                        var data = dataset.data[index];
    //                        ctx.save();
    //                        var random = Math.floor(Math.random() * 16) + 5;
    //                        ctx.translate(bar._model.x + 7, bar._model.y - random);
    //                        ctx.rotate(-0.5 * Math.PI);
    //                        ctx.fillText(data, 0, 0);
    //                        ctx.restore();
    //                    });
    //                });
    //            }
    //        },
    //        scales: {
    //            yAxes: [{
    //                ticks: {
    //                    min: 0,
    //                    max: 100,
    //                },
    //                scaleLabel: {
    //                    display: true,
    //                    labelString: ejeY
    //                }
    //            }],
    //            xAxes: [{
    //                scaleLabel: {
    //                    display: true,
    //                    labelString: ejeX
    //                }
    //            }],
    //        },
    //    }
    //})
    var comp = $("#GreenhouseID option:selected").text();
    if (comp == "Seleccione" || comp == "Todos" || comp == "0")
        comp = ""
    else
        comp = "Bloque " + comp;

    $("#titulo").text(titulo + " " + comp);
    
}

function DrawChartVersus(datos, ejeX, ejeY, titulo, Div) {
    //Grafica de prueba
    var data = datos;

    var ctx = document.getElementById(Div);
    var myChart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: {
            "hover": {
                "animationDuration": 0,
                onHover: function () {
                    return false;
                }
            },
            legend: {
                "display": true
            },
            tooltips: {
                "enabled": true
            },
            legend: {
                onClick: (e) => e.stopPropagation()
            },
            "animation": {
                "hover": {
                    "animationDuration": 0
                },
                "duration": 1,
                "onComplete": function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;

                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textBaseline = 'bottom';
                    ctx.textAlign = 'rigth';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.save();
                            var random = Math.floor(Math.random() * 16);
                            ctx.translate(bar._model.x + 7, bar._model.y);
                            //ctx.rotate(-0.5 * Math.PI);
                            ctx.fillText(data, 0, 0);
                            ctx.restore();
                        });
                    });
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: 100,
                    },
                    scaleLabel: {
                        display: true,
                        labelString: ejeY
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: ejeX
                    }
                }],
            },
        }
    })

    var comp = $("#GreenhouseID option:selected").text();
    if (comp == "Seleccione" || comp == "Todos" || comp == "0")
        comp = "";
    else
        comp = "Bloque " + comp;

    $("#titulo").text(titulo + " " + comp);

}   