﻿jQuery(document).ready(function () {

  
});

function Search() {

    var InitialWeek = jQuery("#InitialWeek").val();
    var EndWeek = jQuery("#EndWeek").val();
    
    var parameters = { "prmSemanaINI": InitialWeek, "prmSemanaFIN": EndWeek};

    BlockScreen();
    jQuery.ajax({
        url: "/PhytosanityDinamicController/GetDynamic",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {           
            var renderers = $.extend(
                $.pivotUtilities.renderers,
                $.pivotUtilities.plotly_renderers,
                $.pivotUtilities.c3_renderers,
                $.pivotUtilities.export_renderers
            );
            $("#pivDynamic").pivotUI(data, {
                rows: ["UnidadNegocio", "BloquesxInv", "Producto", "CantProducto", "Plagas", "Umbral"],
                cols: ["Semana"],
                hiddenAttributes: [""],
                renderers: renderers,
                aggregatorName: "Sum",
                vals: ["CantBloques"],
                rendererName: "Table",
                hideTotals: true
            }, true);
      
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}
