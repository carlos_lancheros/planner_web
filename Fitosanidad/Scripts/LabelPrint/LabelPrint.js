﻿jQuery(document).ready(function () {

    jQuery("#ddlCentrosOperacion").change(function () {

        var CentrosOperacionID = jQuery("#ddlCentrosOperacion").find(":selected").val();
        var CentrosCostoID = 0;

        var values = { "CentrosOperacionID": CentrosOperacionID, "CentrosCostoID": CentrosCostoID }

        jQuery("#tableLabelPrint").empty()

        jQuery.ajax({
            url: "/LabelPrint/showCollaborator",
            data: JSON.stringify(values),
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                var table = jQuery('#tableLabelPrint').DataTable();
                table.destroy();
                jQuery('#tableLabelPrint').html(data);
                jQuery('#tableLabelPrint').DataTable();
            },
            error: function () {

            }
        });        
    });


    jQuery("#ddlCentrosCosto").change(function () {

        var CentrosOperacionID = jQuery("#ddlCentrosOperacion").find(":selected").val();
        var CentrosCostoID = jQuery("#ddlCentrosCosto").find(":selected").val();

        var values = { "CentrosOperacionID": CentrosOperacionID, "CentrosCostoID": CentrosCostoID }        

        jQuery("#tableLabelPrint").empty()

        jQuery.ajax({
            url: "/LabelPrint/showCollaborator",
            data: JSON.stringify(values),
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {                
                var table = jQuery('#tableLabelPrint').DataTable();
                table.destroy();
                jQuery('#tableLabelPrint').html(data);
                jQuery('#tableLabelPrint').DataTable();
            },
            error: function () {

            }
        });
        
    });

    jQuery('#SelectedAll').click(function () {
        if (!$(this).prop('checked')) {
            jQuery("#tableLabelPrint").find("input[type='checkbox']").each(function () {
                jQuery(this).prop('checked', false)
            });
        }
        else {
            jQuery("#tableLabelPrint").find("input[type='checkbox']").each(function () {
                jQuery(this).prop('checked', true)
            });
        }
    });

    jQuery('#tableLabelPrint').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , "columnDefs": [
            {
                "targets": [0, 1, 2, 3],
                //"targets": [0, 1, 2],
                "className": "text-left",
            },
            { className: "dt-right", "width": "5%", "targets": 0 }
            , { "width": "10%", "targets": 1 }
            , { "width": "45%", "targets": 2 }
            , { "width": "40%", "targets": 3 }
        ]
    });
});

function Imprimir() {
    var ArrCodes = [];
    jQuery("#tableLabelPrint").find("input[type='checkbox']").each(function () {
        if (jQuery(this).is(':checked')) {
            var g = jQuery(this).attr('id');
            ArrCodes.push(g);
        };
    });

    var ArrNames = [];
    jQuery("#tableLabelPrint").find("input[type='checkbox']").each(function () {
        if (jQuery(this).is(':checked')) {
            var valores = $(this).parents("tr").find("td")[3].innerHTML;
            ArrNames.push(valores);
        };
    });

    var ArrSurnames = [];
    jQuery("#tableLabelPrint").find("input[type='checkbox']").each(function () {
        if (jQuery(this).is(':checked')) {
            var valores = $(this).parents("tr").find("td")[2].innerHTML;
            ArrSurnames.push(valores);
        };
    });


    var Quantity = jQuery('#LabelQuantity').val();
    var Week = jQuery('#lstWeek').find(":selected").text();
    var Printer = jQuery('#lstPrinter').find(":selected").val();   
    //var NameEnable = 0;    
    var NameEnable = $('#NameEnable').prop('checked');    
    var Abrr = "";
   
    NameEnable = (NameEnable ? 1 : 0);

    var CentrosOperacion = jQuery("#ddlCentrosOperacion").find(":selected").text();
    if (CentrosOperacion.substring(0, 1) == 'P') {
         Abrr = CentrosOperacion.substring(0, 1) + "-" + CentrosOperacion.substring(12, 18);
    }
    else {
        Abrr = CentrosOperacion.substring(0, 7);
    }

    var a = Week.replace(' ', '');
    var a = a.replace(' ', '');
    var parameters = { "Quantity": Quantity, "Week": a, "ArrayCodes": ArrCodes, "ArrNames": ArrNames, "ArrSurnames": ArrSurnames, "LabelUnit": Abrr, "Printer": Printer, "NameEnable": NameEnable }


    jQuery.ajax({
        url: "/LabelPrint/GenerateCode",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);
            location = "/LabelPrint/Download/?nombre=" + data;
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(errorThrown);

            //UnlockScreen();
            //Notifications("error", errorThrown)
        },
    });
}