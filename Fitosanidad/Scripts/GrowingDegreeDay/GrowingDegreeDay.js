﻿jQuery(document).ready(function () {
    ChartDailyGdd();

    jQuery("#startDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#startDate').focus();
            }
        }
    });
    jQuery('#startDate').datepicker('setDate', 'today');
    jQuery('#startDate').attr("autocomplete", "off");

    jQuery("#endDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#endDate').focus();
            }
        }
    });
    jQuery('#endDate').datepicker('setDate', 'today');
    jQuery('#endDate').attr("autocomplete", "off");

    jQuery("#ddlBusinessUnit").change(function () {
        jQuery("#ddlGreenhouse").empty();
        jQuery("#ddlSensor").empty();

        var intBusinessUnitId = $("#ddlBusinessUnit").find(":selected").val();
        var values = { "intBusinessUnitId": intBusinessUnitId }
        jQuery.ajax({
            url: "/WeatherSensor/getGreenhouseList",
            data: JSON.stringify(values),
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {

                $.each(data, function (i, data) {
                    $("#ddlGreenhouse").append('<option value="'
                        + data.Value + '">'
                        + data.Text + '</option>');
                });

                greenHouselistOnChange();
            }
        });
    });

    $("#ddlGreenhouse").change(function () {
        greenHouselistOnChange();
    });

    jQuery('#tableGrowingDegreeDay').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , "columnDefs": [
            {
                "targets": [0],
                "visible": false,
                "searchable": false
            }
            , { "width": "40%", "className": "text-left","targets": 1 }
            , { "width": "10%", "className": "text-right","targets": 2 }
            , { "width": "30%", "className": "text-right","targets": 3 }
            , { "width": "30%", "className": "text-right","targets": 4 }
        ]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    jQuery("#btnExcel").click(function () {
        jQuery('<table>')
            .append(
            jQuery("#tableGrowingDegreeDay").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "GrowingDegreeDay"
            });
    });
});

function greenHouselistOnChange() {
    jQuery("#ddlSensor").empty();

    var intBusinessUnitId = jQuery("#ddlBusinessUnit").find(":selected").val();
    var intGreenhouseId = jQuery("#ddlGreenhouse").find(":selected").val();
    var values = { "intBusinessUnitId": intBusinessUnitId, "intGreenhouseId": intGreenhouseId };
    jQuery.ajax({
        url: "/WeatherSensor/getSensorList",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.each(data, function (i, data) {
                $("#ddlSensor").append('<option value="'
                    + data.Value + '">'
                    + data.Text + '</option>');
            });
        }
    });
}

function Search() {
    if ($("#startDate").val() == '') {
        Notifications("error", GetResource("DebeSeleccionarFechaInicial"))
        $('#startDate').focus();
        return false;
    } else {
        if ($("#startDate").val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarFechaInicial"))
            $('#startDate').focus();
            return false;
        };
    }

    if ($("#endDate").val() == '') {
        Notifications("error", GetResource("DebeSeleccionarFechaFinal"))
        $('#endDate').focus();
        return false;
    } else {
        if ($("#endDate").val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarFechaFinal"))
            $('#endDate').focus();
            return false;
        };
    }

    var intBusinessUnitId = jQuery("#ddlBusinessUnit").find(":selected").val();
    var intGreenhouseId = jQuery("#ddlGreenhouse").find(":selected").val();
    var strDate = $("#startDate").val().split("-")
    var startDate = new Date(strDate[2], strDate[1] - 1, strDate[0])
    var strDate = $("#endDate").val().split("-")
    var endDate = new Date(strDate[2], strDate[1] - 1, strDate[0])
    var values = { "intBusinessUnitId": intBusinessUnitId, "intGreenhouseId": intGreenhouseId, "startDate": startDate, "endDate": endDate }

    BlockScreen();
    jQuery.ajax({
        url: "/GrowingDegreeDay/getGrowingDegreeDaysTable",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblSensor = jQuery('#tableGrowingDegreeDay').DataTable();
            tblSensor.clear().draw();

            $.each(data, function (i, data) {
                var row = tblSensor.row.add([
                    data.GDegreeDayID,
                    jsonDateTimeFormat24(data.Date),
                    data.Week,
                    data.Gdd,
                    data.Temp,
                ]).node().id = "rowId" + i;
            });
            tblSensor.draw(false);

            ChartDailyGdd();

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });

};

function ChartDailyGdd() {
    $("#chartDailyGdd").empty();
    $("#chartDailyGdd").html("");
    $("#chartDailyGdd").remove();
    $("#canvasDailyGdd").append('<canvas class="my-4" id="chartDailyGdd"></canvas>');

    var xGda = new Array();
    var yGda = new Array();

    var intBusinessUnitId = jQuery("#ddlBusinessUnit").find(":selected").val();
    var intGreenhouseId = jQuery("#ddlGreenhouse").find(":selected").val();
    var strDate = $("#startDate").val().split("-")
    var startDate = new Date(strDate[2], strDate[1] - 1, strDate[0])
    var strDate = $("#endDate").val().split("-")
    var endDate = new Date(strDate[2], strDate[1] - 1, strDate[0])
    var parameters = { "intBusinessUnitId": intBusinessUnitId, "intGreenhouseId": intGreenhouseId, "startDate": startDate, "endDate": endDate }

    BlockScreen();
    jQuery.ajax({
        url: "/GrowingDegreeDay/getGrowingDegreeDaysGraphic",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                xGda.push(jsonDateTimeFormat24(data[i].Date));
                yGda.push(data[i].Gdd);
            }

            var chartDegrees = document.getElementById("chartDailyGdd");
            var chartDailyGdd = new Chart(chartDegrees, {
                type: 'bar',
                data: {
                    labels: xGda,
                    datasets: [{
                        label: "Acumulación grados por día",
                        data: yGda,
                        lineTension: 0,
                        fill: true,
                        backgroundColor: 'rgba(220, 20, 60, 0.4)',
                        borderColor: '#DC143C',
                        borderWidth: 1,
                        pointBorderColor: "white",
                        pointBackgroundColor: '#DC143C'
                    }]
                },
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                beginAtZero: true,
                                stacked: true,
                                //display: false,
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                stacked: true,
                            }
                        }]
                    },
                    legend: {
                        display: true,
                    },
                    //animation: {
                    //    onComplete: function () {
                    //        var chartInstance = this.chart;
                    //        var ctx = chartInstance.ctx;
                    //        console.log(chartInstance);
                    //        var height = chartInstance.controller.boxes[0].bottom;
                    //        ctx.textAlign = "center";
                    //        Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                    //            var meta = chartInstance.controller.getDatasetMeta(i);
                    //            Chart.helpers.each(meta.data.forEach(function (bar, index) {
                    //                ctx.fillText(dataset.data[index], bar._model.x, height - ((height - bar._model.y) / 2) - 10);
                    //            }), this)
                    //        }), this);
                    //    }
                    //}
                },
            });

            chartDailyGdd.update();

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            var response = xhr.responseText;
            if (response.Message) {
                Notifications("error", response.Message);
            } else {
                Notifications("error", errorThrown);
            };
        }
    });

    BlockScreen();
    jQuery.ajax({
        url: "/GrowingDegreeDay/getGrowingDegreeDaysTotal",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (jQuery.type(data) == 'number') {
                $("label[for='totalGdd']").text(data + "°C");
            } else {
                $("label[for='totalGdd']").text("°C");
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            var response = xhr.responseText;
            if (response.Message) {
                Notifications("error", response.Message);
            } else {
                Notifications("error", errorThrown);
            };
        }
    });
}
