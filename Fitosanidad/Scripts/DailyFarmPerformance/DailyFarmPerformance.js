﻿jQuery(document).ready(function () {
    jQuery("#startDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#startDate').focus();
            }
        }
    });
    jQuery('#startDate').datepicker('setDate', 'today');
    jQuery('#startDate').attr("autocomplete", "off");

    jQuery("#endDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#startDate').focus();
            }
        }
    });
    jQuery('#endDate').datepicker('setDate', 'today');
    jQuery('#endDate').attr("autocomplete", "off");



    jQuery("#dataExport").click(function () {
        jQuery('<table>')
            .append(
                jQuery("#tableDailyFarmPerformance").DataTable().$('tr').clone()
            )
            .table2excel({
                headings: true,
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "ReporteCorteDiario"
            });
    });






    $("#tableDailyFarmPerformance").DataTable();

    var total = 0;
    $("#tableDailyFarmPerformance tbody tr").find("td:eq(6)").each(function () {
        valor = $(this).html();
        total += parseInt(valor)
    });
    jQuery("#Total").html(total);




});

function Search() {

    //var Fecha = jQuery("#startDate").val();
    var strDate = $("#startDate").val().split("-")
    var srtDateIni = new Date(strDate[2], strDate[1] - 1, strDate[0])
    var strDate = $("#endDate").val().split("-")
    var srtDateFin = new Date(strDate[2], strDate[1] - 1, strDate[0])

    var CentroOperacionID = jQuery("#lstCentrosOperacion").find(":selected").val();

    var values = { "CentroOperacionID": CentroOperacionID, "FechaIni": srtDateIni, "FechaFin": srtDateFin }

    BlockScreen();
    jQuery.ajax({
        url: "/DailyFarmPerformance/DailyFarmPerformance",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            var table = jQuery('#tableDailyFarmPerformance').DataTable();
            table.destroy();
            jQuery('#tableDailyFarmPerformance').html(data);
            jQuery('#tableDailyFarmPerformance').DataTable();


            var total = 0;
            $("#tableDailyFarmPerformance tbody tr").find("td:eq(6)").each(function () {
                valor = $(this).html();
                total += parseInt(valor)
            });
            jQuery("#Total").html(total);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });
}
