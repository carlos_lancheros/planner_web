﻿jQuery(document).ready(function () {
    chartTemp();

    jQuery("#startDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#startDate').focus();
            }
        }
    });
    jQuery('#startDate').datepicker('setDate', 'today');
    jQuery('#startDate').attr("autocomplete", "off");

    jQuery("#endDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#endDate').focus();
            }
        }
    });
    jQuery('#endDate').datepicker('setDate', 'today');
    jQuery('#endDate').attr("autocomplete", "off");

    jQuery("#ddlBusinessUnit").change(function () {
        jQuery("#ddlGreenhouse").empty();
        jQuery("#ddlSensor").empty();

        var intBusinessUnitId = $("#ddlBusinessUnit").find(":selected").val();
        var values = { "intBusinessUnitId": intBusinessUnitId }
        jQuery.ajax({
            url: "/WeatherSensor/getGreenhouseList",
            data: JSON.stringify(values),
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {

                $.each(data, function (i, data) {
                    $("#ddlGreenhouse").append('<option value="'
                        + data.Value + '">'
                        + data.Text + '</option>');
                });

                greenHouselistOnChange();
            }
        });
    });

    $("#ddlGreenhouse").change(function () {
        greenHouselistOnChange();
    });
       
    jQuery('#tableSensor').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , "columnDefs": [
            {
                "targets": [3],
                "className": "text-right",
            },
            { className: "dt-right", "width": "15%", "targets": 0 }
            , { "width": "40%", "targets": 1 }
            , { "width": "30%", "targets": 2 }
            , { "width": "15%", "targets": 3 }
        ]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    jQuery("#btnExcel").click(function () {
        jQuery('<table>')
            .append(
                jQuery("#tableSensor").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Sensor_Data"
            });
    });
});

function greenHouselistOnChange() {
    jQuery("#ddlSensor").empty();

    var intBusinessUnitId = jQuery("#ddlBusinessUnit").find(":selected").val();
    var intGreenhouseId = jQuery("#ddlGreenhouse").find(":selected").val();
    var values = { "intBusinessUnitId": intBusinessUnitId, "intGreenhouseId": intGreenhouseId };
    jQuery.ajax({
        url: "/WeatherSensor/getSensorList",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.each(data, function (i, data) {
                $("#ddlSensor").append('<option value="'
                    + data.Value + '">'
                    + data.Text + '</option>');
            });
        }
    });
}

function Search() {
    if ($("#startDate").val() == '') {
        Notifications("error", GetResource("DebeSeleccionarFechaInicial"))
        $('#startDate').focus();
        return false;
    } else {
        if ($("#startDate").val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarFechaInicial"))
            $('#startDate').focus();
            return false;
        };
    }

    if ($("#endDate").val() == '') {
        Notifications("error", GetResource("DebeSeleccionarFechaFinal"))
        $('#endDate').focus();
        return false;
    } else {
        if ($("#endDate").val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarFechaFinal"))
            $('#endDate').focus();
            return false;
        };
    }

    var intBusinessUnitId = jQuery("#ddlBusinessUnit").find(":selected").val();
    var intGreenhouseId = jQuery("#ddlGreenhouse").find(":selected").val();
    var intSensorId = jQuery("#ddlSensor").find(":selected").val();

    var strDate = $("#startDate").val().split("-")
    var startDate = new Date(strDate[2], strDate[1] - 1, strDate[0])
    var strDate = $("#endDate").val().split("-")
    var endDate = new Date(strDate[2], strDate[1] - 1, strDate[0])

    var values = { "intBusinessUnitId": intBusinessUnitId, "intGreenhouseId": intGreenhouseId, "intSensorId": intSensorId, "startDate": startDate, "endDate": endDate }

    BlockScreen();
    jQuery.ajax({
        url: "/WeatherSensor/getSensorData",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblSensor = jQuery('#tableSensor').DataTable();
            tblSensor.clear().draw();

            $.each(data, function (i, data) {
                var row = tblSensor.row.add([
                    data.DeviceId,
                    jsonDateTimeFormat(data.Time),
                    data.Value,
                    data.Data,
                ]).node().id = "rowId" + i;
            });
            tblSensor.draw(false);

            chartTemp();

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });

};

function chartTemp() {
    $("#chartWeatherSensor").empty();
    $("#chartWeatherSensor").html("");
    $("#chartWeatherSensor").remove();
    $("#canvasDay").append('<canvas class="my-4" id="chartWeatherSensor"></canvas>');

    var xHumeDay = new Array();
    var yHumeDay = new Array();

    var xTempDay = new Array();
    var yTempDay = new Array();

    var xLumiDay = new Array();
    var yLumiDay = new Array();

    var intBusinessUnitId = jQuery("#ddlBusinessUnit").find(":selected").val();
    var intGreenhouseId = jQuery("#ddlGreenhouse").find(":selected").val();
    var intSensorId = jQuery("#ddlSensor").find(":selected").val();

    if ($("#startDate").val() == '') {
        var startDate = new Date()
    } else {
        var strDate = $("#startDate").val().split("-")
        var startDate = new Date(strDate[2], strDate[1] - 1, strDate[0])
    }

    if ($("#endDate").val() == '') {
        var endDate = new Date()
    } else {
        var strDate = $("#endDate").val().split("-")
        var endDate = new Date(strDate[2], strDate[1] - 1, strDate[0])
    }

    var parameters = { "intBusinessUnitId": intBusinessUnitId, "intGreenhouseId": intGreenhouseId, "intSensorId": intSensorId, "startDate": startDate, "endDate": endDate }

    BlockScreen();
    jQuery.ajax({
        url: "/WeatherSensor/getSensorData",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].Value == 'RHUM') {
                    xHumeDay.push(jsonDateTimeFormat(data[i].Time));
                    yHumeDay.push(data[i].Data);
                }
                if (data[i].Value == 'TEMP') {
                    xTempDay.push(jsonDateTimeFormat(data[i].Time));
                    yTempDay.push(data[i].Data);
                }
                if (data[i].Value == 'LUM ') {
                    xLumiDay.push(jsonDateTimeFormat(data[i].Time));
                    yLumiDay.push(data[i].Data);
                }
            }

            var chartWeather = document.getElementById("chartWeatherSensor");
            var chartWeatherSensor = new Chart(chartWeather, {
                type: 'line',
                data: {
                    labels: (xTempDay.length > 0 ? xTempDay : xLumiDay),
                    datasets: [
                        {
                            label: "Temperatura",
                            data: yTempDay,
                            lineTension: 0,
                            fill: true,
                            backgroundColor: 'rgba(220, 20, 60, 0.4)',
                            borderColor: '#DC143C',
                            borderWidth: 1,
                            pointBorderColor: "white",
                            pointBackgroundColor: '#DC143C'
                        },
                        {
                            label: "Humedad Relativa",
                            data: yHumeDay,
                            lineTension: 0,
                            fill: true,
                            backgroundColor: "rgba(0, 123, 255, 0.4)",
                            borderColor: '#007bff',
                            borderWidth: 1,
                            borderJoinStyle: 'miter',
                            pointBorderColor: "white",
                            pointBackgroundColor: '#007bff',
                        }, 
                        {
                            label: "Luminosidad",
                            data: yLumiDay,
                            lineTension: 0,
                            fill: true,
                            backgroundColor: 'rgba(254, 248, 0, 0.4)',
                            borderColor: '#FEF800',
                            borderWidth: 1,
                            pointBorderColor: "white",
                            pointBackgroundColor: '#FEF800'
                        }
                    ]
                },
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                beginAtZero: true,
                                stacked: true,
                                //display: false,
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                stacked: true,
                            }
                        }]
                    },
                    legend: {
                        display: true,
                    }
                },
            });

            chartWeatherSensor.update();

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });
}
