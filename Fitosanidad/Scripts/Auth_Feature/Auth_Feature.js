﻿var tree

$(document).ready(function () {
    GetAuth_Feature();
    CleanForm();

    $('#OptionFlagActive').bootstrapToggle({
        on: 'Activo',
        off: 'Inactivo',
        size: 'mini'
    });

});

function GetAuth_Feature() {
    tree = $('#treeAuth_Feature').tree({
        uiLibrary: 'materialdesign',
        checkboxes: false,
        cascadeCheck: false,
        dataSource: "/Auth_Feature/GetAllAuth_Feature",
        primaryKey: 'id',
        select: function (e, node, id) {
            GetFeatureByFeatureID(id);
        }
    });
};

function GetFeatureByFeatureID(id) {
    var _FeatureID = id;
    var parameters = { "_FeatureID": _FeatureID };

    BlockScreen();
    jQuery.ajax({
        url: "/Auth_Feature/GetParentDataByFeatureID",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.FeatureID > 0) {
                $('#ParentId').val(data.FeatureID);
                $('#ParentName').val(data.Name);
            } else {
                $('#ParentId').val('');
                $('#ParentName').val('');
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });

    BlockScreen();
    jQuery.ajax({
        url: "/Auth_Feature/GetFeatureByFeatureID",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#OptionId').val(data.FeatureID);
            $('#OptionName').val(data.Name);
            $('#OptionController').val(data.Controller);
            $('#OptionAction').val(data.Action);
            $('#OptionIcon').val(data.Icon);
            $('#OptionFlagActive').prop('checked', (data.FlagActive == 1 ? true : false) );

            $("#icoOptionIcon").attr('Class', '')
            $("#icoOptionIcon").attr('Class', data.Icon)

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
};

function CallSubmit() {
    var FeatureID = $('#OptionId').val();
    var ParentFeatureID = $('#ParentId').val();
    var Name = $('#OptionName').val();
    var Controller = $('#OptionController').val();
    var Action = $('#OptionAction').val();
    var Icon = $('#OptionIcon').val();
    var FlagActive = $("#OptionFlagActive").is(":checked");

    if (Name == null) {
        Notifications("error", GetResource("DebeDigitarDescripcion"))
        jQuery('#OptionName').focus();
        return false;
    } else {
        if (Name.length == 0) {
            Notifications("error", GetResource("DebeDigitarDescripcion"))
            jQuery('#OptionName').focus();
            return false;
        };
    }

    var parameters = { "FeatureID": FeatureID, "ParentFeatureID": ParentFeatureID, "Name": Name, "Controller": Controller, "Action": Action, "Icon": Icon, "FlagActive": FlagActive };

    BlockScreen();
    jQuery.ajax({
        url: "/Auth_Feature/Auth_Feature_SaveChanges",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            tree.reload();
            CleanForm();

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
};

function AddChild() {
    var FeatureID = $('#OptionId').val();
    var ParentFeatureID = $('#ParentId').val();
    var Name = $('#OptionName').val();
    var Controller = $('#OptionController').val();
    var Action = $('#OptionAction').val();
    var Icon = $('#OptionIcon').val();
    var FlagActive = $("#OptionFlagActive").is(":checked");

    if (FeatureID == null) {
        Notifications("error", GetResource("DebeSeleccionarOpcionPadre"))
        jQuery('#OptionName').focus();
        return false;
    } else {
        if (FeatureID.length == 0) {
            Notifications("error", GetResource("DebeSeleccionarOpcionPadre"))
            jQuery('#OptionName').focus();
            return false;
        };
    }

    if (Name == null) {
        Notifications("error", GetResource("DebeDigitarDescripcion"))
        jQuery('#OptionName').focus();
        return false;
    } else {
        if (Name.length == 0) {
            Notifications("error", GetResource("DebeDigitarDescripcion"))
            jQuery('#OptionName').focus();
            return false;
        };
    }

    if (Controller.length > 0) {
        Notifications("error", GetResource("DebeQuitarControlador"))
        jQuery('#OptionController').focus();
        return false;
    };

    if (Action.length > 0) {
        Notifications("error", GetResource("DebeQuitarAccion"))
        jQuery('#OptionAction').focus();
        return false;
    };

    $('#ParentId').val(FeatureID)
    $('#ParentName').val(Name)

    $('#OptionId').val('');
    $('#OptionName').val('');
    $('#OptionController').val('');
    $('#OptionAction').val('');
    $('#OptionIcon').val('');
    $('#OptionFlagActive').prop('checked', false);

    jQuery('#OptionName').focus();
};

function CleanForm() {
    $('#ParentId').val('')
    $('#ParentName').val('')

    $('#OptionId').val('');
    $('#OptionName').val('');
    $('#OptionController').val('');
    $('#OptionAction').val('');
    $('#OptionIcon').val('');

    $("#icoOptionIcon").attr('Class', '')

    $('#OptionFlagActive').prop('checked', false);

    jQuery('#OptionName').focus();
};

function Delete() {
    var FeatureID = $('#OptionId').val();
    var ParentFeatureID = $('#ParentId').val();
    var Name = $('#OptionName').val();
    var Controller = $('#OptionController').val();
    var Action = $('#OptionAction').val();
    var Icon = $('#OptionIcon').val();
    var FlagActive = $("#OptionFlagActive").is(":checked");

    var booIndDel = true;

    if (FeatureID == null) {
        Notifications("error", GetResource("DebeSeleccionarOpcionABorrar"))
        jQuery('#OptionName').focus();
        return false;
    } else {
        if (FeatureID.length == 0) {
            Notifications("error", GetResource("DebeSeleccionarOpcionABorrar"))
            jQuery('#OptionName').focus();
            return false;
        };
    }

    var parameters = { "_FeatureID": FeatureID };

    BlockScreen();
    jQuery.ajax({
        url: "/Auth_Feature/DelAuth_FeatureByFeatureID",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        async: false,
        success: function (data) {
            if (data.length > 0) {
                Notifications("error", GetResource("NoSePuedeBorrarLaOpcionSeleccionadaVerifiqueQueNoTieneSubOpcionesOQueHayUsuariosQueLaEstanUtilizando"));
                UnlockScreen();
                booIndDel = false;
                return false;
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
};
