﻿$(document).ready(function () {

    $("#Name").on("change", function () {
        creaUser();
    });

    $("#LastName").on("change", function () {
        creaUser();
    });

    $("#CreateUserForm").on("submit", function (e) {
        e.preventDefault;
        CreateUser();
        return false;
    })
})

function creaUser() {
    var Name = $("#Name").val();
    var LastName = $("#LastName").val();
    if (!Name || !LastName) {
        return false;
    }

    var sub = Name.substr(0, 1);
    var last = LastName.split(" ");
    var User = sub + last[0];
    $("#Login").val(User.toLowerCase());
}

function CreateUser() {
    var Login = $("#Login").val();
    if (!Login) {
        Notifications("error", "Se produjo un error, intentelo nuevamente!");
        return false;
    }

    var Name = $("#Name").val();
    var LastName = $("#LastName").val();
    var Email = $("#Email").val();
    var DocID = $("#DocID").val();

    var datos = {
        "Login": Login,
        "Name": Name,
        "LastName": LastName,
        "Email": Email,
        "DocID": DocID,
    };

    $.ajax({
        DataType: "json",
        type: "POST",
        url: "/UserAdministration/CreateUserInsert",
        data: datos,
        success: function (r) {
            if (r.Error == 0) {
                Notifications("success", r.Mensaje);
                $("#CreateUserForm")[0].reset();
            }
            else {
                Notifications("error", r.Mensaje);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    })

}

function BackToList() {
    location = "/UserAdministration/Index";
}