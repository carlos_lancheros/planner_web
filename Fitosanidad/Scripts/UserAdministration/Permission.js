﻿$(document).ready(function () {
    JsonGetAllAuth_UserCompanyByUserID();
    JsonGetAuth_UserBusinessUnitByCompanyID();
    JsonGetAllAuth_UserFarmByUserID();

    $('#FlagActive').bootstrapToggle({
        on: GetResource("Activo"),
        off: GetResource("Inactivo"),
        size: 'mini'
    });

    var tblAuth_UserCompany = jQuery('#tableAuth_UserCompany').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , destroy: true
        , orderCellsTop: true
        , fixedHeader: true
        , "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
        , "columnDefs": [
            { "width": "10%", "targets": 0 }
            , { "width": "20%", "targets": 1 }
            , { "width": "40%", "targets": 2 }
            , { "width": "20%", "targets": 3 }
        ]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    var tblAuth_UserBusinessUnit = jQuery('#tableAuth_UserBusinessUnit').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , destroy: true
        , orderCellsTop: true
        , fixedHeader: true
        , "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
        , "columnDefs": [
            { "width": "10%", "targets": 0 }
            , { "width": "20%", "targets": 1 }
            , { "width": "30%", "targets": 2 }
            , { "width": "20%", "targets": 3 }
        ]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    var tblAuth_UserFarm = jQuery('#tableAuth_UserFarm').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , destroy: true
        , orderCellsTop: true
        , fixedHeader: true
        , "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
        , "columnDefs": [
            { "width": "10%", "targets": 0 }
            , { "width": "20%", "targets": 1 }
            , { "width": "30%", "targets": 2 }
            , { "width": "20%", "targets": 3 }
        ]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    $("#btnCreateCompany").click(function () {
        CreateCompanyRow();
    });

    $("#btnCreateBusinessUnit").click(function () {
        CreateBusinessUnitRow();
    });

    $("#btnCreateFarm").click(function () {
        CreateFarmRow();
    });

    $("#ddlCompanias").change(function () {
        $("#ddlAuth_UserCompany").val($("#ddlCompanias").val());
        $("#ddlAuth_UserCompany").trigger("change");
    });

    $("#ddlAuth_UserCompany").change(function () {
        GetAllBusinessUnitsByCompanyId($("#ddlAuth_UserCompany").val());
        JsonGetAuth_UserBusinessUnitByCompanyID();
    });
})

function CreateCompanyRow() {
    var UserID = getParameterByName("UserID");
    var CompanyID = $("#ddlCompanias").val();

    var parameters = { "UserID": UserID, "CompanyID": CompanyID };

    BlockScreen();
    jQuery.ajax({
        url: "/UserAdministration/PostAuth_UserCompany",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length > 0) {
                Notifications("error", data);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });

    JsonGetAllAuth_UserCompanyByUserID();
    JsonGetAuth_UserBusinessUnitByCompanyID();
    JsonGetAllAuth_UserFarmByUserID();
};

function CreateBusinessUnitRow() {
    var UserID = getParameterByName("UserID");
    var BusinessUnitID = $("#ddlBusinessUnits").val();

    var parameters = { "UserID": UserID, "BusinessUnitID": BusinessUnitID };

    BlockScreen();
    jQuery.ajax({
        url: "/UserAdministration/PostAuth_UserBusinessUnit",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length > 0) {
                Notifications("error", data);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });

    JsonGetAllAuth_UserCompanyByUserID();
    JsonGetAuth_UserBusinessUnitByCompanyID();
    JsonGetAllAuth_UserFarmByUserID();
};

function CreateFarmRow() {
    var UserID = getParameterByName("UserID");
    var FarmID = $("#ddlFincas").val();

    var parameters = { "UserID": UserID, "FarmID": FarmID };

    BlockScreen();
    jQuery.ajax({
        url: "/UserAdministration/PostAuth_UserFarm",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length > 0) {
                Notifications("error", data);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });

    JsonGetAllAuth_UserCompanyByUserID();
    JsonGetAuth_UserBusinessUnitByCompanyID();
    JsonGetAllAuth_UserFarmByUserID();
};

function DeactivateCompanyRow(UserCompanyID) {
    var parameters = { "UserCompanyID": UserCompanyID };

    BlockScreen();
    jQuery.ajax({
        url: "/UserAdministration/DeactivateCompanyRow",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length > 0) {
                Notifications("error", data);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });

    JsonGetAllAuth_UserCompanyByUserID();
    JsonGetAuth_UserBusinessUnitByCompanyID();
    JsonGetAllAuth_UserFarmByUserID();
};

function JsonGetAllAuth_UserCompanyByUserID() {
    var UserID = getParameterByName("UserID");

    var parameters = { "UserID": UserID};

    BlockScreen();
    jQuery.ajax({
        url: "/UserAdministration/JsonGetAllAuth_UserCompanyByUserID",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $("#ddlAuth_UserCompany").empty();
            $.each(data, function (i, data) {
                $("#ddlAuth_UserCompany").append('<option value="'
                    + data.CompanyID + '">'
                    + data.Nombre + '</option>');
            });
            JsonGetAuth_UserBusinessUnitByCompanyID();

            var tblAuth_UserCompany = jQuery('#tableAuth_UserCompany').DataTable();
            tblAuth_UserCompany.clear().draw();

            $.each(data, function (i, data) {
                tblAuth_UserCompany.row.add([
                    data.UserCompanyID,
                    data.Codigo,
                    data.Nombre,
                    '<input type="checkbox"' + (data.FlagActive ? ' checked="checked"' : '') + ' onClick="DeactivateCompanyRow(' + data.UserCompanyID + ')">',
                ]).draw();
            });
            tblAuth_UserCompany.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#ddlBusinessUnit")).text());
        },
        async: false
    });
};

function JsonGetAllAuth_UserFarmByUserID() {
    var UserID = getParameterByName("UserID");

    var parameters = { "UserID": UserID};

    BlockScreen();
    jQuery.ajax({
        url: "/UserAdministration/JsonGetAllAuth_UserFarmByUserID",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblAuth_UserFarm = jQuery('#tableAuth_UserFarm').DataTable();
            tblAuth_UserFarm.clear().draw();

            $.each(data, function (i, data) {
                tblAuth_UserFarm.row.add([
                    data.UserFarmID,
                    data.Nombre,
                    data.Abbr,
                    '<input type="checkbox"' + (data.FlagActive ? ' checked="checked"' : '') + ' onClick="DeactivateFarmRow(' + data.UserFarmID + ')">',
                ]).draw();
            });
            tblAuth_UserFarm.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#ddlBusinessUnit")).text());
        },
        async: false
    });
};

function GetAllAuth_UserFarmByUserID(CompanyId) {
    if (CompanyId == null) {
        return false;
    }

    $("#ddlBusinessUnits").empty();

    var parameters = { "CompanyId": CompanyId };

    BlockScreen();
    jQuery.ajax({
        url: "/UserAdministration/JsonGetAllBusinessUnitsByCompanyId",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.each(data, function (i, data) {
                $("#ddlBusinessUnits").append('<option value="'
                    + data.ID + '">'
                    + data.Nombre + '</option>');
            });

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#ddlBusinessUnit")).text());
        },
        async: false
    });
}

function JsonGetAuth_UserBusinessUnitByCompanyID() {
    var UserID = getParameterByName("UserID");
    var CompanyID = $("#ddlAuth_UserCompany").val();

    if (CompanyID == null) {
        return false;
    }

    var parameters = { "UserID": UserID, "CompanyID": CompanyID };

    BlockScreen();
    jQuery.ajax({
        url: "/UserAdministration/JsonGetAuth_UserBusinessUnitByCompanyID",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tblAuth_UserBusinessUnit = jQuery('#tableAuth_UserBusinessUnit').DataTable();
            tblAuth_UserBusinessUnit.clear().draw();

            $.each(data, function (i, data) {
                tblAuth_UserBusinessUnit.row.add([
                    data.UserBusinessUnitID,
                    data.CompanyName,
                    data.BusinessUnitName,
                    '<input type="checkbox"' + (data.FlagActive ? ' checked="checked"' : '') + ' onClick="DeactivateBusinessUnitRow(' + data.UserBusinessUnitID + ')">',
                ]).draw();
            });
            tblAuth_UserBusinessUnit.draw(false);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#ddlBusinessUnit")).text());
        },
        async: false
    });
};

function DeactivateBusinessUnitRow(UserBusinessUnitID) {
    var parameters = { "UserBusinessUnitID": UserBusinessUnitID };

    BlockScreen();
    jQuery.ajax({
        url: "/UserAdministration/DeactivateBusinessUnitRow",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length > 0) {
                Notifications("error", data);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });

    JsonGetAllAuth_UserCompanyByUserID();
    JsonGetAuth_UserBusinessUnitByCompanyID();
    JsonGetAllAuth_UserFarmByUserID();
};

function DeactivateFarmRow(UserFarmID) {
    var parameters = { "UserFarmID": UserFarmID };

    BlockScreen();
    jQuery.ajax({
        url: "/UserAdministration/DeactivateFarmRow",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length > 0) {
                Notifications("error", data);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });

    JsonGetAllAuth_UserCompanyByUserID();
    JsonGetAuth_UserBusinessUnitByCompanyID();
    JsonGetAllAuth_UserFarmByUserID();
};

