﻿$("document").ready(function () {
    var today = new Date().toDateInputValue();
    $("#FechaInicial").val(today);
    $("#FechaFinal").val(today);
})

Date.prototype.toDateInputValue = (function () {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0, 10);
});

function TraeData() {
    BlockScreen();
    var GreenHouseID = $("#GreenHouseID").val();
    var FechaInicial = $("#FechaInicial").val();
    var FechaFinal = $("#FechaFinal").val();

        //if (!GreenHouseID) {
        //    Notifications("error", "Debe seleccionar finca, Verifique!");
        //    return;
        //}

    if (FechaInicial > FechaFinal) {
        Notifications("error", "Fecha inicial no debe ser mayor a la fina, Verifique!");
        return;
    }

    var datos = {
        "GreenHouseID": GreenHouseID,
        "FechaInicial": FechaInicial,
        "FechaFinal": FechaFinal,
    }

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/ErradicationReport/TraeData",
        success: function (r) {
            var renderers = $.extend(
                $.pivotUtilities.renderers,
                $.pivotUtilities.plotly_renderers,
                $.pivotUtilities.d3_renderers,
                $.pivotUtilities.export_renderers
            );

            $("#PivotData").pivotUI(r, {
                cols: ["SemErradicacion"],
                rows: ["Bloque", "Cama", "Variedad", "NoPlanSembradas"],
                hiddenAttributes: ["id"],
                renderers: renderers,
                aggregatorName: "Sum",
                vals: ["PlantasErradicadas"],
                rendererName: "Table",
            }, true);
            UnlockScreen();
        }
    });
}

function exportTableToExcel(tableID, filename = '') {
    $(".pvtTable").attr("id", "pvtTable");
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    filename = filename ? filename + '.xls' : 'InformeDinamicoErradicaciones.xls';
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob(blob, filename);
    } else {

        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

        downloadLink.download = filename;

        downloadLink.click();
    }
}