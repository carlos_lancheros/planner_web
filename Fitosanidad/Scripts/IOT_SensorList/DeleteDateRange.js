﻿var intCounter

jQuery(document).ready(function () {
    jQuery("#startDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())){
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#startDate').focus();
            }
        }
    });
    jQuery('#startDate').datepicker('setDate', 'today');
    jQuery('#startDate').attr("autocomplete", "off");

    jQuery("#endDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-mm-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#endDate').focus();
            }
        }
    });
    jQuery('#endDate').datepicker('setDate', 'today');
    jQuery('#endDate').attr("autocomplete", "off");

    jQuery('#tableSensorDataList').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , "columnDefs": [
            {
                "targets": [1, 2, 3],
                "className": "text-right",
            },
            { className: "dt-right", "width": "25%", "targets": 0 }
            , { "width": "25%", "targets": 1 }
            , { "width": "25%", "targets": 2 }
            , { "width": "25%", "targets": 3 }
        ]
    });
});

function BackToList() {
    var Url = "/IOT_SensorList";
    window.location.href = Url;
};

function Search() {
    if ($("#startDate").val() == '') {
        Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
        $('#startDate').focus();
        return false;
    } else {
        if ($("#startDate").val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
            $('#startDate').focus();
            return false;
        };
    }

    if ($("#endDate").val() == '') {
        Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
        $('#endDate').focus();
        return false;
    } else {
        if ($("#endDate").val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
            $('#endDate').focus();
            return false;
        };
    }

    var BusinessUnitId = $("#BusinessUnitID").val();
    var GreenHouseID = $("#GreenHouseID").val();
    var DeviceId = $("#ExternalID").val();

    var strDate = $("#startDate").val().split("-")
    var StartDate = new Date(strDate[2], strDate[1] - 1, strDate[0])
    var strDate = $("#endDate").val().split("-")
    var EndDate = new Date(strDate[2], strDate[1] - 1, strDate[0])

    var parameters = { "BusinessUnitId": BusinessUnitId, "GreenHouseID": GreenHouseID, "DeviceId": DeviceId, "StartDate": StartDate, "EndDate": EndDate };

    BlockScreen();
    $.ajaxSetup({ cache: false });
    $.ajax({
        url: "/IOT_SensorList/GetDeleteDateRange",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        async: false,
        success: function (data) {
            var tblSensorDataList = jQuery('#tableSensorDataList').DataTable();

            intCounter = 1;

            $.each(data, function (i, data) {
                if (data.DeviceId != $("#ExternalID").val()) {
                    UnlockScreen();
                    Notifications("error", GetResource("LaInformacionACargarNoCorrespondeAlSensorSeleccionado"))
                    return false;
                }

                tblSensorDataList.row.add([
                    data.Time,
                    data.Value,
                    data.Data,
                    data.Sensor,
                ]).node().id = "rowId" + intCounter;
            });

            tblSensorDataList.draw(false);

            intCounter++;

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
        dataType: "Json",
        async: false
    });
};

function Delete() {
    if ($("#startDate").val() == '') {
        Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
        $('#startDate').focus();
        return false;
    } else {
        if ($("#startDate").val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
            $('#startDate').focus();
            return false;
        };
    }

    if ($("#endDate").val() == '') {
        Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
        $('#endDate').focus();
        return false;
    } else {
        if ($("#endDate").val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
            $('#endDate').focus();
            return false;
        };
    }

    var BusinessUnitId = $("#BusinessUnitID").val();
    var GreenHouseID = $("#GreenHouseID").val();
    var DeviceId = $("#ExternalID").val();
    var strDate = $("#startDate").val().split("-")
    var StartDate = new Date(strDate[2], strDate[1] - 1, strDate[0])
    var strDate = $("#endDate").val().split("-")
    var EndDate = new Date(strDate[2], strDate[1] - 1, strDate[0])

    var parameters = { "BusinessUnitId": BusinessUnitId, "GreenHouseID": GreenHouseID, "DeviceId": DeviceId, "StartDate": StartDate, "EndDate": EndDate };

    BlockScreen();
    $.ajaxSetup({ cache: false });
    $.ajax({
        url: "/IOT_SensorList/SetDeleteDateRange",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        async: false,
        success: function (data) {
            var tblSensorDataList = jQuery('#tableSensorDataList').DataTable();

            var rows = tblSensorDataList
                .rows()
                .remove()
                .draw();
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
        dataType: "Json",
        async: false
    });
};
