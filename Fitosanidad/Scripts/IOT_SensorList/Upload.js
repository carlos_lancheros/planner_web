﻿jQuery(document).ready(function () {
    jQuery('#tableSensorDataList').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        ,"columnDefs": [
            {
                "targets": [0, 2, 3, 4],
                "className": "text-right",
            },
            { className: "dt-right", "width": "20%", "targets": 0 }
            , { "width": "20%", "targets": 1 }
            , { "width": "20%", "targets": 2 }
            , { "width": "20%", "targets": 3 }
            , { "width": "20%", "targets": 4 }
        ]
    });
});

function BackToList() {
    var Url = "/IOT_SensorList";
    window.location.href = Url;
};

function Upload() {
    if ($("#UploadFile").val() == '') {
        Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
        $('#UploadFile').focus();
        return false;
    } else {
        if ($("#UploadFile").val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
            $('#UploadFile').focus();
            return false;
        };
    }

    var file = $('input[type="file"]').val();
    var exts = ['csv'];
    if (file) {
        var get_ext = file.split('.');
        get_ext = get_ext.reverse();
        if ($.inArray(get_ext[0].toLowerCase(), exts) > -1) {
            Notifications("warning", GetResource("CargandoInformacionDesdeElArchivo"))
        } else {
            Notifications("error", GetResource("TipoDeArchivoNoValido"))
            $('#UploadFile').focus();
            return false;
        }
    }

    var data = new FormData();
    var files = $("#UploadFile").get(0).files;

    if (files.length > 0) {
        data.append("HelpSectionImages", files[0]);
    }
    else {
        Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
        $('#UploadFile').focus();
        return false;
    }
    var extension = $("#UploadFile").val().split('.').pop().toUpperCase();
    if (extension != "CSV") {
        Notifications("error", GetResource("TipoDeArchivoNoValido"))
        $('#UploadFile').focus();
        return false;
    }

    $.ajaxSetup({ cache: false });
    BlockScreen();
    $.ajax({
        type: "POST",
        processData: false,
        url: "/IOT_SensorList/UploadFromFile",
        data: data,
        dataType: "Json",
        contentType: false,
        success: function (data) {
            var tblSensorDataList = jQuery('#tableSensorDataList').DataTable();

            $.each(data, function (i, data) {
                if (data.SensorID != $("#ExternalID").val()) {
                    UnlockScreen();
                    Notifications("error", GetResource("LaInformacionACargarNoCorrespondeAlSensorSeleccionado"))
                    return false;
                }

                var add = tblSensorDataList.row.add([
                    data.SensorID,
                    data.Date,
                    data.Humidity,
                    data.Temperature,
                    data.Luminosity,
                ]).node().id = "rowId" + i;
            });

            tblSensorDataList.draw(false);

            $('input[type="file"]').val("");

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
        dataType: "Json",
        async: false
    });
};

function Save() {
    if (!jQuery.fn.DataTable.isDataTable('#tableSensorDataList')) {
        Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
        return false;
    }
    var table = jQuery('#tableSensorDataList').DataTable();

    if (!table.data().any()) {
        Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
        return false;
    }

    BlockScreen();

    intBusinessUnitID = parseInt($("#ddlBusinessUnit").find(":selected").val());
    intGreenHouseId = parseInt($("#ddlGreenhouse").find(":selected").val());
    strSensor = $("#UploadFile").val();

    var dd = table.rows().data().toArray();
    var data = [];

    $.each(dd, function (index, value) {
        if (value[2].length > 0) {
            //data.push(new Object());  
            //data[intCounter]["DeviceId"] = value[0];
            //data[intCounter]["Time"] = value[1];
            //data[intCounter]["Sensor"] = "iMonit";  
            //data[intCounter]["Value"] = "RHUM";
            //data[intCounter]["Data"] = value[2];
            //intCounter++;

            var sensor = {};
            sensor.DeviceId = value[0];
            sensor.Value = "RHUM";
            sensor.Data = value[2];
            sensor.BusinessUnitId = intBusinessUnitID;
            sensor.GreenHouseID = intGreenHouseId;
            sensor.Time = value[1];
            sensor.Sensor = "iMonit";
            data.push(sensor);
        }
        if (value[3].length > 0) {
            //data.push(new Object());
            //data[intCounter]["DeviceId"] = value[0];
            //data[intCounter]["Time"] = value[1];
            //data[intCounter]["Sensor"] = "iMonit";
            //data[intCounter]["Value"] = "TEMP";
            //data[intCounter]["Data"] = value[2];
            //intCounter++;

            var sensor = {};
            sensor.DeviceId = value[0];
            sensor.Value = "TEMP";
            sensor.Data = value[3];
            sensor.BusinessUnitId = intBusinessUnitID;
            sensor.GreenHouseID = intGreenHouseId;
            sensor.Time = value[1];
            sensor.Sensor = "iMonit";
            data.push(sensor);
        }
        if (value[4].length > 0) {
            //data.push(new Object());
            //data[intCounter]["DeviceId"] = value[0];
            //data[intCounter]["Time"] = value[1];
            //data[intCounter]["Sensor"] = "iMonit";
            //data[intCounter]["Value"] = "LUM";
            //data[intCounter]["Data"] = value[2];
            //intCounter++;

            var sensor = {};
            sensor.DeviceId = value[0];
            sensor.Value = "RHUM";
            sensor.Data = value[4];
            sensor.BusinessUnitId = intBusinessUnitID;
            sensor.GreenHouseID = intGreenHouseId;
            sensor.Time = value[1];
            sensor.Sensor = "iMonit";
            data.push(sensor);
        }
    });

    var parameters = { "IOT_SensorData": JSON.stringify(data) };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/IOT_SensorList/UploadFromFileSave",
        data: JSON.stringify(parameters),
        dataType: "JSON",
        success: function (data) {
            var tblSensorDataList = jQuery('#tableSensorDataList').DataTable();

            var rows = tblSensorDataList
                .rows()
                .remove()
                .draw();

            UnlockScreen();
            Notifications("success", GetResource("InformacionCargadaCorrectamente"));
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        },
    });
   
};