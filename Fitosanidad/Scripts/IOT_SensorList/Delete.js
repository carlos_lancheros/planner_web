﻿jQuery(document).ready(function () {
    jQuery("#ddlGreenhouse").empty();
    jQuery("#ddlGreenhouse").append(jQuery("<option></option>").val('0').html('EXTERIOR - Cód. 0'));
});

function BackToList() {
    var Url = "/IOT_SensorList";
    window.location.href = Url;
}

function CallSubmit() {
    if (jQuery("#Name").val() == "") {
        jQuery('#Name').focus();
        return false;
    }
    if (jQuery("#Service").val() == "") {
        jQuery('#Service').focus();
        return false;
    }
    if (jQuery("#ExternalID").val() == "") {
        jQuery('#ExternalID').focus();
        return false;
    }

    var IOT_SensorListID = jQuery("#IOT_SensorListID").val();
    var businessUnitId = jQuery("#ddlBusinessUnit").find(":selected").val();
    var greenHouseId = jQuery("#ddlGreenhouse").find(":selected").val();
    var name = jQuery("#Name").val();
    var service = jQuery("#Service").val();
    var flagActive = jQuery("#FlagActive").is(":checked");
    var externalId = jQuery("#ExternalID").val();
    var CRUD = "D";
    var values = { "IOT_SensorListID": IOT_SensorListID, "businessUnitId": businessUnitId, "greenHouseId": greenHouseId, "name": name, "service": service, "flagActive": flagActive, "externalId": externalId, "CRUD": CRUD };

    BlockScreen();
    jQuery.ajax({
        url: "/IOT_SensorList/IOT_SensorList_SaveChanges",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery("#Name").val("");
            jQuery("#Service").val("");
            jQuery("#ExternalID").val("");
            jQuery("#FlagActive").attr('checked', false);

            UnlockScreen();
            BackToList();
        },
        error: function () {
            UnlockScreen();
        }
    });
};