﻿$(document).ajaxStart(function () {
    BlockScreen();
});

$(document).ajaxStop(function () {
    UnlockScreen();
});

$(document).ready(function () {

    bloqueaGuardado();

    $("#Company").change(function () {
        var Id = this.value;
        var datos = {
            "Id": Id,
        }
        $.ajax({
            type: "POST",
            data: datos,
            DataType: "json",
            url: "/AspersionPlan/FiltroFincas?IdCompany=" + parseInt(Id),
            contentType: 'application/json; charset=utf-8',
            success: function (r) {
                var html = "<option></option>";
                for (x in r) {
                    html += "<option value='" + r[x].Value + "'>" + r[x].Text + "</option>";
                }
                $("#GreenHouseID").html(html);
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
    });

    $("#GreenHouseID").change(function () {
        traeID();
        pintaBloque();
    });

    $("#LiterPerBed").keypress(function (event) {
        return soloNumerosDecimal(event, this);
    });

    $("#PreparationOrder").keypress(function (event) {
        return soloNumeros(event);
    });

    $("#BedNumber").keypress(function (event) {
        return soloNumeros(event);
    });

    $("#AsperjadorNumber").keypress(function (event) {
        return soloNumeros(event);
    });

    $("#TankVolume").keypress(function (event) {
        return soloNumeros(event);
    });

    $("#CapacityJar").keypress(function (event) {
        return soloNumerosDecimal(event, this);
    });

    traeID();

    TablaMaestra();

    //Funcion guardado general
    $("#form_AspersionPlan").submit(function (e) {
        e.preventDefault;
        var WeekID = $("#WeekID").val();
        var GreenHouseID = $("#GreenHouseID").val();

        var datos = {
            "WeekID": WeekID,
            "GreenHouseID": GreenHouseID,
        };
        $.ajax({
            type: "POST",
            DataType: "json",
            data: JSON.stringify(datos),
            url: "/AspersionPlan/AspersionPlanAddHead",
            cache: false,
            contentType: 'application/json; charset=utf-8',
            success: function (r) {
                if (r === "existe") {
                    Notifications("warning", "Ya existe un plan para finca y semana, verifique!");
                    return false;
                }
                else
                {
                    //$("#form_AspersionPlanDetail").show();
                    //$("#AspersionPlanID").val(r);
                    location = "/AspersionPlan/AspersionPlanEditV2/" + r;
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(errorThrown);
                
            },
        });

        return false;
    });

    //Funcion guardado detalle
    $("#form_AspersionPlanDetail").submit(function (e) {
        e.preventDefault;
        var AspersionPlanID = $("#AspersionPlanID").val();
        var ProductID = $("#ProductID").val();
        var Block = $("#Block").val();
        var DayNumber = $("#DayNumber").val();
        var ApplyTypesID = $("#ApplyTypesID").val();
        var FormID = $("#FormID").val();
        var PreparationOrder = $("#PreparationOrder").val();
        var AgrochemicalProductsID = $("#AgrochemicalProductsID").val();
        var BedNumber = $("#BedNumber").val();
        var LiterPerBed = $("#LiterPerBed").val();
        var ReferenceSpear = $("#ReferenceSpear").val();
        var AsperjadorNumber = $("#AsperjadorNumber").val();
        var Observation = $("#Observation").val();
        var TankVolume = $("#TankVolume").val();
        var CapacityJar = $("#CapacityJar").val();
        var ActiveFlag = true;

        LiterPerBed = parseFloat(LiterPerBed);
        TankVolume = parseFloat(TankVolume);
        CapacityJar = parseFloat(CapacityJar);

        if (AspersionPlanID === "0") {
            Notifications("error", "Debe crear el plan, no existe, Verifique!");
            return false;
        }

        var datos = {
            "AspersionPlanID": AspersionPlanID,
            "ProductID": ProductID,
            "Block": Block,
            "DayNumber": DayNumber,
            "ApplyTypesID": ApplyTypesID,
            "FormID": FormID,
            "PreparationOrder": PreparationOrder,
            "AgrochemicalProductsID": AgrochemicalProductsID,
            "BedNumber": BedNumber,
            "LiterPerBed": LiterPerBed,
            "ReferenceSpear": ReferenceSpear,
            "AsperjadorNumber": AsperjadorNumber,
            "Observation": Observation,
            "TankVolume": TankVolume,
            "CapacityJar": CapacityJar,
            "ActiveFlag": ActiveFlag,
        }

        $.ajax({
            type: "POST",
            DataType: "json",
            data: JSON.stringify(datos),
            url: "/AspersionPlan/AspersionPlanAddDetail",
            cache: false,
            contentType: 'application/json; charset=utf-8',
            success: function (r) {
                if(r === "Ok")
                    Notifications("success", "Registro insertado correctamente");
                TablaMaestra();
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });

        return false;
    });

    pintaBloque();

});

function TablaMaestra() {
    var AspersionPlanID = $("#AspersionPlanID").val();
    var datos = {
        "AspersionPlanID": AspersionPlanID,
    }
    $.ajax({
        type: "POST",
        DataType: "json",
        data: JSON.stringify(datos),
        url: "/AspersionPlan/AspersionPlanListDetail",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            $("#AspersionPlanListDetail").html(r);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
}

function CambiaEstado(estado, id) {
    var ActiveFlag = null;
    if (estado === 0)
        ActiveFlag = true;
    else
        ActiveFlag = false;
    var datos = {
        "ActiveFlag": ActiveFlag,
        "AspersionPlanID": id,
    };
    $.ajax({
        type: "POST",
        DataType: "json",
        data: JSON.stringify(datos),
        url: "/AspersionPlan/CambiaEstado",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            //$("#DetalleMaestro").html(r);
            TablaMaestra();

        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
}

function pintaBloque()
{
    var Id = $("#GreenHouseID").val();

    var datos = {
        "Id": Id,
    }
    $.ajax({
        type: "POST",
        data: datos,
        DataType: "json",
        url: "/AspersionPlan/FiltroBloques?IdCompany=" + parseInt(Id),
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            var html = "";
            for (x in r) {
                html += "<option value='" + r[x].Value + "'>" + r[x].Text + "</option>";
            }
            $("#Block").html(html);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
    
}

function traeID()
{
    var WeekID = $("#WeekID").val();
    var GreenHouseID = $("#GreenHouseID").val();
    if (!WeekID || !GreenHouseID) {
        bloqueaGuardado();
        return false;
    }

    var datos = {
        "WeekID": WeekID,
        "GreenHouseID": GreenHouseID,
    };

    $.ajax({
        type: "POST",
        data: JSON.stringify(datos),
        DataType: "json",
        url: "/AspersionPlan/TraeID",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            console.log(r);
            if (r.Ctrl == 3) {
                //$("#GreenHouseID").val("");
                desbloqueaGuardado();
                //Notifications("warning", "Plan de aspersión ya creado, por favor verifique!");
                return false;
            }

            if(r.Ctrl == 1)
            {
                desbloqueaGuardado();
                $("#AspersionPlanID").val(r);
                TablaMaestra();
            }
            if (r.Ctrl == 2)
            {
                $("#form_AspersionPlanDetail").show();
                desbloqueaGuardado();
                $("#AspersionPlanID").val(r);
                TablaMaestra();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
}

function bloqueaGuardado() {
    $("#bt_AspersionPlanAdd").attr("disabled", "disabled");
    $("#bt_AspersionPlanDetailAdd").attr("disabled", "disabled");
    $("#form_AspersionPlanDetail").find("input, select, textarea").each(function () {
        $(this).attr("disabled", "disabled");
    });
}

function desbloqueaGuardado() { 
    $("#bt_AspersionPlanAdd").removeAttr("disabled");
    $("#bt_AspersionPlanDetailAdd").removeAttr("disabled");
    $("#form_AspersionPlanDetail").find("input, select, textarea").each(function () {
        $(this).removeAttr("disabled");
    });
}

function BackToList() {
    location = "/AspersionPlan/";
}