﻿$(document).ready(function () {

    $("#AspersionEmployeesForm").on("submit", function (e) {
        e.preventDefault();

        Create();
        return false;
    })

    $("#ShowListForm").on("submit", function (e) {
        e.preventDefault();
        GetList();
        return false;
    })

    $("#AspersionEmployeesList").DataTable();
    $("#ShowListTable").DataTable();

    $("#RolID").on("change", function () {
        if ($(this).val() == 1 || $(this).val() == 0) {
            $("#AspersionEmployeesID").attr("disabled", "disabled");
            $("#AspersionEmployeesID").val("");
        }
        else {
            $("#AspersionEmployeesID").removeAttr("disabled");
            $("#AspersionEmployeesID").val("");
        }
    });
});

function CambiaEstado(AspersionEmployeesID) {
    var datos = {
        "AspersionEmployeesID": AspersionEmployeesID,
    };

    $.ajax({
        DataType: "json",
        type: "POST",
        data: datos,
        url: "/AspersionEmployees/CambiaEstado/",
        success: function (r) {
            console.log(r);
            Notifications("success", r.Mensaje);
            var table = $("#AspersionEmployeesList").DataTable();

            var Tabla = r.Tabla;
            console.log(Tabla)
            var datos = [];
            for (i in Tabla) {
                var d = [];
                if (Tabla[i]['ActiveFlag'] == true) {
                    var Estado = "<span class='text-success text-bold'>Activo</span>";
                    var ico = "fa fa-toggle-on text-success";
                }
                else {
                    var Estado = "<span class='text-danger text-bold'>Inactivo</span>";
                    var ico = "fa fa-toggle-off text-danger";
                }
                d.push(Tabla[i]['AspersionEmployeesID']);
                d.push(Tabla[i]['Name']);
                d.push(Tabla[i]['LastName']);
                d.push(Tabla[i]['RolID']);
                d.push(Tabla[i]['SupervisorID']);
                d.push(Tabla[i]['GreenHouseID']);
                d.push(Estado);
                d.push("<button class='btn btn-xs' type='button' data-toggle='tooltip' data-placement='top' title='' onclick=\"location='/AspersionEmployees/Edit?AspersionEmployeesID=" + Tabla[i]['AspersionEmployeesID'] + "'\" data-original-title='Editar Usuario'><i class='fa fa-edit text-primary'></i></button>&nbsp;<button class='btn btn-xs' data-toggle='tooltip' data-placement='top' title='' type='button' onclick='CambiaEstado(" + Tabla[i]['AspersionEmployeesID'] + ")' data-original-title='Inactivar Usuario'><i class='" + ico + "'></i></button>");
                datos.push(d);
            }
            var tmp = datos.map(function (item) {
                return $.map(item, function (element, key) { return element == null ? "" : element });
            });

            table.clear();
            console.log(datos);
            table.rows.add(tmp).draw();
        }
    })
}

function Create() {
    var Name = $("#Name").val();
    var LastName = $("#LastName").val();
    var RolID = $("#RolID").val();
    var AspersionEmployeesID = $("#AspersionEmployeesID").val();
    var GreenHouseID = $("#GreenHouseID").val();
    var ActiveFlag = $("#ActiveFlag").val();


    var datos = {
        "Name": Name,
        "LastName": LastName,
        "RolID": RolID,
        "SupervisorID": AspersionEmployeesID,
        "GreenHouseID": GreenHouseID,
        "ActiveFlag": true,
    }

    $.ajax({
        DataType: "json",
        type: "post",
        url: "/AspersionEmployees/Create",
        data: datos,
        success: function (r) {
            Notifications("success", r.Mensaje);
            var table = $("#AspersionEmployeesList").DataTable();

            $("#AspersionEmployeesForm")[0].reset();

            var Tabla = r.Tabla;
            var datos = [];
            for (i in Tabla) {
                var d = [];
                if (Tabla[i]['ActiveFlag'] == true) {
                    var Estado = "<span class='text-success text-bold'>Activo</span>";
                    var ico = "fa fa-toggle-on text-success";
                }
                else {
                    var Estado = "<span class='text-danger text-bold'>Inactivo</span>";
                    var ico = "fa fa-toggle-off text-danger";
                }
                d.push(Tabla[i]['AspersionEmployeesID']);
                d.push(Tabla[i]['Name']);
                d.push(Tabla[i]['LastName']);
                d.push(Tabla[i]['RolID']);
                d.push(Tabla[i]['SupervisorID']);
                d.push(Tabla[i]['GreenHouseID']);
                d.push(Estado);
                d.push("<button class='btn btn-xs' type='button' data-toggle='tooltip' data-placement='top' title='' onclick=\"location='/AspersionEmployees/Edit?AspersionEmployeesID=" + Tabla[i]['AspersionEmployeesID'] + "'\" data-original-title='Editar Usuario'><i class='fa fa-edit text-primary'></i></button>&nbsp;<button class='btn btn-xs' data-toggle='tooltip' data-placement='top' title='' type='button' onclick='CambiaEstado(" + Tabla[i]['AspersionEmployeesID']+")' data-original-title='Inactivar Usuario'><i class='"+ico+"'></i></button>");
                datos.push(d);
            }
            var tmp = datos.map(function (item) {
                return $.map(item, function (element, key) { return element == null ? "" : element });
            });

            table.clear();
            console.log(datos);
            table.rows.add(tmp).draw();
        }
    })
}

function BackToList() {
    location = "/AspersionEmployees";
}

function ShowList() {
    location = "/AspersionEmployees/ShowList";
}

function GetList() {
    var GreenHouseID = $("#GreenHouseID").val();

    var datos = {
        "GreenHouseID": GreenHouseID,
    }

    $.ajax({
        DataType: "json",
        type: "post",
        data: datos,
        url: "/AspersionEmployees/GetList",
        success: function (r) {
            var table = $("#ShowListTable").DataTable();
            var datos = [];
            for (i in r) {
                var d = [];
                if (r[i]['ActiveFlag'] == true) {
                    var Estado = "<span class='text-success text-bold'>Activo</span>";
                }
                else {
                    var Estado = "<span class='text-danger text-bold'>Inactivo</span>";
                }
                d.push(r[i]['Name']);
                d.push(r[i]['LastName']);
                d.push(r[i]['RolID']);
                d.push(Estado);
                datos.push(d);
            }
            var tmp = datos.map(function (item) {
                return $.map(item, function (element, key) { return element == null ? "" : element });
            });

            table.clear();
            table.rows.add(tmp).draw();

        }
    })
}