﻿//$(document).ajaxStart(function () {
//    BlockScreen();
//});

//$(document).ajaxStop(function () {
//    UnlockScreen();
//BlockScreen();
//});

$(document).ready(function () {
    traeID();

    pintaBloque();

    TablaMaestra();

    //TraeNumeroCamas();

    //TraeIdCamasSeleccionadas();

    $("#form_AspersionPlanDetail").submit(function (e) {
        e.preventDefault;
        var contador = 0;
        var arr = [];
        $("#PPC_div").find("select").each(function (i) {
            if (this.value) {
                contador++;
                arr.push(this.value);
            }
        });

        if (contador == 0) {
            Notifications("error", "Debe seleccionar al menos un PPC verifique!");
            location = "#divNotifications";
            return false;
        }

        var idBloqueSeleccionado = $("#idBloqueSeleccionado").val();

        console.log(idBloqueSeleccionado);

        if (!idBloqueSeleccionado) {
            Notifications("error", "Debe seleccionar al menos un bloque, Verifique");
            location = "#divNotifications";
            return false;
        }

        //guardadoGeneral(JSON.stringify(arr));
        guardadoGeneral(arr);

        return false;
    });

    $("#LiterPerBed").keypress(function (event) {
        return soloNumerosDecimal(event, this);
    });

    $("#PreparationOrder").keypress(function (event) {
        return soloNumeros(event);
    });

    $("#BedNumber").keypress(function (event) {
        return soloNumeros(event);
    });

    $("#AsperjadorNumber").keypress(function (event) {
        return soloNumeros(event);
    });

    $("#TankVolume").keypress(function (event) {
        return soloNumeros(event);
    });

    $("#CapacityJar").keypress(function (event) {
        return soloNumerosDecimal(event, this);
    });

    $("#ApplyTypesID").on("change", function () {
        FiltraPPCPorTipo(this.value);
    });

    $("#Block").on("change", function () {
        var BlockId = $("#Block").val();
        if (!BlockId) {
            $("#BedNumber").val("");
            $("#idBloqueSeleccionado").val("");
            return false;
        }
        else {
            $("#idBloqueSeleccionado").val(BlockId);
        }

        var datos = {
            "BlockId": BlockId,
        };
        $.ajax({
            DataType: "JSON",
            type: "post",
            url: "/AspersionPlan/TraeTotalCamas",
            data: datos,
            success: function (r) {
                $("#BedNumber").val(r);
            }
        })
    });

    $("#PlagasID").change(function () {
        PintaTablaIncidencia();
    })

    $("#PlanType").on("change", function () {
        TypoDePlan();
    })

    //var table = $("#TablaIncidencia").DataTable({
    //    "ordering": false,
    //    "bDestroy": true,
    //});

});

function TablaMaestra() {
    var AspersionPlanID = $("#AspersionPlanID").val();
    var datos = {
        "AspersionPlanID": AspersionPlanID,
    }
    $.ajax({
        type: "POST",
        DataType: "json",
        data: JSON.stringify(datos),
        url: "/AspersionPlan/AspersionPlanListDetail",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            console.log(r);
            $("#AspersionPlanListDetail").html(r);
            table.dataTable({
                "columnDefs": [
                    {
                        "targets": [16],
                        "visible": false,
                        "searchable": false
                    }
                ],
                "bDestroy": true,
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });

}

function BackToList() {
    location = "/AspersionPlan/";
}

function pintaBloque() {
    var Id = $("#GreenHouseID").val();
    var datos = {
        "Id": Id,
    }
    $.ajax({
        type: "POST",
        data: datos,
        DataType: "json",
        url: "/AspersionPlan/FiltroBloques?IdCompany=" + parseInt(Id),
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            var html = "<option value=''>Seleccione</option>";
            for (x in r) {
                html += "<option value='" + r[x].Value + "'>" + r[x].Text + "</option>";
            }
            $("#Block").html(html);
            $("#Block").html(html);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });

}

function traeID() {
    var WeekID = $("#WeekID").val();
    var GreenHouseID = $("#GreenHouseID").val();
    if (!WeekID || !GreenHouseID) {
        bloqueaGuardado();
        return false;
    }

    var datos = {
        "WeekID": WeekID,
        "GreenHouseID": GreenHouseID,
    };

    $.ajax({
        type: "POST",
        data: JSON.stringify(datos),
        DataType: "json",
        url: "/AspersionPlan/TraeID",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            if (r.Ctrl == 2) {
                $("#form_AspersionPlanDetail").show();
                $("#AspersionPlanID").val(r);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
}

function guardadoGeneral(AgrochemicalProductsID)
{
    if (!AgrochemicalProductsID) {
        console.log("No se envió producto agroquímico");
        return false;
    }
    var AspersionPlanID = $("#AspersionPlanID").val();
    var ProductID = $("#ProductID").val();
    var Block = $("#Block").val();
    var DayNumber = $("#DayNumber").val();
    var ApplyTypesID = $("#ApplyTypesID").val();
    var FormID = $("#FormID").val();
    var PreparationOrder = $("#PreparationOrder").val();
    var AspersionEmployeesID = $("#_AspersionEmployeesID").val();
    var BedNumber = $("#BedNumber").val();
    var LiterPerBed = $("#LiterPerBed").val();
    var ReferenceSpear = $("#ReferenceSpear").val();
    var AsperjadorNumber = $("#AsperjadorNumber").val();
    var Observation = $("#Observation").val();
    var TankVolume = $("#TankVolume").val();
    var CapacityJar = $("#CapacityJar").val();
    var ThirdID = $("#ThirdID").val();
    var DirectionID = $("#DirectionID").val();
    var ActiveFlag = true;

    var idBloqueSeleccionado = $("#idBloqueSeleccionado").val();
    var PlanType = $("#PlanType").val();

    if (!PlanType) {
        PlanType = 0;
    }

    LiterPerBed = parseFloat(LiterPerBed);
    TankVolume = parseFloat(TankVolume);

    if (AspersionPlanID === "0") {
        Notifications("error", "Debe crear el plan, no existe, Verifique!");
        return false;
    }

    var datos = {
        "AspersionPlanID": AspersionPlanID,
        "ProductID": ProductID,
        "Block": Block,
        "DayNumber": DayNumber,
        "ApplyTypesID": ApplyTypesID,
        "FormID": FormID,
        "PreparationOrder": PreparationOrder,
        "AgrochemicalProductsID": AgrochemicalProductsID,
        "BedNumber": BedNumber,
        "LiterPerBed": LiterPerBed,
        "ReferenceSpear": ReferenceSpear,
        "AsperjadorNumber": AsperjadorNumber,
        "Observation": Observation,
        "TankVolume": TankVolume,
        "CapacityJar": CapacityJar,
        "ActiveFlag": ActiveFlag,
        "d": AgrochemicalProductsID,
        "AspersionEmployeesID": AspersionEmployeesID,
        "ThirdID": ThirdID,
        "DirectionID": DirectionID,
        "idBloqueSeleccionado": idBloqueSeleccionado,
        "PlanType": PlanType,
    }

    $.ajax({
        type: "POST",
        DataType: "json",
        data: JSON.stringify(datos),
        url: "/AspersionPlan/AspersionPlanAddDetail",
        cache: false,
        contentType: 'application/json; charset=utf-8',
        async : false,
        success: function (r) {
            if (r === "Ok") {
                Notifications("success", "Registro insertado correctamente");
                $("#Block").val("");
                $("#BedNumber").val("");
                CambiaEstadoCamas();
                location = "#divNotifications";
            }

            if (r.Error == 1) {
                Notifications("error", r.Mensaje);
            }
            TablaMaestra();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
        complete: function () {
            TypoDePlan();
        },
    });
}

function ProductByBlock(BlockId) {
    if (!BlockId) {
        $("#ProductID").val("");
        return false;
    }
    $.ajax({
        dataType: "json",
        type: "post",
        url: "/AspersionPlan/ProductByBlock",
        data: { "BlockId": BlockId },
        success: function (r) {
            var html = "";
            for(x in r)
            {
                html += "<option value='" + r[x]['Value'] + "'>" + r[x]['Text'] + "</option>";
            }
            $("#ProductID").html(html);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    })
}

function FiltraPPCPorTipo(ApplyTypesID) {
    if (!ApplyTypesID) {
        return false;
    }
        
    $.ajax({
        DataType: "json",
        type: "post",
        url: "/AspersionPlan/FiltraPPCPorTipo",
        data: { "ApplyTypesID": ApplyTypesID },
        success: function (r) {
            $("#PPC_div").find("select").each(function (i) {
                var html = "<option></option>";
                for (x in r) {
                    html += "<option value = '" + r[x].Value + "'>" + r[x].Text + " </option>";
                }
                $(this).html(html);
            });
        }
    })
}

function CambiaEstado(estado, AspersionPlanDetailID) {
    //alert("Entro");
    //var html = "<input type='hidden' value='" + estado + "' id='estado'>" +
    //    "<input type='hidden' value='" + AspersionPlanDetailID + "' id='AspersionPlanDetailID'>" +
    //    "<span class='text-danger' id='LabelError'></span>" +
    //    "<textarea class = 'form-control' style='min-width:100%' id='Observacion'></textarea>"+
    //    "";
    //$("#modalMensajesTitulo").html("Observación de cambio de estado");
    //$("#modalMensajesCuerpo").html(html);
    //$("#modalMensajes").show();
}

function cambiaEstadoAcepta(estado, AspersionPlanDetailID) {
    var ActiveFlag = null;
    if (estado === "0")
        ActiveFlag = true;
    else
        ActiveFlag = false;
    var datos = {
        "ActiveFlag": ActiveFlag,
        "AspersionPlanDetailID": AspersionPlanDetailID,
    };

    $.ajax({
        type: "POST",
        DataType: "json",
        data: JSON.stringify(datos),
        url: "/AspersionPlan/CambiaEstadoDetail",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            location.reload();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
}

function GuardarObservacion() {
    var Observacion = $("#Observacion").val();
    var estado = $("#estado").val();
    var AspersionPlanDetailID = $("#AspersionPlanDetailID").val();
    if (Observacion.length == 0) {
        $("#LabelError").text("Observación es requerido!");
        return false;
    };
    console.log(AspersionPlanDetailID);
    datos = {
        "AspersionPlanDetailID": AspersionPlanDetailID,
        "Comment": Observacion,
        "ActiveFlagOld": estado,
    }

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/AspersionPlan/GuardarObservacion",
        success: function (r) {
            $("#modalMensajes").hide();
            cambiaEstadoAcepta(estado, AspersionPlanDetailID);
        }
    });

}

function EditarPlan() {
    var Id = $("#AspersionPlanID").val();
    location = "/AspersionPlan/EditarPlan/"+Id;
}

function PintaTablaIncidencia() {
    var Plagas = $("#PlagasID").val();
    var PlagasID = "";
    var ctrl = 0;
    for (x in Plagas) {
        if (Plagas[x] === "") {
            PlagasID = "";
            ctrl = 1;
        }
    }
    if (ctrl === 0) {
        PlagasID = $("#PlagasID").val().toString();
    }
    var WeekID = $("#WeekID").val();
    var GreenHouseID = $("#GreenHouseID").val();

    var datos = {
        "PlagasID": PlagasID,
        "WeekID": WeekID,
        "GreenHouseID": GreenHouseID,
    }

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/AspersionPlan/IncidenciaSeveridad",
        async: false,
        success: function (r) {
            if (r === "0") {
                Notifications("error", "No se encontó información, verifique!");
                return;
            }
            if($.fn.dataTable.isDataTable('#TablaIncidencia')) {
                table.destroy();
            }
            var html = "";
            for (x in r) {
                html += "<tr>" +
                    "<td><input type='checkbox' data-camas ='" + r[x]['Beds'] + "' data-block = \"" + r[x]['block'] + "\" id='check_" + r[x]['IDInvernaderos'] + "' value = '" + r[x]['IDInvernaderos'] + "' onchange='CreaCamas(this.id, this.value, \"" + r[x]['block'] + "\")'/>&nbsp;&nbsp;&nbsp;" + r[x]['block'] + "</td>" +
                    "<td class='text-right'>" + r[x]['Beds'] + "</td>" +
                    "<td class='text-right'>" + r[x]['CamasAfectadas'] + "</td>" +
                    "<td class='text-center' title='" + r[x]['Pest'] +"' style='font-weight:bolder; color: " + r[x]['ColorIdentification'] + "'><span>" + r[x]['Abrr'] + "<span></td>" +
                    "<td class='text-right'>" + r[x]['PorcentajeIncidencia'] + "%</td>" +
                    "<td class='text-right'>" + r[x]['PorcentajeSeveridad'] + "%</td>" +
                "</tr>";
            }
            $("#CuerpoTabla").html(html);

            if ($.fn.dataTable.isDataTable('#TablaIncidencia')) {
                table.destroy();
                table = $('#TablaIncidencia').DataTable({
                    "rowsGroup": [0,1],
                    "ordering": false,
                    "bDestroy": true,
                    "bFilter": false,
                }).draw();
            }
            else {
                table = $('#TablaIncidencia').DataTable({
                    "rowsGroup": [0,1],
                    "ordering": false,
                    "bDestroy": true,
                    "bFilter": false,
                }).draw();
            }
            
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
            Notifications("error", errorThrown)
        },
    })
}

function CreaCamas(id, value, Block) {

    //Validacion por camas o general
    var PlanType = $("#PlanType").val();
    if (PlanType === "2") {
        var Bloques = [];
        var BloquesId = [];

        $("#TablaIncidencia").find("input[type=checkbox]").each(function () {
            if ($(this).prop("checked") === true) {
                Bloques.push($(this).data("block"));
                BloquesId.push(this.value);
            }
        });

        Bloques = Bloques.toString();
        $("#BloqueSeleccionado").text(Bloques);

        BloquesId = BloquesId.toString();
        $("#idBloqueSeleccionado").val(BloquesId);

        console.log($("#idBloqueSeleccionado").val());
        return;
    }

    BlockScreen();

    //Valida que este checkeado
    //Si no está checkeado limpia todos los datos
    if ($("#" + id).prop("checked") === false) {
        $("#ArmaCamas").html("");
        $("#BloqueSeleccionado").html("");
        $("#NumeroCamasSeleccionadas").html("");
        $("#ProductosPorBloque").html("");
        $("#idBloqueSeleccionado").val("");
        $("#BedNumber").val("");
        UnlockScreen();
        return;
    }

    var pagina = table.page();
    $("#Block").val(value);
    table.destroy();
    $("#TablaIncidencia").find("input").each(function () {
        var inp = $(this).prop("id");
        if (inp != id) {
            $(this).prop("checked", false);
        }
    });
    table = $('#TablaIncidencia').DataTable({
        "rowsGroup": [0],
        "ordering": false,
        "bDestroy": true,
        "bFilter": false,
    });

    table.page(pagina).draw('page');

    var PlagasID = $("#PlagasID").val().toString();
    var WeekID = $("#WeekID").val();
    var GreenHouseID = $("#GreenHouseID").val();

    var datos = {
        "IDInvernaderos": value,
        "WeekID": WeekID,
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        async: false,
        url: "/AspersionPlan/TraeDatosCamas",
        success: function (r) {

            //Pinta Bloque Seleccionado :
            $("#BloqueSeleccionado").text(Block);

            //Coloca ID cama seleccionada
            $("#idBloqueSeleccionado").val(value);

            var cantCamas = r.CantCamas.length;
            var NombreCamas = r.CantCamas;
            var InfoBeds = r.InfoBeds;
            var html = "";
            for (var i = 0; i < cantCamas; i++) {
                var cont = i + 1;

                var Productos = "";
                var ProdAnt = "";
                var Inputs = "";

                var Plagas = "";
                var Plagas1 = "";
                var Plagas2 = "";
                var Plagas3 = "";
                var Plagas4 = "";
                var Plagas5 = "";
                var AbrrAnt = "";
                var AbrrAnt1 = "";
                var AbrrAnt2 = "";
                var AbrrAnt3 = "";
                var AbrrAnt4 = "";
                var AbrrAnt5 = "";

                var Color = "";

                //Valida qué productos hay por camas
                for (x in InfoBeds) {
                    if (InfoBeds[x].BedName == cont) {
                        if (InfoBeds[x].ProductName != ProdAnt) {
                            Productos += InfoBeds[x].ProductName + "  ";
                            ProdAnt = InfoBeds[x].ProductName;
                            Inputs += "<input type='hidden' id='product" + x + "' value='" + InfoBeds[x].IDProductos +"' />";
                        }
                    }
                }

                //Valida qué plagas hay por camas y por cuadros
                for (x in InfoBeds) {

                    if (InfoBeds[x].BedName == NombreCamas[i]['Nombre']) {

                        if (InfoBeds[x].Abrr != AbrrAnt) {
                            Plagas += "<span class='text-bold' title='" + InfoBeds[x].PestName + "' style='color: " + InfoBeds[x].ColorIdentification + "'>" + InfoBeds[x].Abrr + "</span>&nbsp;";
                            AbrrAnt = InfoBeds[x].Abrr;
                        }

                        //if (InfoBeds[x].Abrr != AbrrAnt1 && InfoBeds[x].NCuadro == 1) {
                        //    Plagas1 += "<span class='text-bold' title='" + InfoBeds[x].PestName + "' style='color: " + InfoBeds[x].ColorIdentification+"'>" + InfoBeds[x].Abrr + "</span><br>";
                        //    AbrrAnt1 = InfoBeds[x].Abrr;
                        //}

                        //if (InfoBeds[x].Abrr != AbrrAnt1 && InfoBeds[x].NCuadro == 2) {
                        //    Plagas2 += "<span class='text-bold' title='" + InfoBeds[x].PestName + "' style='color: " + InfoBeds[x].ColorIdentification + "'>" + InfoBeds[x].Abrr + "</span><br>";
                        //    AbrrAnt2 = InfoBeds[x].Abrr;
                        //}

                        //if (InfoBeds[x].Abrr != AbrrAnt1 && InfoBeds[x].NCuadro == 3) {
                        //    Plagas3 += "<span class='text-bold' title='" + InfoBeds[x].PestName + "' style='color: " + InfoBeds[x].ColorIdentification + "'>" + InfoBeds[x].Abrr + "</span><br>";
                        //    AbrrAnt3 = InfoBeds[x].Abrr;
                        //}

                        //if (InfoBeds[x].Abrr != AbrrAnt1 && InfoBeds[x].NCuadro == 4) {
                        //    Plagas4 += "<span class='text-bold' title='" + InfoBeds[x].PestName + "' style='color: " + InfoBeds[x].ColorIdentification + "'>" + InfoBeds[x].Abrr + "</span><br>";
                        //    AbrrAnt4 = InfoBeds[x].Abrr;
                        //}

                        //if (InfoBeds[x].Abrr != AbrrAnt1 && InfoBeds[x].NCuadro == 5) {
                        //    Plagas5 += "<span class='text-bold' title='" + InfoBeds[x].PestName + "' style='color: " + InfoBeds[x].ColorIdentification + "'>" + InfoBeds[x].Abrr + "</span><br>";
                        //    AbrrAnt5 = InfoBeds[x].Abrr;
                        //}
                    }
                }
                if (Productos === "") {
                    Productos = "Sin producto";
                    Color = "callout-empty";
                }
                else {
                    Color = "callout-full";
                }
                //style='font-size: 10px!important; background: RGBA(255,255,255,0.4); min-height: 35px; border: 3px dotted #AAA;'
                html += "<div id = '" + NombreCamas[i]['ID'] + "' class='col-sm-6 item callout " + Color + "' style = 'font-size: 10px!important; min-height: 59px;'> " +
                    Inputs +
                    "<div class='col-sm-2'><span class='badge bg-light-blue-active'>" + NombreCamas[i]['Nombre'] + "</span></div>" +
                    "<div class='col-sm-10'><span class='text-left text-bold'>" + Productos + "</span></div>" +
                    "<div class='col-sm-12 text-center'><span class='text-bold'>" + Plagas + "</span></div>" +
                    //"<div class = 'col-sm-2 text-center'>1<br>" + Plagas1 +"</div>" +
                    //"<div class = 'col-sm-2'>2<br>" + Plagas2 +"</div>" +
                    //"<div class = 'col-sm-2'>3<br>" + Plagas3 +"</div>" +
                    //"<div class = 'col-sm-2'>4<br>" + Plagas4 +"</div>" +
                    //"<div class = 'col-sm-2'>5<br>" + Plagas5 +"</div>" +
                    "</div>";

                if (i % 2 != 0)
                    html += "<div class='clearfix'></div>"
            }
            $("#ArmaCamas").html(html);

            //Multiselect Para las camas
            window.document.getElementById("ArmaCamas").multiselected = [];
            var Drag = new DragSelect({
                selectables: document.querySelectorAll('.item'),
                area: document.getElementById('ArmaCamas'),
                multiSelectMode: true,
                //callback: sel => { multiselected = sel.map(sel => sel.id) },
                onElementSelect: function (element) {
                    GuardaCamaSeleccionada(element.id);
                },
                onElementUnselect: function(element) {
                    BorraCamaSeleccionada(element.id);
                },
            });
            DragAndDrop = (Drag);
        },
        complete: function () {
            TraeNumeroCamas();
            TraeProductosPorBloque();
            UnlockScreen();
        },
    })

    setTimeout(function () { TraeIdCamasSeleccionadas() }, 10);
}

function ModalCrearSupervisor() {

    $.ajax({
        url: "/AspersionEmployees/TraeParcial",
        type: "GET",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $('#modalFormulariosCuerpo').html(data);
            $("#modalFormularios").modal("show");
        },
        error: function (xhr) {
            alert('Error: ' + xhr.statusText);
        },
    });
}

function GuardaCamaSeleccionada(BedID) {
    var WeekID = $("#WeekID").val();
    var GreenHouseID = $("#GreenHouseID").val();
    var AspersionPlanID = $("#AspersionPlanID").val();
    var BlockId = $("#idBloqueSeleccionado").val();

    var datos = {
        "BedID": BedID,
        "WeekID": WeekID,
        "GreenHouseID": GreenHouseID,
        "AspersionPlanID": AspersionPlanID,
        "BlockId": BlockId,
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/AspersionPlan/GuardaCamaSeleccionada",
        success: function (r) {
            TraeNumeroCamas();
        }
    });

    $("#" + BedID).find("input[type=hidden]").each(function () {
        var ProductId = this.value;

        var datos = {
            "BedID": BedID,
            "WeekID": WeekID,
            "GreenHouseID": GreenHouseID,
            "AspersionPlanID": AspersionPlanID,
            "ProductId": ProductId,
            "BlockId": BlockId,
        };

        $.ajax({
            DataType: "JSON",
            type: "post",
            data: datos,
            url: "/AspersionPlan/GuardaProductoPorCama",
            complete: function () {
                TraeProductosPorBloque();
            }
        });
    });
}

function BorraCamaSeleccionada(BedID) {
    var WeekID = $("#WeekID").val();
    var GreenHouseID = $("#GreenHouseID").val();
    var AspersionPlanID = $("#AspersionPlanID").val();
    var BlockId = $("#Block").val();

    var datos = {
        "BedID": BedID,
        "WeekID": WeekID,
        "GreenHouseID": GreenHouseID,
        "AspersionPlanID": AspersionPlanID,
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/AspersionPlan/BorraCamaSeleccionada",
        success: function (r) {
            TraeNumeroCamas()
        }
    });

    
    $("#" + BedID).find("input[type=hidden]").each(function () {
        var ProductId = this.value;

        var datos = {
            "BedID": BedID,
            "WeekID": WeekID,
            "GreenHouseID": GreenHouseID,
            "AspersionPlanID": AspersionPlanID,
            "ProductId": ProductId,
            "BlockId": BlockId,
        };
        setTimeout(function () {
            $.ajax({
                DataType: "JSON",
                type: "post",
                data: datos,
                url: "/AspersionPlan/BorraProductoPorCama",
                complete: function () {
                    TraeProductosPorBloque();
                }
            })
        }, 100);
    });
}

function OcultaCamas() {
    $("#DivCamas").toggle();
}

function TraeNumeroCamas() {

    var AspersionPlanId = $("#AspersionPlanID").val();
    var BlockId = $("#idBloqueSeleccionado").val();

    var datos = {
        "AspersionPlanId": AspersionPlanId,
        "BlockId": BlockId,
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/AspersionPlan/TraeNumeroCamas",
        success: function (r) {
            //console.log(r);
            $("#NumeroCamasSeleccionadas").text(r);
            $("#BedNumber").val(r);
        }
    })
}

function TraeIdCamasSeleccionadas() {

    var AspersionPlanId = $("#AspersionPlanID").val();

    var datos = {
        "AspersionPlanId": AspersionPlanId,
    };
    var StringCamas = "";
    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/AspersionPlan/TraeIdCamasSeleccionadas",
        success: function (res) {
            for (x in res) {
                var ele = document.getElementById(res[x]['BedId']);
                DragAndDrop.select(ele);
            }
        }
    })

}

function CambiaEstadoCamas() {
    var AspersionPlanId = $("#AspersionPlanID").val();

    var datos = {
        "AspersionPlanId": AspersionPlanId,
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/AspersionPlan/CambiaEstadoCamas",
        success: function (r) {
            //$("#NumeroCamasSeleccionadas").text(r);
            //$("#BedNumber").val(r);
            console.log(r);
        }
    });
}

function TraeProductosPorBloque() {
    var AspersionPlanID = $("#AspersionPlanID").val();
    var BlockId = $("#Block").val();

    var datos = {
        "AspersionPlanID": AspersionPlanID,
        "BlockId": BlockId,
    };
    
    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/AspersionPlan/TraeProductosPorBloque",
        success: function (res) {
            var Productos = "";
            for (x in res) {
                Productos += '<span class="badge bg-red">' + res[x].Nombre + '</span>';
            }

            if (res.length === 0) {
                Productos = "<i><b>Ninguno</b></i>";
            }
            $("#ProductosPorBloque").html("");
            $("#ProductosPorBloque").html(Productos);
        }

    });
}

function ModalCrearLanza() {
    $("#modalFormulariosTitulo").text("Nueva lanza");

    $html = "<div class='form-inline'>" +
        "<div class='col-md-6'><label>Nombre Lanza: </label></div>" +
        "<div class='col-md-6'><input type='text' id='ReferenceSpearName' class='form-control'></div> " +
        "<div class='col-md-6'><label>Capacidad: </label></div>" +
        "<div class='col-md-6'><input type='text' onkeypress='return soloNumerosDecimal(event, this);' id='ReferenceSpearCapacity' class='form-control'></div> " +
        "<div class='clearfix'></div><br />" +
        "<div class='col-md-12 text-right'><button class='btn btn-success' type='button' onclick='GuardaNuevoRegistro(\"ReferenceSpearName\", ReferenceSpearName.value)'><i class='fa fa-save'></i></button></div> " +
        "</div>" +
        "";

    $("#modalFormulariosCuerpo").html($html);

    $("#modalFormularios").modal("show");
}

function GuardaNuevoRegistro(ctrl, valor) {

    if (!valor) {
        alert("Debe Ingresar un Nombre, verifique");
        return
    }

    if (ctrl === "ReferenceSpearName")
    {
        var NameReal = "ReferenceSpearName";
        ctrl = "ReferenceSpearID";
        var IdForm = "ReferenceSpear";
    }

    Valor2 = $("#ReferenceSpearCapacity").val();
    Valor2 = Valor2.replace(".",",");
    
    var datos = {
        "Ctrl": ctrl,
        "Valor": valor,
        "Valor2": Valor2,
    };

    $.ajax({
        type: "POST",
        DataType: "json",
        data: datos,
        url: "/AspersionPlan/GuardaNuevoRegistro",
        success: function (res) {
            var $html = "";
            for (x in res) {
                $html += "<option value='" + res[x][ctrl] + "'>" + res[x]['Name'] + "</option>";
            }

            $("#" + IdForm).html($html);
            Notifications("success", "Registro creado correctamente");
            $("#modalFormularios").modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }
    });
}

function TypoDePlan() {

    // Limpiado General
    $("#ArmaCamas").html("");
    $("#BloqueSeleccionado").html("");
    $("#NumeroCamasSeleccionadas").html("");
    $("#ProductosPorBloque").html("");
    $("#idBloqueSeleccionado").val("");
    $("#BedNumber").val("");

    $("#TablaIncidencia").find("input[type=checkbox]").each(function () {
        if ($(this).prop("checked") === true) {
            $(this).prop("checked", false);
        }
    });

    var PlanType = $("#PlanType").val();

    if (PlanType === "2") {
        $("#NumeroCamasSeleccionadas").text("N.A.");
        $("#ProductosPorBloque").text("N.A.");
    }
    else {
        $("#NumeroCamasSeleccionadas").text("");
        $("#ProductosPorBloque").text("");
    }

    return;
}