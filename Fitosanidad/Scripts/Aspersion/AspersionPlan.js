﻿$(document).ready(function () {
    $("#btnCreate").on("click", function () {
        location = "/AspersionPlan/AspersionPlanAdd";
    });

    $("#bt_AspersionPlanAdd").click(function () {
        var WeekID = $("#WeekID").val();
        var GreenHouseID = $("#GreenHouseID").val();
        console.log(GreenHouseID);
    });

    $("#AspersioPlanOutputTable").click(function () {
        location = "/AspersionPlan/AspersioPlanOutputTable";
        return false;
    });
});

function CambiaEstado(estado, id)
{
    var html = "<input type='hidden' value='" + estado + "' id='estado'>" +
        "<input type='hidden' value='" + id + "' id='AspersionPlanID'>" +
        "<span class='text-danger' id='LabelError'></span>" +
        "<textarea class = 'form-control' style='min-width:100%' id='Observacion'></textarea>" +
        "";
    $("#modalMensajesTitulo").html("Observación de cambio de estado");
    $("#modalMensajesCuerpo").html(html);
    $("#modalMensajes").modal(); 
}

function CambiaEstadoCabecera(estado, id) {
    var ActiveFlag = null;
    if (estado === "0")
        ActiveFlag = true;
    else
        ActiveFlag = false;
    var datos = {
        "ActiveFlag": ActiveFlag,
        "AspersionPlanID": id,
    };
    $.ajax({
        type: "POST",
        DataType: "json",
        data: JSON.stringify(datos),
        url: "/AspersionPlan/CambiaEstadoHead",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            location = "/AspersionPlan";
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
}

function GuardarObservacion() {
    var Observacion = $("#Observacion").val();
    var estado = $("#estado").val();
    var AspersionPlanID = $("#AspersionPlanID").val();
    if (Observacion.length == 0) {
        $("#LabelError").text("Observación es requerido!");
        return false;
    };

    datos = {
        "AspersionPlanID": AspersionPlanID,
        "Comment": Observacion,
        "ActiveFlagOld": estado,
    }

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/AspersionPlan/GuardarObservacion",
        success: function (r) {
            $("#modalMensajes").hide();
            CambiaEstadoCabecera(estado, AspersionPlanID);
        }
    });

}