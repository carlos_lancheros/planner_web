﻿$(document).ready(function () {
    $("#BlockIDs").on("change", function () {
        var id = $("#BlockIDs").val();
        ProductByBlock(id);
    });

    $("#bt_AspersionPlanDetailAdd").on("click", function () {
        TraeTablaParaEditar();
    });

    //Option para productos
    $.ajax({
        DataType: "JSON",
        type: "post",
        data: { "BlockID": 0 },
        url: "/AspersionPlan/SelectProducto",
        async: false,
        success: function (r) {
            var html = '';
            for (x in r) {
                html += "<option value='" + r[x]['PlantProductID'] + "'>" + r[x]['PlantProductName'] + "</option>";
            }
            OptionProducts = (html);
        }
    });

    //Option para Aplicacion
    $.ajax({
        DataType: "JSON",
        type: "post",
        data: { "BlockID": 0 },
        url: "/AspersionPlan/SelectAplicacion",
        async: false,
        success: function (r) {
            var html = '';
            for (x in r) {
                html += "<option value='" + r[x]['ApplyTypeID'] + "'>" + r[x]['Name'] + "</option>";
            }
            OptionAplicacion = (html);
        }
    });

    //Option para Forma Aplicación
    $.ajax({
        DataType: "JSON",
        type: "post",
        data: { "BlockID": 0 },
        url: "/AspersionPlan/SelectForms",
        async: false,
        success: function (r) {
            var html = '';
            for (x in r) {
                html += "<option value='" + r[x]['FormID'] + "'>" + r[x]['Name'] + "</option>";
            }
            OptionForms = (html);
        }
    });

    //Option para Tercio
    $.ajax({
        DataType: "JSON",
        type: "post",
        data: { "BlockID": 0 },
        url: "/AspersionPlan/SelectThird",
        async: false,
        success: function (r) {
            var html = '';
            for (x in r) {
                html += "<option value='" + r[x]['ThirdID'] + "'>" + r[x]['Name'] + "</option>";
            }
            OptionThirds = (html);
        }
    });

    //Option para Dirección
    $.ajax({
        DataType: "JSON",
        type: "post",
        data: { "BlockID": 0 },
        url: "/AspersionPlan/SelectDirection",
        async: false,
        success: function (r) {
            var html = '';
            for (x in r) {
                html += "<option value='" + r[x]['DirectionID'] + "'>" + r[x]['Name'] + "</option>";
            }
            OptionDirections = (html);
        }
    });

    //Option para Productos Agroquímicos
    $.ajax({
        DataType: "JSON",
        type: "post",
        data: { "BlockID": 0 },
        url: "/AspersionPlan/SelectAgrochemicalProduct",
        async: false,
        success: function (r) {
            var html = '';
            for (x in r) {
                html += "<option value='" + r[x]['AgrochemicalProductsID'] + "'>" + r[x]['Name'] + "</option>";
            }
            OptionAgrochemicalProducts = (html);
        }
    });

    //Option para Lanzas
    $.ajax({
        DataType: "JSON",
        type: "post",
        data: { "BlockID": 0 },
        url: "/AspersionPlan/SelectEReferenceSpread",
        async: false,
        success: function (r) {
            var html = '';
            for (x in r) {
                html += "<option value='" + r[x]['ReferenceSpearID'] + "'>" + r[x]['Name'] + "</option>";
            }
            OptionReferenceSpear = (html);
        }
    });
});


function BackToList() {
    location = "/AspersionPlan/";
}

function ProductByBlock(BlockId) {
    if (!BlockId) {
        $("#ProductID").val("");
        return false;
    }
    $.ajax({
        dataType: "json",
        type: "post",
        url: "/AspersionPlan/ProductByBlock",
        data: { "BlockId": BlockId },
        success: function (r) {
            var html = "";
            for (x in r) {
                html += "<option value='" + r[x]['Value'] + "'>" + r[x]['Text'] + "</option>";
            }
            $("#ProductID").html(html);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    })
}

function TraeTablaParaEditar() {
    var DayNumber = $("#DayNumber").val();
    var BlockIDs = $("#BlockIDs").val();
    var ProductID = $("#ProductID").val();
    var Id = $("#Id").val();

    var datos = {
        "DayNumber": DayNumber,
        "BlockIDs": BlockIDs,
        "ProductID": ProductID,
        "Id": Id
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/AspersionPlan/TraeTablaParaEditar",
        success: function (r) {
            DrawTable(r);
        }
    });

    setTimeout(function () { $("#TablaResultado").find("select").each(function () { $(this).css("min-width", "80px") }); }, 1000);
}

function DrawTable(r) {
    var html = "<table class='table table-bordered table-striped table-condensed table-hover table-responsive nowrap table-responsive' id='TablaResultado'>" +
        "<thead>" +
        "<th>ID</th>" +
        "<th>Producto</th>" +
        "<th>Bloque</th>" +
        "<th>Día</th>" +
        "<th>Aplicación</th>" +
        "<th>Forma</th>" +
        "<th>Tercio</th>" +
        "<th>Dirección</th>" +
        "<th>Orden</th>" +
        "<th>Producto Agro.</th>" +
        "<th>No. Camas</th>" +
        "<th>Litros*Cama</th>" +
        "<th>Lanza</th>" +
        "<th>No. Asper.</th>" +
        "<th>Observaciones</th>" +
        "<th>Vol. Tanque</th>" +
        "</thead>" +
        "<tbody>";
    for (x in r) {
        html += "<tr><td>" + r[x]['AspersionPlanDetailID'] + "</td>" +
            "<td>" + SelectProducto(x, r[x]['AspersionPlanDetailID']) + "</td > " +
            "<td>" + SelectBlock(x, r[x]['AspersionPlanDetailID']) + "</td > " +
            "<td>" + SelectDia(x, r[x]['AspersionPlanDetailID']) + "</td > " +
            "<td>" + SelectAplication(x, r[x]['AspersionPlanDetailID']) + "</td > " +
            "<td>" + SelectForms(x, r[x]['AspersionPlanDetailID']) + "</td > " +
            "<td>" + SelectThirds(x, r[x]['AspersionPlanDetailID']) + "</td > " +
            "<td>" + SelectDirection(x, r[x]['AspersionPlanDetailID']) + "</td > " +
            "<td>" + r[x]['PreparationOrder'] + "</td > " +
            "<td>" + SelectAgrochemicalProduct(x, r[x]['AspersionPlanDetailID']) + "</td > " +
            "<td><input type='text' id='BedNumber" + x + "' value='" + r[x]['BedNumber'] + "' style='max-width:50px;' class='form-control' onchange='GuardarEditar(" + r[x]['AspersionPlanDetailID'] + ", this.value, \"BedNumber\", this.id)'></td > " +
            "<td><input type='text' id='LiterPerBed" + x + "' value='" + r[x]['LiterPerBed'] + "' style='max-width:50px;' class='form-control' onchange='GuardarEditar(" + r[x]['AspersionPlanDetailID'] + ", this.value, \"LiterPerBed\", this.id)'></td > " +
            "<td>" + SelectReferenceSpear(x, r[x]['AspersionPlanDetailID']) + "</td > " +
            "<td><input type='text' id='AsperjadorNumber" + x + "' value='" + r[x]['AsperjadorNumber'] + "' style='max-width:50px;' class='form-control' onchange='GuardarEditar(" + r[x]['AspersionPlanDetailID'] + ", this.value, \"AsperjadorNumber\", this.id)'></td > " +
            "<td><textarea id='Observation" + x + "' class='form-control' onchange='GuardarEditar(" + r[x]['AspersionPlanDetailID'] + ", this.value, \"Observation\", this.id)'>" + r[x]['Observation'] + "</textarea></td> " +
            "<td>" + SelectVolumenTanque(x, r[x]['AspersionPlanDetailID']) + "</td > " +
            "</tr>";
    }
    html += "</table>";



    $("#CargaTabla").empty();
    $("#CargaTabla").html(html);

    //Hace Select de los los selects
    for (x in r) {
        $("#DayNumber" + x).val(r[x]['DayNumber']);
        $("#BlockIDs" + x).val(r[x]['BlockID']);
        $("#ProductID" + x).val(r[x]['IDProduct']);
        $("#AplicationId" + x).val(r[x]['IDApplyTypes']);
        $("#FormID" + x).val(r[x]['IDForm']);
        $("#ThirdID" + x).val(r[x]['IDThird']);
        $("#DirectionID" + x).val(r[x]['IdDirection']);
        $("#AgrochemicalProductsID" + x).val(r[x]['IdAgrochemicalProducts']);
        $("#ReferenceSpearID" + x).val(r[x]['ReferenceSpearID']);
        $("#TankVolume" + x).val(r[x]['TankVolume']);

    }

    $("#TablaResultado").DataTable({
        "ordering": false,
    });
}

//------------------------//
//--- Armado de selects---//
//------------------------//
function SelectProducto(i, id) {
    var html = "<select class='form-control' id='ProductID" + i + "' onchange='GuardarEditar(" + id +", this.value, \"ProductID\", this.id)'>" + OptionProducts + "</select>";
    return html;
}

function SelectDia(i, id) {
    var html = '<select id="DayNumber' + i + '" name="DayNumber" class="form-control select2" required onchange="GuardarEditar(' + id +', this.value, \'DayNumber\', this.id)">' +
        '<option value = "">Todos</option>' +
        '<option value="1">Lunes</option>' +
        '<option value="2">Martes</option>' +
        '<option value="3">Miércoles</option>' +
        '<option value="4">Jueves</option>' +
        '<option value="5">Viernes</option>' +
        '<option value="6">Sábado</option>' +
        '<option value="7">Domingo</option>' +
        '</select>';
    return html;
}

function SelectBlock(i, id) {
    var Options = $("#BlockIDs").html();
    var html = "<select class='form-control' id='BlockIDs" + i + "' onchange='GuardarEditar(" + id +", this.value, \"Block\", this.id)'>" + Options + "</select>";
    return html;
}

function SelectAplication(i, id) {
    var html = "<select class='form-control' id='AplicationId" + i + "' onchange='GuardarEditar(" + id +", this.value, \"ApplyTypesID\", this.id)' > " + OptionAplicacion + "</select> ";
    return html;
}

function SelectForms(i, id) {
    var html = "<select class='form-control' id='FormID" + i + "' onchange='GuardarEditar(" + id +", this.value, \"FormID\", this.id)'>" + OptionForms + "</select>";
    return html;
}

function SelectThirds(i, id) {
    var html = "<select class='form-control' id='ThirdID" + i + "' onchange='GuardarEditar(" + id + ", this.value, \"ThirdID\", this.id)'>" + OptionThirds + "</select>";
    return html;
}

function SelectDirection(i, id) {
    var html = "<select class='form-control' id='DirectionID" + i + "' onchange='GuardarEditar(" + id + ", this.value, \"DirectionID\", this.id)'>" + OptionDirections + "</select>";
    return html;
}

function SelectAgrochemicalProduct(i, id) {
    var html = "<select class='form-control' id='AgrochemicalProductsID" + i + "' onchange='GuardarEditar(" + id + ", this.value, \"AgrochemicalProductsID\", this.id)'>" + OptionAgrochemicalProducts + "</select>";
    return html;
}

function SelectReferenceSpear(i, id) {
    var html = "<select class='form-control' id='ReferenceSpearID" + i + "' onchange='GuardarEditar(" + id + ", this.value, \"ReferenceSpear\", this.id)'>" + OptionReferenceSpear + "</select>";
    return html;
}

function SelectVolumenTanque(i, id) {
    var html = '<select id="TankVolume' + i + '" name="TankVolume" class="form-control" required onchange="GuardarEditar(' + id + ', this.value, \'TankVolume\', this.id)">' +
        '<option value = "200">200</option>' +
        '<option value="500">500</option>' +
        '<option value="1000">1000</option>' +
        '</select>';
    return html;
}

//------------------------//
//----- Guardado edit ----//
//------------------------//

function GuardarEditar(id, valor, campo, IdCampo) {
    if (campo === 'FormID') {
        valor = $("#" + IdCampo + " option:selected").text();
    }

    var datos = {
        "id": id,
        "valor": valor,
        "campo": campo,
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/AspersionPlan/GuardarEditar",
        success: function (r) {
            $("#" + IdCampo).css("background-color", "#99e699");
        },
    })

}