﻿jQuery(document).ready(function () {
    TablaMaestra();

    $("#form_AgrochemicalProducts").submit(function (e) {
        e.preventDefault();

        var Name = $("#Name").val();
        var ChemicalGroupID = $("#ChemicalGroupID").val();
        var FracID = $("#FracID").val();
        var ActiveIngredientsID = $("#ActiveIngredientsID").val();
        var ToxicologicalCategoryID = $("#ToxicologicalCategoryID").val();
        var ReentryHourID = $("#ReentryHourID").val();
        var ProductOrigin = $("#ProductOrigin").val();
        var Aspersion = $("#Aspersion").val();
        var Sprinkle = $("#Sprinkle").val();
        var Drench = $("#Drench").val();
        var Thermonebulizer = $("#Thermonebulizer").val();
        var Density = $("#Density").val();
        var PlagasID = $("#PlagasID").val();
        var MeasurementUnitsID = $("#MeasurementUnitsID").val();
        var Concentration = $("#Concentration").val();
        var ProductType = $("#ProductType").val();

        Aspersion = parseFloat(Aspersion);
        Sprinkle = parseFloat(Sprinkle);
        Drench = parseFloat(Drench);
        Thermonebulizer = parseFloat(Thermonebulizer);
        Density = parseFloat(Density);

        var datos = {
            "Edita": 0,
            "Name": Name.toUpperCase(),
            "ChemicalGroupID": ChemicalGroupID,
            "FracID": FracID,
            "ActiveIngredientsID": ActiveIngredientsID,
            "ToxicologicalCategoryID": ToxicologicalCategoryID,
            "ReentryHourID": ReentryHourID.toUpperCase(),
            "ProductOrigin": ProductOrigin,
            "Aspersion": Aspersion,
            "Sprinkle": Sprinkle,
            "Drench": Drench,
            "Thermonebulizer": Thermonebulizer,
            "Density": Density,
            "PlagasID": PlagasID,
            "MeasurementUnitsID": MeasurementUnitsID,
            "Concentration": Concentration,
            "ProductType": ProductType,
        };
        $.ajax({
            type: "POST",
            DataType: "json",
            data: JSON.stringify(datos),
            url: "/AgrochemicalProducts/Crear?Edita=0",
            cache: false,
            contentType: 'application/json; charset=utf-8',
            success: function (r) {
                $('#modalMensajes .modal-header').addClass("bg-green");
                $("#modalMensajesTitulo").html("<span class='text-green'>Mensaje</span>");
                $('#modalMensajes').modal('show');
                $("#modalMensajesCuerpo").html("Registro adicionado correctamente");

                $('#form_AgrochemicalProducts :input').not("[type=radio],[type=checkbox],[type=hidden],[readonly='readonly'],[style*='display: none'],.select2-offscreen,[class^='select2'],select").each(function () {
                    var name = $(this).attr('name');
                    console.log(name);
                    $(this).val('');
                });

                TablaMaestra();
                setTimeout(function () { $("#TableAgrochemicalProducts").dataTable(); }, 100);
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
        return false;
    });


    $("#formEdit_AgrochemicalProducts").submit(function (e) {
        e.preventDefault;
        var Name = $("#Name").val();
        var ChemicalGroupID = $("#ChemicalGroupID").val();
        var FracID = $("#FracID").val();
        var ActiveIngredientsID = $("#ActiveIngredientsID").val();
        var ToxicologicalCategoryID = $("#ToxicologicalCategoryID").val();
        var ReentryHourID = $("#ReentryHourID").val();
        var ProductOrigin = $("#ProductOrigin").val();
        var Aspersion = $("#Aspersion").val();
        var Sprinkle = $("#Sprinkle").val();
        var Drench = $("#Drench").val();
        var Thermonebulizer = $("#Thermonebulizer").val();
        var Density = $("#Density").val();
        var PlagasID = $("#PlagasID").val();
        var MeasurementUnitsID = $("#MeasurementUnitsID").val();
        var AgrochemicalProductsID = $("#AgrochemicalProductsID").val();
        var Concentration = $("#Concentration").val();
        var ProductType = $("#ProductType").val();

        Aspersion = parseFloat(Aspersion);
        Sprinkle = parseFloat(Sprinkle);
        Drench = parseFloat(Drench);
        Thermonebulizer = parseFloat(Thermonebulizer);
        Density = parseFloat(Density);

        var datos = {
            "Name": Name.toUpperCase(),
            "ChemicalGroupID": ChemicalGroupID,
            "FracID": FracID,
            "ActiveIngredientsID": ActiveIngredientsID,
            "ToxicologicalCategoryID": ToxicologicalCategoryID,
            "ReentryHourID": ReentryHourID.toUpperCase(),
            "ProductOrigin": ProductOrigin,
            "Aspersion": Aspersion,
            "Sprinkle": Sprinkle,
            "Drench": Drench,
            "Thermonebulizer": Thermonebulizer,
            "Density": Density,
            "PlagasID": PlagasID,
            "MeasurementUnitsID": MeasurementUnitsID,
            "AgrochemicalProductsID": AgrochemicalProductsID,
            "Concentration": Concentration,
            "ProductType": ProductType,
        };
        console.log(datos);
        //return false;
        $.ajax({
            type: "POST",
            DataType: "json",
            data: JSON.stringify(datos),
            url: "/AgrochemicalProducts/Crear?Edita=1",
            cache: false,
            contentType: 'application/json; charset=utf-8',
            success: function (r)
            {
                $('#modalMensajes .modal-header').addClass("bg-green");
                $("#modalMensajesTitulo").html("Mensaje");
                $('#modalMensajes').modal('show');
                $("#modalMensajesCuerpo").html("Registro actualizado correctamente");
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
        //window.location = "/AgrochemicalProducts/Index";
        return false;
    });

    $("#ProductOrigin").change(function () {
        var texto = $("#ProductOrigin option:selected").text();
        $("#Name").val(texto.trim()).focus();
    })

});

function TablaMaestra() {
    $.ajax({
        type: "POST",
        DataType: "json",
        url: "/AgrochemicalProducts/DetalleMaestro",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            $("#DetalleMaestro").html(r);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
}

function CambiaEstado(estado, id)
{
    console.log(estado);
    var ActiveFlag = null;
    if (estado === 0)
        ActiveFlag = true;
    else
        ActiveFlag = false;
    var datos = {
        "ActiveFlag": ActiveFlag,
        "AgrochemicalProductsID": id,
    };
    $.ajax({
        type: "POST",
        DataType: "json",
        data: JSON.stringify(datos),
        url: "/AgrochemicalProducts/CambiaEstado",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            //$("#DetalleMaestro").html(r);
            TablaMaestra();
            console.log("Cambio Estado OK");
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
}

function BackToList() {
    location = "/AgrochemicalProducts/";
}

function CreaGrupoQuimico() {
    $("#modalFormulariosTitulo").text("Nuevo grupo químico");

    $html = "<div class='form-inline'>" +
        "<div class='col-md-4'><label>Nombre Grupo Químico: </label></div>" +
        "<div class='col-md-6'><input type='text' id='ChemicalGroupName' class='form-control'></div> " +
        "<div class='col-md-2'><button class='btn btn-success' type='button' onclick='GuardaNuevoRegistro(\"ChemicalGroupID\", ChemicalGroupName.value)'><i class='fa fa-save'></i></button></div> " +
        "<br />"+
        "</div>" +
        "";

    $("#modalFormulariosCuerpo").html($html);

    $("#modalFormularios").modal("show");
}

function CreaFrac() {
    $("#modalFormulariosTitulo").text("Nuevo Frac");

    $html = "<div class='form-inline'>" +
        "<div class='col-md-4'><label>Nombre Frac: </label></div>" +
        "<div class='col-md-6'><input type='text' id='FracIDName' class='form-control'></div> " +
        "<div class='col-md-2'><button class='btn btn-success' type='button' onclick='GuardaNuevoRegistro(\"FracID\", FracIDName.value)'><i class='fa fa-save'></i></button></div> " +
        "<br />" +
        "</div>" +
        "";

    $("#modalFormulariosCuerpo").html($html);

    $("#modalFormularios").modal("show");
}

function CreaIngredienteActivo() {
    $("#modalFormulariosTitulo").text("Nuevo Ingrediente Activo");

    $html = "<div class='form-inline'>" +
        "<div class='col-md-4'><label>Nombre Ingrediente Activo: </label></div>" +
        "<div class='col-md-6'><input type='text' id='ActiveIngredientsIDName' class='form-control'></div> " +
        "<div class='col-md-2'><button class='btn btn-success' type='button' onclick='GuardaNuevoRegistro(\"ActiveIngredientsID\", ActiveIngredientsIDName.value)'><i class='fa fa-save'></i></button></div> " +
        "<br />" +
        "</div>" +
        "";

    $("#modalFormulariosCuerpo").html($html);

    $("#modalFormularios").modal("show");
}

function GuardaNuevoRegistro(ctrl, valor) {
    if (!valor) {
        alert("Debe Ingresar un Nombre, verifique");
        return
    }
    if (ctrl === "ActiveIngredientsID") {
        ctrl = "ActiveIngredientID";
    }

    var datos = {
        "Ctrl": ctrl,
        "Valor": valor,
    };

    if (ctrl === "ActiveIngredientID") {
        ctrl = "ActiveIngredientsID";
    }

    $.ajax({
        type: "POST",
        DataType: "json",
        data: datos,
        url: "/AgrochemicalProducts/GuardaNuevoRegistro",
        success: function (res) {
            var $html = "";
            for (x in res) {
                $html += "<option value='" + res[x][ctrl] + "'>" + res[x]['Name'] +"</option>";
            }

            $("#" + ctrl).html($html);
            Notifications("success", "Registro creado correctamente");
            $("#modalFormularios").modal('hide');//ocultamos el modal
            $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
            $('.modal-backdrop').remove();//eliminamos el backdrop del modal
        }
    })
}