﻿var tblSensor

jQuery(window).load(function () {
    BlockScreen();
});

jQuery(document).ready(function () {
    UnlockScreen();

    $('#tableSensor thead tr').clone(true).appendTo('#tableSensor thead');
    $('#tableSensor thead tr:eq(1) th').each(function (i) {
        $(this).html('<input type="text" />');

        $('input', this).on('keyup change', function () {
            if (tblSensor.column(i).search() !== this.value) {
                tblSensor
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        });
    });

    tblSensor = jQuery('#tableSensor').DataTable({
        "searching": true
        , "info": true
        , paging: true
        , responsive: true
        , orderCellsTop: true
        , scrollX: true
        , scrollCollapse: true
        , "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
        , fixedHeader: true
        , fixedColumns: {
            leftColumns: 2
        }
        , "columnDefs": [
            {
                "targets": [4, 13, 14, 15, 16, 18, 19],
                "className": "text-right",
            },
            { className: "dt-left", "width": "05%", "targets": 0 }
            , { "width": "05%", "targets": 1 }
            , { "width": "05%", "targets": 2 }
            , { "width": "05%", "targets": 3 }
            , { "width": "05%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
            , { "width": "05%", "targets": 6 }
            , { "width": "05%", "targets": 7 }
            , { "width": "05%", "targets": 8 }
            , { "width": "05%", "targets": 9 }
            , { "width": "05%", "targets": 10 }
            , { "width": "05%", "targets": 11 }
            , { "width": "05%", "targets": 12 }
            , { "width": "05%", "targets": 13 }
            , { "width": "05%", "targets": 14 }
            , { "width": "05%", "targets": 15 }
            , { "width": "05%", "targets": 16 }
            , { "width": "05%", "targets": 17 }
            , { "width": "05%", "targets": 18 }
            , { "width": "05%", "targets": 19 }
        ]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    jQuery("#btnExcel").click(function () {
        jQuery('<table>')
            .append(
            jQuery("#tableSensor").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "SatrackSensorInformation"
            });
    });
});
