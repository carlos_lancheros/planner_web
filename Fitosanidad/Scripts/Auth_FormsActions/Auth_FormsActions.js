﻿jQuery(document).ready(function () {
    LoadGrid();

    jQuery('#tableAuth_FormsActions').dataTable({
        "info": true
        , paging: true
        , responsive: true
        , destroy: true
        , autoWidth: false
        , scrollY: 300
        , scrollX: true
        , scrollCollapse: true
        , "columnDefs": [
            {
                "targets": [0],
                "className": "text-right",
            }
            , {
                "targets": [3],
                orderable: false,
            }
            , {
                "targets": [4],
                orderable: false,
                "className": "text-center",
            }
            , { "width": "10%", "targets": 0 }
            , { "width": "40%", "targets": 1 }
            , { "width": "20%", "targets": 2 }
            , { "width": "10%", "targets": 3 }
            , { "width": "20%", "targets": 4 }
        ]
        , "columns": [
            { 'data': 0 },
            { 'data': 1 },
            { 'data': 2 },
            { 'data': 3 },
            { 'data': 4 },
        ]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    $("#btnCreate").click(function () {
        CreateRow();
    });
});

function LoadGrid(e) {
    if (e) {
        e.preventDefault();
    }
    BlockScreen();

    jQuery.ajax({
        url: "/Auth_FormsActions/IndexGrid",
        data: null,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length == 0) {
                Notifications("information", GetResource("NoSeEncontraronDatos"))
            } else {
                var tblAuth_FormsActions = jQuery('#tableAuth_FormsActions').DataTable();

                $.each(data, function (i, data) {
                    tblAuth_FormsActions.row.add([
                        data.ActionID,
                        data.Name,
                        (data.Icon.length > 0 ? "<div class='" + data.Icon + "'></div> " : "") + data.Icon,
                        '<input type="checkbox"' + (data.FlagActive ? ' checked="checked"' : '') + ' onClick="return false">',
                        '<div class="actions">' + 
                        '    <div class="btn-group">' + 
                        '        <button class="btn btn-xs" type="button" data-toggle="tooltip" data-placement="top" title="' + GetResource("Editar") + '" onclick="location=\'/Auth_FormsActions/Edit?ActionID=' + data.ActionID + '\'">' + 
                        '            <i class="fa fa-pencil text-blue"></i>' + 
                        '        </button>' + 
                        '    </div>' + 
                        '</div>'
                    ]).draw();
                });
                tblAuth_FormsActions.draw(false);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#ddlBusinessUnit")).text());
        },
        async: false
    });
};

function CreateRow() {
    var url = "/Auth_FormsActions/Create";
    window.location = url;
};

function EditRow(arrKey) {
    var PermissionID = arrKey[0];
    var url = "/Auth_FormsActions/Edit?PermissionID=" + ID;

    window.location = url;
};
