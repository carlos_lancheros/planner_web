﻿$(document).ready(function () {
    $("#btnHome").click(function () {
        BackToList();
    });
    $("#btnSave").click(function () {
        CallSubmit();
    });
});

function BackToList() {
    var Url = "/Auth_FormsActions";
    window.location.href = Url;
};

function CallSubmit() {
    if (jQuery('#Name').val() == null) {
        Notifications("error", GetResource("DebeDigitarDescripcion"))
        jQuery('#Name').focus();
        return false;
    } else {
        if (jQuery('#Name').val().length == 0) {
            Notifications("error", GetResource("DebeDigitarDescripcion"))
            jQuery('#Name').focus();
            return false;
        };
    }

    jQuery("#Edit").submit();
};