﻿jQuery(document).ready(function () {
    jQuery("#BusinessUnitID").change(function () {
        BlockScreen();
        var BusinessUnitID = jQuery("#BusinessUnitID").val();

        var values = { "BusinessUnitID": BusinessUnitID }
        
        $("#GreenhouseID").empty()
        jQuery.ajax({
            url: "/Phytosanity_Flat/getGreenhousebyBusinessUnit",
            data: JSON.stringify(values),
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $.each(data, function (i, data) {
                    $("#GreenhouseID").append('<option value="'
                        + data.Value + '">'
                        + data.Text + '</option>');
                });  
                UnlockScreen();
            },
            error: function (xhr, textStatus, errorThrown) {
                UnlockScreen();
                Notifications("error", errorThrown)
            }
        });
    });   
});