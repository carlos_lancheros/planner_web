﻿jQuery(document).ready(function () {
    $('#Phytosanity_FlatParDataTable').hide();
    $('#Phytosanity_FlatImparDataTable').hide();
    $('#Phytosanity_FlatADataTable').hide();
    $('#Phytosanity_FlatBDataTable').hide();
    $('#Phytosanity_FlatCDataTable').hide();

    $('#Phytosanity_FlatFooterDataTable').hide();

    $("#ddlBusinessUnit").change(function () {
        var intBusinessId = parseInt($("#ddlBusinessUnit").find(":selected").val());
        GetGreenhousebyBusinessUnit(intBusinessId);
    });

});

function exportPdf() {
    BlockScreen();
    $(".ww").css("display","none");
    var imp = jQuery("#tableImp").val();
    var element = document.getElementById('ContentFlat');

    var widthValue = 0
    var heigthValue = 0
    var orientationVAlue = "";
    if (imp == 1 || imp == 2) {
        orientationVAlue = "portrait";
        widthValue = 740
        heigthValue = 540
    } else {
        orientationVAlue = "landscape"
        widthValue = 980
        heigthValue = 780
    }

    var opt = {
        margin: [12, 8, 8, 8],
        filename: 'PlanoMonitoreo.pdf',
        image: { type: 'png', quality: 0.1 },
        html2canvas: { dpi: 300, letterRendering: true, width: widthValue, heigth: heigthValue },
        //jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' },
        jsPDF: { unit: 'mm', format: 'A4', orientation: orientationVAlue }
    };

    html2pdf().set(opt).from(element).toPdf().get('pdf').then(function (pdf) {
        var number_of_pages = pdf.internal.getNumberOfPages()
        var pdf_pages = pdf.internal.pages
        var myFooter = "Footer info"
        var tituloOutput = $("#tituloPdf").text();

        for (var i = 1; i < pdf_pages.length; i++) {
            pdf.setPage(i)
            pdf.setFontSize(10);
            pdf.text(tituloOutput, 2, 5);
        }
    }).save();
    UnlockScreen();
    setTimeout(function () { $(".ww").css("display", "inline") }, 2000);
    return;
}

function CallSubmit() {
    if (!validForm()) {
        return false;
    }

    $('#Phytosanity_FlatParDataTable').hide();
    $('#Phytosanity_FlatImparDataTable').hide();
    $('#Phytosanity_FlatADataTable').hide();
    $('#Phytosanity_FlatBDataTable').hide();
    $('#Phytosanity_FlatCDataTable').hide();

    $('#Phytosanity_FlatFooterDataTable').hide();

    var prmIDSemana = parseInt($("#ddlWeek").find(":selected").val());;
    var prmIDUnidadesNegocios = parseInt($("#ddlBusinessUnit").find(":selected").val());;
    var prmIDInvernadero = parseInt($("#ddlGreenhouse").find(":selected").val());;
    var prmIDPest = $("#PestID").val();
    if (!prmIDPest) {
        prmIDPest = 0;
    }

    var parameters = { "prmIDSemana": prmIDSemana, "prmIDUnidadesNegocios": prmIDUnidadesNegocios, "prmIDInvernadero": prmIDInvernadero, "prmBlancoBiologico": prmIDPest }

    BlockScreen();
    jQuery.ajax({
        url: "/Phytosanity_Flat/getPhytosanity_FlatHeader",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            jQuery('#NameFarm').html(data[3]);
            jQuery('#strWeek').html(data[0]);
            jQuery('#intBlock').html(data[1]);
            jQuery('#intBeds').html(data[2]);
            jQuery('#datSowingDate').html(data[4]);
            jQuery('#strCultivo').html(data[4]);

            //Para header del PDF
            jQuery('#NameFarmT').html(data[3]);
            jQuery('#strWeekT').html(data[0]);
            jQuery('#intBlockT').html(data[1]);
            jQuery('#intBedsT').html(data[2]);
            jQuery('#datSowingDateT').html(data[4]);
            jQuery('#strCultivoT').html(data[4]);
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });

    BlockScreen();
    jQuery.ajax({
        url: "/Phytosanity_Flat/getPhytosanity_FlatAbrr",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#Phytosanity_FlatAbrrDataTable').html(data);
            console.log(data);
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });


    BlockScreen();
    jQuery.ajax({
        url: "/Phytosanity_Flat/getPhytosanity_Flat",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery("#tableImp").val(data)
            if (data == 2 || data == 1 ) {
                setPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 0, prmIDPest);
                setPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 1, prmIDPest);
            }
            else {
                setPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 6, prmIDPest);
                setPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 7, prmIDPest);
                setPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 8, prmIDPest);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });

    BlockScreen();
    jQuery.ajax({
        url: "/Phytosanity_Flat/getPhytosanity_FlatFooter",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#Phytosanity_FlatFooterDataTable').show();

            $('#Phytosanity_FlatFooterDataTable').html(data);
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });
}

function GetGreenhousebyBusinessUnit(intBusinessId) {
    BlockScreen();

    $("#ddlGreenhouse").empty()
    var parameters = { "intBusinessId": intBusinessId };
    jQuery.ajax({
        url: "/Base/getGreenhousebyBusinessUnitId",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.each(data, function (i, data) {
                $("#ddlGreenhouse").append('<option value="'
                    + data.ID + '">'
                    + data.Codigo + '</option>');
            });
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
        async: false
    });
}

function getPdf() {
    if (!validForm()) {
        return false;
    }

    BlockScreen();

    var prmIDSemana = parseInt($("#ddlWeek").find(":selected").val());;
    var prmIDUnidadesNegocios = parseInt($("#ddlBusinessUnit").find(":selected").val());;
    var prmIDInvernadero = parseInt($("#ddlGreenhouse").find(":selected").val());;
    var prmIDPest = $("#PestID").val();

    if (!prmIDPest) {
        prmIDPest = 0;
    }

    var parameters = { "prmIDSemana": prmIDSemana, "prmIDUnidadesNegocios": prmIDUnidadesNegocios, "prmIDInvernadero": prmIDInvernadero, "prmBlancoBiologico": prmIDPest}

    jQuery.ajax({
        url: "/Phytosanity_Flat/getPhytosanity_FlatPdf",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
        async: false
    });
}

function validForm() {
    if (jQuery('#ddlWeek').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarSemana"))
        jQuery('#ddlWeek').focus();
        return false;
    } else {
        if (jQuery('#ddlWeek').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarSemana"))
            jQuery('#ddlWeek').focus();
            return false;
        };
    }

    if (jQuery('#ddlBusinessUnit').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarFinca"))
        jQuery('#ddlBusinessUnit').focus();
        return false;
    } else {
        if (jQuery('#ddlBusinessUnit').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarFinca"))
            jQuery('#ddlBusinessUnit').focus();
            return false;
        };
    }

    if (jQuery('#ddlGreenhouse').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarBloque"))
        jQuery('#ddlGreenhouse').focus();
        return false;
    } else {
        if (jQuery('#ddlGreenhouse').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarBloque"))
            jQuery('#ddlGreenhouse').focus();
            return false;
        };
    }

    return true;
}

function setPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, prmTipoConsulta, prmIDPest ) {
    BlockScreen();

    var parameters = { "prmIDSemana": prmIDSemana, "prmIDUnidadesNegocios": prmIDUnidadesNegocios, "prmIDInvernadero": prmIDInvernadero, "prmTipoConsulta": prmTipoConsulta, "prmBlancoBiologico": prmIDPest }

    jQuery.ajax({
        url: "/Phytosanity_Flat/setPhytosanity_Flat",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (prmTipoConsulta == 0) {
                $('#Phytosanity_FlatParDataTable').show();
                $('#Phytosanity_FlatParDataTable').html(data);
            }
            if (prmTipoConsulta == 1) {
                $('#Phytosanity_FlatImparDataTable').show();
                $('#Phytosanity_FlatImparDataTable').html(data);
            }

            if (prmTipoConsulta == 6) {
                $('#Phytosanity_FlatADataTable').show();
                $('#Phytosanity_FlatADataTable').html(data);
            }
            if (prmTipoConsulta == 7) {
                $('#Phytosanity_FlatBDataTable').show();
                $('#Phytosanity_FlatBDataTable').html(data);
            }
            if (prmTipoConsulta == 8) {
                $('#Phytosanity_FlatCDataTable').show();
                $('#Phytosanity_FlatCDataTable').html(data);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });
}

function getPlagaColor(strPlaga) {
    BlockScreen();

    var parameters = { "prmIstrPlagaDSemana": strPlaga }


    jQuery.ajax({
        url: "/Phytosanity_Flat/getPlagaColor",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (prmTipoConsulta == 0) {
                $('#Phytosanity_FlatParDataTable').show();
                $('#Phytosanity_FlatParDataTable').html(data);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });

}