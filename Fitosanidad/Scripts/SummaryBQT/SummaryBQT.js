﻿jQuery(document).ready(function () {
    jQuery("#startDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
        dateFormat: 'dd-MM-yy',
        onSelect: function (dateText) {
            if (!ValidateDate(jQuery("#startDate").val(), jQuery("#endDate").val())) {
                Notifications("error", GetResource("LaFechaInicialNoPuedeSerPosteriorALaFechaFinal"))
                $('#startDate').focus();
            }
        }
    });
    jQuery('#startDate').datepicker('setDate', 'today');
    jQuery('#startDate').attr("autocomplete", "off");

    jQuery("#dataExport").click(function () {
        jQuery("#tableLabelPrint").table2excel({
            exclude: ".noExl",
            name: "Worksheet Name",
            filename: "Rendimientos" //do not include extension
        });
    });

    $("#tableLabelPrint").DataTable();

    var total = 0;
    $("#tableLabelPrint tbody tr").find("td:eq(6)").each(function () {
        valor = $(this).html();
        total += parseInt(valor)
    });
    jQuery("#Total").html(total);


    /////////////////////
    //jQuery('#tableLabelPrint').dataTable({
    //    "info": false
    //    , paging: true
    //    , responsive: true
    //    , destroy: true
    //    , "columnDefs": [
    //        {
    //            "targets": [4, 5],
    //            "className": "text-right",
    //        },
    //        { className: "dt-left", "width": "05%", "targets": 0 }
    //        , { "width": "10%", "targets": 1 }
    //        , { "width": "15%", "targets": 2 }
    //        , { "width": "15%", "targets": 3 }
    //        , { "width": "05%", "targets": 4 }
    //        , { "width": "05%", "targets": 5 }            
    //        , {
    //            className: "text-center", type: 'percent', "width": "05%", "targets": 13
    //            , "render": function (data, type, full, meta) {
    //                return +data + '%';
    //            }
    //        }
    //    ]
    //    , "createdRow": function (row, data, index) {
    //        if (data[5] >= 0 && data[5] <= 5) {
    //            $('td', row).eq(5).css('color', 'white');
    //            $('td', row).eq(5).css('background-color', '#00B050');
    //        }
    //        if (data[5] > 5 && data[5] <= 15) {
    //            $('td', row).eq(5).css('color', 'white');
    //            $('td', row).eq(5).css('background-color', '#FFC000');
    //        }
    //        if (data[5] > 15) {
    //            $('td', row).eq(5).css('color', 'white');
    //            $('td', row).eq(5).css('background-color', '#FF0000');
    //        }
    //    }
    //    , "columns": [
    //        { 'data': 0 },
    //        { 'data': 1 },
    //        { 'data': 2 },
    //        { 'data': 3 },
    //        { 'data': 4 },
    //        { 'data': 5 },            
    //    ]
    //    , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    //});
    ////////////////////

});

function onClick() {

    var Fecha = jQuery("#startDate").val();
    var strDate = $("#startDate").val().split("-")
    var Fecha2 = new Date(strDate[2], strDate[1] - 1, strDate[0])


    //var Fecha = jQuery("#startDate").val();
    var strDate = $("#startDate").val().split("-")
    var srtDateIni = new Date(strDate[2], strDate[1] - 1, strDate[0])
    //var strDate = $("#endDate").val().split("-")
    //var srtDateFin = new Date(strDate[2], strDate[1] - 1, strDate[0])



    ////var strDate = jQuery("#startDate").val();
    ////var startDate = new Date(strDate); 
    

    ////var values = { "startDate": startDate, "endDate": endDate }
    //var Fecha = jQuery("#Fecha").val();
    

    ////var CentroOperacionID = jQuery("#lstCentrosOperacion").find(":selected").val();
    //var values = { "Fecha": Fecha }
    var values = { "Fecha": strDate }
    
    

    jQuery.ajax({
        url: "/SummaryBQT/Summary",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            jQuery('#tableLabelPrint').html(data);
            var total = 0;
            $("#tableLabelPrint tbody tr").find("td:eq(6)").each(function () {
                valor = $(this).html();
                total += parseInt(valor)
            });
            jQuery("#Total").html(total);
        },
        error: function (xhr, textStatus, errorThrown) {
            Notifications("error", errorThrown)
        },
    });
}
