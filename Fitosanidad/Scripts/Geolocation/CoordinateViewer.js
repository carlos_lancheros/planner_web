﻿var map
    , intCoordinates
    , arrCoordinates = {}
    , currentLatLng

var getMarkerUniqueId = function (lat, lng) {
    return lat + '_' + lng;
}
var getLatLng = function (lat, lng) {
    return new google.maps.LatLng(lat, lng);
};

$(document).ready(function () {

    jQuery('#tableCoordinateViewerList').DataTable({
        "searching": false
        , "info": true
        , paging: true
        , responsive: true
        , "columnDefs": [
            {
                "targets": [0, 1, 2],
                "className": "text-right",
            },
            { className: "dt-right", "width": "10%", "targets": 0 }
            , { "width": "20%", "targets": 1 }
            , { "width": "20%", "targets": 2 }
            , { "width": "20%", "targets": 3 }
            , { "width": "15%", "targets": 4 }
        ]
    });

    initMap();

});

function Upload() {
    var file = $('input[type="file"]').val();
    var exts = ['csv'];
    if (file) {
        var get_ext = file.split('.');
        get_ext = get_ext.reverse();
        if ($.inArray(get_ext[0].toLowerCase(), exts) > -1) {
            Notifications("warning", GetResource("CargandoInformacionDesdeElArchivo"))
        } else {
            Notifications("error", GetResource("TipoDeArchivoNoValido"))
            $('#UploadFile').focus();
            return false;
        }
    }

    var data = new FormData();
    var files = $("#UploadFile").get(0).files;
    if (files.length > 0) {
        data.append("HelpSectionImages", files[0]);
    }
    else {
        Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
        $('#UploadFile').focus();
        return false;
    }
    var extension = $("#UploadFile").val().split('.').pop().toUpperCase();
    if (extension != "CSV") {
        Notifications("error", GetResource("TipoDeArchivoNoValido"))
        $('#UploadFile').focus();
        return false;
    }

    $.ajaxSetup({ cache: false });
    BlockScreen();
    $.ajax({
        type: "POST",
        processData: false,
        url: "/Geolocation/UploadCoordinateViewer",
        data: data,
        dataType: "Json",
        contentType: false,
        success: function (data) {
            var tableCoordinateViewerList = jQuery('#tableCoordinateViewerList').DataTable();

            $.each(data, function (i, data) {
                var datTime = data.CoordinateDate.replace(/\/Date\((-?\d+)\)\//, '$1');
                var datForm = new Date(parseInt(datTime)).toLocaleString();

                tableCoordinateViewerList.row.add([
                    data.CoordinateId,
                    data.CoordinateLatitude,
                    data.CoordinateLongitude,
                    datForm,
                    '',
                    //"<div class='btn-group'>" +
                    //"<button class='btn btn-xs' type='button' data-toggle='tooltip' data-placement='top' title='" + GetResource("Borrar") + "' onclick='delCoordinateViewerList(" + data.CoordinateId + ")'>" +
                    //"<i class='fa fa-trash-o text-danger'></i>" +
                    //"</button>" +
                    //"</div>"
                ]).node().id = "rowId" + data.CoordinateId;
            });

            tableCoordinateViewerList.draw(false);

            $('input[type="file"]').val("");

            GetMap();

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
        async: false
    });
};

function GetMap() {
    if (!jQuery.fn.DataTable.isDataTable('#tableCoordinateViewerList')) {
        Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
        return false;
    }
    var table = jQuery('#tableCoordinateViewerList').DataTable();

    if (!table.data().any()) {
        Notifications("error", GetResource("DebeSeleccionarArchivoACargar"))
        return false;
    }

    var dd = table.rows().data().toArray();
    $.each(dd, function (index, value) {
        if (value.length > 0) {
            createMarker(map, getLatLng(value[1], value[2]), value[3], "", "");
        }
    });

    var bounds = new google.maps.LatLngBounds();
    $.each(arrCoordinates, function (index, marker) {
        bounds.extend(marker.position);
    });
    map.fitBounds(bounds);
};

function initMap() {
    map = new google.maps.Map(document.getElementById('map_canvas'), {
        center: { lat: 0, lng: 0 },
        zoom: 15,
        mapTypeId: 'hybrid',
    });
    infoWindow = new google.maps.InfoWindow;

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            currentLatLng = pos;

            infoWindow.setPosition(pos);
            infoWindow.setContent(GetResource("SuUbicacionActual"));
            infoWindow.open(map);
            map.setCenter(pos);
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        handleLocationError(false, infoWindow, map.getCenter());
    }
};

function createMarker(map, latLng, title, label, icon) {
    //https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2_hdpi.png
    //http://maps.google.com/mapfiles/ms/icons/green-dot.png

    var iconMarker = ($.trim(icon).length <= 0 ? "http://maps.google.com/mapfiles/ms/icons/green-dot.png" : icon)

    var lat = latLng.lat();
    var lng = latLng.lng();
    var markerId = getMarkerUniqueId(lat, lng);

    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        icon: new google.maps.MarkerImage(iconMarker),
        //label: {
        //    text: label,
        //    color: "white"
        //},
        title: title,
        animation: google.maps.Animation.DROP,
        id: 'marker_' + markerId
    });

    arrCoordinates[markerId] = marker;

    google.maps.event.addListener(marker, "mouseover", function (evt) {
        var label = this.getLabel();
        //label.color = "black";
        this.setLabel(label);
    });
    google.maps.event.addListener(marker, "mouseout", function (evt) {
        var label = this.getLabel();
        //label.color = "white";
        this.setLabel(label);
    });
    return marker;
}
