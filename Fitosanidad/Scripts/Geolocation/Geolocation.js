﻿var map
    , intSensors
    , intExternalPlates
    , intCoordinates
    , intPhotometers
    , arrCoordinates = {}
    , currentLatLng

var getMarkerUniqueId = function (lat, lng) {
    return lat.toFixed(8) + '_' + lng.toFixed(8);
}

$(document).ready(function () {
    $('#divData').hide();

    $('#fun').val("F");

    jQuery('#tableCoordinatesList').DataTable({
        //"searching": false
        //, "info": false
        //, paging: false
        //,responsive: true
        //, "columns": [
        //    { "width": "10%" }
        //    , null
        //    , null
        //    , { "width": "15%" }
        //],
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    });

    jQuery('#tableExternalPlatesList').DataTable({
        "searching": false
        , "info": false
        , paging: true
        , responsive: true
        , "columnDefs": [
            { "width": "10%", "targets": 0 }
            , { "width": "10%", "targets": 1 }
            , { "width": "20%", "targets": 2 }
        ]
    });

    FormInit();

    $("#ddlBusinessUnit").change(function () {
        setdivData();
        FormInit();
        getFormData();
        getGeolocation();
    });

    $("#ddlOptions").change(function () {
        setdivData();
        getOptionsByParentId();
        getGeolocation();
    });

    $("#ddlDetails").change(function () {
        setdivData();
        getFormData();
        getGeolocation();
    });

    $('#radioBtn a').on('click', function () {
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#' + tog).prop('value', sel);

        $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');

        getGeolocation();
    })
});

function getOptionsByParentId() {
    FormInit();

    var ParentId = $("#ddlOptions").val();

    var parameters = { "ParentId": ParentId};

    $("#ddlDetails").empty();

    BlockScreen();
    jQuery.ajax({
        url: "/Geolocation/getOptionsByParentIdJson",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $("#ddlDetails").append('<option value="">'+ GetResource("Seleccionar") + '</option>');

            $.each(data, function (i, data) {
                $("#ddlDetails").append('<option value="'
                    + data.Value + '">'
                    + data.Text + '</option>');
            });

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#ddlBusinessUnit")).text());
        },
        async: false
    });
}

function FormInit() {
    initMap();

    delMapCoordinates();

    $("#Sensores").hide();
    delSensorsTable();

    $("#PlacasExternas").hide();
    delExternalPlatesTable();

    $("#Coordenadas").hide();
    delCoordinatesTable();

    $("#Photometer").hide();
    delPhotometersTable();
}

function getFormData() {
    FormInit();

    var FormId = parseInt($("#ddlDetails").find(":selected").val());
    switch (FormId) {
        case 5:
            //Sensors
            $('#divData').show();
            $("#Sensores").show();
            break;

        case 6:
            //External Plates
            $('#divData').show();
            $("#PlacasExternas").show();
            break;

        case 11:
            //Get Coordinates
            $('#divData').show();
            $("#Coordenadas").show();
            break;

        case 12:
            //Photometer
            $('#divData').show();
            $("#Photometer").show();
            break;

        default:
    }
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map_canvas'), {
        center: { lat: 0, lng: 0 },
        zoom: 15,
        mapTypeId: 'hybrid',
    });
    infoWindow = new google.maps.InfoWindow;

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            currentLatLng = pos;

            infoWindow.setPosition(pos);
            infoWindow.setContent(GetResource("SuUbicacionActual"));
            infoWindow.open(map);
            map.setCenter(pos);
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        handleLocationError(false, infoWindow, map.getCenter());
    }
};

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: ' + GetResource("ElServicioDeGeolocalizacionFallo") :
        'Error: ' + GetResource("SuNavegadorNoAdmiteLaGeolocalizacion"));
    infoWindow.open(map);
}

function getGeolocation() {
    var strFarmName = "";
    var arrFarmCoordinate = [];
    var arrGreehouseCoords = [];

    var intBusinessId = parseInt($("#ddlBusinessUnit").find(":selected").val());
    var strQueryFilter = $('#fun').val();

    var parameters = { "intBusinessId": intBusinessId, "strQueryFilter": strQueryFilter };

    BlockScreen();
    jQuery.ajax({
        url: "/Geolocation/getMapCoordinates",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery.each(data, function (i, arrCoordinates) {
                if (arrCoordinates.GreenhouseId == 0) {
                    arrFarmCoordinate[0] = arrCoordinates.CoordinateLatitude;
                    arrFarmCoordinate[1] = arrCoordinates.CoordinateAltitude;

                    strFarmName = arrCoordinates.Name;
                } else {
                    if (i == 0) {
                        arrFarmCoordinate[0] = arrCoordinates.CoordinateLatitude;
                    }
                    if (i == Object.keys(arrCoordinates).length) {
                        arrFarmCoordinate[1] = arrCoordinates.CoordinateAltitude;
                    }
                };
                arrGreehouseCoords[i] = [arrCoordinates.GreenhouseId, arrCoordinates.CoordinateOrder, arrCoordinates.CoordinateLatitude, arrCoordinates.CoordinateAltitude];
            });
            getBusinessMap(arrFarmCoordinate, strFarmName, arrGreehouseCoords);
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#ddlBusinessUnit")).text());
        },
        async: false
    });
}

function getBusinessMap(arrCenterCoordinates, strBusinessName, arrPolygonCoordinates) {
    iniForm();

    if (arrCenterCoordinates.length <= 0 || arrPolygonCoordinates.length <= 0) {
        initMap();
        Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#ddlBusinessUnit")).text());
        return false;
    }

    var intBusinessId = parseInt($("#ddlBusinessUnit").find(":selected").val());

    var infowindow;
    var infowindowFarm;
    var strClass = '';
    var strHeader = '';
    var strSubHeader = '';
    var strBody = '';
    var strFooter = '';
    var intGreenhouseAct = 0;

    var myLatLng = { lat: arrCenterCoordinates[0], lng: arrCenterCoordinates[1] };

    var map = new google.maps.Map(document.getElementById('map_canvas'), {
        zoom: 15,
        center: myLatLng,
        mapTypeId: 'hybrid',
        heading: 90,
        tilt: 45,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.TOP_CENTER
        },
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        scaleControl: true,
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
        fullscreenControl: true
    });

    var markerFarm = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: strBusinessName,
        animation: google.maps.Animation.DROP,
    });

    var strStrokeColor = "#8482C6";
    var strFillColor = "#8482C6";

    var strQueryFilter = $('#fun').val();
    if (strQueryFilter == "F" || strQueryFilter == "T") {
        var arrFarmCoordinates = [];
        var intFarmCoords = 0;
        for (var i = 0; i < arrPolygonCoordinates.length; i++) {
            if (arrPolygonCoordinates[i][0] == 0) {
                arrFarmCoordinates[intFarmCoords] = new google.maps.LatLng(arrPolygonCoordinates[i][2], arrPolygonCoordinates[i][3])
                intFarmCoords++;
            }
            else {
                break;
            }
        }
        if (arrFarmCoordinates.length > 0) {
            setMapPolygon(map, arrFarmCoordinates, strStrokeColor, strFillColor);
        }
    };

    var FormId = parseInt($("#ddlDetails").find(":selected").val());
    switch (FormId) {
        case 5:
            //Sensors
            delSensorsTable();
            intSensors = 1;

            var intBusinessId = parseInt($("#ddlBusinessUnit").find(":selected").val());

            var parameters = { "intBusinessId": intBusinessId };

            BlockScreen();
            jQuery.ajax({
                url: "/Geolocation/getMapCoordinatesSensorsByBusinessUnitAndGreenHouseID",
                data: JSON.stringify(parameters),
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    var tblSensorsList = jQuery('#tableSensorsList').DataTable({
                        "bDestroy": true,
                        "scrollX": true,
                    });

                    //Data Table
                    $.each(data, function (i, data) {
                        tblSensorsList.row.add([
                            data.IOT_SensorListID,
                            data.ExternalID,
                            data.Name,
                            data.Codigo,
                            data.Latitude,
                            data.Longitude,
                        ]).node().id = "rowId" + intSensors;

                        intSensors++;
                    });
                    tblSensorsList.draw(false);
                    //Data Table

                    //Map Coordinates
                    if (data.length > 0) {
                        $.each(data, function (i, data) {
                            if (data.Latitude != null && data.Longitude != null) {
                                strClass = 'infobubble';
                                strHeader = $("option:selected", $("#ddlBusinessUnit")).text();
                                strSubHeader = GetResource("Id") + ': ' + data.IOT_SensorListID + '<br>' +
                                    GetResource("IdSensor") + ': ' + data.ExternalID + '<br>' +
                                    GetResource("Ubicacion") + ': ' + data.Name + '<br>' +
                                    GetResource("IdInvernadero") + ': ' + data.Codigo;
                                strBody = '';
                                strFooter = GetResource("Latitud") + ': ' + data.Latitude + '<br>' +
                                    GetResource("Longitud") + ': ' + data.Longitude;

                                strBody = 
                                        '      <table class = "' + strClass + '">' +
                                        '         <tbody>' +
                                        '			<tr style="text-align:center; font-size:0.7em">' +
                                        '				<td colspan="2" >' +
                                        GetResource("Humedad") +
                                        '				</td>' +
                                        '				<td colspan="2" >' +
                                        GetResource("Temperatura") +
                                        '				</td>' +
                                        '				<td colspan="2" >' +
                                        GetResource("Luminosidad") +
                                        '				</td>' +
                                        '			</tr>' +
                                        '			<tr style="text-align:center">' +
                                        '				<td style="text-align:right; font-size:0.7em">' +
                                        '0' + ' %' +
                                        '				</td>' +
                                        '				<td rowspan="2" style="font-size:2em">' +
                                        '					<i class="fa fa-tint" aria-hidden="true"></i>' +
                                        '				</td>' +
                                        '				<td style="text-align:right; font-size:0.7em">' +
                                        '0' + ' °C' +
                                        '				</td>' +
                                        '				<td rowspan="2" style="font-size:2em">' +
                                        '					<i class="fa fa-thermometer-half" aria-hidden="true"></i>' +
                                        '				</td>' +
                                        '				<td style="text-align:right; font-size:0.7em">' +
                                        '0' + ' lm' +
                                        '				</td>' +
                                        '				<td rowspan="2" style="font-size:2em">' +
                                        '					<i class="fa fa-sun-o" aria-hidden="true"></i>' +
                                        '				</td>' +
                                        '			</tr>' +
                                        '			<tr style="text-align:center; font-size:0.7em">' +
                                        '				<td colspan="2">' +
                                        '0' +
                                        '				</td>' +
                                        '				<td colspan="2">' +
                                        '0' +
                                        '				</td>' +
                                        '				<td colspan="2">' +
                                        '0' +
                                        '				</td>' +
                                        '			</tr>' +
                                        '         </tbody>' +
                                        '      </table>';

                                infowindow = setInfoWindow(FormId, intGreenhouseAct, strClass, strHeader, strSubHeader, strBody, strFooter);

                                createMarker(map, new google.maps.LatLng(data.Latitude, data.Longitude), "", infowindow, "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png");
                            }
                        });

                        if (Object.keys(arrCoordinates).length > 0) {
                            var bounds = new google.maps.LatLngBounds();
                            $.each(arrCoordinates, function (index, marker) {
                                bounds.extend(marker.position);
                            });
                            map.fitBounds(bounds);
                        }
                    }
                    //Map Coordinates

                    UnlockScreen();
                },
                error: function (xhr, textStatus, errorThrown) {
                    UnlockScreen();
                    Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#ddlBusinessUnit")).text());
                },
                async: false
            });

            break;

        case 6:
            //External Plates
            delExternalPlatesTable();
            intExternalPlates = 1;

            var intBusinessId = parseInt($("#ddlBusinessUnit").find(":selected").val());

            var parameters = { "intBusinessId": intBusinessId };

            BlockScreen();
            jQuery.ajax({
                url: "/Geolocation/getMapCoordinatesPlatesbyBusinessUnit",
                data: JSON.stringify(parameters),
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    var tblExternalPlatesList = jQuery('#tableExternalPlatesList').DataTable();

                    //Data Table
                    $.each(data, function (i, data) {
                        tblExternalPlatesList.row.add([
                            data.ID,
                            data.PlateId,
                            data.PlateName,
                            data.CoordinateLatitude,
                            data.CoordinateLongitude,
                        ]).node().id = "rowId" + intExternalPlates;

                        intExternalPlates++;
                    });
                    tblExternalPlatesList.draw(false);
                    //Data Table

                    //Map Coordinates
                    if (data.length > 0) {
                        $.each(data, function (i, data) {
                            var strTitle = data.PlateName + '\nLatitud: ' + + data.CoordinateLatitude + '\nLongitud: ' + + data.CoordinateLongitude;
                            createMarker(map, new google.maps.LatLng(data.CoordinateLatitude, data.CoordinateLongitude), strTitle, "", "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png");
                        });

                        if (Object.keys(arrCoordinates).length > 0) {
                            var bounds = new google.maps.LatLngBounds();
                            $.each(arrCoordinates, function (index, marker) {
                                bounds.extend(marker.position);
                            });
                            map.fitBounds(bounds);
                        }
                    }
                    //Map Coordinates

                    UnlockScreen();
                },
                error: function (xhr, textStatus, errorThrown) {
                    UnlockScreen();
                    Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#ddlBusinessUnit")).text());
                },
                async: false
            });

            break;

        case 11:
            //Get Coordinates
            delCoordinatesTable();
            intCoordinates = 1;

            google.maps.event.addListener(map, 'click', function (event) {
                var getCoordinates = confirm(event.latLng);
                if (getCoordinates == true) {
                    var tblCoordinatesList = jQuery('#tableCoordinatesList').DataTable();

                    tblCoordinatesList.row.add([
                        intCoordinates,
                        event.latLng.lat(),
                        event.latLng.lng(),
                        "<div class='btn-group'>" +
                        "<button class='btn btn-xs' type='button' data-toggle='tooltip' data-placement='top' title='" + GetResource("Editar") + "' onclick='delCoordinatesList(" + intCoordinates + ")'>" +
                                "<i class='fa fa-trash-o text-danger'></i>" +
                            "</button>" +
                        "</div>"
                    ]).node().id = "rowId" + intCoordinates;
                    tblCoordinatesList.draw(false);

                    createMarker(map, new google.maps.LatLng(event.latLng.lat(), event.latLng.lng()), String(intCoordinates), String(intCoordinates), "");

                    intCoordinates++;
                }
            });

            break;

        case 12:
            //Photometer
            delPhotometersTable();
            intPhotometers = 1;

            var intBusinessId = parseInt($("#ddlBusinessUnit").find(":selected").val());

            var parameters = { "intBusinessId": intBusinessId };

            BlockScreen();
            jQuery.ajax({
                url: "/Geolocation/getMapCoordinatesPhotometerbyBusinessUnit",
                data: JSON.stringify(parameters),
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    var tblPhotometerList = jQuery('#tablePhotometerList').DataTable();

                    //Data Table
                    $.each(data, function (i, data) {
                        var datTime = data.Date.replace(/\/Date\((-?\d+)\)\//, '$1');
                        var datForm = new Date(parseInt(datTime)).toLocaleString();

                        tblPhotometerList.row.add([
                            datForm,
                            data.Product,
                            data.Bed,
                            data.GPSLatitude,
                            data.GPSLongitude,
                        ]).node().id = "rowId" + intPhotometers;

                        intPhotometers++;
                    });
                    tblPhotometerList.draw(false);
                    //Data Table

                    //Map Coordinates
                    if (data.length > 0) {
                        $.each(data, function (i, data) {
                            var datTime = data.Date.replace(/\/Date\((-?\d+)\)\//, '$1');
                            var datForm = new Date(parseInt(datTime)).toLocaleString();

                            var strTitle = data.Product + '\n' + datForm + '\nLumens 1: ' + + data.LumensV1 + '\nLumens 2: ' + + data.LumensV2 + '\nLumens 3: ' + + data.LumensV3;
                            createMarker(map, new google.maps.LatLng(data.GPSLatitude, data.GPSLongitude), strTitle, "", "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png");
                        });

                        if (Object.keys(arrCoordinates).length > 0) {
                            var bounds = new google.maps.LatLngBounds();
                            $.each(arrCoordinates, function (index, marker) {
                                bounds.extend(marker.position);
                            });
                            map.fitBounds(bounds);
                        }
                    }
                    //Map Coordinates

                    UnlockScreen();
                },
                error: function (xhr, textStatus, errorThrown) {
                    UnlockScreen();
                    Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#ddlBusinessUnit")).text());
                },
                async: false
            });

            break;

    }

    var decDistance = getDistanceToFarm(currentLatLng, myLatLng);
    decDistance = ReplaceNumberWithCommas(decDistance / 1000);
    $("#DistanceFromThisLocation").val(parseFloat(decDistance).toFixed(3) + " Km Lineales");

    var strStrokeColor = "#AAD700";
    var strFillColor = "#AAD700";
    if (strQueryFilter == "I" || strQueryFilter == "T") {
        var arrGreenhouseCoordinates = [];
        var intGreenhouseCoords = 0;
        intGreenhouseAct = 0;
        infowindow = undefined;

        arrRHUM = [];
        arrTEMP = [];
        arrLUM = [];
        arrTmp = [];

        strClass = '';
        strHeader = '';
        strSubHeader = '';
        strBody = '';
        strFooter = '';

        for (var i = 0; i < arrPolygonCoordinates.length; i++) {
            if (intGreenhouseAct != arrPolygonCoordinates[i][0] && intGreenhouseAct != 0) {
                if (arrGreenhouseCoordinates.length > 0) {
                    setMapPolygon(map, arrGreenhouseCoordinates, strStrokeColor, strFillColor);

                    switch (FormId) {
                        case 5:
                            //strClass = '';
                            //strHeader = '';
                            //strSubHeader = '';
                            //strBody = '';
                            //strFooter = '';

                            //for (var j = 0; j < arrSensor.length; j++) {
                            //    if (arrSensor[j][0].GreenHouseID == intGreenhouseAct) {
                            //        arrRHUM = [];
                            //        arrTEMP = [];
                            //        arrLUM = [];
                            //        arrTmp = [];

                            //        arrTmp.push(arrSensor[j][0].Value);
                            //        arrTmp.push(arrSensor[j][0].Data);

                            //        var datTime = arrSensor[j][0].Time.replace(/\/Date\((-?\d+)\)\//, '$1');
                            //        var datForm = new Date(parseInt(datTime)).toLocaleString();
                            //        arrTmp.push(datForm);

                            //        if (arrRHUM.length <= 0 && arrSensor[j][0].Value == "RHUM") {
                            //            arrRHUM.push(arrTmp);
                            //        };
                            //        if (arrTEMP.length <= 0 && arrSensor[j][0].Value == "TEMP") {
                            //            arrTEMP.push(arrTmp);
                            //        };
                            //        if (arrLUM.length <= 0 && arrSensor[j][0].Value == "LUM ") {
                            //            arrLUM.push(arrTmp);
                            //        };
                            //    }
                            //}
                            //if (arrRHUM || arrTEMP || arrLUM) {
                            //    if (arrRHUM.length > 0 || arrTEMP.length > 0 || arrLUM.length > 0) {
                            //        strClass = "infobubble";
                            //        strHeader = $("option:selected", $("#ddlBusinessUnit")).text();
                            //        strSubHeader = (intGreenhouseAct <= 0 ? GetResource("NombreAplicacion") : GetResource("Invernadero") + ': ' + intGreenhouseAct);
                            //        strBody = '<table class = "' + strClass + '">' +
                            //            '         <tbody>' +
                            //            '			<tr style="text-align:center; font-size:0.7em">' +
                            //            '				<td colspan="2" >' +
                            //            GetResource("Humedad") +
                            //            '				</td>' +
                            //            '				<td colspan="2" >' +
                            //            GetResource("Temperatura") +
                            //            '				</td>' +
                            //            '				<td colspan="2" >' +
                            //            GetResource("Luminosidad") +
                            //            '				</td>' +
                            //            '			</tr>' +
                            //            '			<tr style="text-align:center">' +
                            //            '				<td style="text-align:right; font-size:0.7em">' +
                            //            (arrRHUM.length > 0 ? arrRHUM[0][1] + ' %' : '') +
                            //            '				</td>' +
                            //            '				<td rowspan="2" style="font-size:2em">' +
                            //            '					<i class="fa fa-tint" aria-hidden="true"></i>' +
                            //            '				</td>' +
                            //            '				<td style="text-align:right; font-size:0.7em">' +
                            //            (arrTEMP.length > 0 ? arrTEMP[0][1] + ' °C' : '') +
                            //            '				</td>' +
                            //            '				<td rowspan="2" style="font-size:2em">' +
                            //            '					<i class="fa fa-thermometer-half" aria-hidden="true"></i>' +
                            //            '				</td>' +
                            //            '				<td style="text-align:right; font-size:0.7em">' +
                            //            (arrLUM.length > 0 ? arrLUM[0][1] + ' lm' : '') +
                            //            '				</td>' +
                            //            '				<td rowspan="2" style="font-size:2em">' +
                            //            '					<i class="fa fa-sun-o" aria-hidden="true"></i>' +
                            //            '				</td>' +
                            //            '			</tr>' +
                            //            '			<tr style="text-align:center; font-size:0.7em">' +
                            //            '				<td colspan="2">' +
                            //            (arrRHUM.length > 0 ? arrRHUM[0][2] : '') +
                            //            '				</td>' +
                            //            '				<td colspan="2">' +
                            //            (arrTEMP.length > 0 ? arrTEMP[0][2] : '') +
                            //            '				</td>' +
                            //            '				<td colspan="2">' +
                            //            (arrLUM.length > 0 ? arrLUM[0][2] : '') +
                            //            '				</td>' +
                            //            '			</tr>' +
                            //            '         </tbody>' +
                            //            '      </table>';

                            //        infowindow = setInfoWindow(FormId, intGreenhouseAct, strClass, strHeader, strSubHeader, strBody, strFooter);

                            //        marker = new google.maps.Marker({
                            //            position: new google.maps.LatLng(arrPolygonCoordinates[i][2], arrPolygonCoordinates[i][3]),
                            //            map: map,
                            //            animation: google.maps.Animation.DROP,
                            //        });
                            //        marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png')

                            //        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            //            return function () {
                            //                infowindow.setContent(1);
                            //                infowindow.open(map, marker);
                            //            }
                            //        })(marker, i));

                            //        google.maps.event.addListener(marker, 'mouseout', (function (marker, i) {
                            //            return function () {
                            //                infowindow.close(map, marker);
                            //            }
                            //        })(marker, i));

                            //        arrRHUM = undefined;
                            //        arrTEMP = undefined;
                            //        arrLUM = undefined;
                            //        arrTmp = undefined;
                            //    }
                            //}
                            break;

                        default:
                    }

                    arrGreenhouseCoordinates = [];
                    intGreenhouseCoords = 0;
                }
            }
            if (arrPolygonCoordinates[i][0] != 0) {
                intGreenhouseAct = arrPolygonCoordinates[i][0];
                arrGreenhouseCoordinates[intGreenhouseCoords] = new google.maps.LatLng(arrPolygonCoordinates[i][2], arrPolygonCoordinates[i][3])
                intGreenhouseCoords++;
            }
        }
    };
};

function createMarker(map, latLng, title, infowindow, icon) {
    //https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2_hdpi.png
    //http://maps.google.com/mapfiles/ms/icons/green-dot.png

    var iconMarker = ($.trim(icon).length <= 0 ? "http://maps.google.com/mapfiles/ms/icons/green-dot.png" : icon)

    var lat = latLng.lat();
    var lng = latLng.lng();
    var markerId = getMarkerUniqueId(lat, lng);

    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        icon: new google.maps.MarkerImage(iconMarker),
        //label: {
        //    text: label,
        //    color: "white"
        //},
        title: title,
        animation: google.maps.Animation.DROP,
        id: 'marker_' + markerId
    });

    arrCoordinates[markerId] = marker;

    google.maps.event.addListener(marker, "mouseover", function (evt) {
        var label = this.getLabel();
        //label.color = "black";
        this.setLabel(label);

        if ($.trim(infowindow).length > 0) {
            infowindow.open(map, marker);
        }
    });
    google.maps.event.addListener(marker, "mouseout", function (evt) {
        var label = this.getLabel();
        //label.color = "white";
        this.setLabel(label);

        if ($.trim(infowindow).length > 0) {
            infowindow.close(map, marker);
        }
    });
    return marker;
}

function getCenterPolygon(arrCoordinates) {
    arrReturn = [0.0, 0.0];

    arrCoordinates.forEach(function (valor, indice, array) {
        arrReturn[0] = arrReturn[0] + valor[0];
        arrReturn[1] = arrReturn[1] + valor[1];
    });

    totalPoints = Object.keys(arrCoordinates).length;
    arrReturn[0] = arrReturn[0] / totalPoints;
    arrReturn[1] = arrReturn[1] / totalPoints;

    return arrReturn;
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        GetResource("ElServicioDeGeolocalizacionFallo") :
        GetResource("SuNavegadorNoAdmiteLaGeolocalizacion"));
    infoWindow.open(map);
}

function GetGreenhousebyBusinessUnit() {
    var intBusinessId = $("#ddlBusinessUnit").find(":selected").val();
    var parameters = { "intBusinessId": intBusinessId };

    BlockScreen();
    jQuery.ajax({
        url: "/Geolocation/getGreenhousebyBusinessUnit",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery('#GreenhouseDataTable').html(data);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            _key = errorThrown;
            UnlockScreen();
        },
        async: false
    });
}

function getGreenhouseArea(intBusinessUnitId, intGreenintGreenhouseId) {
    return 0;

    var intAreaInvernadero = 0;
    var parameters = { "intBusinessUnitId": intBusinessUnitId, "intGreenintGreenhouseId": intGreenintGreenhouseId };

    BlockScreen();
    jQuery.ajax({
        url: "/Geolocation/getGreenhouseArea",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length > 1) {
                intAreaInvernadero = data[0].AreaInvernadero;
            }
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("warning", "No se ha definido información.");
        },
        async: false
    });

    return intAreaInvernadero;
}

function getSensorData(intBusinessUnitId) {
    var arrSensorData = [];
    var parameters = { "intBusinessUnitId": intBusinessUnitId };

    BlockScreen();
    jQuery.ajax({
        url: "/Geolocation/getSensorData",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length > 1) {
                arrSensorData = data;
            }
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("warning", "No se ha definido información.");
        },
        async: false
    });

    return arrSensorData;
}

function setMapPolygon(map, arrPolygonCoordinates, strStrokeColor, strFillColor) {
    var mapPolygon = new google.maps.Polygon({
        paths: arrPolygonCoordinates,
        draggable: false,
        editable: false,
        strokeColor: strStrokeColor,
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: strFillColor,
        fillOpacity: 0.35
    });
    mapPolygon.setMap(map);

    google.maps.event.addListener(mapPolygon, "mouseover", function () {
        strFillColor = this.get('fillColor');
        strStrokeColor = this.get('strokeColor');
        this.setOptions({ fillColor: "#E20064" });
        this.setOptions({ strokeColor: "#FFFFFF" });
    });

    google.maps.event.addListener(mapPolygon, "mouseout", function () {
        this.setOptions({ fillColor: strFillColor });
        this.setOptions({ strokeColor: strStrokeColor });
    });
}

function setInfoWindow(FormId, intGreenhouseId, strClass, strHeader, strSubHeader, strBody, strFooter) {
    var strInfoWindow = "";

    switch(FormId) {
        case 5:
            strInfoWindow = setInfoWindowData(strClass, strHeader, strSubHeader, strBody, strFooter);

        default:
    }

    return new google.maps.InfoWindow({
        content: strInfoWindow
    });
}

function setInfoWindowData(strClass, strHeader, strSubHeader, strBody, strFooter) {
    var strInfoWindow = '';

    strInfoWindow = '<div style="width:100%">' +
        '	<table ' + (strClass.length > 0 ? 'class="' + strClass + '"': 'class = "infobubble"') + '>' +
        '		<tbody>' +
        '			<tr>' +
        '				<td style="text-align:center">' +
        strHeader +
        (strSubHeader.length > 0 ? '<br /><div style="font-size:0.7em;">' + strSubHeader + '</div>' : '') +
        '				</td>' +
        '			</tr>' +
        '			<tr style="text-align:center; font-size:0.7em">' +
        '				<td style="text-align:center">' +
        strBody +
        '				</td>' +
        '			</tr>' +
        (strFooter.length > 0 ? '<tr style="text-align:center; font-size:0.7em"><td style="text-align:center">' + strFooter + '</td></tr>' : '') +
        '		</tbody>' +
        '	</table>' +
        '</div>';

    return strInfoWindow;
}

function setWeatherInfo(arrRHUM, arrTEMP, arrLUM) {
    if (arrRHUM.length > 0) {
        jQuery("#strRHUM").val(arrRHUM[0][1] + "%");
        jQuery("#strRHUMTime").val(arrRHUM[0][2]);
    }
    if (arrTEMP.length > 0) {
        jQuery("#strTEMP").val(arrTEMP[0][1] + "°C");
        jQuery("#strTEMPTime").val(arrTEMP[0][2]);
    }
    if (arrLUM.length > 0) {
        jQuery("#strLUM").val(arrLUM[0][1] + "lm");
        jQuery("#strLUMTime").val(arrLUM[0][2]);
    }
}

function setGreenhouseDetails(intGreenhouseId, intGreenhouseCodigo, intAreaInvernadero) {
    iniForm();

    jQuery("#strGreenhouse").val(GetResource("Invernadero") + ": " + intGreenhouseCodigo);
    jQuery("#strGreenhouseDet").val(GetResource("Area") + ": " + intAreaInvernadero);

    var intBusinessId = parseInt($("#ddlBusinessUnit").find(":selected").val());

    var arrGreehouseDetails = [];
    arrGreehouseDetails = getGreenhouseDetails(intBusinessId, intGreenhouseId);

    var arrRHUM = [];
    var arrTEMP = [];
    var arrLUM = [];
    $(arrGreehouseDetails).each(function () {
        var arrTmp = [];

        arrTmp.push(this[0].Value);
        arrTmp.push(this[0].Data);

        var datTime = this[0].Time.replace(/\/Date\((-?\d+)\)\//, '$1');
        var datForm = new Date(parseInt(datTime)).toLocaleString();
        arrTmp.push(datForm);

        if (arrRHUM.length <= 0 && this[0].Value == "RHUM") {
            arrRHUM.push(arrTmp);
        };
        if (arrTEMP.length <= 0 && this[0].Value == "TEMP") {
            arrTEMP.push(arrTmp);
        };
        if (arrLUM.length <= 0 && this[0].Value == "LUM ") {
            arrLUM.push(arrTmp);
        };
    });

    setWeatherInfo(arrRHUM, arrTEMP, arrLUM);
};

function iniForm() {
    jQuery("#strGreenhouse").val("");
    jQuery("#strGreenhouseDet").val("");

    jQuery("#strRHUM").val("");
    jQuery("#strRHUMTime").val("");

    jQuery("#strTEMP").val("");
    jQuery("#strTEMPTime").val("");

    jQuery("#strLUM").val("");
    jQuery("#strLUMTime").val("");
};

function getDistanceToFarm(arrFrom, arrTo) {
    if (arrFrom && arrTo) {
        var R = 6378137;
        var dLat = getRad(arrTo.lat - arrFrom.lat);
        var dLong = getRad(arrTo.lng - arrFrom.lng);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(getRad(arrTo.lat)) * Math.cos(getRad(arrTo.lat)) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d;
    }

    return 0;
};

function getRad(x) {
    return x * Math.PI / 180;
};

function delCoordinatesList(intCoordinates) {
    var tblCoordinatesList = jQuery('#tableCoordinatesList').DataTable();

    var markerId = getMarkerUniqueId(tblCoordinatesList.row("#rowId" + intCoordinates).data()[1], tblCoordinatesList.row("#rowId" + intCoordinates).data()[2]);

    if (arrCoordinates[markerId]) {
        var marker = arrCoordinates[markerId];
        marker.setMap(map);
        delete arrCoordinates[markerId];
    }

    tblCoordinatesList.row("#rowId" + intCoordinates).remove().draw();
};

//Sensors
function delSensorsTable() {
    var tblSensorsList = jQuery('#tableSensorsList').DataTable();

    for (var i = 1; i <= intSensors; i++) {
        tblSensorsList.row("#rowId" + i).remove().draw();
    }
};

//Sensors

//External Plates
function delExternalPlatesTable() {
    var tblExternalPlatesList = jQuery('#tableExternalPlatesList').DataTable();

    for (var i = 1; i <= intExternalPlates; i++) {
        tblExternalPlatesList.row("#rowId" + i).remove().draw();
    }
};


//Get Coordinates
function delCoordinatesTable() {
    var tblCoordinateList = jQuery('#tableCoordinatesList').DataTable();

    for (var i = 1; i <= intCoordinates; i++) {
        tblCoordinateList.row("#rowId" + i).remove().draw();
    }
};

//Get Coordinates

//Photometer
function delPhotometersTable() {
    var tblPhotometerList = jQuery('#tablePhotometerList').DataTable();

    for (var i = 1; i <= intPhotometers; i++) {
        tblPhotometerList.row("#rowId" + i).remove().draw();
    }
};
//Photometer

function setdivData() {
    $('#divData').hide();
};


//Delete all markers in map
function delMapCoordinates() {
    var tblExternalPlatesList = jQuery('#tableExternalPlatesList').DataTable();

    $.each(arrCoordinates, function (key, value) {
        if (arrCoordinates[key]) {
            var marker = arrCoordinates[key];
            marker.setMap(map);
            delete arrCoordinates[key];
        }
    });
}
//Delete all markers in map
