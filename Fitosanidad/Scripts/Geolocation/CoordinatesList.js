﻿jQuery(document).ready(function () {
    jQuery("#btnExcel").click(function () {
        jQuery('<table>')
            .append(
            jQuery("#tableCoordinatesList").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Coordinates",
                filename: "Coordinates.xls"
            });
    });
    jQuery("#tableCoordinatesList").dataTable();
});