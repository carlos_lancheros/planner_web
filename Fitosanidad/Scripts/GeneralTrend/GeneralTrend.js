﻿jQuery(document).ready(function () {
    $("#VisorDinamico").hide();
    $("#Graficas").hide();

    $("#btnGraphic").click(function () {
        SearchGraficas();
    });
});

function Search() {

    $("#VisorDinamico").show();
    $("#Graficas").hide();

    var WeekIDInic = $("#WeekIDInic").val();
    var WeekIDFina = $("#WeekIDFina").val();
    var GreeHouseID = $("#GreeHouseID").val();
    var ProductID = $("#ProductID").val();
    var PestID = $("#PestID").val();


    if (WeekIDInic > WeekIDFina) {
        Notifications("error", "La semana inicial no puede ser mayor a la final, Verifique!");
        return;
    }

    var datos = {
        "WeekIDInic": WeekIDInic,
        "WeekIDFina": WeekIDFina,
        "GreeHouseID": GreeHouseID,
        "ProductID": ProductID,
        "PestID": PestID,
    }

    BlockScreen();

    var WeekIni = $("#WeekIDInic").val();
    var WeekFin = $("#WeekIDFina").val();
    var BusinessUnitID = $("#BusinessUnitID").val();
    if (BusinessUnitID == "") {
        BusinessUnitID = 0;
    }

    var datos = {
        "weekIni": WeekIni,
        "weekFin": WeekFin,
        "BusinessUnitID": BusinessUnitID,
    };
     

    jQuery.ajax({
        url: "/GeneralTrend/getReport",
        data: JSON.stringify(datos) ,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            var renderers = $.extend(
                $.pivotUtilities.renderers,
                $.pivotUtilities.plotly_renderers,
                $.pivotUtilities.c3_renderers,
                $.pivotUtilities.export_renderers
            );
            $("#pivDynamic").pivotUI(data, {
                rows: ["Finca","ClaseProducto", "Plaga",  "Umbral"],
                cols: ["Semana"],
                hiddenAttributes: [""],
                renderers: renderers,
                aggregatorName: "Sum",
                vals: ["Cant"],
                rendererName: "Table",
                hideTotals: true
            }, true);


            jQuery('#tableDynamic').html(data);

           
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function SearchGraficas() {

    $("#VisorDinamico").hide();
    $("#Graficas").show();

    var WeekIDInic = $("#WeekIDInic").val();
    var WeekIDFina = $("#WeekIDFina").val();
    var GreeHouseID = $("#GreeHouseID").val();
    var ProductID = $("#ProductID").val();
    var PestID = $("#PestID").val();


    if (WeekIDInic > WeekIDFina) {
        Notifications("error", "La semana inicial no puede ser mayor a la final, Verifique!");
        return;
    }

    var datos = {
        "WeekIDInic": WeekIDInic,
        "WeekIDFina": WeekIDFina,
        "GreeHouseID": GreeHouseID,
        "ProductID": ProductID,
        "PestID": PestID,
    }

    BlockScreen();

    var WeekIni = $("#WeekIDInic").val();
    var WeekFin = $("#WeekIDFina").val();
    var BusinessUnitID = $("#BusinessUnitID").val();
    if (BusinessUnitID == "") {
        BusinessUnitID = 0;
    }

    var datos = {
        "weekIni": WeekIni,
        "weekFin": WeekFin,
        "BusinessUnitID": BusinessUnitID,
    };


    jQuery.ajax({
        url: "/GeneralTrend/getReport",
        data: JSON.stringify(datos),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            //var renderers = $.extend(
            //    $.pivotUtilities.renderers,
            //    $.pivotUtilities.plotly_renderers,
            //    $.pivotUtilities.c3_renderers,
            //    $.pivotUtilities.export_renderers
            //);
            //$("#pivDynamic").pivotUI(data, {
            //    rows: ["Finca", "ClaseProducto", "Plaga", "Umbral"],
            //    cols: ["Semana"],
            //    hiddenAttributes: [""],
            //    renderers: renderers,
            //    aggregatorName: "Sum",
            //    vals: ["Cant"],
            //    rendererName: "Table",
            //    hideTotals: true
            //}, true);


            //jQuery('#tableDynamic').html(data);


            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}
