﻿$(document).ready(function () {
    $("#AllTraps").DataTable();

    $("#BusinessUnitId").change(function () {
        GreenHouseByBusinessUnit();
    });

    $("#bt_TrapSave").click(function () {
        TrapSave();
    });
});

function Filtrar() {
    BlockScreen();
    var BusinessUnitId = $("#BusinessUnitId").val();
    var GreenHouseId = $("#GreenHouseId").val();
    var TrapTypeId = $("#TrapTypeId").val();

    if (!BusinessUnitId)
        BusinessUnitId = 0;

    if (!GreenHouseId)
        GreenHouseId = 0;

    if (!TrapTypeId)
        TrapTypeId = 0;

    var datos = {
        "BusinessUnitId": BusinessUnitId,
        "GreenHouseId": GreenHouseId,
        "TrapTypeId": TrapTypeId,
    }

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/Traps/Filtrar",
        success: function (r) {
            for (x in r) {
                var Buttons = ' <button class="btn btn-xs" onclick="location=\'/Traps/EditTrapForm/'+ r[x]["Id"] +'\'" data-toggle="tooltip" data-placement="top" title="Editar Placa"><i class="fa fa-pencil text-primary"></i></button>'+
                    '<button class="btn btn-xs" onclick = "eliminarPlaca(' + r[x]["Id"] +')" data-toggle="tooltip" data-placement="top" title = "Eliminar Placa" ><i class="fa fa-close text-danger"></i></button >'
                r[x]["Data"] = Buttons;
            }

            var table = $("#AllTraps").DataTable({
                "bDestroy": true,
            });

            var tmp = r.map(function (item) {
                return $.map(item, function (element, key) { return element; });
            });
            table.clear();
            console.log(tmp);
            table.columns.className;
            table.rows.add(tmp).draw();
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
            UnlockScreen();
        },
    });
}

function GreenHouseByBusinessUnit() {

    BlockScreen();

    var BusinessUnitId = $("#BusinessUnitId").val();

    if (!GreenHouseId)
        GreenHouseId = 0;

    var datos = {
        "BusinessUnitId": BusinessUnitId,
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/Traps/GreenHouseByBusinessUnit",
        success: function (r) {
            var $html = "<option value='0'>Todos</option>";
            for (x in r) {
                $html += "<option value='" + r[x]["ID"] + "'>" + r[x]["Codigo"] +"</option>";
            }

            $("#GreenHouseId").html($html);
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            Notification("error", errorThrown);
            UnlockScreen();
            return;
        }
    });
}

function BackToList() {
    location = "/Traps/Index";
}

function TrapSave() {
    BlockScreen();
    var Id = $("#Id").val();
    var BusinessUnitId = $("#BusinessUnitId").val();
    var GreenHouseId = $("#GreenHouseId").val();
    var Nombre = $("#Nombre").val();
    var TrapTypeId = $("#TrapTypeId").val();

    if (!Nombre) {
        Notifications("error", "Debe escribir un nombre, verifique!");
        UnlockScreen();
        return;
    }

    var datos = {
        "ID": Id,
        "IDUnidadesNegocios": BusinessUnitId,
        "IDInvernaderos": GreenHouseId,
        "Nombre": Nombre,
        "IDBAS_TiposPlacasMonitoreo": TrapTypeId,
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/Traps/TrapSave",
        success: function (r) {
            Notifications(r.Tipo, r.Mensaje);
            UnlockScreen();
            return;
        },
        error: function (xhr, textStatus, errorThrown) {
            Notification("error", errorThrown);
            UnlockScreen();
            return;
        }
    })
}

function eliminarPlaca(Id) {

    var datos = {
        "Id": Id,
    }

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/Traps/eliminarPlaca",
        success: function (r) {
            Notifications(r.Tipo, r.Mensaje);
            UnlockScreen();
            Filtrar()
            return;
        },
        error: function (xhr, textStatus, errorThrown) {
            Notification("error", errorThrown);
            UnlockScreen();
            return;
        }
    });
}