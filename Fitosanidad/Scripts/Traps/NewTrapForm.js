﻿$(document).ready(function () {
    GreenHouseByBusinessUnit();

    $("#BusinessUnitId").change(function () {
        GreenHouseByBusinessUnit();
    });

    $("#bt_TrapSave").click(function () {
        TrapSave();
    });
});

function BackToList() {
    location = "/Traps/Index";
}

function GreenHouseByBusinessUnit() {

    BlockScreen();

    var BusinessUnitId = $("#BusinessUnitId").val();

    if (!GreenHouseId)
        GreenHouseId = 0;

    var datos = {
        "BusinessUnitId": BusinessUnitId,
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/Traps/GreenHouseByBusinessUnit",
        success: function (r) {
            var $html = "";
            for (x in r) {
                $html += "<option value='" + r[x]["ID"] + "'>" + r[x]["Codigo"] + "</option>";
            }

            $("#GreenHouseId").html($html);
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            Notification("error", errorThrown);
            UnlockScreen();
            return;
        }
    });
}

function TrapSave() {
    BlockScreen();
    var Id = $("#Id").val();
    var BusinessUnitId = $("#BusinessUnitId").val();
    var GreenHouseId = $("#GreenHouseId").val();
    var Nombre = $("#Nombre").val();
    var TrapTypeId = $("#TrapTypeId").val();
    var TrapClassId = $("#TrapClassId").val();

    Nombre = $("#TrapClassId option:selected").text().substring(0,2).concat('-',Nombre);

   
    if (!Nombre) {
        Notifications("error", "Debe escribir un nombre, verifique!");
        UnlockScreen();
        return;
    }

    var datos = {
        "ID": Id,
        "IDUnidadesNegocios": BusinessUnitId,
        "IDInvernaderos": GreenHouseId,
        "Nombre": Nombre,
        "IDBAS_TiposPlacasMonitoreo": TrapTypeId,
        "IDBAS_ClasesPlacasMonitoreo": TrapClassId
    };

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/Traps/TrapSaveNew",
        success: function (r) {
            Notifications(r.Tipo, r.Mensaje);
            $("#Nombre").val("");
            UnlockScreen();
            return;
        },
        error: function (xhr, textStatus, errorThrown) {
            Notification("error", errorThrown);
            UnlockScreen();
            return;
        }
    })
}

function CreaClasePlacas() {
    $("#modalFormulariosTitulo").text("Nuevo Clase Placa Monitoreo");

    $html = "<div class='form-inline'>" +
        "<div class='col-md-4'><label>Nombre: </label></div>" +
        "<div class='col-md-6'><input type='text' id='ClaseIDName' class='form-control'></div> " +
        "<div class='col-md-2'><button class='btn btn-success' type='button' onclick='GuardaNuevoRegistro(\"TrapClassId\", ClaseIDName.value)'><i class='fa fa-save'></i></button></div> " +
        "<br />" +
        "</div>" +
        "";

    $("#modalFormulariosCuerpo").html($html);

    $("#modalFormularios").modal("show");
}

function GuardaNuevoRegistro(ctrl, valor) {
    if (!valor) {
        alert("Debe Ingresar un Nombre, verifique");
        return
    }
    if (ctrl === "TrapClassId") {
        ctrl = "TrapClassId";
    }

    var datos = {
        "Ctrl": ctrl,
        "Valor": valor,
    };

    if (ctrl === "TrapClassId") {
        ctrl = "TrapClassId";
    }

    $.ajax({
        type: "POST",
        DataType: "json",
        data: datos,
        url: "/Traps/GuardaClasePlaca",
        success: function (res) {
            var $html = "";
            for (x in res) {
                $html += "<option value='" + res[x][ctrl] + "'>" + res[x]['Nombre'] + "</option>";
            }

            $("#" + ctrl).html($html);
            Notifications("success", "Registro creado correctamente");
            $("#modalFormularios").modal('hide');//ocultamos el modal
            $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
            $('.modal-backdrop').remove();//eliminamos el backdrop del modal
        }
    })
}