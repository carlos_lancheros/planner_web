﻿$("document").ready(function () {
});

function TraeData() {
    BlockScreen();
    var WeekIDInic = $("#WeekIDInic").val();
    var WeekIDFina = $("#WeekIDFina").val();
    var GreenHouseID = $("#GreenHouseID").val();
    var TipoPlaca = $("#TipoPlaca").val();

    if (!GreenHouseID) {
        //Notifications("error", "Debe seleccionar finca, verifique!");
        //$("#GreenHouseID").focus();
        //return;
        GreenHouseID = 0;
    }

    var datos = {
        "WeekIDInic": WeekIDInic,
        "WeekIDFina": WeekIDFina,
        "GreenHouseID": GreenHouseID,
        "TipoPlaca": TipoPlaca,
    }

    $.ajax({
        DataType: "JSON",
        type: "post",
        data: datos,
        url: "/IndirectMonitoring/TraeData",
        success: function (r) {

            for (i in r)
            {
                var arr = (r[i]);
                r[i].TipoPlaca = r[i].IDBAS_TiposPlacasMonitoreo_Nombre;
            }

            //var pivot = new WebDataRocks({
            //    container: "#wdr-component",
            //    width: "100%",
            //    height: 600,
            //    toolbar: true,
            //    //customizeCell: customizeCellFunction,
            //    report: {
            //        "dataSource": {
            //            "data": r
            //        },
            //        options: {
            //            grid: {
            //                type: "classic"
            //            }
            //        },
            //        "slice": {
            //            "reportFilters": [
            //                {
            //                    "uniqueName": "Producto"
            //                }],
            //            "rows": [{
            //                "uniqueName": "Bloque",
            //                "filter": {
            //                    "negation": true
            //                }
            //            },
            //            {
            //                "uniqueName": "Plaga",
            //                 "filter": {
            //                    "negation": true
            //                }
            //            }
            //            ],
            //            "columns": [{
            //                "uniqueName": "Semana",
            //                "filter": {
            //                    "negation": true
            //                }
            //            }],
            //            measures: [{
            //                "uniqueName": "Promedio",
            //                "aggregation": "sum",
            //                "active": true,
            //                "format": "currency"
            //            }],
            //            "formats": [{
            //                "name": "",
            //                "maxDecimalPlaces": 2
            //            },
            //                {
            //                    "name": "currency",
            //                    "thousandsSeparator": " ",
            //                    "decimalSeparator": ".",
            //                    "currencySymbol": "",
            //                    "currencySymbolAlign": "left",
            //                    "nullValue": "",
            //                    "textAlign": "right",
            //                    "isPercent": false
            //                }
            //            ],
            //            "expands": {
            //                "rows": [{
            //                        "tuple": ["plagas"]
            //                    }]
            //            },
                        
                        
            //        },
            //        reportcomplete: function () {
            //            pivot.off("reportcomplete");
            //            pivotTableReportComplete = true;
            //            // createGoogleChart();
            //        }
            //    }
            //});

            

            //var renderers = $.extend(
            //    $.pivotUtilities.renderers,
            //    $.pivotUtilities.plotly_renderers,
            //    $.pivotUtilities.d3_renderers,
            //    $.pivotUtilities.export_renderers
            //);
            var derivers = $.pivotUtilities.derivers;
            var renderers = $.extend($.pivotUtilities.renderers, $.pivotUtilities.c3_renderers);

            $("#PivotData").pivotUI(r, {
                renderers: renderers,
                cols: ["Semana"],
                rows: ["Bloque", "TipoPlaca", "TotalPlacas", "Plaga"],
                hiddenAttributes: ["IDUnidadesNegocios", "IDTrampas", "IDTrampas_Nombre", "IDInvernaderos", "IDClasesProductos", "IDPlagas", "IDBAS_TiposPlacasMonitoreo_Nombre", "IDBAS_TiposPlacasMonitoreo", "Promedio"],
                aggregatorName: "Average",
                vals: ["Cantidad"],
                rendererName: "Table Barchart",
            }, true, "es");
            UnlockScreen();
        },
        complete: function () {
            var scroll = $('body, html');
            scroll.animate({ scrollTop: scroll.prop("scrollHeight") });
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            Notification("error", errorThrown);
            UnlockScreen();
            return;
        }
    })
}

function customizeCellFunction(cell, data) {
    if (data.rows) {
        for (var i = 0; i < data.rows.length; i++) {
            if (data.rows[i]["hierarchyCaption"] == "Business Type") {
                if (data.rows[i]["caption"] == "Specialty Bike Shop") {
                    cell.attr["hierarchy"] = data.rows[i]["hierarchyCaption"];
                    cell.attr["member"] = data.rows[i]["caption"];
                }
            }
            if (data.rows[i]["hierarchyCaption"] == "Business Type") {
                if (data.rows[i]["caption"] == "Value Added Reseller") {
                    cell.attr["hierarchy"] = data.rows[i]["hierarchyCaption"];
                    cell.attr["member"] = data.rows[i]["caption"];
                }
            }
            if (data.rows[i]["hierarchyCaption"] == "Business Type") {
                if (data.rows[i]["caption"] == "Warehouse") {
                    cell.attr["hierarchy"] = data.rows[i]["hierarchyCaption"];
                    cell.attr["member"] = data.rows[i]["caption"];
                }
            }
        }
    }
}

function exportTableToExcel(tableID, filename = '') {
    $(".pvtTable").attr("id", "pvtTable");
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    filename = filename ? filename + '.xls' : 'InformeMonitoreoIndirecto.xls';
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob(blob, filename);
    } else {

        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

        downloadLink.download = filename;

        downloadLink.click();
    }
}