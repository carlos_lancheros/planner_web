﻿jQuery(document).ready(function () {

    $('#tableBedManager').DataTable({
        destroy: true
    });
});

function onClick() {

    var BusinessUnitID = parseInt($("#lstBusinessUnit").find(":selected").val());

    var values = { "BusinessUnitID": BusinessUnitID }

    BlockScreen();
    jQuery.ajax({
        url: "/Planner_Sowing/GetBedManager",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#BedManagerDataTable').html(data);

            $('#tableBedManager').DataTable({
                destroy: true
            });


            UnlockScreen();
        },
        error: function () {
            UnlockScreen();
        }
    });
}
