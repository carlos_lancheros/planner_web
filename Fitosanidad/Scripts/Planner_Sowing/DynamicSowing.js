﻿jQuery(document).ready(function () {

    $('#tableDynamic').DataTable({
        "scrollX": true,
        "scrollY": 600,
        destroy: true
    });

    GetPivot();
});

function Search() {
    var BusinessUnitID = parseInt($("#lstBusinessUnit").find(":selected").val());

    var parameters = { "BusinessUnitID": BusinessUnitID};

    BlockScreen();
    jQuery.ajax({
        url: "/Planner_Sowing/GetDynamicSowing",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery('#tableDynamic').html(data);

            $('#tableDynamic').DataTable({
                "scrollX": true,
                "scrollY": 600,
                destroy: true
            });

            GetPivot();
            
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function GetPivot() {
    $("#pivDynamic").pivotUI($("#tableDynamic"),
        {
            rows: ["FechaSiembra", "unidadnegocio", "Bloque"]
            , cols: ["Cantidad"]
        }
        , false
        , "es"
    );
};