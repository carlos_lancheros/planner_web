﻿jQuery(document).ready(function () {
    $('.ReportType').iCheck({
        radioClass: 'iradio_flat-green'
    });

    jQuery("#btnExcel").click(function () {
        jQuery('<table>')
            .append(
                jQuery("#tableExecution").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Execution_Data"
            });
    });
});

function Search() {
    if (jQuery('#ddlBusinessUnit').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarUnidadDeNegocio"))
        jQuery('#ddlBusinessUnit').focus();
        return false;
    } else {
        if (jQuery('#ddlBusinessUnit').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarUnidadDeNegocio"))
            jQuery('#ddlBusinessUnit').focus();
            return false;
        };
    }
    if (jQuery('#ddlActivity').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarActividad"))
        jQuery('#ddlActivity').focus();
        return false;
    } else {
        if (jQuery('#ddlActivity').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarActividad"))
            jQuery('#ddlActivity').focus();
            return false;
        };
    }

    if (jQuery('#ddlWeek').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarSemana"))
        jQuery('#ddlWeek').focus();
        return false;
    } else {
        if (jQuery('#ddlWeek').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarSemana"))
            jQuery('#ddlWeek').focus();
            return false;
        };
    }

    var ReportType = 1;
    var year = 0;
    var week = 0;
    var businessUnitID = jQuery('#ddlBusinessUnit').val();
    var activityGroupID = jQuery('#ddlActivity').val();

    if (jQuery('#ddlWeek').val() != null && jQuery('#ddlWeek').val().length != 0) {
        year = parseInt($("#ddlWeek").find(":selected").val().substring(0, 4));
    };
    if (jQuery('#ddlWeek').val() != null && jQuery('#ddlWeek').val().length != 0) {
        week = parseInt($("#ddlWeek").find(":selected").val().substring(4));
    };

    var parameters = { "year": year, "week": week, "businessUnitID": businessUnitID, "activityGroupID": activityGroupID, "ReportType": ReportType };

    BlockScreen();
    jQuery.ajax({
        url: "/Planner_Sowing/GetExecution",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            getTableCombined(data);

            //jQuery('#ExecutionCombinedDataTable').show();
            //jQuery('#ExecutionCombinedDataTable').html(data);


            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}

function getTableHeader() {
    var year = 0;
    var week = 0;
    var strHeader = "";
    var datToday = new Date();
    datToday.setHours(0, 0, 0, 0)

    var ReportType = $('[name="ReportType"]:checked').val();

    if (jQuery('#ddlWeek').val() != null && jQuery('#ddlWeek').val().length != 0) {
        year = parseInt($("#ddlWeek").find(":selected").val().substring(0, 4));
    };
    if (jQuery('#ddlWeek').val() != null && jQuery('#ddlWeek').val().length != 0) {
        week = parseInt($("#ddlWeek").find(":selected").val().substring(4));
    };

    var parameters = { "year": year, "week": week };

    BlockScreen();
    jQuery.ajax({
        url: "/Planner_Sowing/GetExecutionHeader",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.each(data, function (i, data) {
                var dateParts = jsonDateFormat(data.DayDate).split("-");
                var checkindate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
                checkindate.setHours(0, 0, 0, 0)
                if (data.DayNumber == datToday.getDay()) {
                    if (checkindate.getFullYear() == datToday.getFullYear() && checkindate.getMonth() + 1 == datToday.getMonth() + 1 && checkindate.getDate() == datToday.getDate()) {
                        $("#tdCombined").html(GetResource("Hoy"));
                    }
                    else {
                        $("#tdCombined").html(data.DayName);
                    }
                }
            });

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}