﻿jQuery(document).ready(function () {
    $('#ExecutionCombinedDataTable').hide();
    var tblCombined = jQuery('#tableExecutionCombined').DataTable({
        searching: false
        , info: true
        , paging: true
        , responsive: true
        , destroy: true
        , rowGroup: true
        , stateSave: true
        , destroy: true
        , columnDefs: [
            {
                "targets": [4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
                "className": "text-right",
            },
            { className: "dt-left", "width": "05%", "targets": 0 }
            , { "width": "05%", "targets": 1 }
            , { "width": "05%", "targets": 2 }
            , { "width": "05%", "targets": 3 }
            , { "width": "05%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
            , { "width": "05%", "targets": 6 }
            , { "width": "05%", "targets": 7 }
            , { "width": "05%", "targets": 8 }
            , { "width": "05%", "targets": 9 }
            , { "width": "05%", "targets": 10 }
            , { "width": "05%", "targets": 11 }
            , { "width": "05%", "targets": 12 }
            , { "width": "05%", "targets": 13 }
            , { "width": "05%", "targets": 14 }
            , { "width": "05%", "targets": 15 }
            , { "width": "05%", "targets": 16 }
            , { "width": "05%", "targets": 17 }
            , { "width": "05%", "targets": 18 }
            , { "width": "05%", "targets": 19 }
            , { className: "text-center", type: 'percent', "width": "05%", "targets": 20 }
        ],
        "createdRow": function (row, data, index) {
            //verde
            if (data[20] >= 95 && data[20] <= 105) {
                $('td', row).eq(20).css('color', 'white');
                $('td', row).eq(20).css('background-color', '#00B050');
            }
            //verde

            //amarillo
            if (Between(data[20], 85.1, 94.9) || Between(data[20], 105.1, 115.9)) {
                $('td', row).eq(20).css('color', 'white');
                $('td', row).eq(20).css('background-color', '#FFC000');
            }
            //amarillo

            //rojo
            if (data[20] < 85 || data[20] > 115) {
                $('td', row).eq(20).css('color', 'white');
                $('td', row).eq(20).css('background-color', '#FF0000');
            }
            //rojo
        },
    });
});

function getTableCombined(data) {
    $('#ExecutionCombinedDataTable').show();
    getTableHeader();

    return;

    if (tblCombined) {
        tblCombined.clear();
        tblCombined.destroy();
    }
    var tblCombined = jQuery('#tableExecutionCombined').DataTable();
    if (tblCombined.data().any()) {
        tblCombined.clear().draw();
    }

    $.each(data, function (i, data) {
        var prg = data.ExecutionDetailLst[0].WeekTotal;
        var exe = data.ExecutionDetailLst[1].WeekTotal;
        var des = (exe / prg);
        des = Math.floor(100 - des) / 100

        if (addRow) {
            addRow.destroy();
        }
        var addRow = tblCombined.row.add([
            data.ProductClass,
            data.ActivityGroup,
            data.LaborName,
            data.Name,
            addCommas(DecimalPlaces(data.ExecutionDetailLst[0].WeekTotal,2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[1].WeekTotal, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[0].C1, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[1].C1, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[0].C2, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[1].C2, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[0].C3, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[1].C3, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[0].C4, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[1].C4, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[0].C5, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[1].C5, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[0].C6, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[1].C6, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[0].C7, 2)),
            addCommas(DecimalPlaces(data.ExecutionDetailLst[1].C7, 2)),
            DecimalPlaces(des, 2),
        ]).node().id = "rowId" + i;
    });

    tblCombined.draw(false);
}