﻿jQuery(document).ready(function () {
    $('.ReportType').iCheck({
        radioClass: 'iradio_flat-green'
    });

    $("#ddlActivity").change(function () {
        //if (jQuery('#ddlActivity').val().length != jQuery('#ddlActivity').val().filter(function (v) { return v !== '' }).length) {
        //    var filtered = jQuery('#ddlActivity').val().filter(function (el) {
        //        return el != '';
        //    });
        //    $('#ddlActivity').empty(); 
        //    $('#ddlActivity').append(filtered);
        //    $('#ddlActivity').trigger("chosen:updated");
        //}
    })

    jQuery("#btnExcel").click(function () {
        jQuery('<table>')
            .append(
            jQuery("#tableExecutionLaborsWorkforce").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Execution_Data"
            });
    });
});

function Search() {
    if (jQuery('#ddlBusinessUnit').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarUnidadDeNegocio"))
        jQuery('#ddlBusinessUnit').focus();
        return false;
    } else {
        if (jQuery('#ddlBusinessUnit').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarUnidadDeNegocio"))
            jQuery('#ddlBusinessUnit').focus();
            return false;
        };
    }
    if (jQuery('#ddlProduct').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarProducto"))
        jQuery('#ddlProduct').focus();
        return false;
    } else {
        if (jQuery('#ddlProduct').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarProducto"))
            jQuery('#ddlProduct').focus();
            return false;
        };
    }
    if (jQuery('#ddlActivity').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarActividad"))
        jQuery('#ddlActivity').focus();
        return false;
    } else {
        if (jQuery('#ddlActivity').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarActividad"))
            jQuery('#ddlActivity').focus();
            return false;
        };
    }
    if (jQuery('#ddlWeek').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarSemana"))
        jQuery('#ddlWeek').focus();
        return false;
    } else {
        if (jQuery('#ddlWeek').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarSemana"))
            jQuery('#ddlWeek').focus();
            return false;
        };
    }

    var ReportType = $('[name="ReportType"]:checked').val();
    var year = 0;
    var week = 0;
    var businessUnitID = jQuery('#ddlBusinessUnit').val();
    var productID = jQuery('#ddlProduct').val();
    var activityGroupID = jQuery('#ddlActivity').val();

    if (jQuery('#ddlWeek').val() != null && jQuery('#ddlWeek').val().length != 0) {
        year = parseInt($("#ddlWeek").find(":selected").val().substring(0, 4));
    };
    if (jQuery('#ddlWeek').val() != null && jQuery('#ddlWeek').val().length != 0) {
        week = parseInt($("#ddlWeek").find(":selected").val().substring(4));
    };

    if ($('#ddlProduct').val() != null && $('#ddlProduct').val().length != 0) {
        prmProductoVegetal = $('#ddlProduct').val();
    };

    var parameters = { "year": year, "week": week, "businessUnitID": businessUnitID, "productID": productID, "activityGroupID": activityGroupID, "ReportType": ReportType };

    BlockScreen();
    jQuery.ajax({
        url: "/Planner_Sowing/GetExecution",
        data: JSON.stringify(parameters),
        type: 'POST', 
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery('#ExecutionLaborsWorkforceDataTable').show();
            jQuery('#ExecutionLaborsWorkforceDataTable').html(data);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}
