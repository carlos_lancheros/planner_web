﻿jQuery(document).ready(function () {
    $('.ReportType').iCheck({
        radioClass: 'iradio_flat-green'
    });

    
    jQuery("#btnExcel").click(function () {
        jQuery('<table>')
            .append(
            jQuery("#tableExecutionLaborsWorkforce").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Execution_Data"
            });
    });
});

function Search() {
    if (jQuery('#ddlBusinessUnit').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarUnidadDeNegocio"))
        jQuery('#ddlBusinessUnit').focus();
        return false;
    } else {
        if (jQuery('#ddlBusinessUnit').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarUnidadDeNegocio"))
            jQuery('#ddlBusinessUnit').focus();
            return false;
        };
    }
    if (jQuery('#ddlProduct').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarProducto"))
        jQuery('#ddlProduct').focus();
        return false;
    } else {
        if (jQuery('#ddlProduct').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarProducto"))
            jQuery('#ddlProduct').focus();
            return false;
        };
    }

    if (jQuery('#ddlWeek').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarSemana"))
        jQuery('#ddlWeek').focus();
        return false;
    } else {
        if (jQuery('#ddlWeek').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarSemana"))
            jQuery('#ddlWeek').focus();
            return false;
        };
    }

  
    var year = 0;
    var week = 0;
    var businessUnitID = jQuery('#ddlBusinessUnit').val();
    var productID = jQuery('#ddlProduct').val();

    if (jQuery('#ddlWeek').val() != null && jQuery('#ddlWeek').val().length != 0) {
        year = parseInt($("#ddlWeek").find(":selected").val().substring(0, 4));
    };
    if (jQuery('#ddlWeek').val() != null && jQuery('#ddlWeek').val().length != 0) {
        week = parseInt($("#ddlWeek").find(":selected").val().substring(4));
    };
    if ($('#ddlProduct').val() != null && $('#ddlProduct').val().length != 0) {
        prmProductoVegetal = $('#ddlProduct').val();
    };

    var parameters = { "year": year, "week": week, "businessUnitID": businessUnitID, "productID": productID };

    BlockScreen();
    jQuery.ajax({
        url: "/Planner_Sowing/GetPlantingPlan",
        data: JSON.stringify(parameters),
        type: 'POST', 
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var pivot = new Flexmonster({
                container: "#PivotWData",
                componentFolder: "https://cdn.flexmonster.com/",
                beforetoolbarcreated: customizeToolbar,
                toolbar: true,
                height: 600,
                global: {
                    localization: "https://cdn.webdatarocks.com/loc/es.json",
                },


                report: {
                    "formats": [
                        {
                            "name": "decimal",
                            "decimalPlaces": 2,
                            decimalSeparator: ",",
                            currencySymbol: "%",
                            nullValue: "",
                            infinityValue: "Over",
                            divideByZeroValue: ""
                        },
                        {
                            "name": "integer",
                            "decimalPlaces": 0,
                        },
                    ],

                    dataSource: {
                        data: data,
                        dataSourceType: "json",
                    },
                    "slice": {
                        "rows": [
                            {
                                "uniqueName": "Finca"
                            },
                            {
                                "uniqueName": "Producto"
                            },
                            {
                                "uniqueName": "Variedad"
                            },                            
                        ],
                        "columns": [
                            {
                                "uniqueName": "Semana"
                            },
                            {
                                "uniqueName": "fecha",
                            }
                        ],
                        "measures": [
                            {
                                "uniqueName": "Cantidad",
                                "formula": "\"Cantidad\"",
                                "individual": false,
                                "caption": "Cantidad",
                                "format": "integer"
                            },                           
                        ],
                        "expands": {
                            "expandAll": true,
                        },
                    },
                    "options": {
                        "grid": {
                            "showTotals": "off",
                            "showGrandTotals": "off",
                        }
                    }
                },
                licenseKey: "Z792-XABH0H-1N670U-4I3D20",
            });
            //jQuery('#ExecutionLaborsWorkforceDataTable').show();
          //  jQuery('#tablePlanner_Planting').html(data);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown);
        }
    });
}


function customizeToolbar(toolbar) {
    var tabs = toolbar.getTabs();
    toolbar.getTabs = function () {
        delete tabs[0];
        delete tabs[1];
        delete tabs[2];
        delete tabs[4];
        delete tabs[5];
        delete tabs[6];
        delete tabs[7];
        delete tabs[8];
        delete tabs[9];
        return tabs;
    }
}