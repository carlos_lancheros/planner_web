﻿jQuery(document).ready(function () {
    jQuery("#btnExcel").click(function () {
        jQuery('<table>')
            .append(
            jQuery("#tableProjection").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Projection_Data"
            });
    });
    jQuery("#tableProjection").dataTable();
});

function Search() {
    var prmSemanaINI = 0;
    var prmSemanaFIN = 0;
    var prmUnidadNegocio = 0;
    var prmActividad = -1;
    var prmProductoVegetal = 0;
    var prmVariedad = 0;

    if ($('#ddlWeekFrom').val() != null && $('#ddlWeekFrom').val().length != 0) {
        prmSemanaINI = parseInt($("#ddlWeekFrom").find(":selected").val());
    };
    if ($('#ddlWeekTo').val() != null && $('#ddlWeekTo').val().length != 0) {
        prmSemanaFIN = parseInt($("#ddlWeekTo").find(":selected").val());
    };
    if ($('#ddlBusinessUnit').val() != null && $('#ddlBusinessUnit').val().length != 0) {
        prmUnidadNegocio = $('#ddlBusinessUnit').val();
    };
    if ($('#ddlProduct').val() != null && $('#ddlProduct').val().length != 0) {
        prmProductoVegetal = $('#ddlProduct').val();
    };
    if ($('#ddlActivity').val() != null && $('#ddlActivity').val().length != 0) {
        prmActividad = $('#ddlActivity').val();
    };

    var parameters = { "prmUnidadNegocio": prmUnidadNegocio, "prmProductoVegetal": prmProductoVegetal, "prmActividad": prmActividad, "prmSemanaINI": prmSemanaINI, "prmSemanaFIN": prmSemanaFIN, "prmVariedad": prmVariedad };

    BlockScreen();
    $.ajax({
        url: "/Planner_Sowing/JSONGetProjection",
        data: JSON.stringify(parameters),
        type: 'POST',
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            var test = '';

            //var pivot = new dhx.Pivot("pivProjection", {
            //    data: data,
            //    fields: {
            //        rows: ["UnidadNegocio", "ProductoVegetal", "ModoCalculo", "Rendimiento", "Labor", "Actividad"],
            //        columns: ["Semana", "Personas"],
            //        values: [{ id: "Semana", method: "sum" }, { id: "Personas", method: "count" }],
            //    },
            //    fieldList: [
            //        { id: "Semana", label: "Semana"},
            //        { id: "UnidadNegocio", label: "Semana" },
            //        { id: "Bloque", label: "Bloque" },
            //        { id: "ProductoVegetal", label: "ProductoVegetal" },
            //        { id: "Variedad", label: "Variedad" },
            //        { id: "Labor", label: "Labor" },
            //        { id: "Actividad", label: "Actividad" },
            //        { id: "Unidades", label: "Unidades" },
            //        { id: "Minutos", label: "Minutos" },
            //        { id: "Personas", label: "Personas" },
            //        { id: "Rendimiento", label: "Rendimiento" },
            //        { id: "ModoCalculo", label: "ModoCalculo" },
            //    ]
            //});

            var renderers = $.extend(
                $.pivotUtilities.renderers,
                $.pivotUtilities.plotly_renderers,
                $.pivotUtilities.d3_renderers,
                $.pivotUtilities.export_renderers
            );
            $("#pivProjection").pivotUI(data, {
                rows: ["UnidadNegocio", "ProductoVegetal", "ModoCalculo", "Rendimiento", "Labor", "Actividad"],
                cols: ["Semana"],
                hiddenAttributes: [""],
                renderers: renderers,
                aggregatorName: "Sum",
                vals: ["Personas"],
                rendererName: "Table",
            }, true);

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            var response = xhr.responseText;
            if (response.Message) {
                Notifications("error", response.Message);
            } else {
                Notifications("error", errorThrown);
            };
        }
    });
}
