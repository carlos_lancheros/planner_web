﻿jQuery(document).ready(function () {
    jQuery('#tableExecutionLaborsWorkforce').dataTable({
        "info": false
        , paging: true
        , responsive: true
        , destroy: true
        , autoWidth: true
        , "columnDefs": [
            {
                "targets": [4, 5, 6, 7, 8, 9, 10, 11,12],
                "className": "text-right",
            },
            { className: "dt-left", "width": "05%", "targets": 0 }
            , { "width": "10%", "targets": 1 }
            , { "width": "15%", "targets": 2 }
            , { "width": "15%", "targets": 3 }
            , { "width": "05%", "targets": 4 }
            , { "width": "05%", "targets": 5 }
            , { "width": "05%", "targets": 6 }
            , { "width": "05%", "targets": 7 }
            , { "width": "05%", "targets": 8 }
            , { "width": "05%", "targets": 9 }
            , { "width": "05%", "targets": 10 }
            , { "width": "05%", "targets": 11 }
            , { "width": "05%", "targets": 12 }
            , {
                className: "text-center", type: 'percent', "width": "05%", "targets": 13
                ,"render": function (data, type, full, meta) {
                    return +data + '%';
                }
            }
        ]
        ,"createdRow": function (row, data, index) {
            //verde
            if (data[13] >= 95 && data[13] <= 105) {
                $('td', row).eq(13).css('color', 'white');
                $('td', row).eq(13).css('background-color', '#00B050');
            }
            //verde

            //amarillo
            if (Between(data[13], 85.1, 94.9) || Between(data[13], 105.1, 115.9)) {
                $('td', row).eq(13).css('color', 'white');
                $('td', row).eq(13).css('background-color', '#FFC000');
            }
            //amarillo

            //rojo
            if (data[13] < 85 || data[13] > 115) {
                $('td', row).eq(13).css('color', 'white');
                $('td', row).eq(13).css('background-color', '#FF0000');
            }
            //rojo
        }
        ,"columns": [
            { 'data': 0 },
            { 'data': 1 },
            { 'data': 2 },
            { 'data': 3 },
            { 'data': 4 },
            { 'data': 5 },
            { 'data': 6 },
            { 'data': 7 },
            { 'data': 8 },
            { 'data': 9 },
            { 'data': 10 },
            { 'data': 11 },
            { 'data': 12 },
            { 'data': 13 },
        ]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });
});
