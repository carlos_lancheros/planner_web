﻿jQuery(document).ready(function () {

    $("#lstBusinessUnit").change(function () {
        var intBusinessId = parseInt($("#lstBusinessUnit").find(":selected").val());
        GetGreenhousebyBusinessUnit(intBusinessId);
    });

    $('body').on('click', '.clickBed', function () {
        var values = { "BedID": $(this).attr('id') }

        jQuery.ajax({
            url: "/Planner_Sowing/GetDataBedInfo",
            data: JSON.stringify(values),
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $("#modalDetails #innerHead").empty();
                $("#modalDetails #Details").empty();
                $("#modalDetails #innerDetails").empty();

                var html = "";
                var head = "";
                var detail = "";

                for (var i = 0; i < data.length; i++) {

                    head += "<h3>" + data[i].Bed +"</h3><p> Cama</p>";

                    detail += "<h3>" + data[i].CantPlantas + "</h3><p> Cant. Plantas</p>";

                    html += 
                        "<div class='col-sm-3 border-right' > <div class='description-block'><h5 class='description-header'>" + data[i].AreaUtil +"</h5><span class='description-text'>ÁREA ÚTIL</span></div></div>" +
                        "<div class='col-sm-3 border-right' > <div class='description-block'><h5 class='description-header'>" + data[i].AreaInvernadero + "</h5><span class='description-text'>ÁREA INVERNADERO</span></div></div>" +
                        "<div class='col-sm-3 border-right'><div class='description-block'><h5 class='description-header'>" + data[i].AreaOcupada +"</h5><span class='description-text'>ÁREA OCUPADA</span></div></div>" +
                        "<div class='col-sm-3'><div class='description-block'><h5 class='description-header'>" + data[i].AreaDisponible + "</h5><span class='description-text'>ÁREA DISPONIBLE</span></div></div>";
                }
                $("#modalDetails #innerHead").html(head);
                $("#modalDetails #Details").html(html);
                $("#modalDetails #innerDetails").html(detail);
            },
            error: function () {

            }
        });

        jQuery.ajax({
            url: "/Planner_Sowing/GetDataBedInfoDetaill",
            data: JSON.stringify(values),
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                var html = "";

                $("#tableInfo tbody").empty();
              
              //  $("#tableInfo tbody").remove();

                for (var i = 0; i < data.length; i++) {

                    var Fsiembra = jsonDateFormat(data[i].Fsiembra);
                    var FErradicacion = "";
                    if (data[i].FErradicacion != null) {
                        FErradicacion = jsonDateFormat(data[i].FErradicacion);
                    } 

                    var color = "";
                    if (data[i].StateSowing == "ERRADICADA") {
                        color = "bg-blue";
                    } else {
                        color = "bg-green";
                    }

                    html += "<tr>" +
                                "<td><span data-toggle='tooltip' title='' class='badge "+color+"'>-</span></td>"+
                                "<td>" + Fsiembra +"</td>" +
                                "<td>" + data[i].PlantProductName +"</td>" +
                                "<td>" + data[i].Product +"</td>" +
                                "<td>" + data[i].TallosSembrados+ "</td>" +
                                "<td>" + data[i].Densidad+ "</td>" +
                                "<td>" + data[i].Area + "</td>" +
                                "<td>" + data[i].SpreadPhase + "</td>" +                                
                                "<td>" + FErradicacion + "</td>" +
                                "<td>" + data[i].StateSowing + "</td>" +
                            "</tr>";

                }
                
                $("#tableInfo tbody").html(html);

                $('#tableInfo').DataTable({
                    "scrollX": true,
                    "scrollY": 200,
                    destroy: true
                });

            },
            error: function () {

            }
        });
    })
});

function GetGreenhousebyBusinessUnit(intBusinessId) {
    BlockScreen();

    $("#lstGreenhouse").empty()
    var parameters = { "intBusinessId": intBusinessId };
    jQuery.ajax({
        url: "/Base/getGreenhousebyBusinessUnitId",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.each(data, function (i, data) {
                $("#lstGreenhouse").append('<option value="'
                    + data.ID + '">'
                    + data.Codigo + '</option>');
            });
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
        async: false
    });
}

function onClick() {

    var greenhouseID = parseInt($("#lstGreenhouse").find(":selected").val());

    var values = { "greenhouseID": greenhouseID }

    BlockScreen();
    jQuery.ajax({
        url: "/Planner_Sowing/GetDataSowing",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#visorList').html(data);
            UnlockScreen();
        },
        error: function () {
            UnlockScreen();
        }
    });
}
