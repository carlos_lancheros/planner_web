﻿$(document).ready(function () {
    $('#ID').val(0);

    $("#btnHome").click(function () {
        BackToList();
    });

    $("#btnSave").click(function () {
        CallSubmit();
    });

    $('#ColorIdentificacion').colorpicker({
        align: 'right',
        format: 'hex',
    });

    $("#ColorIdentificacion").blur(function () {
        $("#Color").css('background-color', $("#ColorIdentificacion").val());
        $("#Color").css('color', getInvertColor($("#ColorIdentificacion").val()));
        $("#Color").val($("#ColorIdentificacion").val());
    });

    $('#ColorIdentificacion').colorpicker().on('changeColor', function (e) {
        $("#Color").css('background-color', $("#ColorIdentificacion").val());
        $("#Color").css('color', getInvertColor($("#ColorIdentificacion").val()));
        $("#Color").val($("#ColorIdentificacion").val());
    });

    $("#AddFlagActive").change(function () {
        if (this.checked) {
            $("#FlagActivo").val(1);
        } else {
            $("#FlagActivo").val(0);
        }
    });

    $("#Color").css('background-color', $("#ColorIdentificacion").val());
    $("#Color").css('color', getInvertColor($("#ColorIdentificacion").val()));
    $("#Color").val($("#ColorIdentificacion").val());
});

function BackToList() {
    var Url = "/Pests";
    window.location.href = Url;
};

function CallSubmit() {
    if (jQuery('#Nombre').val() == null) {
        Notifications("error", GetResource("DebeDigitarDescripcion"))
        jQuery('#Nombre').focus();
        return false;
    } else {
        if (jQuery('#Nombre').val().length == 0) {
            Notifications("error", GetResource("DebeDigitarDescripcion"))
            jQuery('#Nombre').focus();
            return false;
        };
    }

    jQuery("#Create").submit();
};

