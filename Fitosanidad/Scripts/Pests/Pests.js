﻿var tblPests

jQuery(document).ready(function () {
    LoadGrid();

    tblPests = jQuery('#tablePests').dataTable({
        "info": true
        , paging: true
        , responsive: true
        , destroy: true
        , autoWidth: false
        , scrollY: true
        , scrollX: true
        , scrollCollapse: true
        , "columnDefs": [
            {
                "targets": [0],
                "className": "text-right",
            }
            , {
                "targets": [3],
                orderable: false,
            }
            , {
                "targets": [4, 5],
                orderable: false,
                "className": "text-center",
            }
            , { "width": "10%", "targets": 0 }
            , { "width": "40%", "targets": 1 }
            , { "width": "20%", "targets": 2 }
            , { "width": "10%", "targets": 3 }
            , { "width": "20%", "targets": 4 }
            , { "width": "20%", "targets": 5 }
            , { "width": "20%", "targets": 6 }
            , { "width": "20%", "targets": 7 }
        ]
        , "createdRow": function (row, data, index) {
            $('td', row).eq(5).css('color', getInvertColor(data[4]));
            $('td', row).eq(5).css('background-color', data[4]);
        }
        , "columns": [
            { 'data': 0 },
            { 'data': 1 },
            { 'data': 2 },
            { 'data': 3 },
            { 'data': 4 },
            { 'data': 5 },
            { 'data': 6 },
            { 'data': 7 },
        ]
        , "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
    });

    $("#btnCreate").click(function () {
        CreateRow();
    });
});

function LoadGrid(e) {
    if (e) {
        e.preventDefault();
    }
    BlockScreen();

    jQuery.ajax({
        url: "/Pests/IndexGrid",
        data: null,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.length == 0) {
                Notifications("information", GetResource("NoSeEncontraronDatos"))
            } else {
                var tblPests = jQuery('#tablePests').DataTable();

                $.each(data, function (i, data) {
                    tblPests.row.add([
                        data.ID,
                        data.Nombre,
                        data.Abrr,
                        data.IDClasificacion,
                        data.ColorIdentificacion,
                        data.ColorIdentificacion,
                        '<input type="checkbox"' + (data.FlagActivo == 1 ? ' checked="checked"' : '') + ' onClick="return false">',
                        '<div class="actions">' + 
                        '    <div class="btn-group">' + 
                        '        <button class="btn btn-xs" type="button" data-toggle="tooltip" data-placement="top" title="' + GetResource("Editar") + '" onclick="location=\'/Pests/Edit?ID=' + data.ID + '\'">' + 
                        '            <i class="fa fa-pencil text-blue"></i>' + 
                        '        </button>' + 
                        '    </div>' + 
                        '</div>'
                    ]).draw();
                });
                tblPests.draw(false);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#ddlBusinessUnit")).text());
        },
        async: false
    });
};

function CreateRow() {
    var url = "/Pests/Create";
    window.location = url;
};

function EditRow(arrKey) {
    var ID = arrKey[0];
    var url = "/Pests/Edit?ID=" + ID;

    window.location = url;
};
