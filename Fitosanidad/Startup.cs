﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Fitosanidad.Startup))]
namespace Fitosanidad
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
