﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;
using RestSharp;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class UserAdministrationController : BaseController
    {
        public class Lista
        {
            public int id { get; set; }
            public string text { get; set; }
            public int @checked { get; set; }
            public List<Lista> children { get; set; }
        }

        BASEntities basEntities;
        SIEMBRASEntities siembrasEntities;
        NewSafexEntities newSafexEntities;

        public ActionResult Index()
        {
            basEntities = new BASEntities();

            List<fn_allAuthUser_Result> Users = basEntities.fn_allAuthUser().ToList();

            ViewData["Users"] = Users;
            return View();
        }

        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateUserInsert(Auth_User datos)
        {
            try
            {
                basEntities = new BASEntities();
                var existe = basEntities.Auth_User.Where(x => x.Login == datos.Login).FirstOrDefault();
                if(existe != null)
                {
                    var arre = new
                    {
                        Error = 1,
                        Mensaje = "Usuario ya Existe, Verifique!"
                    };
                    return Json(arre, JsonRequestBehavior.AllowGet);
                }
                var Pass = HelperController.MD5Text(datos.Login);
                datos.Password = Pass;
                datos.FlagActive = true;
                basEntities.Auth_User.Add(datos);
                basEntities.SaveChanges();
                var arr = new
                {
                    Error = 0,
                    Mensaje = "Usuario creado satisfactoriamente!"
                };
                return Json(arr, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ChangepaswordForm(Auth_User datos)
        {
            try
            {
                basEntities = new BASEntities();
                Auth_User Update = basEntities.Auth_User.Where(x => x.UserID == datos.UserID).FirstOrDefault();

                var Pass = HelperController.MD5Text(datos.Password);

                Update.Password = Pass;

                var arr = new
                {
                    Error = 0,
                    Mensaje = "Contraseña actualizada correctamente!"
                };

                basEntities.SaveChanges();
                return Json(arr, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult EditUser(int UserID)
        {
            basEntities = new BASEntities();
            Auth_User Users = basEntities.Auth_User.Where(x => x.UserID == UserID).FirstOrDefault();

            //Lista de areas
            var listA = basEntities.Auth_Area.Where(x => x.ActiveFlag == true).OrderBy(x => x.AreaName).ToList();
            SelectList listaA = new SelectList(listA, "AreaId", "AreaName", Users.AreaId);
            ViewData["listaA"] = listaA;

            //Lista de roles
            var listR = basEntities.Auth_Roles.Where(x => x.ActiveFlag == true).OrderBy(x => x.RoleName).ToList();
            SelectList listaR = new SelectList(listR, "RoleId", "RoleName", Users.RoleId);
            ViewData["listaR"] = listaR;

            ViewBag.Users = Users;
            return View();
        }

        public ActionResult ActiveUser(int UserID)
        {
            basEntities = new BASEntities();

            Auth_User Update = basEntities.Auth_User.Where(x => x.UserID == UserID).FirstOrDefault();

            if(Update.FlagActive == false)
            {
                Update.FlagActive = true;
            }
            else
            {
                Update.FlagActive = false;
            }

            basEntities.SaveChanges();

            List<fn_allAuthUser_Result> Users = basEntities.fn_allAuthUser().ToList();
            ViewData["Users"] = Users;

            return View("Index");
        }

        public ActionResult Permission(int UserID)
        {
            string apiUrl = string.Empty;

            Auth_User _Auth_User = new Auth_User();
            apiUrl = "http://GetAuth_UserById.sendero.api";

            RestClient restClient = new RestClient(apiUrl);
            RestRequest request = new RestRequest("api/GetAuth_UserById", Method.GET);

            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("UserID", UserID);

            var response = restClient.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _Auth_User = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<Auth_User>(response.Content);
                ViewBag.Usuario = _Auth_User;
            }
            else
            {

            }

            List<Companias> Companies_Lst = GetAllCompanies();
            SelectList Companias_Lst = new SelectList(Companies_Lst, "ID", "Nombre".Trim().ToUpper(), 0);
            ViewData["Companias_Lst"] = Companias_Lst;

            List<Auth_UserCompany> Auth_UserCompany = GetAllAuth_UserCompanyByUserID(UserID);
            SelectList Auth_UserCompany_Lst = new SelectList(Auth_UserCompany, "CompanyID", "Nombre".Trim().ToUpper(), 0);
            ViewData["Auth_UserCompany_Lst"] = Auth_UserCompany_Lst;

            List<UnidadesNegocios> BusinessUnits_Lst = GetAllBusinessUnitsByCompanyId(Companies_Lst.FirstOrDefault().ID);
            SelectList UnidadesNegocio_Lst = new SelectList(BusinessUnits_Lst, "ID", "Nombre".Trim().ToUpper(), 0);
            ViewData["BusinessUnits_Lst"] = UnidadesNegocio_Lst;

            List<Farm> Farms_Lst = GetAllFarms();
            SelectList Fincas_Lst = new SelectList(Farms_Lst, "ID", "Nombre".Trim().ToUpper(), 0);
            ViewData["Fincas_Lst"] = Fincas_Lst;

            return View(_Auth_User);
        }

        public JsonResult GetParents(int UserID)
        {
            basEntities = new BASEntities();
            //int UserID = Convert.ToInt32(Session["UserID"]);
            var TodosPermisos = basEntities.fn_GetAllPermissionsByUser(UserID, 0).OrderBy(x => x.Name);

            List<Lista> results = new List<Lista>();
            foreach (var lista in TodosPermisos)
            {
                Lista tmp = new Lista();
                tmp.id = lista.FeatureID;
                tmp.text = lista.Name;
                tmp.@checked = lista.Permision;
                tmp.children = GetChildren(UserID, Convert.ToInt32(lista.FeatureID));
                results.Add(tmp);
            }
            return this.Json(results, JsonRequestBehavior.AllowGet);
        }

        private List<Lista> GetChildren(int UserID, int ParentFeatureID)
        {
            var listas= basEntities.fn_GetAllPermissionsByUser(UserID, ParentFeatureID).OrderBy(x => x.Name);

            List<Lista> results = new List<Lista>();
            foreach (var lista in listas)
            {
                Lista tmp = new Lista();
                tmp.id = lista.FeatureID;
                tmp.text = lista.Name;
                tmp.@checked = lista.Permision;
                tmp.children = GetChildren(UserID, Convert.ToInt32(lista.FeatureID));
                results.Add(tmp);
            }

            return results;
        }

        public void ChangePermision(Auth_UserFeature datos, string State)
        {
            basEntities = new BASEntities();
            var Permiso = basEntities.Auth_UserFeature.Where(x=>x.FeatureID == datos.FeatureID && x.UserID == datos.UserID).FirstOrDefault();

            if(State == "checked" && Permiso == null)
            {
                basEntities.Auth_UserFeature.Add(datos);
                basEntities.SaveChanges();
            }

            if(State == "unchecked" && Permiso != null)
            {
                Auth_UserFeature _Auth_UserFeature = basEntities.Auth_UserFeature.Where(x => x.UserFeatureID == Permiso.UserFeatureID).FirstOrDefault();
                if (_Auth_UserFeature != null)
                {
                    _Auth_UserFeature.UserID = Permiso.UserID;
                    _Auth_UserFeature.FeatureID = Permiso.FeatureID;

                    basEntities.Auth_UserFeature.Remove(_Auth_UserFeature);
                    basEntities.SaveChanges();
                }
            }
        }

        [HttpPost]
        public JsonResult FiltroRoles(Auth_Roles datos)
        {
            basEntities = new BASEntities();
            //Lista de roles
            var listR = basEntities.Auth_Roles.Where(x => x.ActiveFlag == true && x.AreaId == datos.AreaId).OrderBy(x => x.RoleName).ToList();
            return Json(listR, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PostAuth_UserCompany(int UserID, int CompanyID)
        {
            try
            {
                basEntities = new BASEntities();
                siembrasEntities = new SIEMBRASEntities();

                Auth_UserCompany _Auth_UserCompany =
                    (from usercompanies in basEntities.Auth_UserCompany.Where(x => x.UserID == UserID && x.CompanyID == CompanyID).ToList()
                     join companies in siembrasEntities.Companias on usercompanies.CompanyID equals companies.ID
                     select new Auth_UserCompany
                     {
                         UserCompanyID = usercompanies.UserCompanyID,
                         UserID = usercompanies.UserID,
                         CompanyID = usercompanies.CompanyID,
                         FlagActive = usercompanies.FlagActive,
                         Codigo = companies.Codigo,
                         Nombre = companies.Nombre,
                         Direccion = companies.Direccion,
                     }).FirstOrDefault();

                if (_Auth_UserCompany != null)
                {
                    return Json(Resources.Resources.YaExisteLaInformacion.Replace("{0}", _Auth_UserCompany.Nombre), JsonRequestBehavior.AllowGet);
                }

                _Auth_UserCompany = new Auth_UserCompany();
                _Auth_UserCompany.UserID = UserID;
                _Auth_UserCompany.CompanyID = CompanyID;
                _Auth_UserCompany.FlagActive = 1;

                basEntities.Auth_UserCompany.Add(_Auth_UserCompany);
                basEntities.SaveChanges();

                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult PostAuth_UserBusinessUnit(int UserID, int BusinessUnitID)
        {
            try
            {
                basEntities = new BASEntities();
                siembrasEntities = new SIEMBRASEntities();

                Auth_UserBusinessUnit _Auth_UserBusinessUnit =
                    (from auth_UserBusinessUnit in basEntities.Auth_UserBusinessUnit.Where(x => x.UserID == UserID && x.BusinessUnitID == BusinessUnitID).ToList()
                     join businessUnits in siembrasEntities.UnidadesNegocios on auth_UserBusinessUnit.BusinessUnitID equals businessUnits.ID
                     join companies in siembrasEntities.Companias on businessUnits.IDCompanias equals companies.ID
                     select new Auth_UserBusinessUnit
                     {
                         UserBusinessUnitID = auth_UserBusinessUnit.UserBusinessUnitID,
                         UserID = auth_UserBusinessUnit.UserID,
                         BusinessUnitID = auth_UserBusinessUnit.BusinessUnitID,
                         FlagActive = auth_UserBusinessUnit.FlagActive,
                         CompanyId = companies.ID,
                         CompanyCodigo = companies.Codigo,
                         CompanyName = companies.Nombre,
                         BusinessUnitName = businessUnits.Nombre,
                     }).FirstOrDefault();

                if (_Auth_UserBusinessUnit != null)
                {
                    return Json(Resources.Resources.YaExisteLaInformacion.Replace("{0}", _Auth_UserBusinessUnit.BusinessUnitName), JsonRequestBehavior.AllowGet);
                }

                _Auth_UserBusinessUnit = new Auth_UserBusinessUnit();
                _Auth_UserBusinessUnit.UserID = UserID;
                _Auth_UserBusinessUnit.BusinessUnitID = BusinessUnitID;
                _Auth_UserBusinessUnit.FlagActive = 1;

                basEntities.Auth_UserBusinessUnit.Add(_Auth_UserBusinessUnit);
                basEntities.SaveChanges();

                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult DeactivateCompanyRow(int UserCompanyID)
        {
            try
            {
                basEntities = new BASEntities();

                Auth_UserCompany _Auth_UserCompany = basEntities.Auth_UserCompany.Where(x => x.UserCompanyID == UserCompanyID).FirstOrDefault();
                if (_Auth_UserCompany == null)
                {
                    return Json(Resources.Resources.NoSeEncontraronDatos, JsonRequestBehavior.AllowGet);
                }

                if(_Auth_UserCompany.FlagActive == 1)
                {
                    _Auth_UserCompany.FlagActive = 0;
                }
                else
                {
                    _Auth_UserCompany.FlagActive = 1;
                }

                basEntities.SaveChanges();

                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult JsonGetAllAuth_UserCompanyByUserID(int UserID)
        {
            try
            {
                return Json(GetAllAuth_UserCompanyByUserID(UserID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Auth_UserCompany> GetAllAuth_UserCompanyByUserID(int UserID)
        {
            siembrasEntities = new SIEMBRASEntities();

            try
            {
                List<Auth_UserCompany> lstResult = new List<Auth_UserCompany>();

                var url = "http://GetAllAuth_UserCompanyByUserID.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/GetAllAuth_UserCompanyByUserID", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("UserID", UserID);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    lstResult = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<Auth_UserCompany>>(response.Content);
                }
                else
                {

                }

                return lstResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult JsonGetAllAuth_UserFarmByUserID(int UserID)
        {
            try
            {
                return Json(GetAllAuth_UserFarmByUserID(UserID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Auth_UserFarm> GetAllAuth_UserFarmByUserID(int UserID)
        {
            siembrasEntities = new SIEMBRASEntities();

            try
            {
                List<Auth_UserFarm> lstResult = new List<Auth_UserFarm>();

                var url = "http://GetAllAuth_UserFarmByUserID.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/GetAllAuth_UserFarmByUserID", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("UserID", UserID);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    lstResult = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<Auth_UserFarm>>(response.Content);
                }
                else
                {

                }

                return lstResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult JsonGetAuth_UserCompanyByUserIDAndCompanyID(int UserID, int CompanyID)
        {
            try
            {
                return Json(GetAuth_UserCompanyByUserIDAndCompanyID(UserID, CompanyID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Auth_UserCompany GetAuth_UserCompanyByUserIDAndCompanyID(int UserID, int CompanyID)
        {
            basEntities = new BASEntities();
            siembrasEntities = new SIEMBRASEntities();

            Auth_UserCompany Results =
                (from usercompanies in basEntities.Auth_UserCompany.Where(x => x.UserID == UserID && x.CompanyID == CompanyID).ToList()
                 join companies in siembrasEntities.Companias on usercompanies.CompanyID equals companies.ID
                 select new Auth_UserCompany
                 {
                     UserCompanyID = usercompanies.UserCompanyID,
                     UserID = usercompanies.UserID,
                     CompanyID = usercompanies.CompanyID,
                     FlagActive = usercompanies.FlagActive,
                     Codigo = companies.Codigo,
                     Nombre = companies.Nombre,
                     Direccion = companies.Direccion,
                 }).FirstOrDefault();

            return Results;
        }

        public JsonResult JsonGetAllCompanies()
        {
            siembrasEntities = new SIEMBRASEntities();

            try
            {
                List<Companias> lstResult = GetAllCompanies().OrderBy(x => x.Nombre).ToList();

                return this.Json(lstResult, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Companias> GetAllCompanies()
        {
            siembrasEntities = new SIEMBRASEntities();

            try
            {
                List<Companias> lstResult = new List<Companias>();

                var url = "http://GetAllCompanies.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/GetAllCompanies", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    lstResult = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<Companias>>(response.Content).OrderBy( x => x.Nombre).ToList();
                }
                else
                {

                }

                return lstResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult JsonGetAllBusinessUnitsByCompanyId(int CompanyId)
        {
            siembrasEntities = new SIEMBRASEntities();

            try
            {
                List<UnidadesNegocios> lstResult = GetAllBusinessUnitsByCompanyId(CompanyId).OrderBy(x => x.Nombre).ToList();

                return this.Json(lstResult, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UnidadesNegocios> GetAllBusinessUnitsByCompanyId(int CompanyId)
        {
            siembrasEntities = new SIEMBRASEntities();

            try
            {
                List<UnidadesNegocios> lstResult = new List<UnidadesNegocios>();

                var url = "http://GetBusinessUnitsByCompanyId.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/GetBusinessUnitsByCompanyId", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("CompanyId", CompanyId);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    lstResult = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<UnidadesNegocios>>(response.Content);
                }
                else
                {

                }

                return lstResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult JsonGetAuth_UserBusinessUnitByCompanyID(int UserID, int CompanyId)
        {
            try
            {
                return Json(GetAuth_UserBusinessUnitByCompanyID(UserID, CompanyId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Auth_UserBusinessUnit> GetAuth_UserBusinessUnitByCompanyID(int UserID, int CompanyId)
        {
            try
            {
                string jsonResult = string.Empty;
                string apiUrl = string.Empty;

                List<Auth_UserBusinessUnit> _Auth_UserBusinessUnit_Lst = new List<Auth_UserBusinessUnit>();
                apiUrl = "http://GetAllAuth_UserBusinessUnitByUserIDAndCompanyId.sendero.api";

                RestClient restClient = new RestClient(apiUrl);
                RestRequest request = new RestRequest("api/GetAllAuth_UserBusinessUnitByUserIdAndCompanyId", Method.GET);

                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("UserID", UserID);
                request.AddParameter("CompanyId", CompanyId);

                var response = restClient.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    jsonResult = response.Content;

                    _Auth_UserBusinessUnit_Lst = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<Auth_UserBusinessUnit>>(jsonResult);
                    ViewBag.Auth_UserBusinessUnit = _Auth_UserBusinessUnit_Lst;
                }
                else
                {

                }

                return _Auth_UserBusinessUnit_Lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult DeactivateBusinessUnitRow(int UserBusinessUnitID)
        {
            try
            {
                basEntities = new BASEntities();

                Auth_UserBusinessUnit _Auth_UserBusinessUnit = basEntities.Auth_UserBusinessUnit.Where(x => x.UserBusinessUnitID == UserBusinessUnitID).FirstOrDefault();
                if (_Auth_UserBusinessUnit == null)
                {
                    return Json(Resources.Resources.NoSeEncontraronDatos, JsonRequestBehavior.AllowGet);
                }

                if (_Auth_UserBusinessUnit.FlagActive == 1)
                {
                    _Auth_UserBusinessUnit.FlagActive = 0;
                }
                else
                {
                    _Auth_UserBusinessUnit.FlagActive = 1;
                }

                basEntities.SaveChanges();

                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult JsonGetAllFarms()
        {
            try
            {
                List<Farm> lstResult = GetAllFarms().OrderBy(x => x.Nombre).ToList();

                return this.Json(lstResult, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Farm> GetAllFarms()
        {
            newSafexEntities = new NewSafexEntities();

            try
            {
                List<Farm> lstResult = new List<Farm>();

                var url = "http://GetAllFarms.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/GetAllFarms", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    lstResult = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<Farm>>(response.Content).Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre).ToList();
                }
                else
                {

                }

                return lstResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult PostAuth_UserFarm(int UserID, int FarmID)
        {
            try
            {
                basEntities = new BASEntities();
                newSafexEntities = new NewSafexEntities();

                Auth_UserFarm _Auth_UserFarm =
                    (from userFarms in basEntities.Auth_UserFarm.Where(x => x.UserID == UserID && x.FarmID == FarmID).ToList()
                     join farms in newSafexEntities.Farm on userFarms.FarmID equals farms.ID
                     select new Auth_UserFarm
                     {
                         UserFarmID = userFarms.UserFarmID,
                         UserID = userFarms.UserID,
                         FarmID = userFarms.FarmID,
                         FlagActive = userFarms.FlagActive,
                         ID = farms.ID,
                         ModifiedDate = Convert.ToDateTime(farms.ModifiedDate),
                         Nombre = farms.Nombre,
                         Code = farms.Code,
                         Abbr = farms.Abbr,
                         IDOriginsFarms = Convert.ToInt32(farms.IDOriginsFarms),
                         IDPaises = Convert.ToInt32(farms.IDPaises),
                         FlagFarmProduction = Convert.ToInt32(farms.FlagFarmProduction),
                         IDBAS_NFDivision = Convert.ToInt32(farms.IDBAS_NFDivision),
                         IDPaymentTerm = Convert.ToInt32(farms.IDPaymentTerm),
                         Telefono1 = farms.Telefono1,
                         Telefono2 = farms.Telefono2,
                         Direccion1 = farms.Direccion1,
                         Direccion2 = farms.Direccion2,
                         Zip = farms.Zip,
                         ContactoDespachos = farms.ContactoDespachos,
                         EMailContactoDespachos = farms.EMailContactoDespachos,
                         CostoHandling = Convert.ToDecimal(farms.CostoHandling),
                         FlagFarmCode = Convert.ToInt32(farms.FlagFarmCode),
                         UPCFarmCode = farms.UPCFarmCode,
                         Codigo = farms.Codigo,
                         NombreQB = farms.NombreQB,
                         CodigoQB = farms.CodigoQB,
                         FlagIFURelabelReq = Convert.ToInt32(farms.FlagIFURelabelReq),
                         FlagConfirmaWEB = Convert.ToInt32(farms.FlagConfirmaWEB),
                         IDFincasCentrosProduccion = Convert.ToInt32(farms.IDFincasCentrosProduccion),
                         IDProductionPlace = Convert.ToInt32(farms.IDProductionPlace),
                         FlagActivo = Convert.ToInt32(farms.FlagActivo),
                         IDGruposFincas = Convert.ToInt32(farms.IDGruposFincas),
                         FlagFarmLocal = Convert.ToInt32(farms.FlagFarmLocal),
                         FlagEmailFirmOrder = Convert.ToInt32(farms.FlagEmailFirmOrder),
                     }).FirstOrDefault();

                if (_Auth_UserFarm != null)
                {
                    return Json(Resources.Resources.YaExisteLaInformacion.Replace("{0}", _Auth_UserFarm.Nombre), JsonRequestBehavior.AllowGet);
                }

                _Auth_UserFarm = new Auth_UserFarm();
                _Auth_UserFarm.UserID = UserID;
                _Auth_UserFarm.FarmID = FarmID;
                _Auth_UserFarm.FlagActive = 1;

                basEntities.Auth_UserFarm.Add(_Auth_UserFarm);
                basEntities.SaveChanges();

                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult DeactivateFarmRow(int UserFarmID)
        {
            try
            {
                basEntities = new BASEntities();

                Auth_UserFarm _Auth_UserFarm = basEntities.Auth_UserFarm.Where(x => x.UserFarmID == UserFarmID).FirstOrDefault();
                if (_Auth_UserFarm == null)
                {
                    return Json(Resources.Resources.NoSeEncontraronDatos, JsonRequestBehavior.AllowGet);
                }

                if (_Auth_UserFarm.FlagActive == 1)
                {
                    _Auth_UserFarm.FlagActive = 0;
                }
                else
                {
                    _Auth_UserFarm.FlagActive = 1;
                }

                basEntities.SaveChanges();

                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}