﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class IncidenceSeverityController : BaseController
    {
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        SIEMBRASEntities siembrasEntities = null;

        // GET: InsidenceSeverity
        public ActionResult Index()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var ActualYear = DateTime.Today.Year;
            string currentWeek = BaseController.GetWeek(DateTime.Now).ToString("00");
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            SelectList lstWeek = null;
            DateTime strStartDate = DateTime.Now.AddYears(-1);
            DateTime strEndDate = DateTime.Now.AddMonths(1);

            var aListWeek = siembras_PLANNEREntities.sp_GetWeekList(strStartDate, strEndDate).ToList();

            lstWeek = new SelectList(aListWeek, "YEARWEEK", "YEARWEEK".Trim().ToUpper(), DateTime.Now.Year.ToString("0000") + currentWeek.ToString());
            ViewData["listWeek"] = lstWeek;


            var product_Lst = siembras_PLANNEREntities.fn_getAllActiveProducts().OrderBy(x => x.Name).ToList();
            SelectList productList = new SelectList(product_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["productList"] = productList;

            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().OrderBy(x => x.Name).ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            var listBT = siembras_PLANNEREntities.BAS_Pests.OrderBy(a => a.ID).OrderBy(x => x.Name);
            SelectList listaBT = new SelectList((from s in listBT
                                                 select new
                                                 {
                                                     Id = s.ID,
                                                     Name = s.Name + " - " + s.Abrr
                                                 }),
                            "Id",
                            "Name",
                            null);
            ViewData["listaBT"] = listaBT;

            //Datos para dropDown de semanas

            return View();
        }

        [HttpPost]
        public ActionResult ProductByBlock(int BlockId)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var ProductByBlock = siembras_PLANNEREntities.fn_getProductByBlock(BlockId).ToList();
            SelectList ProductByBlockList = new SelectList(ProductByBlock, "PlantProductID", "PlantProductName");
            return Json(ProductByBlockList);
        }

        [HttpPost]
        public JsonResult GreeHousebyBusinessUnit(int BusinessUnitID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            //Datos para dropDown de compañias
            var aList = siembras_PLANNEREntities.fn_getGreenHousebyBusinessUnit(BusinessUnitID).OrderBy(a => a.Orden).ToList();
            SelectList lstGreenhouse = new SelectList(aList, "ID", "Codigo".Trim().ToUpper());
            return Json(lstGreenhouse);
        }

        [HttpPost]
        public JsonResult DrawReportLines(IncidenceSeverityModel d)
        {
            siembrasEntities = new SIEMBRASEntities();

            SqlConnection entityConnection = (SqlConnection)siembrasEntities.Database.Connection;
            SqlConnection cnn = entityConnection;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "IncidenceSeverity";


            SqlParameter WeekIni = new SqlParameter("@fec_inicio", SqlDbType.Int);
            SqlParameter WeekFin = new SqlParameter("@fec_fin", SqlDbType.Int);
            SqlParameter BusinessUnitID = new SqlParameter("@prmUnidadNegocio", SqlDbType.Int);
            SqlParameter GreeHouseID = new SqlParameter("@prmInvernadero", SqlDbType.Int);
            SqlParameter PestID = new SqlParameter("@prmBlancoBiologico", SqlDbType.Int);
            SqlParameter PlantProductID = new SqlParameter("@prmClasesProductos", SqlDbType.Int);
            SqlParameter ReportType = new SqlParameter("@prmTipoConsulta", SqlDbType.Int);


            WeekIni.Value = d.WeekIni;
            WeekFin.Value = d.WeekFin;
            BusinessUnitID.Value = d.BusinessUnitID;
            GreeHouseID.Value = d.GreeHouseID;
            PestID.Value = d.PestID;
            PlantProductID.Value = d.PlantProductID;
            ReportType.Value = d.ReportType;

            cmd.Parameters.Add(WeekIni);
            cmd.Parameters.Add(WeekFin);
            cmd.Parameters.Add(BusinessUnitID);
            cmd.Parameters.Add(GreeHouseID);
            cmd.Parameters.Add(PestID);
            cmd.Parameters.Add(PlantProductID);
            cmd.Parameters.Add(ReportType);

            cnn.Open();

            List<IncidenceSeverity> ResultList = new List<IncidenceSeverity>();

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    IncidenceSeverity result = new IncidenceSeverity();

                    result.Bloque = Convert.ToString(dr[0]);
                    result.Semana = Convert.ToString(dr[1]);
                    result.Anio = Convert.ToInt32(dr[2]);
                    result.PPIncidencia = Convert.ToDecimal(dr[3]);
                    result.PPSeveridad = Convert.ToDecimal(dr[4]);
                    result.Monitoreo = Convert.ToInt32(dr[5]);
                    result.Color = Convert.ToString(dr[6]);
                    ResultList.Add(result);
                }
                dr.Close();
            }

            cnn.Close();

            //var ResultData = siembrasEntities.CargaInformacionGraficaRegistroFitosanitarioDetallado(d.WeekID, d.GreeHouseID, d.BlockID, d.PestID, d.ProductID);
            return Json(ResultList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DrawReport(IncidenceSeverityModel d)
        {
            siembrasEntities = new SIEMBRASEntities();

            SqlConnection entityConnection = (SqlConnection)siembrasEntities.Database.Connection;
            SqlConnection cnn = entityConnection;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "CargaInformacionGraficaRegistroFitosanitarioDetallado";

            SqlParameter WeekID = new SqlParameter("@prmSemana", SqlDbType.Int);
            SqlParameter GreeHouseID = new SqlParameter("@prmUnidadNegocio", SqlDbType.Int);
            SqlParameter BlockID = new SqlParameter("@prmInvernadero", SqlDbType.Int);
            SqlParameter PestID = new SqlParameter("@prmBlancoBiologico", SqlDbType.Int);
            SqlParameter ProductID = new SqlParameter("@prmClasesProductos", SqlDbType.Int);

            WeekID.Value = d.WeekFin;
            GreeHouseID.Value = d.GreeHouseID;
            BlockID.Value = d.GreeHouseID;
            PestID.Value = d.PestID;
            ProductID.Value = d.PlantProductID;

            cmd.Parameters.Add(WeekID);
            cmd.Parameters.Add(GreeHouseID);
            cmd.Parameters.Add(BlockID);
            cmd.Parameters.Add(PestID);
            cmd.Parameters.Add(ProductID);

            cnn.Open();

            List<CargaInformacionGraficaRegistroFitosanitarioDetallado> ResultList = new List<CargaInformacionGraficaRegistroFitosanitarioDetallado>();

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    CargaInformacionGraficaRegistroFitosanitarioDetallado result = new CargaInformacionGraficaRegistroFitosanitarioDetallado();

                    result.Semana = Convert.ToInt32(dr[0]);
                    result.Anio = Convert.ToInt32(dr[1]);
                    result.IDInvernaderos_Codigo = Convert.ToInt32(dr[2]);
                    result.IDPlagas_Nombre = Convert.ToString(dr[3]);
                    result.ClasesProductos_Nombre = Convert.ToString(dr[4]);
                    result.PorcIncidencia = Convert.ToDecimal(dr[5]);
                    result.PorcSeveridad = Convert.ToDecimal(dr[6]);
                    result.Color = Convert.ToString(dr[7]);

                    ResultList.Add(result);
                }
                dr.Close();
            }

            cnn.Close();

            //var ResultData = siembrasEntities.CargaInformacionGraficaRegistroFitosanitarioDetallado(d.WeekID, d.GreeHouseID, d.BlockID, d.PestID, d.ProductID);
            return Json(ResultList, JsonRequestBehavior.AllowGet);
        }

    }
}