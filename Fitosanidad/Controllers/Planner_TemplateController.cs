﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planner.Model;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class Planner_TemplateController : BaseController
    {
        SIEMBRAS_PLANNEREntities SIEMBRAS_PLANNEREntities = null;

        public ActionResult Index()
        {
            SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            ViewBag.Planner_TemplateList = SIEMBRAS_PLANNEREntities.fn_getAllPlanner_Template().OrderBy(x => x.PT_Name).ThenBy(x => x.PTT_Name);

            return View(SIEMBRAS_PLANNEREntities);
        }

        public ActionResult Create()
        {
            SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var Planner_TemplateTypeLst = SIEMBRAS_PLANNEREntities.fn_getAllPlanner_TemplateType().Where(n => n.ActiveFlag == true).OrderBy(x => x.Name);
            SelectList lstPlanner_TemplateType = new SelectList(Planner_TemplateTypeLst, "TemplateTypeId", "Name");
            ViewData["lstPlanner_TemplateType"] = lstPlanner_TemplateType;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Planner_Template _Planner_Template, FormCollection Form)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                    Planner_Template getPlanner_Template = SIEMBRAS_PLANNEREntities.Planner_Template.Where(x => x.TemplateID == _Planner_Template.TemplateID).First();

                    if(getPlanner_Template == null)
                    {
                        Planner_TemplateInsert(_Planner_Template);
                    }
                    else
                    {
                        Planner_TemplateInsert(_Planner_Template);
                    }

                    return RedirectToAction("Planner_Template");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(_Planner_Template);
        }

        public ActionResult Planner_TemplateCreate(string Name, string Description, int TemplateTypeID, bool ActiveFlag)
        {
            string strRetVlr = "";
            try
            {
                SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

                Planner_Template _Planner_Template = new Planner_Template();
                _Planner_Template.TemplateID = SIEMBRAS_PLANNEREntities.Planner_Template.Max(x => x.TemplateID) + 1;
                _Planner_Template.Name = Name;
                _Planner_Template.Description = Description;
                _Planner_Template.TemplateTypeID = TemplateTypeID;
                _Planner_Template.ActiveFlag = ActiveFlag;
                Planner_TemplateInsert(_Planner_Template);
            }
            catch (Exception ex)
            {
                strRetVlr = ex.ToString();
                throw ex;
            }
            return Json(strRetVlr, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int _TemplateID)
        {
            SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            Planner_Template _Planner_Template = SIEMBRAS_PLANNEREntities.Planner_Template.First(x => x.TemplateID == _TemplateID);

            SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var Planner_TemplateTypeLst = SIEMBRAS_PLANNEREntities.fn_getAllPlanner_TemplateType().Where(n => n.ActiveFlag == true).OrderBy(x => x.Name);
            SelectList lstPlanner_TemplateType = new SelectList(Planner_TemplateTypeLst, "TemplateTypeId", "Name", _Planner_Template.TemplateTypeID);
            ViewData["lstPlanner_TemplateType"] = lstPlanner_TemplateType;

            ViewBag.Planner_TemplateDetailDetailList = SIEMBRAS_PLANNEREntities.fn_getAllPlanner_TemplateDetailByTemplateID(_Planner_Template.TemplateID).OrderBy(x => x.Name);

            return View(_Planner_Template);
        }

        public void Planner_TemplateInsert(Planner_Template _Planner_Template)
        {
            try
            {
                SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                SIEMBRAS_PLANNEREntities.Planner_Template.Add(_Planner_Template);
                SIEMBRAS_PLANNEREntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Planner_TemplateUpdate(Planner_Template _Planner_Template)
        {
            try
            {
                SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                SIEMBRAS_PLANNEREntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Planner_TemplateDetail(int _TemplateID)
        {
            SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            ViewBag.Planner_TemplateList = SIEMBRAS_PLANNEREntities.fn_getAllPlanner_TemplateDetailByTemplateID(_TemplateID).OrderBy(x => x.Name);

            return View();
        }

    }
}