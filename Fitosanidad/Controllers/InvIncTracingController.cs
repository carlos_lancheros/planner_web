﻿using Planner.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Fitosanidad.Controllers
{
    public class InvIncTracingController : BaseController
    {
        // GET: InvIncTracing
        SIEMBRASEntities siembrasEntities = null;
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        public ActionResult Index()
        {
            var ActualYear = DateTime.Today.Year;
            string currentWeek = BaseController.GetWeek(DateTime.Now).ToString("00");
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            SelectList lstWeek = null;
            DateTime strStartDate = DateTime.Now.AddYears(-1);
            DateTime strEndDate = DateTime.Now.AddMonths(1);

            var aListWeek = siembras_PLANNEREntities.sp_GetWeekList(strStartDate, strEndDate).ToList();

            lstWeek = new SelectList(aListWeek, "YEARWEEK", "YEARWEEK".Trim().ToUpper(), DateTime.Now.Year.ToString("0000") + currentWeek.ToString());
            ViewData["listWeek"] = lstWeek;

            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().OrderBy(x => x.Name).ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            ShowData(lstWeek.First().Value, lstWeek.First().Value, BussinesUnitList.First().Value);
            return View();
        }

        public ActionResult ShowData(string WeekIDInic, string WeekIDFina, string BusinessUnitID)
        {
            int AnioInic = Convert.ToInt32(WeekIDInic.Substring(0, 4));
            var SemanaInic = WeekIDInic.Substring(4, 2);

            int AnioFina = Convert.ToInt32(WeekIDFina.Substring(0, 4));
            var SemanaFina = WeekIDFina.Substring(4, 2);

            siembrasEntities = new SIEMBRASEntities();
            siembrasEntities.Database.CommandTimeout = 200000;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var SemanasInic = siembras_PLANNEREntities.Semanas.Where(x => x.Anio == AnioInic && x.Nombre == SemanaInic).FirstOrDefault();
            var SemanasFina = siembras_PLANNEREntities.Semanas.Where(x => x.Anio == AnioFina && x.Nombre == SemanaFina).FirstOrDefault();

            DateTime Inicia = Convert.ToDateTime(SemanasInic.FInicial).Date;
            DateTime Termina = Convert.ToDateTime(SemanasFina.FFinal).Date;

            var InvTracingList = siembras_PLANNEREntities.sp_GetInvTracing(null).ToList();
            ViewBag.InvTracing_List = InvTracingList;

            return Json(ViewBag.InvTracing_List, JsonRequestBehavior.AllowGet);

        }

        public ActionResult DocTracing(int SowingID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var InvTracingList = siembras_PLANNEREntities.sp_GetInvTracing(SowingID).ToList();
            ViewBag.InvTracing_List = InvTracingList;


            var InvTracingListDetail = siembras_PLANNEREntities.sp_GetInvTracingDetail(SowingID).ToList();
            ViewBag.InvTracing_ListDetail = InvTracingListDetail;

            //GetUPCPhotoFile("InvInc_f21.jpg");

            return View();
        }

        [HttpPost]
        public ActionResult GetUPCPhotoFile(string strPhotoFileName)
        {
            try
            {
                strPhotoFileName = strPhotoFileName.Trim();

                HelperController.DownloadFile("10.99.0.13", "ftpdev", "bogota19+", "", "", strPhotoFileName);

                var jsonResult = Json(string.Empty, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                var jsonResult = Json(ex.Message.ToString(), JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
        }

        public static void DownloadFile(string strFtpServer, string strFtpUser, string strFtpPassword, string strFileSource, string strFileTarget, string strFileName)
        {
            if (string.IsNullOrEmpty(strFileTarget))
            {
                strFileTarget = HostingEnvironment.MapPath("\\Downloads");
            }

            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create("ftp://" + strFtpServer.Trim() + "/" + strFileSource.Trim() + "/" + strFileName.Trim());

            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential(strFtpUser.Trim(), strFtpPassword.Trim());
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = true;
            request.EnableSsl = false;

            if (System.IO.File.Exists(strFileTarget + "/" + strFileName))
            {
                System.IO.File.Delete(strFileTarget + "/" + strFileName);
            }

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (Stream responseStream = response.GetResponseStream())
            {
                using (Stream fileStream = new FileStream(strFileTarget + "/" + strFileName, FileMode.CreateNew))
                {
                    responseStream.CopyTo(fileStream);
                }
            }
        }

    }
}