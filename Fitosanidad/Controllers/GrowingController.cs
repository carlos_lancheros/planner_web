﻿using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using Microsoft.Win32.SafeHandles;
using Newtonsoft.Json;
using Planner.Model;
using System.Globalization;
using System;

namespace Fitosanidad.Controllers
{

    public class GrowingController : Controller
    {
        // GET: Growing
        HORUS_CTRL_NOVEDADESEntities _Horus_Ctrl_NovedadesEntities = null;
        public ActionResult Index()
        {
            _Horus_Ctrl_NovedadesEntities = new HORUS_CTRL_NOVEDADESEntities();


            SelectList ddlCentrosOperacion = null;
            //var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1 && x.Nombre.Contains("POST")).OrderBy(x => x.Nombre);
            var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1 && !x.Nombre.Contains("POST")).OrderBy(x => x.Nombre);
            ddlCentrosOperacion = new SelectList(aList, "ID", "Nombre".Trim().ToUpper());
            ViewData["ddlCentrosOperacion"] = ddlCentrosOperacion;

            SelectList lstCentrosCosto = null;
            var aListc = _Horus_Ctrl_NovedadesEntities.CentrosCosto.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
            lstCentrosCosto = new SelectList(aListc, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCentrosCosto"] = lstCentrosCosto;           


            showCollaborator(ddlCentrosOperacion.First().Value, lstCentrosCosto.First().Value);

            return View();
        }

        public ActionResult showCollaborator(string CentrosOperacionID, string CentrosCostoID)
        {
            var CO = Convert.ToInt32(CentrosOperacionID);
            var CC = Convert.ToInt32(CentrosCostoID);
            _Horus_Ctrl_NovedadesEntities = new HORUS_CTRL_NOVEDADESEntities();

            if (CC == 0) //Solo Centro de Operación
            {
                var GrowingList = _Horus_Ctrl_NovedadesEntities.Empleados.Where(x => x.FlagActivo == 1 && x.IDCentrosOperacion == CO).OrderBy(x => x.Nombre).ToList();
                ViewBag.Growing_List = GrowingList;
            }
            else
            {
                var GrowingList = _Horus_Ctrl_NovedadesEntities.Empleados.Where(x => x.FlagActivo == 1 && x.IDCentrosOperacion == CO && x.IDCentrosCosto == CC).OrderBy(x => x.Nombre).ToList();
                ViewBag.Growing_List = GrowingList;
            }
            return PartialView("Growing_List");
        }        
    }
}