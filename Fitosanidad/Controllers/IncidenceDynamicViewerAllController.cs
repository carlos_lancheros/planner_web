﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;
using System.Data.SqlClient;
using System.Data;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class IncidenceDynamicViewerAllController : BaseController
    {
        SIEMBRASEntities siembrasEntities = null;
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        // GET: IncidenceDynamicViewerAll
        public ActionResult Index()
        {
            var ActualYear = DateTime.Today.Year;
            string currentWeek = BaseController.GetWeek(DateTime.Now).ToString("00");
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            SelectList lstWeek = null;
            DateTime strStartDate = DateTime.Now.AddYears(-1);
            DateTime strEndDate = DateTime.Now.AddMonths(1);

            var aListWeek = siembras_PLANNEREntities.sp_GetWeekList(strStartDate, strEndDate).ToList();

            lstWeek = new SelectList(aListWeek, "YEARWEEK", "YEARWEEK".Trim().ToUpper(), DateTime.Now.Year.ToString("0000") + currentWeek.ToString());
            ViewData["listWeek"] = lstWeek;

            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().OrderBy(x => x.Name).ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            //Lista Blancos Biológicos
            var listBT = siembras_PLANNEREntities.BAS_Pests.OrderBy(a => a.ID);
            SelectList listaBT = new SelectList((from s in listBT
                                                 select new
                                                 {
                                                     Id = s.ID,
                                                     Name = s.Name + " - " + s.Abrr
                                                 }),
                            "Id",
                            "Name",
                            null);
            ViewData["listaBT"] = listaBT;
            return View();
        }

        [HttpPost]
        public ActionResult TraeData(string WeekIDInic, string WeekIDFina, int GreeHouseID, int Alto,int Medio, int Bajo)
        {

            siembrasEntities = new SIEMBRASEntities();
            siembrasEntities.Database.CommandTimeout = 200000;
          
            List<sp_VisorDinamicoV2All_Result> Data = siembrasEntities.sp_VisorDinamicoV2All(WeekIDInic, WeekIDFina, GreeHouseID, 0, null, Alto, Medio, Bajo).ToList();

            var jsonResult = Json(Data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}