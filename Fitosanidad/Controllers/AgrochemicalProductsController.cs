﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Planner.Model;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class AgrochemicalProductsController : BaseController
    {
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;

        // GET: AgrochemicalProducts
        public ActionResult Index()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Lista Productos genericos
            var listPG = siembras_PLANNEREntities.fn_AgroChemicalsProducts().OrderBy(x => x.Name).ToList();
            SelectList listaPG = new SelectList(listPG, "Reference", "Name");
            ViewData["listaPG"] = listaPG;

            //Lista HORA DE ENTRADA
            var listRH = siembras_PLANNEREntities.Aspersion_ReentryHour.Distinct().OrderBy(x => x.Name).ToList();
            SelectList listaRH = new SelectList(listRH, "ReentryHourID", "Name");
            ViewData["listaRH"] = listaRH;

            //Lista grupos Químicos
            var listCG = siembras_PLANNEREntities.Aspersion_ChemicalGroup.Distinct().OrderBy(x => x.Name).ToList();
            SelectList listaCG = new SelectList(listCG, "ChemicalGroupID", "Name");
            ViewData["listaCG"] = listaCG;

            //Lista frac
            var listFrac = siembras_PLANNEREntities.Aspersion_Frac.Distinct().OrderBy(x => x.Name).ToList();
            SelectList listaFrac = new SelectList(listFrac, "FracID", "Name");
            ViewData["listaFrac"] = listaFrac;

            //Lista Ingrediente activo
            var listIA = siembras_PLANNEREntities.Aspersion_ActiveIngredient.Distinct().OrderBy(x => x.Name).ToList();
            SelectList listaIA = new SelectList(listIA, "ActiveIngredientID", "Name");
            ViewData["listaIA"] = listaIA;

            //Lista categoría toxicológica
            var listCT = siembras_PLANNEREntities.Aspersion_ToxicologicalCategory.Distinct().OrderBy(x => x.Name).ToList();
            SelectList listaCT = new SelectList(listCT, "ToxicologicalCategoryID", "Name");
            ViewData["listaCT"] = listaCT;

            //Lista Blancos Biológicos
            var listBT = siembras_PLANNEREntities.BAS_Pests.OrderBy(a => a.ID);
            SelectList listaBT = new SelectList((from s in listBT
                                                 select new
                                                 {
                                                     Id = s.ID,
                                                     Name = s.Name + " - " + s.Abrr
                                                 }),
                            "Id",
                            "Name",
                            null);
            ViewData["listaBT"] = listaBT;

            //Lista unidades de medida
            var listMU = siembras_PLANNEREntities.Aspersion_MeasurementUnits.Distinct().OrderBy(x => x.Name).ToList();
            SelectList listaMU = new SelectList(listMU, "MeasurementUnitsID", "Name");
            ViewData["listaMU"] = listaMU;


            return View();
        }

        public ActionResult DetalleMaestro()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var DetalleMaestro = siembras_PLANNEREntities.fn_detalleAgrochemicalProducts().OrderByDescending(a => a.AgrochemicalProductsID).ToList();
            ViewBag.DetalleMaestro = DetalleMaestro;

            //return Json(DetalleMaestro, JsonRequestBehavior.AllowGet);
            return PartialView("DetalleMaestro");
        }

        [HttpPost]
        public ActionResult Crear(Aspersion_AgrochemicalProducts datos)
        {
            var Edita = Request.QueryString["Edita"];
            try
            {
                //if (ModelState.IsValid)
                //{
                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                Aspersion_AgrochemicalProducts aspersion_AgrochemicalProducts = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.Where(x => x.AgrochemicalProductsID == datos.AgrochemicalProductsID).FirstOrDefault();

                if (aspersion_AgrochemicalProducts == null)
                {
                    Aspersion_AgrochemicalProducts _Aspersion_AgrochemicalProducts = new Aspersion_AgrochemicalProducts();
                    _Aspersion_AgrochemicalProducts.ProductOrigin = datos.ProductOrigin;
                    _Aspersion_AgrochemicalProducts.Name = datos.Name;
                    _Aspersion_AgrochemicalProducts.ChemicalGroupID = datos.ChemicalGroupID;
                    _Aspersion_AgrochemicalProducts.FracID = datos.FracID;
                    _Aspersion_AgrochemicalProducts.ActiveIngredientsID = datos.ActiveIngredientsID;
                    _Aspersion_AgrochemicalProducts.ToxicologicalCategoryID = datos.ToxicologicalCategoryID;
                    _Aspersion_AgrochemicalProducts.ReentryHourID = datos.ReentryHourID;
                    _Aspersion_AgrochemicalProducts.Aspersion = datos.Aspersion;
                    _Aspersion_AgrochemicalProducts.Sprinkle = datos.Sprinkle;
                    _Aspersion_AgrochemicalProducts.Drench = datos.Drench;
                    _Aspersion_AgrochemicalProducts.Thermonebulizer = datos.Thermonebulizer;
                    _Aspersion_AgrochemicalProducts.Density = datos.Density;
                    _Aspersion_AgrochemicalProducts.PlagasID = datos.PlagasID;
                    _Aspersion_AgrochemicalProducts.MeasurementUnitsID = datos.MeasurementUnitsID;
                    _Aspersion_AgrochemicalProducts.Concentration = datos.Concentration;
                    _Aspersion_AgrochemicalProducts.ProductType = datos.ProductType;
                    _Aspersion_AgrochemicalProducts.ActiveFlag = true;

                    if (Edita == "0")
                    {
                        AgrochemicalProductsInsert(_Aspersion_AgrochemicalProducts);

                    }
                    else
                    {
                        _Aspersion_AgrochemicalProducts.AgrochemicalProductsID = datos.AgrochemicalProductsID;
                        AgrochemicalProductsUpdate(_Aspersion_AgrochemicalProducts);
                    }
                }
                else
                {
                    if (Edita == "0")
                    {
                        AgrochemicalProductsInsert(datos);

                    }
                    else
                    {
                        AgrochemicalProductsUpdate(datos);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index");
        }

        public void AgrochemicalProductsInsert(Aspersion_AgrochemicalProducts _Aspersion_AgrochemicalProducts)
        {
            try
            {
                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.Add(_Aspersion_AgrochemicalProducts);
                siembras_PLANNEREntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AgrochemicalProductsUpdate(Aspersion_AgrochemicalProducts _Aspersion_AgrochemicalProducts)
        {
            try
            {
                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                Aspersion_AgrochemicalProducts Update = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.First(x => x.AgrochemicalProductsID == _Aspersion_AgrochemicalProducts.AgrochemicalProductsID);
                Update.ProductOrigin= _Aspersion_AgrochemicalProducts.ProductOrigin;
                Update.Name= _Aspersion_AgrochemicalProducts.Name;
                Update.ChemicalGroupID = _Aspersion_AgrochemicalProducts.ChemicalGroupID;
                Update.FracID = _Aspersion_AgrochemicalProducts.FracID;
                Update.ActiveIngredientsID = _Aspersion_AgrochemicalProducts.ActiveIngredientsID;
                Update.ToxicologicalCategoryID = _Aspersion_AgrochemicalProducts.ToxicologicalCategoryID;
                Update.ReentryHourID = _Aspersion_AgrochemicalProducts.ReentryHourID;
                Update.Aspersion = _Aspersion_AgrochemicalProducts.Aspersion;
                Update.Sprinkle = _Aspersion_AgrochemicalProducts.Sprinkle;
                Update.Drench = _Aspersion_AgrochemicalProducts.Drench;
                Update.Thermonebulizer = _Aspersion_AgrochemicalProducts.Thermonebulizer;
                Update.Density = _Aspersion_AgrochemicalProducts.Density;
                Update.PlagasID = _Aspersion_AgrochemicalProducts.PlagasID;
                Update.MeasurementUnitsID = _Aspersion_AgrochemicalProducts.MeasurementUnitsID;
                Update.Concentration = _Aspersion_AgrochemicalProducts.Concentration;
                Update.ProductType = _Aspersion_AgrochemicalProducts.ProductType;
                siembras_PLANNEREntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Edit(int id)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            Aspersion_AgrochemicalProducts _AgrochemicalProducts = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.First(x => x.AgrochemicalProductsID == id);
            int _ProductOrigin = Convert.ToInt32(_AgrochemicalProducts.ProductOrigin);

            //Lista Productos genericos
            var listPG = siembras_PLANNEREntities.AgroChemicalsProducts.Where(x => x.CompanyID == 1).Distinct().OrderBy(x => x.Name).ToList();
            SelectList listaPG = new SelectList(listPG, "Reference", "Name", _ProductOrigin);
            ViewData["listaPG"] = listaPG;

            //Lista HORA DE ENTRADA
            var listRH = siembras_PLANNEREntities.Aspersion_ReentryHour.Distinct().OrderBy(x => x.Name).ToList();
            SelectList listaRH = new SelectList(listRH, "ReentryHourID", "Name", _AgrochemicalProducts.ReentryHourID);
            ViewData["listaRH"] = listaRH;

            //Lista grupos Químicos
            var listCG = siembras_PLANNEREntities.Aspersion_ChemicalGroup.Distinct().OrderBy(x => x.Name).ToList();
            SelectList listaCG = new SelectList(listCG, "ChemicalGroupID", "Name", _AgrochemicalProducts.ChemicalGroupID);
            ViewData["listaCG"] = listaCG;

            //Lista frac
            var listFrac = siembras_PLANNEREntities.Aspersion_Frac.Distinct().OrderBy(x => x.Name).ToList();
            SelectList listaFrac = new SelectList(listFrac, "FracID", "Name", _AgrochemicalProducts.FracID);
            ViewData["listaFrac"] = listaFrac;

            //Lista Ingrediente activo
            var listIA = siembras_PLANNEREntities.Aspersion_ActiveIngredient.Distinct().OrderBy(x => x.Name).ToList();
            SelectList listaIA = new SelectList(listIA, "ActiveIngredientID", "Name", _AgrochemicalProducts.ActiveIngredientsID);
            ViewData["listaIA"] = listaIA;

            //Lista categoría toxicológica
            var listCT = siembras_PLANNEREntities.Aspersion_ToxicologicalCategory.Distinct().OrderBy(x => x.Name).ToList();
            SelectList listaCT = new SelectList(listCT, "ToxicologicalCategoryID", "Name", _AgrochemicalProducts.ToxicologicalCategoryID);
            ViewData["listaCT"] = listaCT;

            //Lista Blancos Biológicos
            var listBT = siembras_PLANNEREntities.BAS_Pests.OrderBy(a => a.ID);
            SelectList listaBT = new SelectList((from s in listBT
                                                 select new
                                                 {
                                                     Id = s.ID,
                                                     Name = s.Name + " - " + s.Abrr
                                                 }),
                            "Id",
                            "Name",
                            null);
            ViewData["listaBT"] = listaBT;

            //Lista unidades de medida
            var listMU = siembras_PLANNEREntities.Aspersion_MeasurementUnits.Distinct().OrderBy(x => x.Name).ToList();
            SelectList listaMU = new SelectList(listMU, "MeasurementUnitsID", "Name", _AgrochemicalProducts.MeasurementUnitsID);
            ViewData["listaMU"] = listaMU;
            ViewData["Concentration"] = _AgrochemicalProducts.Concentration;
            ViewData["ProductType"] = _AgrochemicalProducts.ProductType;

            return View(_AgrochemicalProducts);
        }

        [HttpPost]
        public void CambiaEstado(Aspersion_AgrochemicalProducts _Aspersion_AgrochemicalProducts)
        {
            try
            {
                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                Aspersion_AgrochemicalProducts Update = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.First(x => x.AgrochemicalProductsID == _Aspersion_AgrochemicalProducts.AgrochemicalProductsID);
                Update.ActiveFlag = _Aspersion_AgrochemicalProducts.ActiveFlag;
                siembras_PLANNEREntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult GuardaNuevoRegistro(Aspersion_ActiveIngredient datosActive, Aspersion_Frac datosFrac, Aspersion_ChemicalGroup datos, string Ctrl, string Valor)
        {
            if(Ctrl == "ChemicalGroupID")
            {
                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                datos.ActiveFlag = true;
                datos.Name = Valor;
                siembras_PLANNEREntities.Aspersion_ChemicalGroup.Add(datos);
                siembras_PLANNEREntities.SaveChanges();

                var ListaGrupos = siembras_PLANNEREntities.Aspersion_ChemicalGroup.OrderBy(x => x.Name).ToList();
                return Json(ListaGrupos, JsonRequestBehavior.AllowGet);

            }

            if (Ctrl == "FracID")
            {
                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                datosFrac.ActiveFlag = true;
                datosFrac.Name = Valor;
                siembras_PLANNEREntities.Aspersion_Frac.Add(datosFrac);
                siembras_PLANNEREntities.SaveChanges();

                var ListaFrac = siembras_PLANNEREntities.Aspersion_Frac.OrderBy(x => x.Name).ToList();
                return Json(ListaFrac, JsonRequestBehavior.AllowGet);

            }

            if (Ctrl == "ActiveIngredientID")
            {
                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                datosActive.ActiveFlag = true;
                datosActive.Name = Valor;
                siembras_PLANNEREntities.Aspersion_ActiveIngredient.Add(datosActive);
                siembras_PLANNEREntities.SaveChanges();

                var ListaActive = siembras_PLANNEREntities.Aspersion_ActiveIngredient.OrderBy(x => x.Name).ToList();
                return Json(ListaActive, JsonRequestBehavior.AllowGet);

            }
            return Json("OK");
        }
    }
}