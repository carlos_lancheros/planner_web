﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planner.Model;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Xml.Linq;
using System.Globalization;


namespace Fitosanidad.Controllers
{
    [Authorize]
    public class FrozenAlertController : BaseController
    {
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;

        public ActionResult Alert()
        {
            Guid sessionId = Guid.NewGuid();
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            siembras_PLANNEREntities.Database.CommandTimeout = 200000;

            siembras_PLANNEREntities.sp_GetFrozenAlerts(sessionId.ToString());
            ViewBag.dataVariables = (from FA in siembras_PLANNEREntities.IOT_FrozenAlert_Variables
                                     where FA.sessionId == sessionId.ToString()
                                     select FA).ToList();
            ViewBag.dataResults = (from FA in siembras_PLANNEREntities.IOT_FrozenAlert_Results
                                   where FA.sessionId == sessionId.ToString()
                                   select FA).ToList();
            siembras_PLANNEREntities.sp_DelFrozenAlerts(sessionId.ToString());
            return View(siembras_PLANNEREntities);
        }
    }
}