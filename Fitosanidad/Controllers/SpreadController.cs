﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planner.Model;
using Resources;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class SpreadController : BaseController
    {
        SIEMBRAS_PLANNEREntities _SIEMBRAS_PLANNEREntities = null;

        public ActionResult ProductParameter()
        {
            GetAllSpread_ProductParameter();

            return View();
        }

        public ActionResult GetAllSpread_ProductParameter()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var Result = _SIEMBRAS_PLANNEREntities.fn_getAllSpread_ProductParameter().ToList();

            ViewData["ProductParameterList"] = Result.ToList();
            return PartialView("ProductParameterList");
        }

        public ActionResult Create()
        {
            return View();
        }

    }
}