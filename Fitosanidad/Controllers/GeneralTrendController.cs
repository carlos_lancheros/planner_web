﻿using Planner.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class GeneralTrendController : BaseController
    {
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        SIEMBRASEntities siembrasEntities = null;
        // GET: GeneralTrend
        public ActionResult Index()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var ActualYear = DateTime.Today.Year;
            string currentWeek = BaseController.GetWeek(DateTime.Now).ToString("00");
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            SelectList lstWeek = null;
            DateTime strStartDate = DateTime.Now.AddYears(-1);
            DateTime strEndDate = DateTime.Now.AddMonths(1);

            var aListWeek = siembras_PLANNEREntities.sp_GetWeekList(strStartDate, strEndDate).ToList();

            lstWeek = new SelectList(aListWeek, "YEARWEEK", "YEARWEEK".Trim().ToUpper(), DateTime.Now.Year.ToString("0000") + currentWeek.ToString());
            ViewData["listWeek"] = lstWeek;

            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().OrderBy(x => x.Name).ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            var product_Lst = siembras_PLANNEREntities.fn_getAllActiveProducts().OrderBy(x => x.Name).ToList();
            SelectList productList = new SelectList(product_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["productList"] = productList;


            return View();
        }

        public ActionResult getReport(int weekIni, int weekFin,int BusinessUnitID)
        {
          

            siembrasEntities = new SIEMBRASEntities();

            ViewBag.GeneralTrendList = siembrasEntities.GreenhouseTendence(weekIni.ToString(),weekFin.ToString(),BusinessUnitID).ToList();
            return Json(ViewBag.GeneralTrendList, JsonRequestBehavior.AllowGet);

          //  return PartialView("GeneralTrendList");
        }
    }
}