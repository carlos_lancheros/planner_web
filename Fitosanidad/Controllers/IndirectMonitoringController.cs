﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;
using System.Data.SqlClient;
using System.Data;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class IndirectMonitoringController : BaseController
    {
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        // GET: IndirectMonitoring
        public ActionResult Index()
        {
            var ActualYear = DateTime.Today.Year;
            string currentWeek = BaseController.GetWeek(DateTime.Now).ToString("00");
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            SelectList lstWeek = null;
            DateTime strStartDate = DateTime.Now.AddYears(-1);
            DateTime strEndDate = DateTime.Now.AddMonths(1);

            var aListWeek = siembras_PLANNEREntities.sp_GetWeekList(strStartDate, strEndDate).ToList();

            lstWeek = new SelectList(aListWeek, "YEARWEEK", "YEARWEEK".Trim().ToUpper(), DateTime.Now.Year.ToString("0000") + currentWeek.ToString());
            ViewData["listWeek"] = lstWeek;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            return View();
        }

        [HttpPost]
        public ActionResult TraeData(string WeekIDInic, string WeekIDFina, int GreenHouseID, int TipoPlaca)
        {
            int AnioInic = Convert.ToInt32(WeekIDInic.Substring(0, 4));
            var SemanaInic = WeekIDInic.Substring(4, 2);

            int AnioFina = Convert.ToInt32(WeekIDFina.Substring(0, 4));
            var SemanaFina = WeekIDFina.Substring(4, 2);

            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var SemanasInic = siembras_PLANNEREntities.Semanas.Where(x => x.Anio == AnioInic && x.Nombre == SemanaInic).FirstOrDefault();

            var SemanasFina = siembras_PLANNEREntities.Semanas.Where(x => x.Anio == AnioFina && x.Nombre == SemanaFina).FirstOrDefault();
            //var Data = siembrasEntities.VisorDinamicoRegistroFitosanitario().ToList();
            List<CargaInformeMonitoreoIndirectoUnidad_Result> Data = siembras_PLANNEREntities.CargaInformeMonitoreoIndirectoUnidad(SemanasInic.ID, SemanasFina.ID, GreenHouseID, TipoPlaca).ToList();

            //return Json(Data, JsonRequestBehavior.AllowGet);

            var jsonResult = Json(Data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}