﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Planner.Model;

namespace Fitosanidad.Controllers
{
    public class SummaryBQTController : Controller
    {
        BASEntities _BASEntities = null;

        public class SummaryBQT
        {
            public string Empleado { get; set; }
            public string HInicialDefinitivo { get; set; }
            public string HFinalDefinitivo { get; set; }
            public string CantidadTallo { get; set; }
            public string Horas { get; set; }
            public string Rendimiento { get; set; }
        }

        // GET: SummaryBQT
        public ActionResult Index()
        {
            _BASEntities = new BASEntities();

            SelectList lstCentrosOperacion = null;
            var aList = _BASEntities.BusinessUnit.OrderBy(x => x.Name);
            //lstCentrosOperacion = new SelectList(aList, "BusinessUnitID", "Name".Trim().ToUpper());
            //ViewData["lstCentrosOperacion"] = lstCentrosOperacion;

            Summary("2019-09-02");            

            return View();
        }

        [HttpPost]
        public ActionResult Summary(string Fecha)
        {
            _BASEntities = new BASEntities();

            DateTime FechaInicial = DateTime.Parse(Fecha);

            SqlConnection entityConnection = (SqlConnection)_BASEntities.Database.Connection;
            SqlConnection cnn = entityConnection;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_GetPerformanceBQT";

            SqlParameter prmFInicial = new SqlParameter("@prmFInicial", SqlDbType.DateTime);
            SqlParameter prmFFinal = new SqlParameter("@prmFFinal", SqlDbType.DateTime);
            SqlParameter prmTipoReporte = new SqlParameter("@prmTipoReporte", SqlDbType.Int);



            prmFInicial.Value = FechaInicial;
            prmFFinal.Value = FechaInicial;
            prmTipoReporte.Value = 1;




            cmd.Parameters.Add(prmFInicial);
            cmd.Parameters.Add(prmFFinal);
            cmd.Parameters.Add(prmTipoReporte);

            cnn.Open();

            List<SummaryBQT> SummaryBQTList = new List<SummaryBQT>();

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    SummaryBQT _SummaryBQT = new SummaryBQT();

                    _SummaryBQT.Empleado = dr[0].ToString();
                    _SummaryBQT.HInicialDefinitivo = dr[1].ToString();
                    _SummaryBQT.HFinalDefinitivo = dr[2].ToString();
                    _SummaryBQT.CantidadTallo = dr[3].ToString();
                    _SummaryBQT.Horas = dr[4].ToString();
                    _SummaryBQT.Rendimiento = dr[5].ToString();

                    SummaryBQTList.Add(_SummaryBQT);
                }

                dr.Close();
            }

            cnn.Close();

            ViewBag.SummaryBQT_List = SummaryBQTList;

            //var SummaryBQT_List = _BASEntities.sp_GetPerformanceBQT(Fecha, Fecha, CentroOperacionID,1).ToList();
            //ViewBag.Summary_List = SummaryBQT_List;

            return PartialView("SummaryBQT_List");
        }
    }
}