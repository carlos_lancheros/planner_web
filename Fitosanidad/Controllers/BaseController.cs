﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

using Planner.Model;
using Globalization;
using Resources;
using System.Web.Security;

using System.Web.Routing;

namespace Fitosanidad.Controllers
{
    public class BaseController : Controller
    {
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        BASEntities _BASEntities = null;

        string _Error = string.Empty;
        string _Menu = string.Empty;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string _Controller = this.ControllerContext.RouteData.Values["controller"].ToString();
            string _Action = this.ControllerContext.RouteData.Values["action"].ToString();
            string _Login = User.Identity.Name;

            try
            {
                if (Session.IsNewSession & User.Identity.IsAuthenticated)
                {
                    System.Web.HttpContext.Current.Cache.Remove(User.Identity.Name);
                    FormsAuthentication.SignOut();
                    Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-1);

                    Session["IsAutenticated"] = true;
                }
                else if ((Session.IsNewSession & !User.Identity.IsAuthenticated) || (!Session.IsNewSession & !User.Identity.IsAuthenticated))
                {
                    Session["IsAutenticated"] = null;
                }
                else
                {
                    Session["IsAutenticated"] = true;
                }

                if (Session["IsAutenticated"] != null)
                {
                    if ((bool)Session["IsAutenticated"] == true && Session["SessionID"] == null)
                    {
                        Session["SessionID"] = Guid.NewGuid();
                    }

                    _BASEntities = new BASEntities();

                    int _UserID = _BASEntities.Auth_User.Where(x => x.Login == User.Identity.Name).FirstOrDefault().UserID;

                    Session["usrUserID"] = _UserID;
                    Session["usrAuth_UserCompany"] = _BASEntities.Auth_UserCompany.Where(x => x.UserID == _UserID && x.FlagActive == 1).ToList();
                    Session["usrAuth_UserBusinessUnit"] = _BASEntities.Auth_UserBusinessUnit.Where(x => x.UserID == _UserID && x.FlagActive == 1).ToList();
                    Session["usrAuth_UserFarm"] = _BASEntities.Auth_UserFarm.Where(x => x.UserID == _UserID && x.FlagActive == 1).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception exception = filterContext.Exception;
            //Logging the Exception
            filterContext.ExceptionHandled = true;

            var Result = this.View("Error", new HandleErrorInfo(exception,
                filterContext.RouteData.Values["controller"].ToString(),
                filterContext.RouteData.Values["action"].ToString()));

            filterContext.Result = Result;
        }

        private bool IsAjax(ExceptionContext filterContext)
        {
            return filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
        }

        public ActionResult setError(string Controller, string Action, HandleErrorInfo Error )
        {

            return RedirectToAction("ErrorHandling", "Home", new HandleErrorInfo(new System.ArgumentException(_Error), Controller, Action));
        }

        public ActionResult SetCulture(string culture)
        {
            //culture = CultureHelper.GetImplementedCulture(culture);
            //RouteData.Values["culture"] = culture;

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Retorna el valor la traducción de _key
        /// </summary>
        /// <param name="_Key">Palabra o frase a traducir</param>
        /// <returns>Palabra o frase en el idioma actual</returns>
        public ActionResult GetResource(string _Key, string[] _Parametros)
        {
            if (Session["Idioma"] == null)
            {
                Session["Idioma"] = CultureInfo.CurrentUICulture;// "es-CO";
            }

            CultureInfo culture = new CultureInfo(Session["Idioma"].ToString());

            if (Resources.Resources.ResourceManager.GetString(_Key, culture) == null)
            {
                return Json(Resources.Resources.ResourceManager.GetString("RecursoNoEncontrado", culture).ToString() + " " + _Key, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (_Parametros != null)
                {
                    return Json(Resources.Resources.ResourceManager.GetString(_Key, culture).Replace("{Valor}", _Parametros[0]), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(Resources.Resources.ResourceManager.GetString(_Key, culture), JsonRequestBehavior.AllowGet);
                }
            }
        }

        public static int GetWeek(DateTime time)
        {
            int Week = 0;

            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            Week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            return Week;
        }

        public static string GetWeekWithYear(DateTime time)
        {
            int Week = 0;

            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            Week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            return time.Year.ToString("0000") + " - " + Week.ToString();
        }

        public static List<DateTime> GetWeekDateFromYearAndWeek(int year, int week)
        {
            var firstDate = FirstDateOfWeek(year, week);
            var allWeekDays = new List<DateTime>();
            allWeekDays.Add(firstDate);
            var currentDate = firstDate;
            for (int d = 1; d < 7; d++)
            {
                currentDate = currentDate.AddDays(1);
                allWeekDays.Add(currentDate);
            }

            return allWeekDays;
        }

        public static DateTime FirstDateOfWeek(int year, int week)
        {
            DateTime FirstDayOfYear = new DateTime(year, 1, 1);
            int daysOffset = Convert.ToInt32(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek) - Convert.ToInt32(FirstDayOfYear.DayOfWeek);
            DateTime firstWeekDay = FirstDayOfYear.AddDays(daysOffset);
            System.Globalization.CultureInfo curCulture = System.Globalization.CultureInfo.CurrentCulture;
            int firstWeek = curCulture.Calendar.GetWeekOfYear(FirstDayOfYear, curCulture.DateTimeFormat.CalendarWeekRule, curCulture.DateTimeFormat.FirstDayOfWeek);
            if (firstWeek <= 1)
            {
                week -= 1;
            }
            return firstWeekDay.AddDays(week * 7);
        }

        private void getMenu(IList<sp_GetAccessByUser_Result> AccessByUser)
        {
            IList<sp_GetAccessByUser_Result> _Nivel0 = AccessByUser.Where(x => x.ParentFeatureID == 0).OrderBy(x => x.Name).ToList();

            try
            {
                foreach (var _opcion in _Nivel0)
                {
                    _Menu += "<li class='treeview'>";
                    _Menu += "    <a href = '#' >";
                    _Menu += "        <i class='" + _opcion.Icon.Trim() + "'></i><span>" + _opcion.Name.Trim() + "</span><span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span>";
                    _Menu += "    </a>";
                    _Menu += "    <ul class='treeview-menu'>";
                    getOptions(_opcion, AccessByUser);
                    _Menu += "    </ul>";
                    _Menu += "</li>";

                    //_Menu += "<li class='dropdown dropdown-submenu'>";
                    ////_Menu += "    <a class='dropdown-toggle' data-toggle='dropdown' style='font-size:80%'  title='" + _textoInv + "' href='#' >" + _texto + "</a>";
                    //_Menu += "    <ul class='dropdown-menu'>";
                    //// getOptions(_opcion, AccessByUser);
                    //_Menu += "    </ul>";
                    //_Menu += "</li>";
                }
                Session["Menu"] = _Menu;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void getOptions(sp_GetAccessByUser_Result _opcion, IList<sp_GetAccessByUser_Result> AccessByUser)
        {
            IList<sp_GetAccessByUser_Result> _SubModulos = AccessByUser.Where(x => x.ParentFeatureID == _opcion.FeatureID).OrderBy(x => x.Name).ToList();

            foreach (var _opcionsub in _SubModulos.ToList())
            {
                if (Convert.ToString(_opcion.FeatureID) == Convert.ToString(_opcionsub.ParentFeatureID))
                {
                    _Menu += "<li><a href = /" + _opcionsub.Controller.Trim() + "/" + _opcionsub.Action.Trim() + "><i class='fa fa-circle-o'></i>" + _opcionsub.Name.Trim() + "</a></li>";
                }
            }
        }

        public ActionResult getBusinnesUnit()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var lstBusinessUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing()
                .OrderBy(a => a.Name)
                .ToList();

            ViewData["lstBusinessUnit"] = lstBusinessUnit;
            TempData["lstBusinessUnit"] = lstBusinessUnit;

            return Json(lstBusinessUnit, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGreenhousebyBusinessUnit(int intBusinessId)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var lstGreenhouse = siembras_PLANNEREntities.fn_getGreenHousebyBusinessUnit(intBusinessId)
                .OrderBy(a => a.Orden)
                .ToList();

            ViewData["lstGreenhouse"] = lstGreenhouse;
            TempData["lstGreenhouse"] = lstGreenhouse;

            return Json(lstGreenhouse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getGreenHousebyBusinessUnitId(int intBusinessId)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var greenhouseList = siembras_PLANNEREntities.fn_getGreenHousebyBusinessUnit(intBusinessId)
                .OrderBy(a => a.Orden)
                .ToList();
            ViewBag.greenhouseList = greenhouseList;

            return Json(greenhouseList, JsonRequestBehavior.AllowGet);
        }

        static String DayByNumber(int Day)
        {
            var DayName = Enum.GetName(typeof(DayOfWeek), Day);
            switch (DayName)
            {
                case "Monday":
                    DayName = "Lunes";
                    break;
                case "Tuesday":
                    DayName = "Martes";
                    break;
                case "Wednesday":
                    DayName = "Miércoles";
                    break;
                case "Thursday":
                    DayName = "Jueves";
                    break;
                case "Friday":
                    DayName = "Viernes";
                    break;
                case "Saturday":
                    DayName = "Sábado";
                    break;
                case "Sunday":
                    DayName = "Domingo";
                    break;
            }

            return DayName;
        }
    }
}