﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;
using System.Data.SqlClient;
using System.Data;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class TrapsController : BaseController
    {
        SIEMBRASEntities siembrasEntities = null;
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        // GET: Pest
        public ActionResult Index()
        {
            siembrasEntities = new SIEMBRASEntities();
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().OrderBy(x => x.Name).ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            List<sp_getAllTraps_Result> Datos = siembrasEntities.sp_getAllTraps(0, 0, 0).ToList();
            ViewBag.Data = Datos;

            List<BAS_TiposPlacasMonitoreo> TiposPlacas = siembrasEntities.BAS_TiposPlacasMonitoreo.ToList();
            SelectList TiposPlacasList = new SelectList(TiposPlacas, "ID", "Nombre");
            ViewData["TiposPlacas"] = TiposPlacasList;

            List<BAS_ClasesPlacasMonitoreo> ClasesPlacas = siembrasEntities.BAS_ClasesPlacasMonitoreo.ToList();
            SelectList ClasesPlacasList = new SelectList(ClasesPlacas, "ID", "Nombre");
            ViewData["ClasesPlacas"] = ClasesPlacasList;

            return View();
        }

        [HttpPost]
        public JsonResult Filtrar(int BusinessUnitId, int GreenHouseId, int TrapTypeId)
        {
            siembrasEntities = new SIEMBRASEntities();
            List<sp_getAllTraps_Result> Datos = siembrasEntities.sp_getAllTraps(BusinessUnitId, GreenHouseId, TrapTypeId).ToList();
            return Json(Datos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditTrapForm(int Id)
        {
            siembrasEntities = new SIEMBRASEntities();
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var datos = siembrasEntities.PlacasMonitoreo.Where(x => x.ID == Id).FirstOrDefault();

            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().OrderBy(x => x.Name).ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name", datos.IDUnidadesNegocios);
            ViewData["BussinesUnitList"] = BussinesUnitList;

            List<fn_getGreenHousebyBusinessUnit_Result> GreenHouse = siembras_PLANNEREntities.fn_getGreenHousebyBusinessUnit(datos.IDUnidadesNegocios).OrderBy(x => x.Codigo).ToList();
            SelectList GreenHouseList = new SelectList(GreenHouse, "ID", "Codigo", datos.IDInvernaderos);
            ViewData["GreenHouseList"] = GreenHouseList;

            List<BAS_TiposPlacasMonitoreo> TiposPlacas = siembrasEntities.BAS_TiposPlacasMonitoreo.ToList();
            SelectList TiposPlacasList = new SelectList(TiposPlacas, "ID", "Nombre", datos.IDBAS_TiposPlacasMonitoreo);
            ViewData["TiposPlacas"] = TiposPlacasList;

            List<BAS_ClasesPlacasMonitoreo> ClasesPlacas = siembrasEntities.BAS_ClasesPlacasMonitoreo.ToList();
            SelectList ClasesPlacasList = new SelectList(ClasesPlacas, "ID", "Nombre");
            ViewData["ClasesPlacas"] = ClasesPlacasList;

            ViewBag.data = datos;

            return View();
        }

        [HttpPost]
        public JsonResult GreenHouseByBusinessUnit(int BusinessUnitId)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            List<fn_getGreenHousebyBusinessUnit_Result> Data = siembras_PLANNEREntities.fn_getGreenHousebyBusinessUnit(BusinessUnitId).OrderBy(x => x.Codigo).ToList();

            return Json(Data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TrapSave(PlacasMonitoreo d)
        {
            siembrasEntities = new SIEMBRASEntities();

            var Registro = siembrasEntities.PlacasMonitoreo.Where(x => x.Nombre == d.Nombre && x.IDInvernaderos == d.IDInvernaderos && x.IDUnidadesNegocios == d.IDUnidadesNegocios && x.IDBAS_TiposPlacasMonitoreo == d.IDBAS_TiposPlacasMonitoreo).FirstOrDefault();

            if (Registro != null)
            {
                var res = new
                {
                    Error = "1",
                    Tipo = "error",
                    Mensaje = "Ya existe trampa, verifique!"
                };
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            else
            {
                PlacasMonitoreo U = siembrasEntities.PlacasMonitoreo.Where(x => x.ID == d.ID).FirstOrDefault();
                U.Nombre = d.Nombre;
                U.IDBAS_TiposPlacasMonitoreo = d.IDBAS_TiposPlacasMonitoreo;
                siembrasEntities.SaveChanges();

                var res = new
                {
                    Error = "1",
                    Tipo = "success",
                    Mensaje = "Registro editado correctamente!"
                };
                return Json(res, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult NewTrapForm()
        {
            siembrasEntities = new SIEMBRASEntities();
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().OrderBy(x => x.Name).ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            List<BAS_TiposPlacasMonitoreo> TiposPlacas = siembrasEntities.BAS_TiposPlacasMonitoreo.ToList();
            SelectList TiposPlacasList = new SelectList(TiposPlacas, "ID", "Nombre");
            ViewData["TiposPlacas"] = TiposPlacasList;

            List<BAS_ClasesPlacasMonitoreo> ClasesPlacas = siembrasEntities.BAS_ClasesPlacasMonitoreo.ToList();
            SelectList ClasesPlacasList = new SelectList(ClasesPlacas, "ID", "Nombre");
            ViewData["ClasesPlacas"] = ClasesPlacasList;

            return View();
        }

        public JsonResult TrapSaveNew(PlacasMonitoreo d)
        {
            siembrasEntities = new SIEMBRASEntities();

            var Registro = siembrasEntities.PlacasMonitoreo.Where(x => x.Nombre == d.Nombre && x.IDInvernaderos == d.IDInvernaderos && x.IDUnidadesNegocios == d.IDUnidadesNegocios && x.IDBAS_TiposPlacasMonitoreo == d.IDBAS_TiposPlacasMonitoreo && x.IDBAS_ClasesPlacasMonitoreo == d.IDBAS_ClasesPlacasMonitoreo ).FirstOrDefault();

            if (Registro != null)
            {
                var res = new
                {
                    Error = "1",
                    Tipo = "error",
                    Mensaje = "Ya existe trampa, verifique!"
                };
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            else
            {
                siembrasEntities.PlacasMonitoreo.Add(d);
                siembrasEntities.SaveChanges();

                var res = new
                {
                    Error = "1",
                    Tipo = "success",
                    Mensaje = "Registro creado correctamente!"
                };
                return Json(res, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult eliminarPlaca(int ID)
        {
            siembrasEntities = new SIEMBRASEntities();
            var Borrar = siembrasEntities.PlacasMonitoreo.Where(x => x.ID == ID).FirstOrDefault();
            siembrasEntities.PlacasMonitoreo.Remove(Borrar);
            siembrasEntities.SaveChanges();
            var res = new
            {
                Error = "1",
                Tipo = "success",
                Mensaje = "Registro Borrado correctamente!"
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GuardaClasePlaca(BAS_ClasesPlacasMonitoreo datos, string Ctrl, string Valor)
        {
            
            if (Ctrl == "TrapClassId")
            {
                siembrasEntities = new SIEMBRASEntities();
                datos.Nombre = Valor;
                siembrasEntities.BAS_ClasesPlacasMonitoreo.Add(datos);
                siembrasEntities.SaveChanges();

                var ClasesPlacas = siembrasEntities.BAS_ClasesPlacasMonitoreo.OrderBy(x => x.Nombre).ToList();
                return Json(ClasesPlacas, JsonRequestBehavior.AllowGet);

            }
            
            return Json("OK");
        }
    }
}