﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using System.Web.UI.WebControls;

using Planner.Model;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Xml.Linq;
using System.Globalization;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class PlantingPlanController : BaseController
    {

        public class UploadSiembras
        {
            public string ID_Unidad_Negocio { get; set; }
            public string Unidad_de_Negocios { get; set; }
            public string FasePropagacion { get; set; }
            public string ID_Producto { get; set; }
            public string Producto { get; set; }
            public string IDVariedad { get; set; }
            public string Variedad { get; set; }
            public string Bloque { get; set; }
            public string NCama { get; set; }
            public string Semana { get; set; }
            public string Fecha { get; set; }
            public string PBruta { get; set; }
            public string CantSembrar { get; set; }
            public string Temporada { get; set; }
        }

        // GET: PlantingPlan
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadFromFile()
        {
            string _DataMessageGUID = string.Empty;
            string _SensorID = string.Empty;
            string _Date = string.Empty;
            string _DataRaw = string.Empty;
            string _Humidity = string.Empty;
            string _Temperature = string.Empty;
            string _Luminosity = string.Empty;

            try
            {
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    var pic = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                    HttpPostedFileBase filebase = new HttpPostedFileWrapper(pic);
                    var fileName = Path.GetFileName(filebase.FileName);
                    var path = Path.Combine(Server.MapPath("~/Uploads/"), fileName);
                    filebase.SaveAs(path);

                    DataTable dtUpload = HelperController.Csv2DataTable(",", path);
                    List<UploadSiembras> lstUpload = new List<UploadSiembras>();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        UploadSiembras _UploadSensor = new UploadSiembras();

                        //_DataMessageGUID = dtUpload.Rows[i][0].ToString().Trim();
                        //_SensorID = dtUpload.Rows[i][1].ToString().Trim();
                        //_Date = dtUpload.Rows[i][2].ToString().Trim();
                        //_DataRaw = dtUpload.Rows[i][3].ToString().Trim();
                        //_DataRaw = _DataRaw.Substring(1, _DataRaw.Length - 2);

                        //_Humidity = _DataRaw.Split(',')[0].Trim();
                        //_Temperature = _DataRaw.Split(',')[1].Trim();
                        //_Luminosity = string.Empty;

                        //_UploadSensor.DataMessageGUID = _DataMessageGUID.Substring(1, _DataMessageGUID.Length - 2);
                        //_UploadSensor.SensorID = _SensorID.Substring(1, _SensorID.Length - 2);
                        //_UploadSensor.Date = Convert.ToDateTime(_Date.Replace("\"", ""), new CultureInfo("en-US")).AddHours(-5).ToString();
                        //_UploadSensor.Humidity = _Humidity;
                        //_UploadSensor.Temperature = _Temperature;
                        //_UploadSensor.Luminosity = _Luminosity;
                        //lstUpload.Add(_UploadSensor);
                    }

                    return Json(lstUpload, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("No File Saved.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}