﻿using Planner.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fitosanidad.Controllers
{
    public class PhotometerController : Controller
    {
        SIEMBRAS_PLANNEREntities _SIEMBRAS_PLANNEREntities = null;

        SelectList businessUnitList = null;
        // GET: Photometer
        public ActionResult Index()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            string currentWeek = BaseController.GetWeekWithYear(DateTime.Now);

            //View
            List<SelectListItem> View_Lst = new List<SelectListItem>();
            View_Lst.Add(new SelectListItem() { Text = Resources.Resources.PorDia, Value = "D" });
            View_Lst.Add(new SelectListItem() { Text = Resources.Resources.PorSemana, Value = "S" });
            ViewData["View_Lst"] = new SelectList(View_Lst, "Value", "Text");
            //View

            //Business Unit
            var businessUnitList_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            businessUnitList = new SelectList(businessUnitList_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["businessUnitList"] = businessUnitList;
            //Business Unit

            //Week Range
            SelectList lstWeek = null;
            var aListWeek = _SIEMBRAS_PLANNEREntities.Semanas.Where(n => n.Anio >= 2018).Select(x => new { ID = x.ID, Name = x.Anio + " - " + x.Nombre }).ToList();
            var intSelected = aListWeek.Find(x => x.Name == currentWeek);
            lstWeek = new SelectList(aListWeek, "ID", "Name".Trim().ToUpper(), intSelected.ID);
            ViewBag.lstWeekFrom = lstWeek;
            ViewBag.lstWeekTo = lstWeek;

            ViewBag.Photometer_List = _SIEMBRAS_PLANNEREntities.sp_GetPhotometer(Convert.ToInt32(lstWeek.FirstOrDefault().Value), Convert.ToInt32(lstWeek.FirstOrDefault().Value), Convert.ToInt32(businessUnitList.FirstOrDefault().Value));
            
            return View();
        }

        public ActionResult GetPhotometer(int prmSemanaINI, int prmSemanaFIN, int prmUnidadNegocio)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            ViewBag.Photometer_List = _SIEMBRAS_PLANNEREntities.sp_GetPhotometer(prmSemanaINI, prmSemanaFIN, prmUnidadNegocio);
            return PartialView("Photometer_List");
        }


    }
}