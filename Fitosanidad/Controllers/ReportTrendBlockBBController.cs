﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;
using System.Data.SqlClient;
using System.Data;
namespace Fitosanidad.Controllers
{
    [Authorize]
    public class ReportTrendBlockBBController : BaseController
    {
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        SIEMBRASEntities siembrasEntities = null;
        //SIEMBRASEntities siembrasEntities = null;
        // GET: ReportTrendBlockBB
        public ActionResult Index()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var ActualYear = DateTime.Today.Year;
            string currentWeek = BaseController.GetWeek(DateTime.Now).ToString("00");

            SelectList lstWeek = null;
            DateTime strStartDate = DateTime.Now.AddYears(-1);
            DateTime strEndDate = DateTime.Now.AddMonths(1);

            var aListWeek = siembras_PLANNEREntities.sp_GetWeekList(strStartDate, strEndDate).ToList();

            lstWeek = new SelectList(aListWeek, "YEARWEEK", "YEARWEEK".Trim().ToUpper(), DateTime.Now.Year.ToString("0000") + currentWeek.ToString());
            ViewData["listWeek"] = lstWeek;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            //Lista Blancos Biológicos
            var listBT = siembras_PLANNEREntities.BAS_Pests.OrderBy(a => a.ID);
            SelectList listaBT = new SelectList((from s in listBT
                                                 select new
                                                 {
                                                     Id = s.ID,
                                                     Name = s.Name + " - " + s.Abrr
                                                 }),
                            "Id",
                            "Name",
                            null);
            ViewData["listaBT"] = listaBT;

            return View();
        }

        public ActionResult CargaProductos(int GreenHouseID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            List<fn_getProductsByGreenHouse_Result> Productos = siembras_PLANNEREntities.fn_getProductsByGreenHouse(GreenHouseID).ToList();
            return Json(Productos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreaReporte(IncidenceSeverityModel d)
        {
            siembrasEntities = new SIEMBRASEntities();
            SqlConnection entityConnection = (SqlConnection)siembrasEntities.Database.Connection;
            SqlConnection cnn = entityConnection;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "IncidenceSeverityBB";


            SqlParameter WeekIni = new SqlParameter("@fec_inicio", SqlDbType.Int);
            SqlParameter WeekFin = new SqlParameter("@fec_fin", SqlDbType.Int);
            SqlParameter BusinessUnitID = new SqlParameter("@prmUnidadNegocio", SqlDbType.Int);
            SqlParameter GreeHouseID = new SqlParameter("@prmInvernadero", SqlDbType.Int);
            SqlParameter PlagaIdM = new SqlParameter("@prmBlancoBiologico", SqlDbType.VarChar);
            SqlParameter PlantProductID = new SqlParameter("@prmClasesProductos", SqlDbType.Int);
            SqlParameter ReportType = new SqlParameter("@prmTipoConsulta", SqlDbType.Int);


            WeekIni.Value = d.WeekIni;
            WeekFin.Value = d.WeekFin;
            BusinessUnitID.Value = d.BusinessUnitID;
            GreeHouseID.Value = d.GreeHouseID;
            PlagaIdM.Value = d.PlagaIdM;
            PlantProductID.Value = d.PlantProductID;
            ReportType.Value = d.ReportType;

            cmd.Parameters.Add(WeekIni);
            cmd.Parameters.Add(WeekFin);
            cmd.Parameters.Add(BusinessUnitID);
            cmd.Parameters.Add(GreeHouseID);
            cmd.Parameters.Add(PlagaIdM);
            cmd.Parameters.Add(PlantProductID);
            cmd.Parameters.Add(ReportType);

            cnn.Open();

            List<IncidenceSeverity> ResultList = new List<IncidenceSeverity>();

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    IncidenceSeverity result = new IncidenceSeverity();

                    result.Bloque = Convert.ToString(dr[0]);
                    result.Semana = dr[1].ToString();
                    result.Anio = Convert.ToInt32(dr[2]);
                    result.PPIncidencia = Convert.ToDecimal(dr[3]);
                    result.PPSeveridad = Convert.ToDecimal(dr[4]);
                    result.Plaga = Convert.ToString(dr[5]);
                    result.PlagaId = Convert.ToInt32(dr[6]);
                    result.Monitoreo = Convert.ToInt32(dr[7]);
                    result.Color = Convert.ToString(dr[8]);

                    ResultList.Add(result);
                }
                dr.Close();
            }

            cnn.Close();

            return Json(ResultList, JsonRequestBehavior.AllowGet);
        }


    }


}