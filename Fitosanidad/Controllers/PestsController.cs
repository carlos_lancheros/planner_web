﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planner.Model;
using Resources;

using System.Globalization;

using System.Data;
using System.Data.SqlClient;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class PestsController : BaseController
    {
        SIEMBRASEntities _SIEMBRASEntities = null;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexGrid()
        {
            try
            {
                return Json(GetAllPests(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Plagas _Pests, FormCollection form)
        {
            try
            {
                _SIEMBRASEntities = new SIEMBRASEntities();
                if (ModelState.IsValid)
                {
                    Plagas Pests_Add = new Plagas();
                    Pests_Add.ID = _Pests.ID;
                    Pests_Add.Nombre = _Pests.Nombre;
                    Pests_Add.Abrr = _Pests.Abrr;
                    Pests_Add.IDClasificacion = _Pests.IDClasificacion;
                    Pests_Add.ColorIdentificacion = _Pests.ColorIdentificacion;
                    Pests_Add.FlagActivo = _Pests.FlagActivo;

                    _SIEMBRASEntities.Plagas.Add(Pests_Add);
                    _SIEMBRASEntities.SaveChanges();

                    return RedirectToAction("Index");
                }

                return View(_Pests);
            }
            catch (Exception ex)
            {
                return View(_Pests);
            }
        }

        public ActionResult Edit(int ID)
        {
            _SIEMBRASEntities = new SIEMBRASEntities();
            Plagas Pests = _SIEMBRASEntities.Plagas.Where(x => x.ID == ID).FirstOrDefault();
            if (Pests == null)
            {
                return RedirectToAction("Index");
            }
            return View(Pests);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Plagas _Pests, FormCollection form)
        {
            try
            {
                _SIEMBRASEntities = new SIEMBRASEntities();
                if (ModelState.IsValid)
                {
                    Plagas Pests_Edit = _SIEMBRASEntities.Plagas.Where(x => x.ID == _Pests.ID).FirstOrDefault();
                    if (Pests_Edit == null)
                    {
                        return RedirectToAction("Index");
                    }
                    Pests_Edit.Nombre = _Pests.Nombre;
                    Pests_Edit.Abrr = _Pests.Abrr;
                    Pests_Edit.IDClasificacion = _Pests.IDClasificacion;
                    Pests_Edit.ColorIdentificacion = _Pests.ColorIdentificacion;
                    Pests_Edit.FlagActivo = (Convert.ToInt32(_Pests.FlagActivo) != 1 ? 0 : _Pests.FlagActivo);

                    _SIEMBRASEntities.SaveChanges();

                    return RedirectToAction("Index");
                }

                return View(_Pests);
            }
            catch (Exception ex)
            {
                return View(_Pests);
            }
        }

        public List<Plagas> GetAllPests()
        {
            _SIEMBRASEntities = new SIEMBRASEntities();
            List<Plagas> Results = _SIEMBRASEntities.Plagas.ToList();

            return Results;
        }

    }
}