﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;
using System.Data.SqlClient;
using System.Data;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class AspersionPlanController : BaseController
    {
        public class AspersionPlanConsolidated
        {
            public string AgrochemicalProductsID { get; set; }
            public string Unidad { get; set; }
            public string Lunes { get; set; }
            public string Martes { get; set; }
            public string Miercoles { get; set; }
            public string Jueves { get; set; }
            public string Luminosity { get; set; }
            public string Viernes { get; set; }
            public string Sabado { get; set; }
            public string Domingo { get; set; }
            public string total { get; set; }
            public int BussinesUnitId { get; set; }
            public int WeekID { get; set; }
        }

        public class Titulos
        {
            public string DayNumber { get; set; }
            public string Block { get; set; }
            public int DiaID { get; set; }
        }

        public class InidenciaSeveridadlist
        {
            public int Semana { get; set; }
            public int Anio { get; set; }
            public int IDInvernaderos { get; set; }
            public string block { get; set; }
            public int IdPlagas { get; set; }
            public string Pest { get; set; }
            public string Abrr { get; set; }
            public string ColorIdentification { get; set; }
            public int Beds { get; set; }
            public int CamasAfectadas { get; set; }
            public int CantidadCuadros { get; set; }
            public decimal PorcentajeIncidencia { get; set; }
            public decimal PorcentajeSeveridad { get; set; }
        }


        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        BASEntities basEntities = null;
        SIEMBRASEntities siembrasEntities = null;
        public ActionResult Index()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var AspersionPlan = siembras_PLANNEREntities.fn_detalleAspersionPlan(Convert.ToInt32(Session["UserID"])).ToList();
            ViewBag.datos = AspersionPlan;
            return View();
        }

        public ActionResult AspersionPlanAdd()
        {
            var ActualYear = DateTime.Today.Year;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Datos para dropDown de semanas
            var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio == ActualYear);
            SelectList listWeek = new SelectList((from s in Week
                                                  select new
                                                  {
                                                      ID = s.ID,
                                                      Nombre = s.Anio + "-" + s.Nombre
                                                  }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["listWeek"] = listWeek;

            //Datos para dropDown de compañias
            var company = siembras_PLANNEREntities.Company.Where(x => x.FlagActivo == 1).Distinct().OrderBy(x => x.ID);
            SelectList CompanyList = new SelectList(company, "ID", "Nombre");
            ViewData["CompanyList"] = CompanyList;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            //Datos para dropDown de compañias
            var PlanProducts = siembras_PLANNEREntities.PlantProducts.Where(x => x.FlagActive == 1).Distinct().OrderBy(x => x.PlantProductName);
            SelectList PlanProductsList = new SelectList(PlanProducts, "PlantProductID", "PlantProductName");
            ViewData["PlanProductsList"] = PlanProductsList;

            //Datos para dropDown de compañias
            var aplyTypes = siembras_PLANNEREntities.Aspersion_ApplyTypes.Where(n => n.ActiveFlag == true).OrderBy(x => x.ApplyTypeID).ToList();
            SelectList aplyTypesList = new SelectList(aplyTypes, "ApplyTypeID", "Name");
            ViewData["aplyTypesList"] = aplyTypesList;

            //Datos para dropDown de formas
            var Forms = siembras_PLANNEREntities.Aspersion_Forms.Where(n => n.ActiveFlag == true).OrderBy(x => x.FormID).ToList();
            SelectList FormsList = new SelectList(Forms, "FormID", "Name");
            ViewData["FormsList"] = FormsList;

            //Datos para dropDown de productos agroquimicos
            var AgrochemicalProduct = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.Where(n => n.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList AgrochemicalProductList = new SelectList(AgrochemicalProduct, "AgrochemicalProductsID", "Name");
            ViewData["AgrochemicalProductList"] = AgrochemicalProductList;

            //Datos para dropDown volumen tanque
            var tankVolume = siembras_PLANNEREntities.Aspersion_TankVolume.Where(n => n.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList tankVolumeList = new SelectList(tankVolume, "TankVolumeID", "Name");
            ViewData["tankVolumeList"] = tankVolumeList;


            return View();
        }

        public ActionResult AspersionPlanAddHead(Aspersion_AspersionPlan datos)
        {

            datos.ActiveFlag = true;
            datos.DateInsert = DateTime.Now;
            int UserID = Convert.ToInt32(Session["UserID"]);
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Aspersion_AspersionPlan aspersion = siembras_PLANNEREntities.Aspersion_AspersionPlan.Where(x => x.WeekID == datos.WeekID && x.GreenHouseID == datos.GreenHouseID && x.UserID == UserID).FirstOrDefault();
            //if (aspersion != null)
            //{
            //    return Json("existe", JsonRequestBehavior.AllowGet);
            //}

            datos.UserID = Convert.ToInt32(Session["UserID"]);
            siembras_PLANNEREntities.Aspersion_AspersionPlan.Add(datos);
            siembras_PLANNEREntities.SaveChanges();
            var AspersionPlanID = datos.AspersionPlanID;

            return Json(AspersionPlanID);
        }

        [HttpPost]
        public void CambiaEstadoHead(Aspersion_AspersionPlan datos)
        {
            try
            {
                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                Aspersion_AspersionPlan Update = siembras_PLANNEREntities.Aspersion_AspersionPlan.First(x => x.AspersionPlanID == datos.AspersionPlanID);
                Update.ActiveFlag = datos.ActiveFlag;
                siembras_PLANNEREntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult AspersionPlanListDetail(Aspersion_AspersionPlanDetail datos)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var AspersionPlanList = siembras_PLANNEREntities.fn_DetalleAspersionPlanDetail(datos.AspersionPlanID, Convert.ToInt32(Session["UserID"])).Where(x => x.Block != "0").OrderByDescending(a => a.AspersionPlanID).ToList();
            ViewBag.datos = AspersionPlanList;

            return PartialView("AspersionPlanList");
        }

        public JsonResult FiltroBloques()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var IdCompany = Request.QueryString["IdCompany"];
            if (IdCompany == "NaN")
                IdCompany = "0";
            var Id = Int32.Parse(IdCompany);

            //Datos para dropDown de compañias
            var aList = siembras_PLANNEREntities.fn_getGreenHousebyBusinessUnit(Id).OrderBy(a => a.Orden).ToList();
            SelectList lstGreenhouse = new SelectList(aList, "ID", "Codigo".Trim().ToUpper());
            return Json(lstGreenhouse);
        }

        public ActionResult AspersionPlanAddDetail(Aspersion_AspersionPlanDetail datos, string[] d, string idBloqueSeleccionado, int PlanType)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var transac = siembras_PLANNEREntities.Database.BeginTransaction();

            string[] NoBloques = idBloqueSeleccionado.Split(",".ToCharArray());

            for (var a = 0; a < NoBloques.Count(); a++)
            {
                var Block = Convert.ToInt32(NoBloques[a]);

                //Consigue el consecutivo del orden de preparación
                var PreparationOrder = siembras_PLANNEREntities.Aspersion_AspersionPlanDetail.Where(
                    x => x.AspersionPlanID == datos.AspersionPlanID &&
                    x.Block == Block &&
                    x.DayNumber == datos.DayNumber &&
                    x.AgrochemicalProductsID == datos.AgrochemicalProductsID &&
                    x.ProductID == datos.ProductID).Select(x => x.PreparationOrder).DefaultIfEmpty(0).Max();
                var suma = 1;
                for (var i = 0; i < d.Length; i++)
                {
                    datos.AgrochemicalProductsID = Convert.ToInt32(d[i]);
                    //Verifica que no exista la llave bloque, dia, PPC y producto
                    var verificaLlave = siembras_PLANNEREntities.Aspersion_AspersionPlanDetail.Where(
                            x => x.AspersionPlanID == datos.AspersionPlanID &&
                            x.Block == datos.Block &&
                            x.DayNumber == datos.DayNumber &&
                            x.AgrochemicalProductsID == datos.AgrochemicalProductsID &&
                            x.ProductID == datos.ProductID
                        ).FirstOrDefault();

                    //if (verificaLlave != null)
                    //{
                    //    var arr = new
                    //    {
                    //        Error = 1,
                    //        Mensaje = "Error en <b>PPC " + (i+1) + "</b>! Bloque / Día / Producto ya asignado, por favor verifique :" +
                    //        "<p>• Ya asignado anteriormente</p>" +
                    //        "<p>• Repetido en este formulario</p>",
                    //    };
                    //    transac.Rollback();
                    //    return Json(arr, JsonRequestBehavior.AllowGet);

                    //}

                    datos.PreparationOrder = (PreparationOrder + suma);

                    var date = DateTime.Today;
                    datos.DateInsert = date;
                    datos.Block = Block;
                    if (PlanType == 2)
                    {
                        var Camas = siembras_PLANNEREntities.Bed.Where(x => x.IDInvernaderos == Block).Count();
                        datos.BedNumber = Camas;
                    }
                    siembras_PLANNEREntities.Aspersion_AspersionPlanDetail.Add(datos);
                    siembras_PLANNEREntities.SaveChanges();

                    suma++;
                }
            }

            

            transac.Commit();
            return Json("Ok");
        }

        public ActionResult TraeID(Aspersion_AspersionPlan datos)
        {
            int UserID = Convert.ToInt32(Session["UserID"]);
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            Aspersion_AspersionPlan aspersion = siembras_PLANNEREntities.Aspersion_AspersionPlan.Where(x => x.WeekID == datos.WeekID && x.GreenHouseID == datos.GreenHouseID && x.UserID == UserID && x.ActiveFlag == true).FirstOrDefault();

            DateTime Hoy = DateTime.Now;

            var datosDetalle = 0;
            if (aspersion != null)
            {
                datosDetalle = siembras_PLANNEREntities.Aspersion_AspersionPlanDetail.Where(x => x.AspersionPlanID == aspersion.AspersionPlanID).Count();
            }

            if (aspersion == null)
            {
                var arr = new
                {
                    Id = 0,
                    Ctrl = 1
                };
                return Json(arr, JsonRequestBehavior.AllowGet);
            }
            else if (aspersion != null && datosDetalle == 0)
            {
                var arr = new
                {
                    Id = aspersion.AspersionPlanID,
                    Ctrl = 3
                };

                return Json(arr, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var arr = new
                {
                    Id = aspersion.AspersionPlanID,
                    Ctrl = 3
                };
                return Json(arr, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AspersionPlanEdit(int Id)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var datosCabecera = siembras_PLANNEREntities.Aspersion_AspersionPlan.Where(x => x.AspersionPlanID == Id).FirstOrDefault();
            Aspersion_AspersionPlan datosCabeceraSelect = siembras_PLANNEREntities.Aspersion_AspersionPlan.First(x => x.AspersionPlanID == Id);
            ViewBag.AspersionPlanID = datosCabecera.AspersionPlanID;
            var ActualYear = DateTime.Today.Year;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Datos para dropDown de semanas
            var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio == ActualYear);
            SelectList listWeek = new SelectList((from s in Week
                                                  select new
                                                  {
                                                      Id = s.ID,
                                                      Nombre = s.Anio + "-" + s.Nombre
                                                  }),
                            "Id",
                            "Nombre",
                            datosCabeceraSelect.WeekID);
            ViewData["listWeek"] = listWeek;

            //Datos para dropDown de compañias
            var company = siembras_PLANNEREntities.Company.Where(x => x.FlagActivo == 1).Distinct().OrderBy(x => x.ID);
            SelectList CompanyList = new SelectList(company, "Id", "Nombre");
            ViewData["CompanyList"] = CompanyList;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name", datosCabeceraSelect.GreenHouseID);
            ViewData["BussinesUnitList"] = BussinesUnitList;

            //Datos para dropDown de compañias
            var aplyTypes = siembras_PLANNEREntities.Aspersion_ApplyTypes.Where(n => n.ActiveFlag == true).OrderBy(x => x.ApplyTypeID).ToList();
            SelectList aplyTypesList = new SelectList(aplyTypes, "ApplyTypeID", "Name");
            ViewData["aplyTypesList"] = aplyTypesList;

            //Datos para dropDown de formas
            var Forms = siembras_PLANNEREntities.Aspersion_Forms.Where(n => n.ActiveFlag == true).OrderBy(x => x.FormID).ToList();
            SelectList FormsList = new SelectList(Forms, "Name", "Name");
            ViewData["FormsList"] = FormsList;

            //Datos para dropDown de productos agroquimicos
            var AgrochemicalProduct = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.Where(n => n.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList AgrochemicalProductList = new SelectList(AgrochemicalProduct, "AgrochemicalProductsID", "Name");
            ViewData["AgrochemicalProductList"] = AgrochemicalProductList;

            //Datos para dropDown volumen tanque
            var tankVolume = siembras_PLANNEREntities.Aspersion_TankVolume.Where(n => n.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList tankVolumeList = new SelectList(tankVolume, "Name", "Name");
            ViewData["tankVolumeList"] = tankVolumeList;

            //Datos para DropDown de  lanza
            var ReferenceSpear = siembras_PLANNEREntities.Aspersion_ReferenceSpear.Where(x => x.ActiveFlag == true).ToList();
            SelectList ReferenceSpearList = new SelectList(ReferenceSpear, "ReferenceSpearID", "Name");
            ViewData["ReferenceSpearList"] = ReferenceSpearList;

            //Datos para supervisor
            var AspersionEmployes = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.ActiveFlag == true && x.RolID == 1 && x.GreenHouseID == datosCabecera.GreenHouseID);
            SelectList AspersionEmployesList = new SelectList((from s in AspersionEmployes
                                                               select new
                                                               {
                                                                   Id = s.AspersionEmployeesID,
                                                                   Nombre = s.Name + " " + s.LastName
                                                               }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["AspersionEmployesList"] = AspersionEmployesList;

            //Datos para Tercios
            var Third = siembras_PLANNEREntities.Aspersion_Thirds.Where(x => x.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList ThirdList = new SelectList(Third, "ThirdID", "Name");
            ViewData["ThirdList"] = ThirdList;

            //Datos para Tercios
            var Direction = siembras_PLANNEREntities.Aspersion_Directions.Where(x => x.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList DirectionList = new SelectList(Direction, "DirectionID", "Name");
            ViewData["DirectionList"] = DirectionList;

            return View();
        }

        public ActionResult AspersioPlanOutputTable(int Id)
        {
            var ActualYear = DateTime.Today.Year;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Aspersion Plan
            var dPlan = siembras_PLANNEREntities.Aspersion_AspersionPlan.Where(x => x.AspersionPlanID == Id).FirstOrDefault();

            //Datos para dropDown de semanas
            var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio == ActualYear);
            SelectList listWeek = new SelectList((from s in Week
                                                  select new
                                                  {
                                                      Id = s.ID,
                                                      Nombre = s.Anio + "-" + s.Nombre
                                                  }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["listWeek"] = listWeek;

            //Datos para supervisor
            var AspersionEmployes = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.ActiveFlag == true && x.RolID == 1 && x.GreenHouseID == dPlan.GreenHouseID);
            SelectList AspersionEmployesList = new SelectList((from s in AspersionEmployes
                                                               select new
                                                               {
                                                                   Id = s.AspersionEmployeesID,
                                                                   Nombre = s.Name + " " + s.LastName
                                                               }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["AspersionEmployesList"] = AspersionEmployesList;

            var AspersionPlan = siembras_PLANNEREntities.fn_detalleAspersionPlan(Convert.ToInt32(Session["UserID"])).Where(x => x.AspersionPlanID == Id);
            ViewBag.AspersionPlan = AspersionPlan;
            ViewBag.Id = Id;

            var PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id).OrderBy(x => x.AspersionPlanDetailID).OrderBy(x => x.AspersionPlanDetailID).OrderBy(x => x.DiaID).ThenBy(x => x.Block).ThenBy(x => x.AspersionPlanDetailID).ToList();
            ViewBag.PlanProducts = PlanProducts;


            //Para armar tabla de PDF
            List<fn_AspersioPlanOutputTable_Result> Titulos = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id).ToList();

            var TitulosLst = Titulos.Select(x => new { x.DayNumber, x.Block, x.DiaID }).Distinct().OrderBy(x => x.DiaID).ThenBy(x => x.Block).ToList();

            ViewBag.Titulos = (from Projection in TitulosLst
                               select new Titulos
                               {
                                   DayNumber = Projection.DayNumber,
                                   Block = Projection.Block,
                                   DiaID = Projection.DiaID
                               }).ToList();



            List<fn_AspersioPlanOutputTable_Result> Aplications = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id).OrderBy(x => x.AspersionPlanDetailID).OrderBy(x => x.AspersionPlanDetailID).OrderBy(x => x.DiaID).ThenBy(x => x.Block).ThenBy(x => x.AspersionPlanDetailID).ToList();
            ViewBag.Aplications = Aplications;

            List<fn_getBedsByAspersionPlan_Result> Beds = siembras_PLANNEREntities.fn_getBedsByAspersionPlan(Id).OrderBy(x => x.Nombre).ToList();
            ViewBag.Beds = Beds;

            return View();
        }

        public ActionResult cargaTabla(int Id)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id).OrderBy(x => x.AspersionPlanDetailID).OrderBy(x => x.DiaID).ThenBy(x => x.Block).ThenBy(x => x.AspersionPlanDetailID).ToList();
            //var PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(datos.AspersionPlanID, datos.ProductID, datos.WeekID, datos.GreenHouseID, datos.Anio).ToList();
            SelectList PlanProductsList = new SelectList(PlanProducts, "PlantProductID", "PlantProductName");
            return Json(PlanProducts, JsonRequestBehavior.AllowGet);
        }

        public ActionResult imprimeEtiquetas(int Id)
        {
            var IdDetail = Request.QueryString["IdDetail"];
            var WeekID = Request.QueryString["WeekID"];
            var NumberDay = Request.QueryString["NumberDay"];
            var AspersionEmployeesID = Request.QueryString["AspersionEmployeesID"];


            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            //List<fn_AspersioPlanOutputTable_Result> PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id);
            var PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id);
            if (Convert.ToInt32(IdDetail) > 0)
            {
                int IDDetail = Convert.ToInt32(IdDetail);
                PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id).Where(x => x.AspersionPlanDetailID == IDDetail);
            }
            else
            {
                //Filtros del formulario
                if (NumberDay != "")
                {
                    int ND = Convert.ToInt32(NumberDay);
                    PlanProducts = PlanProducts.Where(x => x.DiaID == ND);
                }

                if (WeekID != "")
                {
                    int WI = Convert.ToInt32(WeekID);
                    PlanProducts = PlanProducts.Where(x => x.SemanaId == WI);
                }

                if (AspersionEmployeesID != "")
                {
                    int AE = Convert.ToInt32(AspersionEmployeesID);
                    PlanProducts = PlanProducts.Where(x => x.AspersionEmployeesID == AE);
                }
            }
            List<fn_AspersioPlanOutputTable_Result> PlanProductsList = PlanProducts.OrderBy(x => x.DiaID).ThenBy(x => x.AgrochemicalProductsID).ThenBy(x => x.Block).ToList();
            if (Convert.ToInt32(Session["Printer"]) == 1)
            {
                return IMPDatamax(PlanProductsList);
            }
            else
            {
                return IMPZebra(PlanProductsList);
            }
        }

        public ActionResult IMPDatamax(List<fn_AspersioPlanOutputTable_Result> PlanProducts)
        {
            var mes = DateTime.Now.Month;
            var day = DateTime.Now.Day;
            var hour = DateTime.Now.Hour;
            var second = DateTime.Now.Second;
            var NombreArchivo = "LPFASP" + "00" + mes + "00" + day + "00" + hour + "00" + second + ".txt";

            string strPath = HelperController.VerifyExportEnvironment(NombreArchivo);
            StreamWriter sw = new StreamWriter(strPath);

            var cont = 1;
            var contadorGeneral = 0;
            var contadorTotal = PlanProducts.Count;
            int cierre = 0;
            foreach (var x in PlanProducts)
            {
                decimal total = (decimal)(x.dose * x.Density) * x.BedNumber * x.LiterPerBed;
                if (x.Sald % x.TankVolume == 0)
                {
                    x.Sald = 0;
                }
                else
                {
                    x.Sald = Math.Round((decimal)(x.Sald), 2);
                    //Se hace de esta forma ya que el Abs() no funciona
                    if (x.Sald < 0)
                    {
                        x.Sald = x.Sald * (-1);
                    }
                }

                //------------------------------------------//
                //-----------Etiquetas de Tanques-----------//
                //------------------------------------------//
                var Semana = x.WeekID.Replace("-", "0");
                if (x.Sald > 0)
                {
                    x.NumberJar = x.NumberJar - 1;
                }

                var CantidadGramos = Math.Round(Convert.ToDecimal(x.dose) * Convert.ToDecimal(x.TankVolume) * x.Density, 2);
                if (total < CantidadGramos)
                {
                    CantidadGramos = total;
                }

                var Etiq = Convert.ToDecimal(CantidadGramos) / x.CapacityJar;
                var Etiquetas = Math.Ceiling(Etiq);


                //Primer ciclo cantidad de tarros
                for (var z = 0; z < x.NumberJar; z++)
                {
                    var CantidadGrPorTanque = CantidadGramos;
                    //segundo ciclo cantidad de etiquetas por tanque
                    for (var i = 0; i < Etiquetas; i++)
                    {
                        //Etiqueta izquierda
                        if (contadorGeneral % 2 == 0)
                        {
                            sw.WriteLine("L");
                            sw.WriteLine("D11");
                            sw.WriteLine("H16");
                            var sem = x.WeekID.Length;
                            sw.WriteLine("131100001200020" + Semana.Substring((sem - 2), 2));
                            sw.WriteLine("131100001200050-" + x.DayNumber.Substring(0, 3));
                            sw.WriteLine("131100001200090-" + cont);
                            sw.WriteLine("131100001200120BL: " + x.Block);
                            if(x.GreenHouseID.Length > 22)
                                sw.WriteLine("131100001000020" + x.GreenHouseID.Substring(0, 22));
                            else
                                sw.WriteLine("131100001000020" + x.GreenHouseID);
                            sw.WriteLine("131100000800020CAT. TOX:" + x.ToxicologicalCategoryID);
                            sw.WriteLine("131100000600020" + x.ProductID);
                            sw.WriteLine("131100000400020" + x.AgrochemicalProductsID);
                            if (CantidadGrPorTanque > x.CapacityJar)
                            {
                                sw.WriteLine("131100000200020" + Math.Ceiling(x.CapacityJar) + " GR");
                            }
                            else
                            {
                                sw.WriteLine("131100000200020" + Math.Ceiling(CantidadGrPorTanque) + " GR");
                            }
                            CantidadGrPorTanque = CantidadGrPorTanque - x.CapacityJar;
                            cierre = 0;
                        }
                        //Etiqueta derecha
                        if (contadorGeneral % 2 != 0)
                        {
                            var sem = x.WeekID.Length;
                            sw.WriteLine("131100001200230" + Semana.Substring((sem - 2), 2));
                            sw.WriteLine("131100001200260-" + x.DayNumber.Substring(0, 3));
                            sw.WriteLine("131100001200300-" + cont);
                            sw.WriteLine("131100001200330BL: " + x.Block);
                            if (x.GreenHouseID.Length > 22)
                                sw.WriteLine("131100001000230" + x.GreenHouseID.Substring(0, 22));
                            else
                                sw.WriteLine("131100001000230" + x.GreenHouseID);
                            sw.WriteLine("131100000800230CAT. TOX:" + x.ToxicologicalCategoryID);
                            sw.WriteLine("131100000600230" + x.ProductID);
                            sw.WriteLine("131100000400230" + x.AgrochemicalProductsID);
                            if (CantidadGrPorTanque > x.CapacityJar)
                            {
                                sw.WriteLine("131100000200230" + Math.Ceiling(x.CapacityJar) + " GR");
                            }
                            else
                            {
                                sw.WriteLine("131100000200230" + Math.Ceiling(CantidadGrPorTanque) + " GR");
                            }
                            CantidadGrPorTanque = CantidadGrPorTanque - x.CapacityJar;

                            sw.WriteLine("Q1");
                            sw.WriteLine("E");
                            cierre = 1;
                        }
                        contadorGeneral++;
                        cont++;
                    }
                }
                //------------------------------------------//
                //-------------Etiquetas de saldo-----------//
                //------------------------------------------//
                if (x.Sald > 0)
                {
                    var CantidadSaldoGramos = Math.Round(Convert.ToDecimal(x.dose) * Convert.ToDecimal(x.Sald) * x.Density, 2);
                    var EtiquetasSaldo = Math.Ceiling(Convert.ToDecimal(CantidadSaldoGramos) / x.TankVolume);
                    for (var i = 0; i < EtiquetasSaldo; i++)
                    {
                        if (contadorGeneral % 2 == 0)
                        {
                            sw.WriteLine("L");
                            sw.WriteLine("D11");
                            sw.WriteLine("H16");
                            var sem = x.WeekID.Length;
                            sw.WriteLine("131100001200020" + Semana.Substring((sem - 2), 2));
                            sw.WriteLine("131100001200050-" + x.DayNumber.Substring(0, 3));
                            sw.WriteLine("131100001200090-" + cont);
                            sw.WriteLine("131100001200120BL: " + x.Block);
                            if (x.GreenHouseID.Length > 22)
                                sw.WriteLine("131100001000020" + x.GreenHouseID.Substring(0, 22));
                            else
                                sw.WriteLine("131100001000020" + x.GreenHouseID);
                            
                            sw.WriteLine("131100000800020CAT. TOX:" + x.ToxicologicalCategoryID);
                            sw.WriteLine("131100000600020" + x.ProductID);
                            sw.WriteLine("131100000400020" + x.AgrochemicalProductsID);

                            if (CantidadSaldoGramos > x.CapacityJar)
                            {
                                sw.WriteLine("131100000200020" + Math.Ceiling(x.CapacityJar) + " GR");
                            }
                            else
                            {
                                sw.WriteLine("131100000200020" + Math.Ceiling(CantidadSaldoGramos) + " GR");
                            }

                            CantidadSaldoGramos = CantidadSaldoGramos - x.CapacityJar;
                            cierre = 0;
                        }
                        //Etiqueta derecha
                        if (contadorGeneral % 2 != 0)
                        {
                            var sem = x.WeekID.Length;
                            sw.WriteLine("131100001200230" + Semana.Substring((sem - 2), 2));
                            sw.WriteLine("131100001200260-" + x.DayNumber.Substring(0, 3));
                            sw.WriteLine("131100001200300-" + cont);
                            sw.WriteLine("131100001200330BL: " + x.Block);
                            if (x.GreenHouseID.Length > 22)
                                sw.WriteLine("131100001000230" + x.GreenHouseID.Substring(0, 22));
                            else
                                sw.WriteLine("131100001000230" + x.GreenHouseID);
                            sw.WriteLine("131100000800230CAT. TOX:" + x.ToxicologicalCategoryID);
                            sw.WriteLine("131100000600230" + x.ProductID);
                            sw.WriteLine("131100000400230" + x.AgrochemicalProductsID);
                            if (CantidadSaldoGramos > x.CapacityJar)
                            {
                                sw.WriteLine("131100000200230" + Math.Ceiling(x.CapacityJar) + " GR");
                            }
                            else
                            {
                                sw.WriteLine("131100000200230" + Math.Ceiling(CantidadSaldoGramos) + " GR");
                            }
                            sw.WriteLine("Q1");
                            sw.WriteLine("E");
                            cierre = 1;
                            CantidadSaldoGramos = CantidadSaldoGramos - x.CapacityJar;
                        }
                        contadorGeneral++;
                        cont++;
                    }
                }

            }
            if (cierre == 0)
            {
                sw.WriteLine("Q1");
                sw.WriteLine("E");
            }
            sw.Close();

            byte[] fileBytes = System.IO.File.ReadAllBytes(strPath);
            string fileName = NombreArchivo;
            System.IO.File.Delete(strPath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult IMPZebra(List<fn_AspersioPlanOutputTable_Result> PlanProducts)
        {
            var mes = DateTime.Now.Month;
            var day = DateTime.Now.Day;
            var hour = DateTime.Now.Hour;
            var second = DateTime.Now.Second;
            var NombreArchivo = "LPFASP" + "00" + mes + "00" + day + "00" + hour + "00" + second + ".txt";

            string strPath = HelperController.VerifyExportEnvironment(NombreArchivo);
            StreamWriter sw = new StreamWriter(strPath);

            var cont = 1;
            var contadorGeneral = 0;
            var contadorTotal = PlanProducts.Count;
            int cierre = 0;
            foreach (var x in PlanProducts)
            {
                decimal total = (decimal)(x.dose * x.Density) * x.BedNumber * x.LiterPerBed;
                if (x.Sald % x.TankVolume == 0)
                {
                    x.Sald = 0;
                }
                else
                {
                    x.Sald = Math.Round((decimal)(x.Sald), 2);
                    //Se hace de esta forma ya que el Abs() no funciona
                    if (x.Sald < 0)
                    {
                        x.Sald = x.Sald * (-1);
                    }
                }

                //------------------------------------------//
                //-----------Etiquetas de Tanques-----------//
                //------------------------------------------//
                var Semana = x.WeekID.Replace("-", "0");
                if (x.Sald > 0)
                {
                    x.NumberJar = x.NumberJar - 1;
                }
                var CantidadGramos = Math.Round(Convert.ToDecimal(x.dose) * Convert.ToDecimal(x.TankVolume) * x.Density, 2);
                
                if(total < CantidadGramos)
                {
                    CantidadGramos = total;
                }
                var Etiquetas = Math.Ceiling(Convert.ToDecimal(CantidadGramos) / x.CapacityJar);

                //Primer ciclo cantidad de tarros
                for (int i = 0; i < x.NumberJar; i++)
                {
                    var CantidadGrPorTanque = CantidadGramos;
                    //segundo ciclo cantidad de etiquetas por tanque
                    for (int r = 0; r < Etiquetas; r++)
                    {
                        if (contadorGeneral % 2 == 0)
                        {
                            sw.WriteLine("^XA");
                            sw.WriteLine("^PW800");
                            sw.WriteLine("^LH0,0");
                            var sem = x.WeekID.Length;
                            sw.WriteLine("^FO20,20^A0N10,50^FD" + Semana.Substring((sem - 2), 2) + "^FS");
                            sw.WriteLine("^FO60,30^A0N10,30^FD - " + x.DayNumber.Substring(0, 3) + "^FS");
                            sw.WriteLine("^FO120,30^A0N10,30^FD -" + cont + "^FS");
                            sw.WriteLine("^FO240,30^A0N10,30^FD BL: " + x.Block + "^FS");
                            if (x.GreenHouseID.Length > 22)
                                sw.WriteLine("^FO20,70^A0N10,40^FD" + x.GreenHouseID.Substring(0, 22) + "^FS");
                            else
                                sw.WriteLine("^FO20,70^A0N10,40^FD" + x.GreenHouseID + "^FS");
                            sw.WriteLine("^FO20,120^A0N10,40^FDCAT. TOX:" + x.ToxicologicalCategoryID + "^FS");
                            sw.WriteLine("^FO20,170^A0N10,40^FD" + x.ProductID + "^FS");
                            sw.WriteLine("^FO20,220^A0N10,40^FD" + x.AgrochemicalProductsID + "^FS");

                            if (CantidadGrPorTanque > x.CapacityJar)
                            {
                                sw.WriteLine("^FO20,260^A0N10,50^FD" + Math.Ceiling(x.CapacityJar) + " GR ^FS");
                            }
                            else
                            {
                                sw.WriteLine("^FO20,260^A0N10,50^FD" + Math.Ceiling(CantidadGrPorTanque) + " GR ^FS");
                            }

                            CantidadGrPorTanque = CantidadGrPorTanque - x.CapacityJar;
                            cierre = 0;
                        }
                        //Etiqueta derecha
                        if (contadorGeneral % 2 != 0)
                        {
                            var sem = x.WeekID.Length;
                            sw.WriteLine("^FO460,20^A0N10,50^FD" + Semana.Substring((sem - 2), 2) + "^FS");
                            sw.WriteLine("^FO500,30^A0N10,30^FD - " + x.DayNumber.Substring(0, 3) + "^FS");
                            sw.WriteLine("^FO560,30^A0N10,30^FD -" + cont + "^FS");
                            sw.WriteLine("^FO680,30^A0N10,30^FD BL: " + x.Block + "^FS");
                            if (x.GreenHouseID.Length > 22)
                                sw.WriteLine("^FO460,70^A0N10,40^FD" + x.GreenHouseID.Substring(0, 22) + "^FS");
                            else
                                sw.WriteLine("^FO460,70^A0N10,40^FD" + x.GreenHouseID + "^FS");
                            sw.WriteLine("^FO460,120^A0N10,40^FDCAT. TOX:" + x.ToxicologicalCategoryID + "^FS");
                            sw.WriteLine("^FO460,170^A0N10,40^FD" + x.ProductID + "^FS");
                            sw.WriteLine("^FO460,220^A0N10,40^FD" + x.AgrochemicalProductsID + "^FS");

                            if (CantidadGrPorTanque > x.CapacityJar)
                            {
                                sw.WriteLine("^FO460,260^A0N10,50^FD" + Math.Ceiling(x.CapacityJar) + " GR ^FS");
                            }
                            else
                            {
                                sw.WriteLine("^FO460,260^A0N10,50^FD" + Math.Ceiling(CantidadGrPorTanque) + " GR ^FS");
                            }

                            CantidadGrPorTanque = CantidadGrPorTanque - x.CapacityJar;


                            sw.WriteLine("^PQ1");
                            sw.WriteLine("^XZ");
                            cierre = 1;
                        }
                        contadorGeneral++;
                        cont++;
                    }
                }
                //------------------------------------------//
                //-------------Etiquetas de saldo-----------//
                //------------------------------------------//
                if (x.Sald > 0)
                {
                    var CantidadSaldoGramos = Math.Round(Convert.ToDecimal(x.dose) * Convert.ToDecimal(x.Sald) * x.Density, 2);
                    var EtiquetasSaldo = Math.Ceiling(Convert.ToDecimal(CantidadSaldoGramos) / x.TankVolume);
                    for (var i = 0; i < EtiquetasSaldo; i++)
                    {
                        if (contadorGeneral % 2 == 0)
                        {
                            sw.WriteLine("^XA");
                            sw.WriteLine("^PW800");
                            sw.WriteLine("^LH0,0");
                            var sem = Semana.Length;
                            sw.WriteLine("^FO20,20^A0N10,50^" + Semana.Substring((sem - 2), 2) + "^FS");
                            sw.WriteLine("^FO60,30^A0N10,30^FD - " + x.DayNumber.Substring(0, 3) + "^FS");
                            sw.WriteLine("^FO120,30^A0N10,30^FD -" + cont + "^FS");
                            sw.WriteLine("^FO240,30^A0N10,30^FD BL: " + x.Block + "^FS");
                            if (x.GreenHouseID.Length > 22)
                                sw.WriteLine("^FO20,70^A0N10,40^FD" + x.GreenHouseID.Substring(0, 22) + "^FS");
                            else
                                sw.WriteLine("^FO20,70^A0N10,40^FD" + x.GreenHouseID + "^FS");
                            sw.WriteLine("^FO20,120^A0N10,40^FDCAT. TOX:" + x.ToxicologicalCategoryID + "^FS");
                            sw.WriteLine("^FO20,170^A0N10,40^FD" + x.ProductID + "^FS");
                            sw.WriteLine("^FO20,220^A0N10,40^FD" + x.AgrochemicalProductsID + "^FS");

                            if (CantidadSaldoGramos > x.TankVolume)
                            {
                                sw.WriteLine("^FO20,260^A0N10,50^FD" + Math.Ceiling(x.TankVolume) + " GR ^FS");
                            }
                            else
                            {
                                sw.WriteLine("^FO20,260^A0N10,50^FD" + Math.Ceiling(CantidadSaldoGramos) + " GR ^FS");
                            }

                            CantidadSaldoGramos = CantidadSaldoGramos - x.TankVolume;
                            cierre = 0;
                        }
                        //Etiqueta derecha
                        if (contadorGeneral % 2 != 0)
                        {
                            var sem = x.WeekID.Length;
                            sw.WriteLine("^FO460,20^A0N10,50^FD" + Semana.Substring((sem - 2), 2) + "^FS");
                            sw.WriteLine("^FO500,30^A0N10,30^FD - " + x.DayNumber.Substring(0, 3) + "^FS");
                            sw.WriteLine("^FO560,30^A0N10,30^FD -" + cont + "^FS");
                            sw.WriteLine("^FO680,30^A0N10,30^FD BL: " + x.Block + "^FS");
                            if (x.GreenHouseID.Length > 22)
                                sw.WriteLine("^FO460,70^A0N10,40^FD" + x.GreenHouseID.Substring(0, 22) + "^FS");
                            else
                            sw.WriteLine("^FO460,70^A0N10,40^FD" + x.GreenHouseID + "^FS");
                            sw.WriteLine("^FO460,120^A0N10,40^FDCAT. TOX:" + x.ToxicologicalCategoryID + "^FS");
                            sw.WriteLine("^FO460,170^A0N10,40^FD" + x.ProductID + "^FS");
                            sw.WriteLine("^FO460,220^A0N10,40^FD" + x.AgrochemicalProductsID + "^FS");

                            if (CantidadSaldoGramos > x.TankVolume)
                            {
                                sw.WriteLine("^FO460,260^A0N10,50^FD" + Math.Ceiling(x.TankVolume) + " GR ^FS");
                            }
                            else
                            {
                                sw.WriteLine("^FO460,260^A0N10,50^FD" + Math.Ceiling(CantidadSaldoGramos) + " GR ^FS");
                            }
                            CantidadSaldoGramos = CantidadSaldoGramos - x.TankVolume;
                            sw.WriteLine("^PQ1");
                            sw.WriteLine("^XZ");
                            cierre = 1;
                        }
                        contadorGeneral++;
                        cont++;
                    }
                }
            }
            if (cierre == 0)
            {
                sw.WriteLine("^PQ1");
                sw.WriteLine("^XZ");
            }
            sw.Close();

            byte[] fileBytes = System.IO.File.ReadAllBytes(strPath);
            string fileName = NombreArchivo;
            System.IO.File.Delete(strPath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public ActionResult OutputTableFilter(int? WeekID, int? NumberDay, int? AspersionEmployeesID, int? Id)
        {
            var ActualYear = DateTime.Today.Year;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Datos para dropDown de semanas
            var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio == ActualYear);
            SelectList listWeek = new SelectList((from s in Week
                                                  select new
                                                  {
                                                      Id = s.ID,
                                                      Nombre = s.Anio + "-" + s.Nombre
                                                  }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["listWeek"] = listWeek;

            //Datos para supervisor
            var AspersionEmployes = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.ActiveFlag == true && x.RolID == 1);
            SelectList AspersionEmployesList = new SelectList((from s in AspersionEmployes
                                                               select new
                                                               {
                                                                   Id = s.AspersionEmployeesID,
                                                                   Nombre = s.Name + " " + s.LastName
                                                               }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["AspersionEmployesList"] = AspersionEmployesList;

            var AspersionPlan = siembras_PLANNEREntities.fn_detalleAspersionPlan(Convert.ToInt32(Session["UserID"])).Where(x => x.AspersionPlanID == Id);
            ViewBag.AspersionPlan = AspersionPlan;
            ViewBag.Id = Id;


            var PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id);


            //------------------------------------------------Para armar tabla de PDF
            var Titulos = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id);

            var Aplications = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id);

            var Beds = siembras_PLANNEREntities.fn_getBedsByAspersionPlan(Id).OrderBy(x => x.Nombre).ToList();


            //Filtros del formulario
            if (NumberDay != null)
            {
                PlanProducts = PlanProducts.Where(x => x.DiaID == NumberDay);
                Titulos = Titulos.Where(x => x.DiaID == NumberDay);
                Aplications = Aplications.Where(x => x.DiaID == NumberDay);
            }

            if (WeekID != null)
            {
                PlanProducts = PlanProducts.Where(x => x.SemanaId == WeekID);
                Titulos = Titulos.Where(x => x.SemanaId == WeekID);
                Aplications = Aplications.Where(x => x.SemanaId == WeekID);
            }

            if (AspersionEmployeesID != null)
            {
                PlanProducts = PlanProducts.Where(x => x.AspersionEmployeesID == AspersionEmployeesID);
                Titulos = Titulos.Where(x => x.AspersionEmployeesID == AspersionEmployeesID);
                Aplications = Aplications.Where(x => x.AspersionEmployeesID == AspersionEmployeesID);
            }

            Aplications = Aplications.OrderBy(x => x.AspersionPlanDetailID).OrderBy(x => x.AspersionPlanDetailID).OrderBy(x => x.DiaID).ThenBy(x => x.Block).ThenBy(x => x.AspersionPlanDetailID);

            ViewBag.PlanProducts = PlanProducts.OrderBy(x => x.AspersionPlanDetailID);
            ViewBag.Aplications = Aplications;
            var TitulosLst = Titulos.Select(x => new { x.DayNumber, x.Block, x.DiaID }).Distinct().OrderBy(x => x.DiaID).ThenBy(x => x.Block).ToList();
            ViewBag.Titulos = (from Projection in TitulosLst
                               select new Titulos
                               {
                                   DayNumber = Projection.DayNumber,
                                   Block = Projection.Block,
                                   DiaID = Projection.DiaID
                               }).ToList();
            ViewBag.Beds = Beds;

            return View("AspersioPlanOutputTable");
        }

        public ActionResult ProductByBlock(int BlockId)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var ProductByBlock = siembras_PLANNEREntities.fn_getProductByBlock(BlockId).ToList();
            SelectList ProductByBlockList = new SelectList(ProductByBlock, "PlantProductID", "PlantProductName");
            return Json(ProductByBlockList);
        }

        public ActionResult Consolidated()
        {
            var ActualYear = DateTime.Today.Year;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Datos para dropDown de semanas
            var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio == ActualYear);
            SelectList listWeek = new SelectList((from s in Week
                                                  select new
                                                  {
                                                      Id = s.ID,
                                                      Nombre = s.Anio + "-" + s.Nombre
                                                  }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["listWeek"] = listWeek;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name", null);
            ViewData["BussinesUnitList"] = BussinesUnitList;

            SqlConnection entityConnection = (SqlConnection)siembras_PLANNEREntities.Database.Connection;
            SqlConnection cnn = entityConnection;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_AspersionPlanConsolidated";

            SqlParameter DiaID = new SqlParameter("@DiaID", SqlDbType.Int);
            SqlParameter BusinessUnitID = new SqlParameter("@BusinessUnitID", SqlDbType.Int);

            DiaID.Value = 0;
            BusinessUnitID.Value = 0;

            cmd.Parameters.Add(DiaID);
            cmd.Parameters.Add(BusinessUnitID);

            cnn.Open();

            List<AspersionPlanConsolidated> AspersionPlanConsolidatedList = new List<AspersionPlanConsolidated>();

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    AspersionPlanConsolidated _AspersionPlanConsolidated = new AspersionPlanConsolidated();

                    _AspersionPlanConsolidated.AgrochemicalProductsID = dr[0].ToString();
                    _AspersionPlanConsolidated.Lunes = dr[1].ToString();
                    _AspersionPlanConsolidated.Martes = dr[2].ToString();
                    _AspersionPlanConsolidated.Miercoles = dr[3].ToString();
                    _AspersionPlanConsolidated.Jueves = dr[4].ToString();
                    _AspersionPlanConsolidated.Viernes = dr[5].ToString();
                    _AspersionPlanConsolidated.Sabado = dr[6].ToString();
                    _AspersionPlanConsolidated.Domingo = dr[7].ToString();
                    _AspersionPlanConsolidated.total = dr[8].ToString();
                    //_AspersionPlanConsolidated.Unidad = dr[9].ToString();

                    AspersionPlanConsolidatedList.Add(_AspersionPlanConsolidated);
                }

                dr.Close();
            }

            cnn.Close();

            ViewBag.Consolidate = AspersionPlanConsolidatedList;

            return View();
        }

        [HttpPost]
        public ActionResult ConsolidatedFiltred(int? BussinesUnitId, int? WeekID)
        {
            if (BussinesUnitId == null)
            {
                BussinesUnitId = 0;
            }

            if (WeekID == null)
            {
                WeekID = 0;
            }

            var ActualYear = DateTime.Today.Year;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            AspersionPlanConsolidated DatDropDown = new AspersionPlanConsolidated();
            DatDropDown.WeekID = Convert.ToInt32(WeekID);
            DatDropDown.BussinesUnitId = Convert.ToInt32(BussinesUnitId);

            //Datos para dropDown de semanas
            var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio == ActualYear);
            SelectList listWeek = new SelectList((from s in Week
                                                  select new
                                                  {
                                                      Id = s.ID,
                                                      Nombre = s.Anio + "-" + s.Nombre
                                                  }),
                            "Id",
                            "Nombre",
                            DatDropDown.WeekID);
            ViewData["listWeek"] = listWeek;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name", DatDropDown.BussinesUnitId);
            ViewData["BussinesUnitList"] = BussinesUnitList;

            SqlConnection entityConnection = (SqlConnection)siembras_PLANNEREntities.Database.Connection;
            SqlConnection cnn = entityConnection;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_AspersionPlanConsolidated";

            SqlParameter DiaID = new SqlParameter("@DiaID", SqlDbType.Int);
            SqlParameter BusinessUnitID = new SqlParameter("@BusinessUnitID", SqlDbType.Int);

            DiaID.Value = WeekID;
            BusinessUnitID.Value = BussinesUnitId;

            cmd.Parameters.Add(DiaID);
            cmd.Parameters.Add(BusinessUnitID);

            cnn.Open();

            List<AspersionPlanConsolidated> AspersionPlanConsolidatedList = new List<AspersionPlanConsolidated>();

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    AspersionPlanConsolidated _AspersionPlanConsolidated = new AspersionPlanConsolidated();

                    _AspersionPlanConsolidated.AgrochemicalProductsID = dr[0].ToString();
                    //_AspersionPlanConsolidated.Unidad = dr[9].ToString();
                    _AspersionPlanConsolidated.Lunes = dr[1].ToString();
                    _AspersionPlanConsolidated.Martes = dr[2].ToString();
                    _AspersionPlanConsolidated.Miercoles = dr[3].ToString();
                    _AspersionPlanConsolidated.Jueves = dr[4].ToString();
                    _AspersionPlanConsolidated.Viernes = dr[5].ToString();
                    _AspersionPlanConsolidated.Sabado = dr[6].ToString();
                    _AspersionPlanConsolidated.Domingo = dr[7].ToString();
                    _AspersionPlanConsolidated.total = dr[8].ToString();

                    AspersionPlanConsolidatedList.Add(_AspersionPlanConsolidated);
                }

                dr.Close();
            }

            cnn.Close();

            ViewBag.Consolidate = AspersionPlanConsolidatedList;

            return Json(AspersionPlanConsolidatedList);
        }

        [HttpPost]
        public ActionResult FiltraPPCPorTipo(int ApplyTypesID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            //Datos para dropDown de productos agroquimicos
            List<Aspersion_AgrochemicalProducts> AgrochemicalProducts = new List<Aspersion_AgrochemicalProducts>();

            if (ApplyTypesID == 1)
            {
                AgrochemicalProducts = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.Where(x => x.ActiveFlag == true && x.Aspersion > 0).OrderBy(x => x.Name).ToList();

                SelectList AgrochemicalProductList = new SelectList((from s in AgrochemicalProducts
                                                                     select new
                                                                     {
                                                                         AgrochemicalProductsID = s.AgrochemicalProductsID,
                                                                         Name = s.Name + " - Dosis. " + Math.Round(s.Aspersion, 2)
                                                                     }),
                                "AgrochemicalProductsID",
                                "Name",
                                null);
                return Json(AgrochemicalProductList, JsonRequestBehavior.AllowGet);
            }

            if (ApplyTypesID == 2)
            {
                AgrochemicalProducts = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.Where(x => x.ActiveFlag == true && x.Sprinkle > 0).OrderBy(x => x.Name).ToList();
                SelectList AgrochemicalProductList = new SelectList((from s in AgrochemicalProducts
                                                                     select new
                                                                     {
                                                                         AgrochemicalProductsID = s.AgrochemicalProductsID,
                                                                         Name = s.Name + " - Dosis. " + Math.Round(s.Sprinkle, 2)
                                                                     }),
                                "AgrochemicalProductsID",
                                "Name",
                                null);
                return Json(AgrochemicalProductList, JsonRequestBehavior.AllowGet);
            }

            if (ApplyTypesID == 3)
            {
                AgrochemicalProducts = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.Where(x => x.ActiveFlag == true && x.Drench > 0).OrderBy(x => x.Name).ToList();

                SelectList AgrochemicalProductList = new SelectList((from s in AgrochemicalProducts
                                                                     select new
                                                                     {
                                                                         AgrochemicalProductsID = s.AgrochemicalProductsID,
                                                                         Name = s.Name + " - Dosis. " + Math.Round(s.Drench, 2)
                                                                     }),
                                "AgrochemicalProductsID",
                                "Name",
                                null);
                return Json(AgrochemicalProductList, JsonRequestBehavior.AllowGet);
            }

            if (ApplyTypesID == 4)
            {
                AgrochemicalProducts = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.Where(x => x.ActiveFlag == true && x.Thermonebulizer > 0).OrderBy(x => x.Name).ToList();

                SelectList AgrochemicalProductList = new SelectList((from s in AgrochemicalProducts
                                                                     select new
                                                                     {
                                                                         AgrochemicalProductsID = s.AgrochemicalProductsID,
                                                                         Name = s.Name + " - Dosis. " + Math.Round(s.Thermonebulizer, 2)
                                                                     }),
                                "AgrochemicalProductsID",
                                "Name",
                                null);
                return Json(AgrochemicalProductList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("null");
            }
        }

        [HttpPost]
        public ActionResult GetSupervisor(int? AspersionEmployeesID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var AllAspersionEmployees = siembras_PLANNEREntities.fn_getAllUserBySupervisor(AspersionEmployeesID).ToList().OrderBy(x => x.IDRol);

            var AllAspersionEmployeesList = from s in AllAspersionEmployees
                                            select new
                                            {
                                                Nombre = s.Name + " " + s.LastName,
                                                Cargo = s.RolID
                                            };

            return Json(AllAspersionEmployeesList);
        }

        public ActionResult TraeTotalCamas(int BlockId)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            int Camas = siembras_PLANNEREntities.Bed.Where(x => x.FlagActivo == 1 && x.IDInvernaderos == BlockId).Count();
            return Json(Camas);
        }

        public void CambiaEstadoDetail(Aspersion_AspersionPlanDetail datos)
        {
            try
            {
                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                Aspersion_AspersionPlanDetail Update = siembras_PLANNEREntities.Aspersion_AspersionPlanDetail.First(x => x.AspersionPlanDetailID == datos.AspersionPlanDetailID);
                Update.ActiveFlag = datos.ActiveFlag;
                siembras_PLANNEREntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GuardarObservacion(Aspersion_InactiveComments datos)
        {
            datos.DateComment = DateTime.Now;
            datos.UserId = Convert.ToInt32(Session["UserID"]);
            datos.ActiveFlag = true;
            //datos.AspersionPlanId = 0;
            var x = DateTime.Today;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            siembras_PLANNEREntities.Aspersion_InactiveComments.Add(datos);
            siembras_PLANNEREntities.SaveChanges();

            return Json("");
        }

        public ActionResult EditarPlan(int Id)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var AspersionPlan = siembras_PLANNEREntities.Aspersion_AspersionPlan.Where(x => x.AspersionPlanID == Id).FirstOrDefault();
            //Datos para dropDown de compañias
            var aList = siembras_PLANNEREntities.fn_getGreenHousebyBusinessUnit(AspersionPlan.GreenHouseID).OrderBy(a => a.Orden).ToList();
            SelectList lstGreenhouse = new SelectList(aList, "ID", "Codigo".Trim().ToUpper());
            ViewBag.BlockList = lstGreenhouse;

            ViewBag.AspersionPlanId = Id;

            return View();
        }

        public JsonResult TraeTablaParaEditar(int? DayNumber, int? BlockIDs, int? ProductID, int Id)
        {

            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            List<fn_DetalleAspersionPlanDetail_Result> AspersionPlanList = siembras_PLANNEREntities.fn_DetalleAspersionPlanDetail(Id, Convert.ToInt32(Session["UserID"])).Where(x => x.Block != "0").OrderByDescending(a => a.AspersionPlanID).ToList();

            List<fn_DetalleAspersionPlanDetail_Result> tmp = AspersionPlanList;

            if (DayNumber != null)
            {
                AspersionPlanList = AspersionPlanList.Where(x => x.DayNumber == DayNumber).ToList();
            }

            if (BlockIDs != null)
            {
                AspersionPlanList = AspersionPlanList.Where(x => x.BlockID == BlockIDs).ToList();
            }

            if (ProductID != null)
            {
                AspersionPlanList = AspersionPlanList.Where(x => x.IDProduct == ProductID).ToList();
            }

            return Json(AspersionPlanList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SelectProducto(int BlockID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var PlanProducts = siembras_PLANNEREntities.PlantProducts.Where(x => x.FlagActive == 1).Distinct().OrderBy(x => x.PlantProductName);
            return Json(PlanProducts);
        }

        public JsonResult SelectAplicacion(int BlockID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var aplyTypes = siembras_PLANNEREntities.Aspersion_ApplyTypes.Where(n => n.ActiveFlag == true).OrderBy(x => x.ApplyTypeID).ToList();
            return Json(aplyTypes);
        }

        public JsonResult SelectForms(int BlockID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var Forms = siembras_PLANNEREntities.Aspersion_Forms.Where(n => n.ActiveFlag == true).OrderBy(x => x.FormID).ToList();
            return Json(Forms);
        }

        public JsonResult SelectThird()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var Forms = siembras_PLANNEREntities.Aspersion_Thirds.Where(n => n.ActiveFlag == true).OrderBy(x => x.ThirdID).ToList();
            return Json(Forms);
        }

        public JsonResult SelectDirection()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var Forms = siembras_PLANNEREntities.Aspersion_Directions.Where(n => n.ActiveFlag == true).OrderBy(x => x.DirectionID).ToList();
            return Json(Forms);
        }

        public JsonResult SelectAgrochemicalProduct()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var Forms = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.Where(n => n.ActiveFlag == true).OrderBy(x => x.AgrochemicalProductsID).ToList();
            return Json(Forms);
        }

        public JsonResult SelectEReferenceSpread()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var Forms = siembras_PLANNEREntities.Aspersion_ReferenceSpear.Where(n => n.ActiveFlag == true).OrderBy(x => x.ReferenceSpearID).ToList();
            return Json(Forms);
        }

        public ActionResult GuardarEditar(int id, string valor, string campo)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var sp = siembras_PLANNEREntities.sp_InsertaAuditoriaAspersionPlanDetail(Convert.ToString(id), campo, valor, Convert.ToString(Session["UserID"]));
            return Json("");
        }

        public ActionResult AspersionPlanEditV2(int Id)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            basEntities = new BASEntities();
            var datosCabecera = siembras_PLANNEREntities.Aspersion_AspersionPlan.Where(x => x.AspersionPlanID == Id).FirstOrDefault();
            Aspersion_AspersionPlan datosCabeceraSelect = siembras_PLANNEREntities.Aspersion_AspersionPlan.First(x => x.AspersionPlanID == Id);
            ViewBag.AspersionPlanID = datosCabecera.AspersionPlanID;
            var ActualYear = DateTime.Today.Year;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            basEntities = new BASEntities();

            //Datos para dropDown de semanas
            var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio == ActualYear);
            SelectList listWeek = new SelectList((from s in Week
                                                  select new
                                                  {
                                                      Id = s.ID,
                                                      Nombre = s.Anio + "-" + s.Nombre
                                                  }),
                            "Id",
                            "Nombre",
                            datosCabeceraSelect.WeekID);
            ViewData["listWeek"] = listWeek;

            //Datos para dropDown de compañias
            var company = siembras_PLANNEREntities.Company.Where(x => x.FlagActivo == 1).Distinct().OrderBy(x => x.ID);
            SelectList CompanyList = new SelectList(company, "Id", "Nombre");
            ViewData["CompanyList"] = CompanyList;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name", datosCabeceraSelect.GreenHouseID);
            ViewData["BussinesUnitList"] = BussinesUnitList;

            //Datos para dropDown de compañias
            var aplyTypes = siembras_PLANNEREntities.Aspersion_ApplyTypes.Where(n => n.ActiveFlag == true).OrderBy(x => x.ApplyTypeID).ToList();
            SelectList aplyTypesList = new SelectList(aplyTypes, "ApplyTypeID", "Name");
            ViewData["aplyTypesList"] = aplyTypesList;

            //Datos para dropDown de formas
            var Forms = siembras_PLANNEREntities.Aspersion_Forms.Where(n => n.ActiveFlag == true).OrderBy(x => x.FormID).ToList();
            SelectList FormsList = new SelectList(Forms, "Name", "Name");
            ViewData["FormsList"] = FormsList;

            //Datos para dropDown de productos agroquimicos
            var AgrochemicalProduct = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.Where(n => n.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList AgrochemicalProductList = new SelectList(AgrochemicalProduct, "AgrochemicalProductsID", "Name");
            ViewData["AgrochemicalProductList"] = AgrochemicalProductList;

            //Datos para dropDown volumen tanque
            var tankVolume = siembras_PLANNEREntities.Aspersion_TankVolume.Where(n => n.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList tankVolumeList = new SelectList(tankVolume, "Name", "Name");
            ViewData["tankVolumeList"] = tankVolumeList;

            //Datos para DropDown de  lanza
            var ReferenceSpear = siembras_PLANNEREntities.Aspersion_ReferenceSpear.Where(x => x.ActiveFlag == true).ToList();
            SelectList ReferenceSpearList = new SelectList(ReferenceSpear, "ReferenceSpearID", "Name");
            ViewData["ReferenceSpearList"] = ReferenceSpearList;

            //Datos para supervisor
            var AspersionEmployes = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.ActiveFlag == true && x.RolID == 1 && x.GreenHouseID == datosCabecera.GreenHouseID);
            SelectList AspersionEmployesList = new SelectList((from s in AspersionEmployes
                                                               select new
                                                               {
                                                                   Id = s.AspersionEmployeesID,
                                                                   Nombre = s.Name + " " + s.LastName
                                                               }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["AspersionEmployesList"] = AspersionEmployesList;

            //Datos para Tercios
            var Third = siembras_PLANNEREntities.Aspersion_Thirds.Where(x => x.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList ThirdList = new SelectList(Third, "ThirdID", "Name");
            ViewData["ThirdList"] = ThirdList;

            //Datos para Tercios
            var Direction = siembras_PLANNEREntities.Aspersion_Directions.Where(x => x.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList DirectionList = new SelectList(Direction, "DirectionID", "Name");
            ViewData["DirectionList"] = DirectionList;

            //Datos para pestes
            var Plagas = siembras_PLANNEREntities.BAS_Pests.OrderBy(x => x.Name).ToList();
            SelectList PlagasList = new SelectList(Plagas, "ID", "Name");
            ViewData["PlagasList"] = PlagasList;

            //Datos para dropDown de compañias
            var aList = siembras_PLANNEREntities.fn_getGreenHousebyBusinessUnit(datosCabecera.GreenHouseID).OrderBy(a => a.Orden).ToList();
            ViewBag.lstGreenhouse = aList;

            return View();
        }

        public ActionResult IncidenciaSeveridad(int WeekID, int GreenHouseID, string PlagasID)
        {
            if (PlagasID == "")
            {
                PlagasID = null;
            }

            //try
            //{
                siembrasEntities = new SIEMBRASEntities();
                SqlConnection entityConnection = (SqlConnection)siembrasEntities.Database.Connection;
                SqlConnection cnn = entityConnection;
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cnn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_bloquesPorPlagasPorFincaSemana";

                SqlParameter IdSemana = new SqlParameter("@IdSemana", SqlDbType.Int);
                SqlParameter IdBussinesUnit = new SqlParameter("@IdBussinesUnit", SqlDbType.Int);
                SqlParameter IdBiologicTarget = new SqlParameter("@IdBiologicTarget", SqlDbType.VarChar);

                IdSemana.Value = WeekID;
                IdBussinesUnit.Value = GreenHouseID;
                IdBiologicTarget.Value = PlagasID;

                cmd.Parameters.Add(IdSemana);
                cmd.Parameters.Add(IdBussinesUnit);
                cmd.Parameters.Add(IdBiologicTarget);

                cnn.Open();

                List<InidenciaSeveridadlist> InidenciaSeveridadlist = new List<InidenciaSeveridadlist>();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        InidenciaSeveridadlist _InidenciaSeveridadlist = new InidenciaSeveridadlist();

                        _InidenciaSeveridadlist.Semana = Convert.ToInt32(dr[0]);
                        _InidenciaSeveridadlist.Anio = Convert.ToInt32(dr[1]);
                        _InidenciaSeveridadlist.IDInvernaderos = Convert.ToInt32(dr[2]);
                        _InidenciaSeveridadlist.block = dr[3].ToString();
                        _InidenciaSeveridadlist.IdPlagas = Convert.ToInt32(dr[4]);
                        _InidenciaSeveridadlist.Pest = dr[5].ToString();
                        _InidenciaSeveridadlist.Abrr = dr[6].ToString();
                        _InidenciaSeveridadlist.ColorIdentification = dr[7].ToString();
                        _InidenciaSeveridadlist.Beds = Convert.ToInt32(dr[8]);
                        _InidenciaSeveridadlist.CamasAfectadas = Convert.ToInt32(dr[9]);
                        _InidenciaSeveridadlist.CantidadCuadros = Convert.ToInt32(dr[10]);
                        _InidenciaSeveridadlist.PorcentajeIncidencia = Convert.ToDecimal(dr[11]);
                        _InidenciaSeveridadlist.PorcentajeSeveridad = Convert.ToDecimal(dr[12]);

                        InidenciaSeveridadlist.Add(_InidenciaSeveridadlist);
                    }
                    dr.Close();
                }

                cnn.Close();

                return Json(InidenciaSeveridadlist, JsonRequestBehavior.AllowGet);
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //    //return Json("0", JsonRequestBehavior.AllowGet);
            //}
        }

        [HttpPost]
        public ActionResult TraeDatosCamas(int IDInvernaderos, int WeekID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            List<Bed> ListaCamas = siembras_PLANNEREntities.Bed.Where(x => x.IDInvernaderos == IDInvernaderos && x.FlagActivo == 1).OrderBy(x => x.Orden).ToList();
            var ListaPlagas = siembras_PLANNEREntities.sp_getAllInfoBed(0, WeekID, IDInvernaderos, null).ToList();

            var data = new
            {
                CantCamas = ListaCamas,
                InfoBeds = ListaPlagas
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AllAspersionEmployees(int GreenHouseID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var AspersionEmployees = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.ActiveFlag == true && x.RolID == 1 && x.GreenHouseID == GreenHouseID).OrderBy(x => x.Name).ToList();
            return Json(AspersionEmployees, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GuardaCamaSeleccionada(AspersionPlanBeds datos)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            datos.Flag = "TMP";
            datos.ActiveFlag = true;
            datos.Usuario = Convert.ToInt32(Session["UserID"]);

            var CuentaCamas = siembras_PLANNEREntities.AspersionPlanBeds.Where(x => x.ActiveFlag == true && x.AspersionPlanId == datos.AspersionPlanId && x.Flag == "TMP" && x.Usuario == datos.Usuario && x.BedId == datos.BedId).Select(x => x.BedId).ToArray();
            if(CuentaCamas.Length == 0)
            {
                siembras_PLANNEREntities.AspersionPlanBeds.Add(datos);
                siembras_PLANNEREntities.SaveChanges();
            }
       
            return Json("");
        }

        [HttpPost]
        public JsonResult GuardaProductoPorCama(Aspersion_PlanProducts datos)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            datos.UserId = Convert.ToInt32(Session["UserID"]);

            var cuenta = siembras_PLANNEREntities.Aspersion_PlanProducts.Where(x => x.BedId == datos.BedId && x.BlockId == datos.BlockId && x.ProductId == datos.ProductId).ToArray();

            if (cuenta.Length == 0)
            {
                siembras_PLANNEREntities.Aspersion_PlanProducts.Add(datos);
                siembras_PLANNEREntities.SaveChanges();
            }
            return Json("");
        }

        public ActionResult BorraCamaSeleccionada(AspersionPlanBeds datos)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            datos.Usuario = Convert.ToInt32(Session["UserID"]);
            AspersionPlanBeds DatoBorrar = siembras_PLANNEREntities.AspersionPlanBeds.Where(x => x.Flag == "TMP" && x.BedId == datos.BedId && x.Usuario == datos.Usuario).FirstOrDefault();

            if (DatoBorrar != null)
            {
                siembras_PLANNEREntities.AspersionPlanBeds.Remove(DatoBorrar);
                siembras_PLANNEREntities.SaveChanges();
            }

            return Json("");
        }

        public JsonResult BorraProductoPorCama(Aspersion_PlanProducts datos)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            datos.UserId = Convert.ToInt32(Session["UserID"]);

            Aspersion_PlanProducts DatoBorrar = siembras_PLANNEREntities.Aspersion_PlanProducts.Where(x => x.BedId == datos.BedId && x.BlockId == datos.BlockId && x.ProductId == datos.ProductId).FirstOrDefault();

            if (DatoBorrar != null)
            {
                siembras_PLANNEREntities.Aspersion_PlanProducts.Remove(DatoBorrar);
                siembras_PLANNEREntities.SaveChanges();
            }

            return Json("");
        }

        public ActionResult TraeNumeroCamas(AspersionPlanBeds datos)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            datos.Usuario = Convert.ToInt32(Session["UserID"]);
            var CuentaCamas = siembras_PLANNEREntities.AspersionPlanBeds.Where(x => x.ActiveFlag == true && x.AspersionPlanId == datos.AspersionPlanId && x.Flag == "TMP" && x.Usuario == datos.Usuario && x.BlockId == datos.BlockId).Count();
            return Json(CuentaCamas, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TraeIdCamasSeleccionadas(AspersionPlanBeds datos)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            datos.Usuario = Convert.ToInt32(Session["UserID"]);
            var CuentaCamas = siembras_PLANNEREntities.AspersionPlanBeds.Where(x => x.ActiveFlag == true && x.AspersionPlanId == datos.AspersionPlanId && x.Usuario == datos.Usuario);
            return Json(CuentaCamas, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CambiaEstadoCamas(AspersionPlanBeds datos)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            datos.Usuario = Convert.ToInt32(Session["UserID"]);
            var aspersionPlanBeds = siembras_PLANNEREntities.sp_ActualizaCamasPlanAspersion(datos.AspersionPlanId, datos.Usuario);
            return Json("");
        }

        [HttpPost]
        public JsonResult TraeProductosPorBloque(Aspersion_PlanProducts datos)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            datos.UserId = Convert.ToInt32(Session["UserID"]);
            var ProductsByBlock = siembras_PLANNEREntities.fn_getProductsByBlock(datos.AspersionPlanId, datos.BlockId).Where(x => x.UserId == datos.UserId).ToList();
            return Json(ProductsByBlock, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GuardaNuevoRegistro(Aspersion_ReferenceSpear datos, string Ctrl, string Valor, string Valor2)
        {
            if (Ctrl == "ReferenceSpearID")
            {
                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                datos.ActiveFlag = true;
                datos.Name = Valor.ToUpper();
                datos.Capacity = Convert.ToDecimal(Valor2);
                siembras_PLANNEREntities.Aspersion_ReferenceSpear.Add(datos);
                siembras_PLANNEREntities.SaveChanges();

                var ListaGrupos = siembras_PLANNEREntities.Aspersion_ReferenceSpear.OrderBy(x => x.Name).ToList();
                return Json(ListaGrupos, JsonRequestBehavior.AllowGet);

            }
            return Json("OK");
        }
    }
}