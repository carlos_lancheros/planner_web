﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planner.Model;
using Resources;

using System.Globalization;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class Planner_SowingController : BaseController
    {
        SIEMBRAS_PLANNEREntities _SIEMBRAS_PLANNEREntities = null;
        SIEMBRASEntities _SIEMBRASEntities = null;

        SelectList businessUnitList = null;
        SelectList vegetableProductList = null;
        SelectList productList = null;
        SelectList activityList = null;

        public class ExecutionHeader
        {
            public int DayNumber { get; set; }
            public DateTime DayDate { get; set; }
            public string DayName { get; set; }
            public string ReportName { get; set; }
        }

        public class ExecutionList
        {
            public string ProductClass { get; set; }
            public string ActivityGroup { get; set; }
            public string LaborName { get; set; }
            public string Name { get; set; }
            public int DayNumber { get; set; }
            public DateTime DayDate { get; set; }
            public int ReportType { get; set; }
            public List<getTaskExecutionBoard_Result> ExecutionDetailLst { get; set; }
        }

        public class ExecutionDetail
        {
            public int MonUnits { get; set; }
            public decimal MonWorkforce { get; set; }
            public int TueUnits { get; set; }
            public decimal TueWorkforce { get; set; }
            public int WedUnits { get; set; }
            public decimal WedWorkforce { get; set; }
            public int ThuUnits { get; set; }
            public decimal ThuWorkforce { get; set; }
            public int FriUnits { get; set; }
            public decimal FriWorkforce { get; set; }
            public int SatUnits { get; set; }
            public decimal SatWorkforce { get; set; }
            public int SunUnits { get; set; }
            public decimal SunWorkforce { get; set; }
        }

        public ActionResult Index()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            ViewBag.Planner_SowingList = _SIEMBRAS_PLANNEREntities.sp_ReportSowing(Convert.ToDateTime("02/01/2019"), Convert.ToDateTime("3/4/2019")).ToList();

            return View(_SIEMBRAS_PLANNEREntities);
        }

        public ActionResult GetData(DateTime startDate, DateTime endDate)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            ViewBag.Planner_SowingList = _SIEMBRAS_PLANNEREntities.sp_ReportSowing(startDate, endDate).ToList();

            return PartialView("Planner_SowingList");
        }

        public ActionResult Visor()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //BusinessUnit
            SelectList lstBusinessUnit = null;
            var aList = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            lstBusinessUnit = new SelectList(aList, "ID", "Name".Trim().ToUpper());
            ViewData["lstBusinessUnit"] = lstBusinessUnit;

            var a = lstBusinessUnit.First().Value;
            GetGreenhousebyBusinessUnit(a);

            return View();
        }

        public ActionResult BedManager()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //BusinessUnit
            SelectList lstBusinessUnit = null;
            var aList = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            lstBusinessUnit = new SelectList(aList, "ID", "Name".Trim().ToUpper());
            ViewData["lstBusinessUnit"] = lstBusinessUnit;

            var BusinessUnitID = lstBusinessUnit.First().Value;
            GetBedManager(Convert.ToInt32(BusinessUnitID));
            return View();
        }

        public ActionResult DynamicSowing()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            _SIEMBRASEntities = new SIEMBRASEntities();
            //BusinessUnit
            SelectList lstBusinessUnit = null;
            var aList = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            lstBusinessUnit = new SelectList(aList, "ID", "Name".Trim().ToUpper());
            ViewData["lstBusinessUnit"] = lstBusinessUnit;

            ViewBag.DynamicSowingList = _SIEMBRASEntities.ConsultaVisorDinamicoSiembras(139).ToList();

            //  var BusinessUnitID = lstBusinessUnit.First().Value;
            // GetBedManager(Convert.ToInt32(BusinessUnitID));
            return View();
        }

        public ActionResult Projection()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            string currentWeek = BaseController.GetWeek(DateTime.Now).ToString("00");

            //View
            List<SelectListItem> View_Lst = new List<SelectListItem>
            {
                new SelectListItem() { Text = Resources.Resources.PorDia, Value = "D" },
                new SelectListItem() { Text = Resources.Resources.PorSemana, Value = "S" }
            };
            ViewData["View_Lst"] = new SelectList(View_Lst, "Value", "Text");
            //View

            //Business Unit
            var businessUnitList_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            businessUnitList = new SelectList(businessUnitList_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["businessUnitList"] = businessUnitList;
            //Business Unit

            //Vegetable Product
            var vegetableProduct_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllActiveProducts().OrderBy(x => x.Name).ToList();
            vegetableProductList = new SelectList(vegetableProduct_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["vegetableProductList"] = vegetableProductList;
            //Vegetable Product

            //Product
            var product_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllActiveProducts().OrderBy(x => x.Name).ToList();
            productList = new SelectList(product_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["productList"] = productList;
            //Product

            //Week Range
            SelectList lstWeek = null;
            DateTime strStartDate = DateTime.Now.AddYears(-1);
            DateTime strEndDate = DateTime.Now.AddMonths(1);

            var aListWeek = _SIEMBRAS_PLANNEREntities.sp_GetWeekList(strStartDate, strEndDate).ToList();

            lstWeek = new SelectList(aListWeek, "YEARWEEK", "YEARWEEK".Trim().ToUpper(), DateTime.Now.Year.ToString("0000") + currentWeek.ToString());
            ViewBag.lstWeekFrom = lstWeek;
            ViewBag.lstWeekTo = lstWeek;
            //Week Range

            //Activity
            var activity_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllPlanner_ActivityGroup().OrderBy(x => x.Name).ToList();
            activityList = new SelectList(activity_Lst, "ActivityGroupID", "Name".Trim().ToUpper());
            ViewData["activityList"] = activityList;
            //Activity

            ViewBag.ProjectionList = _SIEMBRAS_PLANNEREntities.sp_GetInfoPlannerTask_2("", "", "", BaseController.GetWeek(DateTime.Now), BaseController.GetWeek(DateTime.Now)).ToList();

            return View();
        }

        public ActionResult ProjectionV2()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            string currentWeek = BaseController.GetWeek(DateTime.Now).ToString("00");

            //View
            List<SelectListItem> View_Lst = new List<SelectListItem>
            {
                new SelectListItem() { Text = Resources.Resources.PorDia, Value = "D" },
                new SelectListItem() { Text = Resources.Resources.PorSemana, Value = "S" }
            };
            ViewData["View_Lst"] = new SelectList(View_Lst, "Value", "Text");
            //View

            //Business Unit
            var businessUnitList_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            businessUnitList = new SelectList(businessUnitList_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["businessUnitList"] = businessUnitList;
            //Business Unit

            //Vegetable Product
            var vegetableProduct_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllActiveProducts().OrderBy(x => x.Name).ToList();
            vegetableProductList = new SelectList(vegetableProduct_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["vegetableProductList"] = vegetableProductList;
            //Vegetable Product

            //Product
            var product_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllActiveProducts().OrderBy(x => x.Name).ToList();
            productList = new SelectList(product_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["productList"] = productList;
            //Product

            //Week Range
            SelectList lstWeek = null;
            DateTime strStartDate = DateTime.Now.AddYears(-1);
            DateTime strEndDate = DateTime.Now.AddMonths(1);

            var aListWeek = _SIEMBRAS_PLANNEREntities.sp_GetWeekList(strStartDate, strEndDate).ToList();

            lstWeek = new SelectList(aListWeek, "YEARWEEK", "YEARWEEK".Trim().ToUpper(), DateTime.Now.Year.ToString("0000") + currentWeek.ToString());
            ViewBag.lstWeekFrom = lstWeek;
            ViewBag.lstWeekTo = lstWeek;
            //Week Range

            //Activity
            var activity_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllPlanner_ActivityGroup().OrderBy(x => x.Name).ToList();
            activityList = new SelectList(activity_Lst, "ActivityGroupID", "Name".Trim().ToUpper());
            ViewData["activityList"] = activityList;
            //Activity

            ViewBag.ProjectionList = _SIEMBRAS_PLANNEREntities.sp_GetInfoPlannerTask_3("", "", "", BaseController.GetWeek(DateTime.Now), BaseController.GetWeek(DateTime.Now)).ToList();

            return View();
        }

        [HttpPost]
        public ActionResult JSONGetProjection(string[] prmUnidadNegocio, string[] prmProductoVegetal, string[] prmActividad, string prmSemanaINI, string prmSemanaFIN)
        {
            int InitialWeek = Convert.ToInt32(prmSemanaINI.Substring(4));
            int FinalWeek = Convert.ToInt32(prmSemanaFIN.Substring(4));

            string strUnidadNegocio = (string.Join(",", prmUnidadNegocio) == string.Empty ? null : string.Join(",", prmUnidadNegocio));
            string strProductoVegetal = (string.Join(",", prmProductoVegetal) == string.Empty ? null : string.Join(",", prmProductoVegetal));
            string strActividad = (string.Join(",", prmActividad) == string.Empty ? null : string.Join(",", prmActividad));

            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            _SIEMBRAS_PLANNEREntities.Configuration.LazyLoadingEnabled = false;

            var Planner_SowingProjectionLst = _SIEMBRAS_PLANNEREntities.sp_GetInfoPlannerTask_2(strUnidadNegocio, strProductoVegetal, strActividad, InitialWeek, FinalWeek);

            var jsonResult = Json(Planner_SowingProjectionLst, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        [HttpPost]
        public ActionResult JSONGetProjectionV2(string[] prmUnidadNegocio, string[] prmProductoVegetal, string[] prmActividad, string prmSemanaINI, string prmSemanaFIN)
        {
            int InitialWeek = Convert.ToInt32(prmSemanaINI.Substring(4));
            int FinalWeek = Convert.ToInt32(prmSemanaFIN.Substring(4));

            string strUnidadNegocio = (string.Join(",", prmUnidadNegocio) == string.Empty ? null : string.Join(",", prmUnidadNegocio));
            string strProductoVegetal = (string.Join(",", prmProductoVegetal) == string.Empty ? null : string.Join(",", prmProductoVegetal));
            string strActividad = (string.Join(",", prmActividad) == string.Empty ? null : string.Join(",", prmActividad));

            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            _SIEMBRAS_PLANNEREntities.Configuration.LazyLoadingEnabled = false;

            var Planner_SowingProjectionLst = _SIEMBRAS_PLANNEREntities.sp_GetInfoPlannerTask_3(strUnidadNegocio, strProductoVegetal, strActividad, InitialWeek, FinalWeek);

            var jsonResult = Json(Planner_SowingProjectionLst, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult GetDynamicSowing(int BusinessUnitID)
        {
            _SIEMBRASEntities = new SIEMBRASEntities();
            ViewBag.DynamicSowingList = _SIEMBRASEntities.ConsultaVisorDinamicoSiembras(BusinessUnitID).ToList();

            return PartialView("DynamicSowingList");
        }

        public ActionResult GetGreenhousebyBusinessUnit(string BusinessUnitID)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            SelectList lstGreenhouse = null;
            var aList = _SIEMBRAS_PLANNEREntities.fn_getGreenHousebyBusinessUnit(Convert.ToInt32(BusinessUnitID)).OrderBy(a => a.Orden).ToList();
            lstGreenhouse = new SelectList(aList, "ID", "Codigo".Trim().ToUpper());
            ViewBag.GreenhouseId = Convert.ToInt32(lstGreenhouse.FirstOrDefault().Value);
            ViewData["lstGreenhouse"] = lstGreenhouse;

            var b = lstGreenhouse.First().Value;
            ViewBag.aListy = _SIEMBRAS_PLANNEREntities.sp_Sowing(Convert.ToInt32(b)).ToList();
            return Json(lstGreenhouse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataSowing(int greenhouseID)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            ViewBag.aListy = _SIEMBRAS_PLANNEREntities.sp_Sowing(greenhouseID).ToList();

            return PartialView("Visor_List");
        }

        public ActionResult GetBedManager(int BusinessUnitID)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            ViewBag.BedManager_List = _SIEMBRAS_PLANNEREntities.sp_GetBedState(BusinessUnitID).ToList();

            return PartialView("BedManager_List");
        }

        public ActionResult Execution()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            string currentWeek = BaseController.GetWeek(DateTime.Now).ToString("00");

            //Business Unit
            var businessUnitList_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            businessUnitList = new SelectList(businessUnitList_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["businessUnitList"] = businessUnitList;
            //Business Unit

            //Product
            var product_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllActiveProducts().OrderBy(x => x.Name).ToList();
            productList = new SelectList(product_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["productList"] = productList;
            //Product

            //Week
            SelectList lstWeek = null;
            DateTime strStartDate = DateTime.Now.AddYears(-1);
            DateTime strEndDate = DateTime.Now.AddMonths(1);

            var aListWeek = _SIEMBRAS_PLANNEREntities.sp_GetWeekList(strStartDate, strEndDate).ToList();

            lstWeek = new SelectList(aListWeek, "YEARWEEK", "YEARWEEK".Trim().ToUpper(), DateTime.Now.Year.ToString("0000") + currentWeek.ToString());
            ViewBag.lstWeek = lstWeek;
            //Week

            //Labors
            var activity_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllPlanner_ActivityGroup().OrderBy(x => x.Name).ToList();
            activityList = new SelectList(activity_Lst, "ActivityGroupID", "Name".Trim().ToUpper());
            ViewData["activityList"] = activityList;
            //Labors

            GetExecution(DateTime.Now.Year, Convert.ToInt32(currentWeek), new string[] { businessUnitList.FirstOrDefault().Value.ToString() }, new string[] { productList.FirstOrDefault().Value.ToString() }, new string[] { activityList.FirstOrDefault().Value.ToString() }, 1);

            return View();
        }

        public ActionResult GetExecutionHeader(int year, int week, int ReportType)
        {
            year = (year <= 0 ? DateTime.Now.Year : year);
            week = (week <= 0 ? BaseController.GetWeek(DateTime.Now.Date) : week);

            string ReportName = (ReportType == 1 ? Resources.Resources.Cantidad : (ReportType == 2 ? Resources.Resources.ManoDeObraAbr : Resources.Resources.NoSeEncontraronDatos));

            List<DateTime> _ExecutionDates = BaseController.GetWeekDateFromYearAndWeek(year, week);

            List<ExecutionHeader> ExecutionList = new List<ExecutionHeader>();

            ExecutionHeader _ExecutionHeader = new ExecutionHeader
            {
                DayNumber = 1,
                DayDate = _ExecutionDates[1].Date,
                DayName = Resources.Resources.Lunes,
                ReportName = ReportName
            };
            ExecutionList.Add(_ExecutionHeader);

            _ExecutionHeader = new ExecutionHeader
            {
                DayNumber = 2,
                DayDate = _ExecutionDates[2].Date,
                DayName = Resources.Resources.Martes,
                ReportName = ReportName
            };
            ExecutionList.Add(_ExecutionHeader);

            _ExecutionHeader = new ExecutionHeader
            {
                DayNumber = 3,
                DayDate = _ExecutionDates[3].Date,
                DayName = Resources.Resources.Miercoles,
                ReportName = ReportName
            };
            ExecutionList.Add(_ExecutionHeader);

            _ExecutionHeader = new ExecutionHeader
            {
                DayNumber = 4,
                DayDate = _ExecutionDates[4].Date,
                DayName = Resources.Resources.Jueves,
                ReportName = ReportName
            };
            ExecutionList.Add(_ExecutionHeader);

            _ExecutionHeader = new ExecutionHeader
            {
                DayNumber = 5,
                DayDate = _ExecutionDates[5].Date,
                DayName = Resources.Resources.Viernes,
                ReportName = ReportName
            };
            ExecutionList.Add(_ExecutionHeader);

            _ExecutionHeader = new ExecutionHeader
            {
                DayNumber = 6,
                DayDate = _ExecutionDates[6].Date,
                DayName = Resources.Resources.Sabado,
                ReportName = ReportName
            };
            ExecutionList.Add(_ExecutionHeader);

            _ExecutionHeader = new ExecutionHeader
            {
                DayNumber = 7,
                DayDate = _ExecutionDates[0].Date,
                DayName = Resources.Resources.Domingo,
                ReportName = ReportName
            };
            ExecutionList.Add(_ExecutionHeader);

            ViewBag.ExecutionHeader = ExecutionList.ToList();

            return Json(ExecutionList.ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetExecution(int year, int week, string[] businessUnitID, string[] productID, string[] activityGroupID, int ReportType)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            GetExecutionHeader(year, week, ReportType);

            string strUnidadNegocio = (string.Join(",", businessUnitID) == string.Empty ? null : string.Join(",", businessUnitID));
            string strProductoVegetal = (string.Join(",", productID) == string.Empty ? null : string.Join(",", productID));
            string strActividad = (string.Join(",", activityGroupID) == string.Empty ? null : string.Join(",", activityGroupID));

            var ExecutionDetailList = _SIEMBRAS_PLANNEREntities.getTaskExecutionBoard(year, week, strUnidadNegocio, strProductoVegetal, strActividad).ToList();

            var Result = from c in ExecutionDetailList
                         group c by new
                         {
                             c.ProductClass,
                             c.ActivityGroup,
                             c.LaborName,
                             c.Name,
                         } into gcs
                         select new ExecutionList()
                         {
                             ProductClass = gcs.Key.ProductClass,
                             ActivityGroup = gcs.Key.ActivityGroup,
                             LaborName = gcs.Key.LaborName,
                             Name = gcs.Key.Name,
                             DayNumber = (int)DateTime.Now.DayOfWeek,
                             DayDate = DateTime.Now,
                             ReportType = (ReportType == 1 ? 2 : 1),
                             ExecutionDetailLst = gcs.ToList(),
                         };

            ViewBag.ExecutionList = Result.ToList();

            ViewData["ExecutionList"] = Result.ToList();
            return PartialView("ExecutionLaborsWorkforceList");
        }

        public ActionResult GetDataBedInfo(int BedID)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var aListy = _SIEMBRAS_PLANNEREntities.fn_getBedInfo(BedID.ToString()).ToList();

            var jsonResult = Json(aListy, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult GetDataBedInfoDetaill(int BedID)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var aListy = _SIEMBRAS_PLANNEREntities.fn_getBedInfoDetail(BedID.ToString()).OrderByDescending(x => x.Fsiembra).ToList();

            return Json(aListy, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExecutionLabors()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            string currentWeek = BaseController.GetWeek(DateTime.Now).ToString("00");

            //Business Unit
            var businessUnitList_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            businessUnitList = new SelectList(businessUnitList_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["businessUnitList"] = businessUnitList;
            //Business Unit

            //Product
            var product_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllActiveProducts().OrderBy(x => x.Name).ToList();
            productList = new SelectList(product_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["productList"] = productList;
            //Product

            //Week
            SelectList lstWeek = null;
            DateTime strStartDate = DateTime.Now.AddYears(-1);
            DateTime strEndDate = DateTime.Now.AddMonths(1);

            var aListWeek = _SIEMBRAS_PLANNEREntities.sp_GetWeekList(strStartDate, strEndDate).ToList();

            lstWeek = new SelectList(aListWeek, "YEARWEEK", "YEARWEEK".Trim().ToUpper(), DateTime.Now.Year.ToString("0000") + currentWeek.ToString());
            ViewBag.lstWeek = lstWeek;
            //Week

            //Labors
            var activity_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllPlanner_ActivityGroup().OrderBy(x => x.Name).ToList();
            activityList = new SelectList(activity_Lst, "ActivityGroupID", "Name".Trim().ToUpper());
            ViewData["activityList"] = activityList;
            //Labors

            GetExecution(DateTime.Now.Year, Convert.ToInt32(currentWeek), new string[] { businessUnitList.FirstOrDefault().Value.ToString() }, new string[] { productList.FirstOrDefault().Value.ToString() }, new string[] { activityList.FirstOrDefault().Value.ToString() }, 1);

            return View();
        }

        public ActionResult PlantingPlan()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            string currentWeek = BaseController.GetWeek(DateTime.Now).ToString("00");

            //Business Unit
            var businessUnitList_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            businessUnitList = new SelectList(businessUnitList_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["businessUnitList"] = businessUnitList;
            //Business Unit

            //Product
            var product_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllActiveProducts().OrderBy(x => x.Name).ToList();
            productList = new SelectList(product_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["productList"] = productList;
            //Product

            //Week
            SelectList lstWeek = null;
            DateTime strStartDate = DateTime.Now.AddYears(-1);
            DateTime strEndDate = DateTime.Now.AddMonths(1);

            var aListWeek = _SIEMBRAS_PLANNEREntities.sp_GetWeekList(strStartDate, strEndDate).ToList();

            lstWeek = new SelectList(aListWeek, "YEARWEEK", "YEARWEEK".Trim().ToUpper(), DateTime.Now.Year.ToString("0000") + currentWeek.ToString());
            ViewBag.lstWeek = lstWeek;


            GetPlantingPlan(DateTime.Now.Year, Convert.ToInt32(currentWeek), new string[] { businessUnitList.FirstOrDefault().Value.ToString() }, new string[] { productList.FirstOrDefault().Value.ToString() });


            return View();
        }
        public JsonResult GetPlantingPlan(int year, int week, string[] businessUnitID, string[] productID)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();


            string strUnidadNegocio = (string.Join(",", businessUnitID) == string.Empty ? null : string.Join(",", businessUnitID));
            string strProductoVegetal = (string.Join(",", productID) == string.Empty ? null : string.Join(",", productID));

            //var PlantingList = _SIEMBRAS_PLANNEREntities.getPlantingPlan(year, week, strUnidadNegocio, strProductoVegetal).ToList();
            //ViewBag.PlantingPlan_List = PlantingList;

            //return PartialView("PlantingPlan_List");

            //var jsonResult = Json(PlantingList, JsonRequestBehavior.AllowGet);
            //jsonResult.MaxJsonLength = int.MaxValue;
            //return jsonResult;


            List<getPlantingPlan_Result> Data = _SIEMBRAS_PLANNEREntities.getPlantingPlan(year, week, strUnidadNegocio, strProductoVegetal).ToList();


            //var jsonResult = Json(Data, JsonRequestBehavior.AllowGet);
            //jsonResult.MaxJsonLength = int.MaxValue;
            return Json(Data, JsonRequestBehavior.AllowGet);

            //eturn Json(ViewBag.PlantingPlan_List, JsonRequestBehavior.AllowGet);
        }

    }
}