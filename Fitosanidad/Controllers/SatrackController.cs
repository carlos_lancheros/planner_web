﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class SatrackController : BaseController
    {
        satrack.getEventsSoapClient wsSatrack = new satrack.getEventsSoapClient();

        public ActionResult Index()
        {
            try
            {
                DataSet dsSatrack = wsSatrack.retrieveEventsByIDV3("katama2019", "katama2019", "*", "45", 1, 300);

                return View(dsSatrack);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}