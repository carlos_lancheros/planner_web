﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Planner.Model;


namespace Fitosanidad.Controllers
{
    public class DailyFarmPerformanceController : Controller
    {
        BASEntities _BASEntities = null;

        // GET: DailyFarmPerformance
        public ActionResult Index()
        {
            _BASEntities = new BASEntities();

            SelectList lstCentrosOperacion = null;
            var aList = _BASEntities.BusinessUnit.Where(x => x.BusinessUnitfunctionsID == 9 || x.BusinessUnitfunctionsID == 12).OrderBy(x => x.Name);
            lstCentrosOperacion = new SelectList(aList, "EquiIDSunttel", "Name".Trim().ToUpper());
            ViewData["lstCentrosOperacion"] = lstCentrosOperacion;

            DailyFarmPerformance(Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value), "2019-04-09", "2019-04-09");

            return View();
        }

        public ActionResult DailyFarmPerformance(int CentroOperacionID, string FechaIni, string FechaFin)
        {
            _BASEntities = new BASEntities();
            _BASEntities.Database.CommandTimeout = 200000;
            //var FechaIni2 = Convert.ToDateTime("2019/06/07");
            //var FechaFin2 = Convert.ToDateTime("2019/06/07");

            DateTime StartDate = Convert.ToDateTime(FechaIni);
            DateTime EndDate = Convert.ToDateTime(FechaFin); // Convert.ToDateTime(FechaFin).AddDays(1).AddSeconds(-1);

            string FechaIni2 = StartDate.ToString("yyyyMMdd");
            string FechaFin2 = EndDate.ToString("yyyyMMdd");

            var DailyFarmPerformance_List = _BASEntities.sp_GetDailyFarmPerformance(StartDate, EndDate, CentroOperacionID).ToList();
            ViewBag.DailyFarmPerformance_List = DailyFarmPerformance_List;

            return PartialView("DailyFarmPerformance_List");
        }
    }
}
