﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        BASEntities basentities = new BASEntities();
        public ActionResult Index()
        {
            basentities = new BASEntities();
            if (Session["UserID"] == null)
            {
                return View("../Account/Login");
            }
            else
            {
                int Id = Convert.ToInt32(Session["UserID"]);
                var Users = basentities.Auth_User.Where(x => x.UserID == Id).FirstOrDefault();

                //Lista de areas
                var listA = basentities.Auth_Area.Where(x => x.ActiveFlag == true).OrderBy(x => x.AreaName).ToList();
                SelectList listaA = new SelectList(listA, "AreaId", "AreaName", Users.AreaId);

                Session["AreaId"] = Users.AreaId;

                ViewData["listaA"] = listaA;

                //Lista de roles
                var listR = basentities.Auth_Roles.Where(x => x.ActiveFlag == true).OrderBy(x => x.RoleName).ToList();
                SelectList listaR = new SelectList(listR, "RoleId", "RoleName", Users.RoleId);
                ViewData["listaR"] = listaR;
                Session["RoleId"] = Users.RoleId;
                ViewBag.Users = Users;
                return View(Users);
            }
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}