﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Fitosanidad.Models;

using Planner.Model;
using Resources;
using System.Collections.Generic;
using RestSharp;
using System.Net.Http;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;


namespace Fitosanidad.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        BASEntities _BASEntities = null;
        string _Menu = string.Empty;

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        ////[ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            //ActiveDirectory();

            string _Error = string.Empty;

            var url = "http://login.sendero.api";

            RestClient restClient = new RestClient(url);
            RestRequest request = new RestRequest("api/BAS/Login", Method.GET);

            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("Login", model.Email);
            request.AddParameter("Password", model.Password);

            var response = restClient.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                string jsonResult = response.Content;

                bool booResult = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<bool>(jsonResult);
                if(!booResult)
                {
                    ModelState.AddModelError("", "NO existe el usuario: " + model.Email + " o la contraseña es incorrecta.");
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }

            _BASEntities = new BASEntities();

            Auth_User _Auth_User = _BASEntities.Auth_User.Where(x => x.Login == model.Email && x.FlagActive == true).FirstOrDefault();
            if (_Auth_User == null)
            {
                ModelState.AddModelError("", "NO existe el usuario: " + model.Email + " o la contraseña es incorrecta.");
                return View(model);
            }

            List<Auth_UserFeature> _Auth_UserFeature = _BASEntities.Auth_UserFeature.Where(x => x.UserID == _Auth_User.UserID).ToList();
            if (_Auth_UserFeature.Count <= 0)
            {
                ModelState.AddModelError("", "El usuario: " + model.Email + " NO tiene opciones definidas.");
                return View(model);
            }

            FormsAuthentication.SetAuthCookie(model.Email.ToUpper(), false);

            Session["UserID"] = _Auth_User.UserID;
            Session["Login"] = _Auth_User.Login;
            Session["Name"] = _Auth_User.Name;
            Session["Printer"] = _Auth_User.Printer;

            HelperController.VerifyWorkEnvironment();

            var _GetAccessByUser = _BASEntities.sp_GetAccessByUser(_Auth_User.UserID).ToList();
            if (_GetAccessByUser == null || _GetAccessByUser.Count <= 0)
            {
                _Error = _Auth_User.Login + " - " + Resources.Resources.NoTienePermisosAsignados;
                throw new System.ArgumentException(_Error);
            }
            else
            {
                getMenu(_GetAccessByUser);
            }

            bool llAcceso = false;
            foreach (var _opcion in _GetAccessByUser)
            {
                if (llAcceso == false)
                {
                    llAcceso = true;
                }
            }
            if (!llAcceso)
            {
                _Error = _Auth_User.Login + " - " + Resources.Resources.NoTieneOpcionesHabilitadas;
                throw new System.ArgumentException(_Error);
            }

            if (!string.IsNullOrEmpty(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Requerir que el usuario haya iniciado sesión con nombre de usuario y contraseña o inicio de sesión externo
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // El código siguiente protege de los ataques por fuerza bruta a los códigos de dos factores. 
            // Si un usuario introduce códigos incorrectos durante un intervalo especificado de tiempo, la cuenta del usuario 
            // se bloqueará durante un período de tiempo especificado. 
            // Puede configurar el bloqueo de la cuenta en IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Código no válido.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);
                    
                    // Para obtener más información sobre cómo habilitar la confirmación de cuentas y el restablecimiento de contraseña, visite https://go.microsoft.com/fwlink/?LinkID=320771
                    // Enviar correo electrónico con este vínculo
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirmar cuenta", "Para confirmar la cuenta, haga clic <a href=\"" + callbackUrl + "\">aquí</a>");

                    return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }

            // Si llegamos a este punto, es que se ha producido un error y volvemos a mostrar el formulario
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            IdentityMessage message = new IdentityMessage();
            message.Destination = "jose.becerra@floreskatama.com";
            message.Subject = "Prueba Correo";
            message.Body = @"<html>
                                 <body>
                                     <p>PRUEBA</p>
                                 </body>
                             </html>";

            UserManager.EmailService.SendAsync(message);

            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // No revelar que el usuario no existe o que no está confirmado
                    return View("ForgotPasswordConfirmation");
                }

                // Para obtener más información sobre cómo habilitar la confirmación de cuentas y el restablecimiento de contraseña, visite https://go.microsoft.com/fwlink/?LinkID=320771
                // Enviar correo electrónico con este vínculo
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Restablecer contraseña", "Para restablecer la contraseña, haga clic <a href=\"" + callbackUrl + "\">aquí</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // Si llegamos a este punto, es que se ha producido un error y volvemos a mostrar el formulario
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // No revelar que el usuario no existe
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Solicitar redireccionamiento al proveedor de inicio de sesión externo
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generar el token y enviarlo
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Si el usuario ya tiene un inicio de sesión, iniciar sesión del usuario con este proveedor de inicio de sesión externo
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // Si el usuario no tiene ninguna cuenta, solicitar que cree una
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Obtener datos del usuario del proveedor de inicio de sesión externo
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Asistentes
        // Se usa para la protección XSRF al agregar inicios de sesión externos
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        private void getMenu(IList<sp_GetAccessByUser_Result> AccessByUser)
        {
            IList<sp_GetAccessByUser_Result> _Nivel0 = AccessByUser.Where(x => x.ParentFeatureID == 0).OrderBy(x => x.Name).ToList();

            try
            {
                foreach (var _opcion in _Nivel0)
                {
                    _Menu += "<li class='treeview'>";
                    _Menu += "    <a href = '#' >";
                    _Menu += "        <i class='fa " + _opcion.Icon.Trim() + "'></i><span>" + _opcion.Name.Trim() + "</span><span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span>";
                    _Menu += "    </a>";
                    _Menu += "    <ul class='treeview-menu'>";
                    getOptions(_opcion, AccessByUser);
                    _Menu += "    </ul>";
                    _Menu += "</li>";
                }
                Session["Menu"] = _Menu;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void getOptions(sp_GetAccessByUser_Result _opcion, IList<sp_GetAccessByUser_Result> AccessByUser)
        {
            IList<sp_GetAccessByUser_Result> _SubModulos = AccessByUser.Where(x => x.ParentFeatureID == _opcion.FeatureID).OrderBy(x => x.Name).ToList();

            foreach (var _opcionsub in _SubModulos.ToList())
            {
                if (Convert.ToString(_opcion.FeatureID) == Convert.ToString(_opcionsub.ParentFeatureID))
                {
                    if (String.IsNullOrEmpty(_opcionsub.Controller) && String.IsNullOrEmpty(_opcionsub.Action))
                    {
                        _Menu += "<li class='treeview'>";
                        _Menu += "    <a href = '#' >";
                        _Menu += "        <i class='fa " + _opcionsub.Icon.Trim() + "'></i><span>" + _opcionsub.Name.Trim() + "</span><span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span>";
                        _Menu += "    </a>";
                        _Menu += "    <ul class='treeview-menu'>";
                        getOptions(_opcionsub, AccessByUser);
                        _Menu += "    </ul>";
                        _Menu += "</li>";
                    }
                    else
                    {
                        _Menu += "<li><a href = /" + _opcionsub.Controller.Trim() + "/" + _opcionsub.Action.Trim() + "><i class='fa " + (String.IsNullOrEmpty(_opcionsub.Icon) ? "fa-circle-o" : _opcionsub.Icon.Trim()) + "'></i>" + _opcionsub.Name.Trim() + "</a></li>";
                    }
                }
            }
        }

        public void ActiveDirectory()
        {
            try
            {
                //BLSECUsuarios _BLSECUsuarios = new BLSECUsuarios();

                //BLICSPersonas _BLICSPersonas = new BLICSPersonas();
                //List<Personas> _Personas_Lst = _BLICSPersonas.GetAllPersonasActivas();

                //foreach (var _Persona in _Personas_Lst)
                //{
                //    _BLSECUsuarios.Autenticar(_Persona.Email.Substring(0, _Persona.Email.IndexOf("@")), string.Empty);
                //}

                DirectoryEntry de = new DirectoryEntry("LDAP://sendero/ldapaccount/,DC=sendero,DC=com");

                DirectorySearcher deSearch = new DirectorySearcher(de);
                deSearch.PageSize = 10000;

                SearchResultCollection result = deSearch.FindAll();

                for (int x = 0; x < result.Count; x++)
                {
                    string SAMAccountName = Convert.ToBoolean(result[x].Properties["sAMAccountName"].Count > 0) ? result[x].Properties["sAMAccountName"][0].ToString() : "";
                    string DisplayName = Convert.ToBoolean(result[x].Properties["displayName"].Count > 0) ? result[x].Properties["displayName"][0].ToString() : "";
                    string givenName = Convert.ToBoolean(result[x].Properties["givenName"].Count > 0) ? result[x].Properties["givenName"][0].ToString() : "";
                    string mail = Convert.ToBoolean(result[x].Properties["mail"].Count > 0) ? result[x].Properties["mail"][0].ToString() : "";

                    if (SAMAccountName != string.Empty
                        && !SAMAccountName.Contains("$")
                        && DisplayName != string.Empty
                        && !DisplayName.Contains("$")
                        && !SAMAccountName.ToLower().Contains("autoenvio")
                        && !SAMAccountName.ToLower().Contains("exchange")
                        && !SAMAccountName.ToLower().Contains("enterprise")
                        && !SAMAccountName.ToLower().Contains("schema")
                        && !SAMAccountName.ToLower().Contains("sql")
                        && !SAMAccountName.ToLower().Contains("backup")
                        && !SAMAccountName.ToLower().Contains("administrator")
                        && !SAMAccountName.ToLower().Contains("ecommerce")
                        && !SAMAccountName.ToLower().Contains("estadistica")
                        && !SAMAccountName.ToLower().Contains("credits")
                        && !SAMAccountName.ToLower().Contains("msexch")
                        && !SAMAccountName.ToLower().Contains("discovery")
                        && !SAMAccountName.ToLower().Contains("dicovery")
                        && !SAMAccountName.ToLower().Contains("federate")
                        && !SAMAccountName.ToLower().Contains("fondo")
                        && !SAMAccountName.ToLower().Contains("kenya")
                        && !SAMAccountName.ToLower().Contains("komet")
                        && !SAMAccountName.ToLower().Contains("rfid")
                        && !SAMAccountName.ToLower().Contains("system")
                        && !SAMAccountName.ToLower().Contains("video")
                        && !SAMAccountName.ToLower().Contains("soporte")
                        && !SAMAccountName.ToLower().Contains("tecnologia")
                        && !SAMAccountName.ToLower().Contains("packagingnj")
                        && !SAMAccountName.ToLower().Contains("payroll")
                        && !SAMAccountName.ToLower().Contains("monitoreo")
                        && !SAMAccountName.ToLower().Contains("production")
                        && !SAMAccountName.ToLower().Contains("qa")
                        && !SAMAccountName.ToLower().Contains("admin")
                        && !SAMAccountName.ToLower().Contains("shipping")
                        && !SAMAccountName.ToLower().Contains("westbrook")
                        && !SAMAccountName.ToLower().Contains("seleccion")
                        && !SAMAccountName.ToLower().Contains("temporal")
                        && !SAMAccountName.ToLower().Contains("datos")
                        && !SAMAccountName.ToLower().Contains("etiquetas")
                        && !SAMAccountName.ToLower().Contains("pasantes")
                        && !SAMAccountName.ToLower().Contains("awbstatus")
                        && !SAMAccountName.ToLower().Contains("bercomex")
                        && !SAMAccountName.ToLower().Contains("spti")
                        && !SAMAccountName.ToLower().Contains("inventory")
                        && !SAMAccountName.ToLower().Contains("reception")
                        && !SAMAccountName.ToLower().Contains("transportation")
                        && !SAMAccountName.ToLower().Contains("dtinzuque")
                        && !SAMAccountName.ToLower().Contains("dmorado")
                        && !SAMAccountName.ToLower().Contains("fdimco")
                        && !SAMAccountName.ToLower().Contains("fdimmvc")
                        && !SAMAccountName.ToLower().Contains("MsExch")
                        && !SAMAccountName.ToLower().Contains("FederatedEmail")
                        && !SAMAccountName.ToLower().Contains("sistemas")
                        && !SAMAccountName.ToLower().Contains("SPTI")
                        && !SAMAccountName.ToLower().Contains("dicoverysearch")
                        && !SAMAccountName.ToLower().Contains("SystemMailbox")
                        && !SAMAccountName.ToLower().Contains("helpdesk")
                    )
                    {
                        //_BLSECUsuarios.Autenticar(SAMAccountName, string.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




    }
}