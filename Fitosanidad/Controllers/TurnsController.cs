﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Planner.Model;


namespace Fitosanidad.Controllers
{
    public class TurnsController : Controller
    {
        // GET: Turns
        HORUS_CTRL_NOVEDADESEntities _Horus_Ctrl_NovedadesEntities = null;
        
        public ActionResult Index()
        {
            _Horus_Ctrl_NovedadesEntities = new HORUS_CTRL_NOVEDADESEntities();


            SelectList lstCompania = null;
            //var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1 && x.Nombre.Contains("POST")).OrderBy(x => x.Nombre);
            var aListCo = _Horus_Ctrl_NovedadesEntities.Companias.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);

            lstCompania = new SelectList(aListCo, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCompania"] = lstCompania;

            SelectList lstUnidadNegocio = null;
            var aListUN = _Horus_Ctrl_NovedadesEntities.EntidadesProductivas.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
            lstUnidadNegocio = new SelectList(aListUN, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstUnidadNegocio"] = lstUnidadNegocio;

            SelectList lstCentrosOperacion = null;
            //var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1 && x.Nombre.Contains("POST")).OrderBy(x => x.Nombre);
            var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);

            lstCentrosOperacion = new SelectList(aList, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCentrosOperacion"] = lstCentrosOperacion;

            SelectList lstCentrosCosto = null;
            var aListc = _Horus_Ctrl_NovedadesEntities.CentrosCosto.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
            lstCentrosCosto = new SelectList(aListc, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCentrosCosto"] = lstCentrosCosto;


            //Employees(Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value), Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value),Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value), Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value), 1);
            Turns("2019-04-09", "2019-04-09",0, 0, 0, 0, 1);

            return View();

        }

        public ActionResult Turns(string FechaIni, string FechaFin, int CompaniaID, int UnidadNegocioID, int CentrosOperacionID, int CentrosCostoID, int FlagActivo)
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetTurns(string FechaIni, string FechaFin, int CompaniaID, int UnidadNegocioID, int CentrosOperacionID, int CentrosCostoID, int FlagActivo)
        {
            _Horus_Ctrl_NovedadesEntities = new HORUS_CTRL_NOVEDADESEntities();

            DateTime FechaInicial = DateTime.Parse(FechaIni);
            DateTime FechaFinal = DateTime.Parse(FechaFin);

            var Turns_List = _Horus_Ctrl_NovedadesEntities.InformeTurnosProgramados(FechaInicial, FechaFinal, CompaniaID, CentrosOperacionID, UnidadNegocioID, CentrosCostoID, FlagActivo);

            var jsonResult = Json(Turns_List, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult Create()
        {

            _Horus_Ctrl_NovedadesEntities = new HORUS_CTRL_NOVEDADESEntities();


            SelectList lstCompania = null;
            //var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1 && x.Nombre.Contains("POST")).OrderBy(x => x.Nombre);
            var aListCo = _Horus_Ctrl_NovedadesEntities.Companias.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);

            lstCompania = new SelectList(aListCo, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCompania"] = lstCompania;

            SelectList lstUnidadNegocio = null;
            var aListUN = _Horus_Ctrl_NovedadesEntities.EntidadesProductivas.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
            lstUnidadNegocio = new SelectList(aListUN, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstUnidadNegocio"] = lstUnidadNegocio;

            SelectList lstCentrosOperacion = null;
            //var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1 && x.Nombre.Contains("POST")).OrderBy(x => x.Nombre);
            var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);

            lstCentrosOperacion = new SelectList(aList, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCentrosOperacion"] = lstCentrosOperacion;

            SelectList lstCentrosCosto = null;
            var aListc = _Horus_Ctrl_NovedadesEntities.CentrosCosto.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
            lstCentrosCosto = new SelectList(aListc, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCentrosCosto"] = lstCentrosCosto;

            SelectList lstEmpleados = null;
            var aListe = _Horus_Ctrl_NovedadesEntities.Empleados.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
            lstEmpleados = new SelectList(aListe, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstEmpleados"] = lstEmpleados;

            ViewBag.Turns_List = 0;

            return View();
        }

        public ActionResult Edit(int ActionID)
        {
            _Horus_Ctrl_NovedadesEntities = new HORUS_CTRL_NOVEDADESEntities();
            //Auth_FormsActions Auth_FormsActions = _Horus_Ctrl_NovedadesEntities.Auth_FormsActions.Where(x => x.ActionID == ActionID).FirstOrDefault();
            //if (Auth_FormsActions == null)
            //{
            //    return RedirectToAction("Index");
            //}
            ////return View(Auth_FormsActions);
            SelectList lstCompania = null;
            //var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1 && x.Nombre.Contains("POST")).OrderBy(x => x.Nombre);
            var aListCo = _Horus_Ctrl_NovedadesEntities.Companias.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);

            lstCompania = new SelectList(aListCo, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCompania"] = lstCompania;

            SelectList lstUnidadNegocio = null;
            var aListUN = _Horus_Ctrl_NovedadesEntities.EntidadesProductivas.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
            lstUnidadNegocio = new SelectList(aListUN, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstUnidadNegocio"] = lstUnidadNegocio;

            SelectList lstCentrosOperacion = null;
            //var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1 && x.Nombre.Contains("POST")).OrderBy(x => x.Nombre);
            var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);

            lstCentrosOperacion = new SelectList(aList, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCentrosOperacion"] = lstCentrosOperacion;

            SelectList lstCentrosCosto = null;
            var aListc = _Horus_Ctrl_NovedadesEntities.CentrosCosto.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
            lstCentrosCosto = new SelectList(aListc, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCentrosCosto"] = lstCentrosCosto;

            SelectList lstEmpleados = null;
            var aListe = _Horus_Ctrl_NovedadesEntities.Empleados.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
            lstEmpleados = new SelectList(aListe, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstEmpleados"] = lstEmpleados;

            ViewBag.Turns_List = 0;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit()
        {
            try
            {
                //_Horus_Ctrl_NovedadesEntities = new HORUS_CTRL_NOVEDADESEntities();
                //if (ModelState.IsValid)
                //{
                //    //Auth_FormsActions Auth_FormsActions_Edit = _Horus_Ctrl_NovedadesEntities.Auth_FormsActions.Where(x => x.ActionID == _Auth_FormsActions.ActionID).FirstOrDefault();
                //    if (Auth_FormsActions_Edit == null)
                //    {
                //        return RedirectToAction("Index");
                //    }
                _Horus_Ctrl_NovedadesEntities = new HORUS_CTRL_NOVEDADESEntities();

                SelectList lstUnidadNegocio = null;
                var aListUN = _Horus_Ctrl_NovedadesEntities.EntidadesProductivas.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
                lstUnidadNegocio = new SelectList(aListUN, "ID", "Nombre".Trim().ToUpper());
                ViewData["lstUnidadNegocio"] = lstUnidadNegocio;

                SelectList lstCentrosOperacion = null;
                //var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1 && x.Nombre.Contains("POST")).OrderBy(x => x.Nombre);
                var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);

                lstCentrosOperacion = new SelectList(aList, "ID", "Nombre".Trim().ToUpper());
                ViewData["lstCentrosOperacion"] = lstCentrosOperacion;

                SelectList lstCentrosCosto = null;
                var aListc = _Horus_Ctrl_NovedadesEntities.CentrosCosto.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
                lstCentrosCosto = new SelectList(aListc, "ID", "Nombre".Trim().ToUpper());
                ViewData["lstCentrosCosto"] = lstCentrosCosto;

                SelectList lstEmpleados = null;
                var aListe = _Horus_Ctrl_NovedadesEntities.Empleados.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
                lstEmpleados = new SelectList(aListe, "ID", "Nombre".Trim().ToUpper());
                ViewData["lstEmpleados"] = lstEmpleados;

                ViewBag.Turns_List = 0;

                return View();

                //    _Horus_Ctrl_NovedadesEntities.SaveChanges();

                //    return RedirectToAction("Index");
                //}

                ////return View(Auth_FormsActions);
                //return View();
            }
            catch (Exception ex)
            {
                ////return View(Auth_FormsActions);
                return View();
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(Turns _Turns, FormCollection form)
        public ActionResult Save(string Nombre, string FechaIni, string FechaFin, string[] CompaniaID, string[] UnidadNegocioID, string[] CentrosOperacionID
                                , string[] CentrosCostoID, string HInicial, string HFin, int Almuerzo, int Refrigerio1, int Refrigerio2
                                ,int Cena, int TurnoOtroDia, string Observaciones, int IDAppUsrs, string[] EmpleadosID)
        {
            try
            {
                _Horus_Ctrl_NovedadesEntities = new HORUS_CTRL_NOVEDADESEntities();

                //string X = "1,23,25";                
                string strCompaniaID = (string.Join(",", CompaniaID) == string.Empty ? null : string.Join(",", CompaniaID));
                string strUnidadNegocioID = (string.Join(",", UnidadNegocioID) == string.Empty ? null : string.Join(",", UnidadNegocioID));
                string strCentrosOperacionID = (string.Join(",", CentrosOperacionID) == string.Empty ? null : string.Join(",", CentrosOperacionID));
                string strCentrosCostoID = (string.Join(",", CentrosCostoID) == string.Empty ? null : string.Join(",", CentrosCostoID));
                string strEmpleadosID = (string.Join(",", EmpleadosID) == string.Empty ? null : string.Join(",", EmpleadosID));
                DateTime FechaInicial = DateTime.Parse(FechaIni);
                DateTime FechaFinal = DateTime.Parse(FechaFin);

                var Turns_List = _Horus_Ctrl_NovedadesEntities.CreaTurnos(Nombre, FechaInicial, FechaFinal, strCompaniaID, strUnidadNegocioID,
                                    strCentrosOperacionID, strCentrosCostoID, HInicial, HFin, Almuerzo, Refrigerio1, Refrigerio2, Cena,
                                    TurnoOtroDia, Observaciones, IDAppUsrs, strEmpleadosID);


                ////var Turns_List = _Horus_Ctrl_NovedadesEntities.InformeTurnosProgramados(FechaInicial, FechaFinal, CompaniaID, CentrosOperacionID, UnidadNegocioID, CentrosCostoID, FlagActivo);
                ////var GeneratedLabels_List = _BASEntities.sp_GetGeneratedLabels1(FechaIni2, FechaFin2, CentroOperacionID).ToList();

                return Json(Turns_List, JsonRequestBehavior.AllowGet);
                

            }
            catch (Exception ex)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult JSONGetCompanies()
        {
            try
            {
                var aListCo = _Horus_Ctrl_NovedadesEntities.Companias.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);

                var jsonResult = Json(aListCo, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }



    }
}