﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class AspersionEmployeesController : BaseController
    {
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        // GET: AspersionEmployees
        public ActionResult Index()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Lista de roles para aspersion
            List<Aspersion_RolEmployees> RolEmployees = siembras_PLANNEREntities.Aspersion_RolEmployees.Where(x => x.ActiveFlag == true).ToList();
            SelectList RolEmployeesList = new SelectList(RolEmployees, "RolID", "Name");
            ViewData["RolEmployeesList"] = RolEmployeesList;

            //Lista de Supervisores
            List<Aspersion_AspersionEmployees> AspersionEmployees = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.ActiveFlag == true && x.RolID == 1).ToList();
            //SelectList AspersionEmployeesList = new SelectList(AspersionEmployees, "AspersionEmployeesID", "Name");
            SelectList AspersionEmployeesList = new SelectList((from s in AspersionEmployees
                                                                select new
                                                 {
                                                     Id = s.AspersionEmployeesID,
                                                     Name = s.Name + " " + s.LastName
                                                 }),
                            "Id",
                            "Name",
                            null);
            ViewData["AspersionEmployeesList"] = AspersionEmployeesList;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            var AllAspersionEmployees = siembras_PLANNEREntities.fn_getAllAspersionEmployees(null, null).ToList();
            ViewBag.AllEmployees = AllAspersionEmployees;

            return View();
        }

        [HttpPost]
        public ActionResult CambiaEstado(int? AspersionEmployeesID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var Employees = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.AspersionEmployeesID == AspersionEmployeesID).FirstOrDefault();
            var Estado = Employees.ActiveFlag;

            Aspersion_AspersionEmployees Update = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.AspersionEmployeesID == AspersionEmployeesID).FirstOrDefault();
            if (Estado == true)
            {
                Update.ActiveFlag = false;
                siembras_PLANNEREntities.SaveChanges();
            }
            else
            {
                Update.ActiveFlag = true;
                siembras_PLANNEREntities.SaveChanges();
            }
            var AllAspersionEmployees = siembras_PLANNEREntities.fn_getAllAspersionEmployees(null, null).ToList();
            var res = new
            {
                Mensaje = "Cambio de estado realizado satisfactoriamente!",
                Error = 0,
                Tabla = AllAspersionEmployees
            };

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int AspersionEmployeesID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var AllAspersionEmployees = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.AspersionEmployeesID == AspersionEmployeesID).FirstOrDefault();
            //var AllAspersionEmployees = siembras_PLANNEREntities.fn_getAllAspersionEmployees(AspersionEmployeesID).FirstOrDefault();
            ViewData["Datos"] = AllAspersionEmployees;
            
            //Lista de roles para aspersion
            List<Aspersion_RolEmployees> RolEmployees = siembras_PLANNEREntities.Aspersion_RolEmployees.Where(x => x.ActiveFlag == true).ToList();
            SelectList RolEmployeesList = new SelectList(RolEmployees, "RolID", "Name", AllAspersionEmployees.RolID);
            ViewData["RolEmployeesList"] = RolEmployeesList;

            //Lista de Supervisores
            List<Aspersion_AspersionEmployees> AspersionEmployees = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.ActiveFlag == true && x.RolID == 1).ToList();
            //SelectList AspersionEmployeesList = new SelectList(AspersionEmployees, "AspersionEmployeesID", "Name");
            SelectList AspersionEmployeesList = new SelectList((from s in AspersionEmployees
                                                                select new
                                                                {
                                                                    Id = s.AspersionEmployeesID,
                                                                    Name = s.Name + " " + s.LastName
                                                                }),
                            "Id",
                            "Name",
                            AllAspersionEmployees.SupervisorID);
            ViewData["AspersionEmployeesList"] = AspersionEmployeesList;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name", AllAspersionEmployees.GreenHouseID);
            ViewData["BussinesUnitList"] = BussinesUnitList;

            return View(AllAspersionEmployees);
        }

        public ActionResult Create(Aspersion_AspersionEmployees datos)
        {
            try
            {
                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                siembras_PLANNEREntities.Aspersion_AspersionEmployees.Add(datos);
                siembras_PLANNEREntities.SaveChanges();

                var AllAspersionEmployees = siembras_PLANNEREntities.fn_getAllAspersionEmployees(null, null).ToList();
                var res = new
                {
                    Mensaje = "Usuario creado satisfactoriamente",
                    Error =  0,
                    Tabla = AllAspersionEmployees
                };
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult EditSave(Aspersion_AspersionEmployees datos)
        {
            try
            {
                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                Aspersion_AspersionEmployees Update = siembras_PLANNEREntities.Aspersion_AspersionEmployees.First(x => x.AspersionEmployeesID == datos.AspersionEmployeesID);
                Update.Name = datos.Name;
                Update.LastName = datos.LastName;
                Update.RolID = datos.RolID;
                Update.SupervisorID = datos.SupervisorID;
                Update.GreenHouseID = datos.GreenHouseID;
                var arr = new
                {
                    Error = 0,
                    Mensaje = "Registro actualizado correctamente!"
                };
                siembras_PLANNEREntities.SaveChanges();
                return Json(arr, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ShowList()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            return View();
        }

        public JsonResult GetList(int GreenHouseID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var AllAspersionEmployees = siembras_PLANNEREntities.fn_getAllAspersionEmployees(null, GreenHouseID).ToList();
            return Json(AllAspersionEmployees);
        }

        public ActionResult TraeParcial()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Lista de roles para aspersion
            List<Aspersion_RolEmployees> RolEmployees = siembras_PLANNEREntities.Aspersion_RolEmployees.Where(x => x.ActiveFlag == true).ToList();
            SelectList RolEmployeesList = new SelectList(RolEmployees, "RolID", "Name");
            ViewData["RolEmployeesList"] = RolEmployeesList;

            //Lista de Supervisores
            List<Aspersion_AspersionEmployees> AspersionEmployees = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.ActiveFlag == true && x.RolID == 1).ToList();
            //SelectList AspersionEmployeesList = new SelectList(AspersionEmployees, "AspersionEmployeesID", "Name");
            SelectList AspersionEmployeesList = new SelectList((from s in AspersionEmployees
                                                                select new
                                                                {
                                                                    Id = s.AspersionEmployeesID,
                                                                    Name = s.Name + " " + s.LastName
                                                                }),
                            "Id",
                            "Name",
                            null);
            ViewData["AspersionEmployeesList"] = AspersionEmployeesList;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            var AllAspersionEmployees = siembras_PLANNEREntities.fn_getAllAspersionEmployees(null, null).ToList();
            ViewBag.AllEmployees = AllAspersionEmployees;

            return PartialView();
            //return Json(html, JsonRequestBehavior.AllowGet);
        }
    }

}