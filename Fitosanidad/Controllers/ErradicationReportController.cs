﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;
using System.Data.SqlClient;
using System.Data;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class ErradicationReportController : BaseController
    {

        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        SIEMBRASEntities siembrasEntities = null;
        // GET: ErradicationReport
        public ActionResult Index()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            return View();
        }

        public ActionResult TraeData(int? GreenHouseID, DateTime FechaInicial, DateTime FechaFinal)
        {
            siembrasEntities = new SIEMBRASEntities();
            List<sp_ErradicationReport_Result> Data = siembrasEntities.sp_ErradicationReport(GreenHouseID, FechaInicial, FechaFinal).ToList();

            var jsonResult = Json(Data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}