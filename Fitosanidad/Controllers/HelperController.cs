﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;
using System.Web.Hosting;

using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

using Resources;
using System.Security.Cryptography;
using System.Text;

using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
//using iTextSharp.tool.xml;
//using System.Drawing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

using System.IO;
using System.Net;

namespace Fitosanidad.Controllers
{
    public class HelperController : Controller
    {
        /// <summary>
        /// Valida que exista la carpeta para guardar los archivos y que le archivo que se va a guardar no exista y retorna 
        /// la ruta y el nombre del archivo 
        /// </summary>
        /// <param name="_filename">nombre del archivo que se va a guardar</param>
        /// <returns></returns>
        public static string VerifyExportEnvironment(string strFileName)
        {
            try
            {
                if (!System.IO.Directory.Exists(HostingEnvironment.MapPath("\\Downloads")))
                {
                    System.IO.Directory.CreateDirectory(HostingEnvironment.MapPath("\\Downloads"));
                }
                var fileLocation = string.Format("{0}/{1}", HostingEnvironment.MapPath("\\Downloads"), strFileName);

                if (System.IO.File.Exists(fileLocation))
                {
                    System.IO.File.Delete(fileLocation);
                }
                return fileLocation;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Valida que exista la carpeta para guardar los archivos y que le archivo que se va a guardar no exista y retorna 
        /// la ruta y el nombre del archivo 
        /// </summary>
        /// <param name="_filename">nombre del archivo que se va a guardar</param>
        /// <returns></returns>
        public static void VerifyWorkEnvironment()
        {
            try
            {
                if (!System.IO.Directory.Exists(HostingEnvironment.MapPath("\\Uploads")))
                {
                    System.IO.Directory.CreateDirectory(HostingEnvironment.MapPath("\\Uploads"));
                }
                if (!System.IO.Directory.Exists(HostingEnvironment.MapPath("\\Downloads")))
                {
                    System.IO.Directory.CreateDirectory(HostingEnvironment.MapPath("\\Downloads"));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static DataTable GetPivotTable(DataTable table, string columnX, params string[] columnsToIgnore)
        {
            DataTable returnTable = new DataTable();

            if (columnX == "")
                columnX = table.Columns[0].ColumnName;

            returnTable.Columns.Add(columnX);

            List<string> columnXValues = new List<string>();

            List<string> listColumnsToIgnore = new List<string>();
            if (columnsToIgnore.Length > 0)
                listColumnsToIgnore.AddRange(columnsToIgnore);

            if (!listColumnsToIgnore.Contains(columnX))
                listColumnsToIgnore.Add(columnX);

            foreach (DataRow dr in table.Rows)
            {
                string columnXTemp = dr[columnX].ToString();
                if (!columnXValues.Contains(columnXTemp))
                {
                    columnXValues.Add(columnXTemp);
                    returnTable.Columns.Add(columnXTemp);
                }
                else
                {
                    throw new Exception("ERROR: La columna " + columnX + " debe tener valores UNICOS.");
                }
            }

            foreach (DataColumn dc in table.Columns)
            {
                if (!columnXValues.Contains(dc.ColumnName) &&
                    !listColumnsToIgnore.Contains(dc.ColumnName))
                {
                    DataRow dr = returnTable.NewRow();
                    dr[0] = dc.ColumnName;
                    returnTable.Rows.Add(dr);
                }
            }

            for (int i = 0; i < returnTable.Rows.Count; i++)
            {
                for (int j = 1; j < returnTable.Columns.Count; j++)
                {
                    returnTable.Rows[i][j] =
                      table.Rows[j - 1][returnTable.Rows[i][0].ToString()].ToString();
                }
            }

            return returnTable;
        }

        public static Document DataTable2Pdf(DataTable tblDataTable, Document pdfDoc, Font fntContent, int intColTot, int intRecByPage, string[,] strColumns)
        {
            PdfPTable pdfTable = new PdfPTable(intColTot);
            PdfPCell cell = null;

            PdfHeader(pdfTable, fntContent, strColumns);

            for (int i = 1; i < tblDataTable.Rows.Count; i++)
            {
                if (i % intRecByPage == 0)
                {
                    pdfDoc.NewPage();
                    for (int j = 0; j < tblDataTable.Columns.Count; j++)
                    {
                        pdfDoc.Add(pdfTable);

                        pdfTable = new PdfPTable(intColTot);

                        for (int k = 0; k < strColumns.GetLength(0); k++)
                        {
                            cell = new PdfPCell();
                            cell = new PdfPCell(new Phrase(tblDataTable.Rows[i][j].ToString(), fntContent));
                            pdfTable.AddCell(cell);
                        }
                    }
                }
                else
                {
                    for (int j = 0; j < tblDataTable.Columns.Count; j++)
                    {
                        for (int k = 0; k < strColumns.GetLength(0); k++)
                        {
                            if (tblDataTable.Columns[j].ToString() == strColumns[k, 0])
                            {
                                cell = new PdfPCell();
                                cell = new PdfPCell(new Phrase(tblDataTable.Rows[i][j].ToString(), fntContent));
                                pdfTable.AddCell(cell);
                            }
                        }
                    }
                }
            }
            pdfDoc.Add(pdfTable);

            return pdfDoc;
        }

        public static PdfPTable PdfHeader(PdfPTable pdfPTable, Font fntContent, string[,] strColumns)
        {
            PdfPCell cell = null;

            for (int i = 0; i < strColumns.GetLength(0); i++)
            {
                cell = new PdfPCell();
                cell.Border = Convert.ToInt32(strColumns[i, 2].Split(',')[1]);
                cell.HorizontalAlignment = Convert.ToInt32(strColumns[i, 2].Split(',')[2]);
                //cell.BackgroundColor = new BaseColor(Convert.ToInt32(strColumns[i, 2].Split(',')[0].Split(';')[0]), Convert.ToInt32(strColumns[i, 2].Split(',')[0].Split(';')[1]), Convert.ToInt32(strColumns[i, 2].Split(',')[0].Split(';')[2]));
                cell.BackgroundColor = new BaseColor(System.Drawing.Color.Red);
                cell = new PdfPCell(new Phrase(strColumns[i, 1].ToString(), fntContent));
                pdfPTable.AddCell(cell);
            }

            return pdfPTable;
        }

        public static DataTable Csv2DataTable(string _Separator, string _FilePath)
        {
            DataTable dtCsv = new DataTable();
            string Fulltext;

            string FileSaveWithPath = _FilePath;
            using (StreamReader sr = new StreamReader(FileSaveWithPath))
            {
                while (!sr.EndOfStream)
                {
                    Fulltext = sr.ReadToEnd().ToString();
                    string[] rows = Fulltext.Split('\n');
                    for (int i = 0; i < rows.Count() - 1; i++)
                    {
                        Regex CSVParser = new Regex(_Separator + "(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

                        string[] rowValues = CSVParser.Split(rows[i]);
                        {
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtCsv.Columns.Add(rowValues[j]);
                                }
                            }
                            else
                            {
                                DataRow dr = dtCsv.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    dr[k] = rowValues[k].ToString();
                                }
                                dtCsv.Rows.Add(dr);
                            }
                        }
                    }
                }
            }
            return dtCsv;
        }

        public static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static string MD5Text(string text)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string Pass = GetMd5Hash(md5Hash, text);
                return Pass;
            }
        }

        public static string GetDayName(DateTime _Date)
        {
            string Results = _Date.DayOfWeek.ToString();
            string DayOfWeek = _Date.DayOfWeek.ToString();
            DayOfWeek = DayOfWeek.ToUpper();

            if (DayOfWeek.Equals("MONDAY"))
            {
                Results = Resources.Resources.Lunes;
            }
            if (DayOfWeek.Equals("TUESDAY"))
            {
                Results = Resources.Resources.Martes;
            }
            if (DayOfWeek.Equals("WEDNESDAY"))
            {
                Results = Resources.Resources.Miercoles;
            }
            if (DayOfWeek.Equals("THURSDAY"))
            {
                Results = Resources.Resources.Jueves;
            }
            if (DayOfWeek.Equals("FRIDAY"))
            {
                Results = Resources.Resources.Viernes;
            }
            if (DayOfWeek.Equals("SATURDAY"))
            {
                Results = Resources.Resources.Sabado;
            }
            if (DayOfWeek.Equals("SUNDAY"))
            {
                Results = Resources.Resources.Domingo;
            }

            return Results;
        }

        public ActionResult GetSessionVar( string strSession)
        {
            return Json(Session[strSession], JsonRequestBehavior.AllowGet);
        }

        private void UploadFile(string path, string NombreArchivo)
        {
            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create("ftp://10.99.0.13/" + NombreArchivo);

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential("ftpdev", "bogota19+");
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = true;
            FileStream stream = System.IO.File.OpenRead(path);
            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);
            stream.Close();
            System.IO.Stream reqStream = request.GetRequestStream();
            reqStream.Write(buffer, 0, buffer.Length);
            reqStream.Flush();
            reqStream.Close();
        }

        public static void DownloadFile(string strFtpServer, string strFtpUser, string strFtpPassword, string strFileSource, string strFileTarget, string strFileName)
        {
            if (string.IsNullOrEmpty(strFileTarget))
            {
                strFileTarget = HostingEnvironment.MapPath("\\Downloads");
            }

            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create("ftp://" + strFtpServer.Trim() + "/" + strFileSource.Trim() + "/" + strFileName.Trim());

            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential(strFtpUser.Trim(), strFtpPassword.Trim());
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = true;
            request.EnableSsl = false;

            if (System.IO.File.Exists(strFileTarget + "/" + strFileName))
            {
                System.IO.File.Delete(strFileTarget + "/" + strFileName);
            }

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (Stream responseStream = response.GetResponseStream())
            {
                using (Stream fileStream = new FileStream(strFileTarget + "/" + strFileName, FileMode.CreateNew))
                {
                    responseStream.CopyTo(fileStream);
                }
            }
        }
    }
}