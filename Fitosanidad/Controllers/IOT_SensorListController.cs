﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using System.Web.UI.WebControls;

using Planner.Model;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Xml.Linq;
using System.Globalization;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class IOT_SensorListController : BaseController
    {
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;

        SelectList businessUnitList = null;
        SelectList greenhouseList = null;

        public class UploadSensor
        {
            public string DataMessageGUID { get; set; }
            public string SensorID { get; set; }
            public string Date { get; set; }
            public string Humidity { get; set; }
            public string Temperature { get; set; }
            public string Luminosity { get; set; }
        }

        public class SensorData
        {
            public string DeviceId { get; set; }
            public string Value { get; set; }
            public string Data { get; set; }
            public string BusinessUnitId { get; set; }
            public string GreenHouseID { get; set; }
            public string Time { get; set; }
            public string Sensor { get; set; }
        }

        public ActionResult Index()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            ViewBag.SensorList = siembras_PLANNEREntities.fn_getIOT_SensorList().OrderBy(x => x.BusinessUnitName).ThenBy(x => x.Codigo);

            return View();
        }

        public ActionResult Create()
        {
            int BusinessUnitId = 0;

            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var businessUnitList_Lst = siembras_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            businessUnitList = new SelectList(businessUnitList_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["businessUnitList"] = businessUnitList;

            BusinessUnitId = businessUnitList_Lst.FirstOrDefault().ID;

            var greenhouseList_Lst = siembras_PLANNEREntities.fn_getGreenHouseSensorInstall(BusinessUnitId).OrderBy(a => a.Codigo);
            greenhouseList = new SelectList((from s in greenhouseList_Lst.ToList()
                                             select new
                                             {
                                                 ID = s.ID,
                                                 Codigo = s.Codigo + " - Cód. " + s.ID
                                             }),
                            "ID",
                            "Codigo",
                            BusinessUnitId);
            ViewData["greenhouseList"] = greenhouseList;
            return View();
        }

        public ActionResult Edit(int IOT_SensorListID)
        {
            int BusinessUnitId = 0;

                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            IOT_SensorList _IOT_SensorList = siembras_PLANNEREntities.IOT_SensorList.Where(x => x.IOT_SensorListID == IOT_SensorListID).FirstOrDefault();
            if (_IOT_SensorList == null)
            {
                return RedirectToAction("Index");
            }

            var businessUnitList_Lst = siembras_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            businessUnitList = new SelectList(businessUnitList_Lst, "ID", "Name".Trim().ToUpper(), _IOT_SensorList.BusinessUnitID);
            ViewData["businessUnitList"] = businessUnitList;

            BusinessUnitId = businessUnitList_Lst.FirstOrDefault().ID;

            var greenhouseList_Lst = siembras_PLANNEREntities.fn_getGreenHouseSensorInstall(_IOT_SensorList.BusinessUnitID).OrderBy(a => a.Codigo);
            greenhouseList = new SelectList((from s in greenhouseList_Lst.ToList()
                                             select new
                                             {
                                                 ID = s.ID,
                                                 Codigo = s.Codigo + " - Cód. " + s.ID
                                             }),
                            "ID",
                            "Codigo",
                            BusinessUnitId);
            ViewData["greenhouseList"] = greenhouseList;
            return View(_IOT_SensorList);
        }

        public ActionResult Delete(int IOT_SensorListID)
        {
            int BusinessUnitId = 0;

            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            IOT_SensorList _IOT_SensorList = siembras_PLANNEREntities.IOT_SensorList.Where(x => x.IOT_SensorListID == IOT_SensorListID).FirstOrDefault();
            if (_IOT_SensorList == null)
            {
                return RedirectToAction("Index");
            }

            var businessUnitList_Lst = siembras_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            businessUnitList = new SelectList(businessUnitList_Lst, "ID", "Name".Trim().ToUpper(), _IOT_SensorList.BusinessUnitID);
            ViewData["businessUnitList"] = businessUnitList;

            BusinessUnitId = businessUnitList_Lst.FirstOrDefault().ID;

            var greenhouseList_Lst = siembras_PLANNEREntities.fn_getGreenHouseSensorInstall(_IOT_SensorList.BusinessUnitID).OrderBy(a => a.Codigo);
            greenhouseList = new SelectList((from s in greenhouseList_Lst.ToList()
                                             select new
                                             {
                                                 ID = s.ID,
                                                 Codigo = s.Codigo + " - Cód. " + s.ID
                                             }),
                            "ID",
                            "Codigo",
                            BusinessUnitId);
            ViewData["greenhouseList"] = greenhouseList;
            return View(_IOT_SensorList);
        }

        public ActionResult Upload(int IOT_SensorListID)
        {
            int BusinessUnitId = 0;

            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            IOT_SensorList _IOT_SensorList = siembras_PLANNEREntities.IOT_SensorList.Where(x => x.IOT_SensorListID == IOT_SensorListID).FirstOrDefault();
            if (_IOT_SensorList == null)
            {
                return RedirectToAction("Index");
            }

            var businessUnitList_Lst = siembras_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            businessUnitList = new SelectList(businessUnitList_Lst, "ID", "Name".Trim().ToUpper(), _IOT_SensorList.BusinessUnitID);
            ViewData["businessUnitList"] = businessUnitList;

            BusinessUnitId = businessUnitList_Lst.FirstOrDefault().ID;

            var greenhouseList_Lst = siembras_PLANNEREntities.fn_getGreenHouseSensorInstall(_IOT_SensorList.BusinessUnitID).OrderBy(a => a.Codigo);
            greenhouseList = new SelectList((from s in greenhouseList_Lst.ToList()
                                             select new
                                             {
                                                 ID = s.ID,
                                                 Codigo = s.Codigo + " - Cód. " + s.ID,
                                             }),
                            "ID",
                            "Codigo",
                            BusinessUnitId);
            ViewData["greenhouseList"] = greenhouseList;
            return View(_IOT_SensorList);
        }

        [HttpPost]
        public ActionResult UploadFromFile()
        {
            string _DataMessageGUID = string.Empty;
            string _SensorID = string.Empty;
            string _Date = string.Empty;
            string _DataRaw = string.Empty;
            string _Humidity = string.Empty;
            string _Temperature = string.Empty;
            string _Luminosity = string.Empty;

            try
            {
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    var pic = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                    HttpPostedFileBase filebase = new HttpPostedFileWrapper(pic);
                    var fileName = Path.GetFileName(filebase.FileName);
                    var path = Path.Combine(Server.MapPath("~/Uploads/"), fileName);
                    filebase.SaveAs(path);

                    DataTable dtUpload = HelperController.Csv2DataTable(",", path);
                    List<UploadSensor> lstUpload = new List<UploadSensor>();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        UploadSensor _UploadSensor = new UploadSensor();

                        _DataMessageGUID = dtUpload.Rows[i][0].ToString().Trim();
                        _SensorID = dtUpload.Rows[i][1].ToString().Trim();
                        _Date = dtUpload.Rows[i][2].ToString().Trim();
                        _DataRaw = dtUpload.Rows[i][3].ToString().Trim();
                        _DataRaw = _DataRaw.Substring(1, _DataRaw.Length - 2);

                        _Humidity = _DataRaw.Split(',')[0].Trim();
                        _Temperature = _DataRaw.Split(',')[1].Trim();
                        _Luminosity = string.Empty;

                        _UploadSensor.DataMessageGUID = _DataMessageGUID.Substring(1, _DataMessageGUID.Length - 2);
                        _UploadSensor.SensorID = _SensorID.Substring(1, _SensorID.Length - 2);
                        _UploadSensor.Date = Convert.ToDateTime(_Date.Replace("\"", ""), new CultureInfo("en-US")).AddHours(-5).ToString();
                        _UploadSensor.Humidity = _Humidity;
                        _UploadSensor.Temperature = _Temperature;
                        _UploadSensor.Luminosity = _Luminosity;
                        lstUpload.Add(_UploadSensor);
                    }

                    return Json(lstUpload, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("No File Saved.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult UploadFromFileSave(string IOT_SensorData)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            try
            {
                var IOT_SensorDataLst = JsonConvert.DeserializeObject<SensorData[]>(IOT_SensorData);

                XElement root = new XElement("arrIOT_SensorData",
                    new XAttribute(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance"),
                    new XAttribute(XNamespace.Xmlns + "xsd", "http://www.w3.org/2001/XMLSchema"));

                foreach (var SensorData in IOT_SensorDataLst)
                {
                    XElement addElement = new XElement("recIOT_SensorData",
                        new XElement("DeviceId", SensorData.DeviceId),
                        new XElement("Value", SensorData.Value),
                        new XElement("Data", SensorData.Data),
                        new XElement("BusinessUnitId", SensorData.BusinessUnitId),
                        new XElement("GreenHouseID", SensorData.GreenHouseID),
                        new XElement("Time", Convert.ToDateTime(SensorData.Time).ToString("yyyy-MM-dd HH:mm:ss.fff")),
                        new XElement("Sensor", SensorData.Sensor));

                    root.Add(addElement);
                }

                siembras_PLANNEREntities.sp_GetSensorWeatherInfoFromXML(root.ToString());

                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult DeleteDateRange(int IOT_SensorListID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            IOT_SensorList _IOT_SensorList = siembras_PLANNEREntities.IOT_SensorList.Where(x => x.IOT_SensorListID == IOT_SensorListID).FirstOrDefault();
            if (_IOT_SensorList == null)
            {
                return RedirectToAction("Index");
            }

            return View(_IOT_SensorList);
        }

        public ActionResult GetDeleteDateRange(string BusinessUnitId, string GreenHouseID, string DeviceId, string StartDate, string EndDate)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            List<fn_getIOT_SensorDataForDeleteDateRange_Result> _IOT_SensorDataList = siembras_PLANNEREntities.fn_getIOT_SensorDataForDeleteDateRange(Convert.ToInt32(BusinessUnitId), Convert.ToInt32(GreenHouseID), Convert.ToInt32(DeviceId), Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1)).ToList();

            return Json(_IOT_SensorDataList, JsonRequestBehavior.AllowGet); ;
        }

        public ActionResult SetDeleteDateRange(string BusinessUnitId, string GreenHouseID, string DeviceId, string StartDate, string EndDate)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            siembras_PLANNEREntities.sp_setIOT_SensorDataForDeleteDateRange(Convert.ToInt32(BusinessUnitId), Convert.ToInt32(GreenHouseID), Convert.ToInt32(DeviceId), Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1));

            return Json("", JsonRequestBehavior.AllowGet); ;
        }

        public ActionResult GetGreenhouseSensorInstall(int BusinessUnitId)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var aList = siembras_PLANNEREntities.fn_getGreenHouseSensorInstall(BusinessUnitId).OrderBy(a => a.Codigo); ;
            greenhouseList = new SelectList(aList, "ID", "Codigo");

            return Json(greenhouseList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSensorList()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            ViewBag.SensorList = siembras_PLANNEREntities.fn_getIOT_SensorList().OrderBy(x => x.BusinessUnitName).OrderBy(x => x.Codigo);
            return PartialView("SensorList", siembras_PLANNEREntities);
        }

        public ActionResult greenhouseListByBusinessUnitID(int businessUnitID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            try
            {

                var aghList = siembras_PLANNEREntities.fn_getGreenHousebyBusinessUnit(businessUnitID).OrderBy(a => a.Codigo);
                greenhouseList = new SelectList((from s in aghList
                                                 select new
                                                 {
                                                     ID = s.ID,
                                                     Codigo = s.Codigo + " - Cód. " + s.ID
                                                 }),
                                "ID",
                                "Codigo",
                                null);
                return Json(greenhouseList, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public void IOT_SensorList_SaveChanges(int IOT_SensorListID, string businessUnitId, string greenHouseId, string name, string service, string flagActive, string externalId, string CRUD)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            IOT_SensorList _IOT_SensorList = new IOT_SensorList();
            try
            {
                if (IOT_SensorListID != 0 && (CRUD == "U" || CRUD == "D"))
                {
                    _IOT_SensorList = siembras_PLANNEREntities.IOT_SensorList.Where(x => x.IOT_SensorListID == IOT_SensorListID).First();
                }

                _IOT_SensorList.IOT_SensorListID = IOT_SensorListID;
                _IOT_SensorList.Name = name;
                _IOT_SensorList.BusinessUnitID = Convert.ToInt32(businessUnitId);
                _IOT_SensorList.GreenHouseID = Convert.ToInt32(greenHouseId);
                _IOT_SensorList.Service = service;
                _IOT_SensorList.FlagActive = (flagActive.ToUpper().Equals("TRUE") ? true : false);
                _IOT_SensorList.ExternalID = Convert.ToInt32(externalId);

                if (IOT_SensorListID == 0 && CRUD == "C")
                {
                    siembras_PLANNEREntities.IOT_SensorList.Add(_IOT_SensorList);
                }
                if (IOT_SensorListID != 0 && CRUD == "D")
                {
                    siembras_PLANNEREntities.IOT_SensorList.Remove(_IOT_SensorList);
                }
                siembras_PLANNEREntities.SaveChanges();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public ActionResult GetIOT_SensorListByIOT_SensorListID(int _IOT_SensorListID)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            try
            {
                var query = siembras_PLANNEREntities.IOT_SensorList
                   .Where(s => s.IOT_SensorListID == _IOT_SensorListID)
                   .FirstOrDefault<IOT_SensorList>();
                return Json(query, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}