﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planner.Model;
using Resources;

using System.Globalization;

using System.Data;
using System.Data.SqlClient;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class Auth_FormsActionsController : BaseController
    {
        BASEntities _BASEntities = null;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexGrid()
        {
            try
            {
                return Json(GetAllAuth_FormsActions(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Auth_FormsActions _Auth_FormsActions, FormCollection form)
        {
            try
            {
                _BASEntities = new BASEntities();
                if (ModelState.IsValid)
                {
                    Auth_FormsActions Auth_FormsActions_Add = new Auth_FormsActions();
                    Auth_FormsActions_Add.Name = _Auth_FormsActions.Name;
                    Auth_FormsActions_Add.Icon = _Auth_FormsActions.Icon;
                    Auth_FormsActions_Add.FlagActive = _Auth_FormsActions.FlagActive;

                    _BASEntities.Auth_FormsActions.Add(Auth_FormsActions_Add);
                    _BASEntities.SaveChanges();

                    return RedirectToAction("Index");
                }

                return View(_Auth_FormsActions);
            }
            catch (Exception ex)
            {
                return View(_Auth_FormsActions);
            }
        }

        public ActionResult Edit(int ActionID)
        {
            _BASEntities = new BASEntities();
            Auth_FormsActions Auth_FormsActions = _BASEntities.Auth_FormsActions.Where(x => x.ActionID == ActionID).FirstOrDefault();
            if (Auth_FormsActions == null)
            {
                return RedirectToAction("Index");
            }
            return View(Auth_FormsActions);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Auth_FormsActions _Auth_FormsActions, FormCollection form)
        {
            try
            {
                _BASEntities = new BASEntities();
                if (ModelState.IsValid)
                {
                    Auth_FormsActions Auth_FormsActions_Edit = _BASEntities.Auth_FormsActions.Where(x => x.ActionID == _Auth_FormsActions.ActionID).FirstOrDefault();
                    if (Auth_FormsActions_Edit == null)
                    {
                        return RedirectToAction("Index");
                    }
                    Auth_FormsActions_Edit.Name = _Auth_FormsActions.Name;
                    Auth_FormsActions_Edit.Icon = _Auth_FormsActions.Icon;
                    Auth_FormsActions_Edit.FlagActive = _Auth_FormsActions.FlagActive;

                    _BASEntities.SaveChanges();

                    return RedirectToAction("Index");
                }

                return View(_Auth_FormsActions);
            }
            catch (Exception ex)
            {
                return View(_Auth_FormsActions);
            }
        }

        public List<Auth_FormsActions> GetAllAuth_FormsActions()
        {
            _BASEntities = new BASEntities();
            List<Auth_FormsActions> Results = _BASEntities.Auth_FormsActions.ToList();

            return Results;
        }

    }
}