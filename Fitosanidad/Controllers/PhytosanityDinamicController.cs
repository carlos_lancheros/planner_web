﻿using Planner.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fitosanidad.Controllers
{
    public class PhytosanityDinamicController : Controller
    {
        SIEMBRAS_PLANNEREntities _SIEMBRAS_PLANNEREntities = null;
        SIEMBRASEntities _SIEMBRASEntities = null;
        // GET: PhytosanityDinamic
        public ActionResult Index()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            string currentWeek = BaseController.GetWeekWithYear(DateTime.Now);

            //Week
            SelectList lstWeek = null;
            var aListWeek = _SIEMBRAS_PLANNEREntities.Semanas.Where(n => n.Anio >= 2018).Select(x => new { ID = x.ID, Name = x.Anio + " - " + x.Nombre }).ToList();
            var intSelected = aListWeek.Find(x => x.Name == currentWeek);
            lstWeek = new SelectList(aListWeek, "ID", "Name".Trim().ToUpper(), intSelected.ID);
            ViewBag.lstWeek = lstWeek;


            return View();
        }

        public JsonResult GetDynamic(string prmSemanaINI, string prmSemanaFIN)
        {
            _SIEMBRASEntities = new SIEMBRASEntities();
            int InitialWeek = Convert.ToInt32(prmSemanaINI.Substring(4));
            int FinalWeek = Convert.ToInt32(prmSemanaFIN.Substring(4));

            var DinamicLst = _SIEMBRASEntities.VisorDinamicoRegistroFitosanitario(InitialWeek,0, FinalWeek,0);

            var jsonResult = Json(DinamicLst, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}