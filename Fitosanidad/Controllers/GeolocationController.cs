﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Resources;
using Planner.Model;
using Newtonsoft.Json;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class GeolocationController : BaseController
    {
        SIEMBRAS_PLANNEREntities _SIEMBRAS_PLANNEREntities = null;

        public ActionResult Index()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            SelectList lstOptions = getOptionsByParentId(0);
            ViewData["lstOptions"] = lstOptions;

            SelectList lstDetails = getOptionsByParentId(Convert.ToInt32(lstOptions.FirstOrDefault().Value));
            ViewData["lstDetails"] = lstDetails;

            SelectList lstBusinessUnit = null;
            var aList = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0 ).OrderBy(x => x.Name).ToList();
            lstBusinessUnit = new SelectList(aList, "ID", "Name".Trim().ToUpper());
            ViewData["lstBusinessUnit"] = lstBusinessUnit;

            GetGreenhousebyBusinessUnit(0);

            return View();
        }

        public ActionResult getMapCoordinates(string intBusinessId, string strQueryFilter)
        {
            GetGreenhousebyBusinessUnit(Convert.ToInt32(intBusinessId));

            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var arrCoordinates = _SIEMBRAS_PLANNEREntities.fn_getBusinnesUnitAndGreenhouseCoordinates(Convert.ToInt32(intBusinessId))
                .OrderBy(a => a.GreenhouseId)
                .ThenBy(a => a.CoordinateOrder).ToList();

            var arrFarmCoordinate = new Planner.Model.fn_getBusinnesUnitAndGreenhouseCoordinates_Result();

            if (arrCoordinates.Count > 0)
            {
                arrFarmCoordinate = arrCoordinates.First();
            }

            if (strQueryFilter.Equals("F"))
            {
                arrCoordinates = arrCoordinates.Where(x => x.GreenhouseId == 0).ToList();
            }
            if (strQueryFilter.Equals("I"))
            {
                arrCoordinates = arrCoordinates.Where(x => x.GreenhouseId != 0).ToList();
                if (arrCoordinates.Count > 0)
                {
                    arrCoordinates.Add(arrFarmCoordinate);
                }
            }
            if (strQueryFilter.Equals("T"))
            {
                arrCoordinates = arrCoordinates.ToList();
            }

            var arrResults = arrCoordinates.OrderBy(a => a.GreenhouseId)
                .ThenBy(a => a.CoordinateOrder).ToList();

            return Json(arrResults, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGreenhousebyBusinessUnit(int intBusinessId)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var greenhouseList = _SIEMBRAS_PLANNEREntities.fn_getGreenHousebyBusinessUnit(Convert.ToInt32(intBusinessId))
                .OrderBy(a => a.Orden)
                .ToList();
            ViewBag.greenhouseList = greenhouseList;

            return PartialView("greenhouseList");
        }

        public ActionResult getMapCoordinatesSensorsByBusinessUnitAndGreenHouseID(int intBusinessId)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var SensorsList = _SIEMBRAS_PLANNEREntities.fn_getIOT_SensorListCoordinatesbyBusinessUnitIDAndGreenHouseID(intBusinessId, 0)
                .ToList();

            var arrResults = SensorsList;

            return Json(arrResults, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getMapCoordinatesPlatesbyBusinessUnit(int intBusinessId)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var CoordinatesPleatesList = _SIEMBRAS_PLANNEREntities.fn_getPhytosanity_CoordinatesPlatesbyBusinessUnit(Convert.ToInt32(intBusinessId))
                .ToList();

            var arrResults = CoordinatesPleatesList;

            return Json(arrResults, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Return JSON list of photometer data and coordinates
        /// </summary>
        /// <param name="intBusinessId">Id of Business Unit</param>
        /// <returns>Json Results</returns>
        public ActionResult getMapCoordinatesPhotometerbyBusinessUnit(int intBusinessId)
        {
            int InitialWeek = BaseController.GetWeek(DateTime.Now);
            int FinalWeek = BaseController.GetWeek(DateTime.Now);

            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var CoordinatesPhotometerList = _SIEMBRAS_PLANNEREntities.sp_GetPhotometer(InitialWeek, FinalWeek, Convert.ToInt32(intBusinessId))
                .ToList();

            var arrResults = CoordinatesPhotometerList;

            return Json(arrResults, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getGreenhouseArea(string intBusinessUnitId, string intGreenintGreenhouseId)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            int tt = Convert.ToInt32(intGreenintGreenhouseId);

            var arrCoordinates = _SIEMBRAS_PLANNEREntities.fn_getGreenHousebyBusinessUnit(Convert.ToInt32(intBusinessUnitId))
                .Where(x => x.ID == tt)
                .OrderBy(a => a.Orden)
                .ToList();

            return Json(arrCoordinates, JsonRequestBehavior.AllowGet);
        }

        public SelectList getOptionsByParentId(int ParentId)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var Map_OptionsLst = _SIEMBRAS_PLANNEREntities.Map_Options.Where(x => x.ParentId == ParentId).OrderBy(x => x.Name).ToList();

            SelectList lstMap_Options = new SelectList(Map_OptionsLst, "Id", "Name".Trim().ToUpper());

            return lstMap_Options;
        }

        public ActionResult getOptionsByParentIdJson(int ParentId)
        {
            SelectList lstMap_Options = getOptionsByParentId(ParentId);

            return Json(lstMap_Options, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CoordinateViewer()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var CoordinateViewerList = _SIEMBRAS_PLANNEREntities.fn_getMap_CoordinateViewerbySession(Session["SessionID"].ToString()).ToList();
            ViewData["CoordinateViewerList"] = CoordinateViewerList;

            return View();
        }

        [HttpPost]
        public ActionResult UploadCoordinateViewer()
        {
            try
            {
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                    _SIEMBRAS_PLANNEREntities.sp_DelMap_CoordinateViewerbySession(Session["SessionID"].ToString());

                    var pic = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                    HttpPostedFileBase filebase = new HttpPostedFileWrapper(pic);
                    var fileName = Path.GetFileName(filebase.FileName);
                    var path = Path.Combine(Server.MapPath("~/Uploads/"), fileName);
                    filebase.SaveAs(path);

                    DataTable dtMap_CoordinateViewer = HelperController.Csv2DataTable(";", path);
                    List<Map_CoordinateViewer> lstMap_CoordinateViewer = new List<Map_CoordinateViewer>();
                    for (int i = 0; i < dtMap_CoordinateViewer.Rows.Count; i++)
                    {
                        Map_CoordinateViewer _Map_CoordinateViewer = new Map_CoordinateViewer();

                        decimal decLatitude = decimal.Parse(dtMap_CoordinateViewer.Rows[i][1].ToString().Trim(), CultureInfo.InvariantCulture);
                        decimal decLongitude = decimal.Parse(dtMap_CoordinateViewer.Rows[i][2].ToString().Trim(), CultureInfo.InvariantCulture);

                        _Map_CoordinateViewer.CoordinateSession = Session["SessionID"].ToString();
                        _Map_CoordinateViewer.CoordinateId = Convert.ToInt32(dtMap_CoordinateViewer.Rows[i][0].ToString().Trim());
                        _Map_CoordinateViewer.CoordinateLatitude = decLatitude;
                        _Map_CoordinateViewer.CoordinateLongitude = decLongitude;
                        _Map_CoordinateViewer.CoordinateDate = Convert.ToDateTime(dtMap_CoordinateViewer.Rows[i][3].ToString().Trim());

                        lstMap_CoordinateViewer.Add(_Map_CoordinateViewer);
                    }

                    return Json(lstMap_CoordinateViewer, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("No File Saved.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}