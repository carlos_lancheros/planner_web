﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Planner.Model;

namespace Fitosanidad.Controllers
{
    public class EmployeesController : Controller
    {

        HORUS_CTRL_NOVEDADESEntities _Horus_Ctrl_NovedadesEntities = null;

        // GET: Employees
        public ActionResult Index()
        {
            _Horus_Ctrl_NovedadesEntities = new HORUS_CTRL_NOVEDADESEntities();


            SelectList lstCompania = null;
            //var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1 && x.Nombre.Contains("POST")).OrderBy(x => x.Nombre);
            var aListCo = _Horus_Ctrl_NovedadesEntities.Companias.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);

            lstCompania = new SelectList(aListCo, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCompania"] = lstCompania;

            SelectList lstUnidadNegocio = null;
            var aListUN = _Horus_Ctrl_NovedadesEntities.EntidadesProductivas.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
            lstUnidadNegocio = new SelectList(aListUN, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstUnidadNegocio"] = lstUnidadNegocio;

            SelectList lstCentrosOperacion = null;
            //var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1 && x.Nombre.Contains("POST")).OrderBy(x => x.Nombre);
            var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);

            lstCentrosOperacion = new SelectList(aList, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCentrosOperacion"] = lstCentrosOperacion;

            SelectList lstCentrosCosto = null;
            var aListc = _Horus_Ctrl_NovedadesEntities.CentrosCosto.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
            lstCentrosCosto = new SelectList(aListc, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCentrosCosto"] = lstCentrosCosto;


            //Employees(Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value), Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value),Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value), Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value), 1);
            Employees(0, 0, 0, 0, 1);

            return View();

        }
        public ActionResult Employees(int CompaniaID, int UnidadNegocioID, int CentrosOperacionID, int CentrosCostoID, int FlagActivo)
        {
            _Horus_Ctrl_NovedadesEntities = new HORUS_CTRL_NOVEDADESEntities();


            var Employees_List = _Horus_Ctrl_NovedadesEntities.sp_GetEmployees(CompaniaID, UnidadNegocioID, CentrosOperacionID, CentrosCostoID, FlagActivo).ToList();
            //var GeneratedLabels_List = _BASEntities.sp_GetGeneratedLabels1(FechaIni2, FechaFin2, CentroOperacionID).ToList();
            ViewBag.Employees_List = Employees_List;

            return PartialView("Employees_List");
        }
    }
}