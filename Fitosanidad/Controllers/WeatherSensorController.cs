﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planner.Model;
using Resources;
using Newtonsoft.Json;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class WeatherSensorController : BaseController
    {
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;

        SelectList lstBusinessUnit = null;
        SelectList lstGreenhouse = null;
        SelectList lstSensor = null;

        int intBusinessUnitId;
        int intGreenhouseId;
        int intSensorId;

        public ActionResult Sensor()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            getBusinessUnitList();

            intBusinessUnitId = Int32.Parse(lstBusinessUnit.FirstOrDefault().Value);

            getGreenhouseList(intBusinessUnitId);

            intGreenhouseId = Int32.Parse(lstGreenhouse.FirstOrDefault().Value);

            getSensorList(intBusinessUnitId, intGreenhouseId);

            intSensorId = Int32.Parse(lstSensor.FirstOrDefault().Value);

            getSensorData(intBusinessUnitId, intGreenhouseId, intSensorId, DateTime.Now, DateTime.Now);

            return View();
        }

        public ActionResult getBusinessUnitList()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var listBusinessUnit = siembras_PLANNEREntities.fn_getBusinessUnitsSensor().Where(n => n.BusinessUnitID != 0);
            lstBusinessUnit = new SelectList(listBusinessUnit, "BusinessUnitID", "Name".Trim().ToUpper());
            ViewData["lstBusinessUnit"] = lstBusinessUnit;

            return Json(lstBusinessUnit, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getGreenhouseList(int intBusinessUnitId)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var listGreenhouse = siembras_PLANNEREntities.fn_getGreenHouseSensorInstall(intBusinessUnitId).OrderBy(a => a.Codigo);
            lstGreenhouse = new SelectList(listGreenhouse, "ID", "Codigo".Trim().ToUpper(), intBusinessUnitId);
            ViewData["lstGreenhouse"] = lstGreenhouse;

            return Json(lstGreenhouse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getSensorList(int intBusinessUnitId, int intGreenhouseId)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var listSensor = siembras_PLANNEREntities.fn_getIOT_SensorListbyBusinessUnitIDAndGreenHouseID(intBusinessUnitId, intGreenhouseId).OrderBy(a => a.ExternalID);
            lstSensor = new SelectList(listSensor, "ExternalID", "Name");
            ViewData["lstSensor"] = lstSensor;

            return Json(lstSensor, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getSensorData(int intBusinessUnitId, int intGreenhouseId, int intSensorId, DateTime startDate, DateTime endDate)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var Result = siembras_PLANNEREntities.fn_getSensorTempAndRhumByParams(intBusinessUnitId, intGreenhouseId, intSensorId, startDate, endDate).OrderBy(a => a.Time);

            ViewBag.SensorData = Result.ToList();

            return Json(Result.ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}