﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planner.Model;
using Resources;

using System.Globalization;

using System.Data;
using System.Data.SqlClient;

namespace Fitosanidad.Controllers
{
    public class lstAuth_Feature
    {
        public int id { get; set; }
        public string text { get; set; }
        public List<lstAuth_Feature> children { get; set; }
    }

    [Authorize]
   public class Auth_FeatureController : BaseController
    {
        BASEntities _BASEntities = null;

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAllAuth_Feature()
        {
            _BASEntities = new BASEntities();
            var TodosPermisos = _BASEntities.fn_GetAllAuth_FeatureByParentFeatureID(0).OrderBy(x => x.Name);

            List<lstAuth_Feature> results = new List<lstAuth_Feature>();
            foreach (var lista in TodosPermisos)
            {
                lstAuth_Feature _lstAuth_Feature = new lstAuth_Feature();
                _lstAuth_Feature.id = lista.FeatureID;
                _lstAuth_Feature.text = lista.Name;
                _lstAuth_Feature.children = GetChildren(Convert.ToInt32(lista.FeatureID));

                results.Add(_lstAuth_Feature);
            }
            return this.Json(results, JsonRequestBehavior.AllowGet);
        }

        private List<lstAuth_Feature> GetChildren(int ParentFeatureID)
        {
            _BASEntities = new BASEntities();
            var listas = _BASEntities.fn_GetAllAuth_FeatureByParentFeatureID(ParentFeatureID).OrderBy(x => x.Name);

            List<lstAuth_Feature> results = new List<lstAuth_Feature>();
            foreach (var lista in listas)
            {
                lstAuth_Feature _lstAuth_Feature = new lstAuth_Feature();
                _lstAuth_Feature.id = lista.FeatureID;
                _lstAuth_Feature.text = lista.Name;
                _lstAuth_Feature.children = GetChildren(Convert.ToInt32(lista.FeatureID));

                results.Add(_lstAuth_Feature);
            }

            return results;
        }
        
        [HttpPost]
        public ActionResult GetParentDataByFeatureID(int _FeatureID)
        {
            _BASEntities = new BASEntities();

            Auth_Feature _Auth_Feature = _BASEntities.Auth_Feature.Where(x => x.FeatureID == _FeatureID).FirstOrDefault();
            if (_Auth_Feature != null)
            {
                _Auth_Feature = _BASEntities.Auth_Feature.Where(x => x.FeatureID == _Auth_Feature.ParentFeatureID).FirstOrDefault();
            }

            if (_Auth_Feature == null)
            {
                _Auth_Feature = new Auth_Feature();
                _Auth_Feature.FeatureID = 0;
                _Auth_Feature.ParentFeatureID = 0;
                _Auth_Feature.Name = string.Empty;
                _Auth_Feature.Controller = string.Empty;
                _Auth_Feature.Action = string.Empty;
                _Auth_Feature.Icon = string.Empty;
                _Auth_Feature.FlagActive = false;
            }

            var jsonResult = Json(_Auth_Feature, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        [HttpPost]
        public ActionResult GetFeatureByFeatureID(int _FeatureID)
        {
            _BASEntities = new BASEntities();

            Auth_Feature _Auth_Feature = _BASEntities.Auth_Feature.Where(x => x.FeatureID == _FeatureID).FirstOrDefault();

            var jsonResult = Json(_Auth_Feature, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public void Auth_Feature_SaveChanges(string FeatureID, string ParentFeatureID, string Name, string Controller, string Action, string Icon, bool FlagActive)
        {
            int prmFeatureID = (FeatureID != string.Empty ? Convert.ToInt32(FeatureID) : 0);

            _BASEntities = new BASEntities();
            Auth_Feature _Auth_Feature = new Auth_Feature();
            try
            {
                _Auth_Feature.FeatureID = prmFeatureID;
                _Auth_Feature.ParentFeatureID = (ParentFeatureID != string.Empty ? Convert.ToInt32(ParentFeatureID) : 0);
                _Auth_Feature.Name = Name;
                _Auth_Feature.Controller = Controller;
                _Auth_Feature.Action = Action;
                _Auth_Feature.Icon = Icon;
                _Auth_Feature.FlagActive = FlagActive;

                if (FeatureID == string.Empty)
                {
                    _BASEntities.Auth_Feature.Add(_Auth_Feature);
                }
                else
                {
                    _Auth_Feature = _BASEntities.Auth_Feature.Where(x => x.FeatureID == prmFeatureID).FirstOrDefault();
                    _Auth_Feature.ParentFeatureID = (ParentFeatureID != string.Empty ? Convert.ToInt32(ParentFeatureID) : 0);
                    _Auth_Feature.Name = Name;
                    _Auth_Feature.Controller = Controller;
                    _Auth_Feature.Action = Action;
                    _Auth_Feature.Icon = Icon;
                    _Auth_Feature.FlagActive = FlagActive;
                }

                _BASEntities.SaveChanges();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult jsonGetAllAuth_FeatureSubItemsByFeatureID(int _FeatureID)
        {
            _BASEntities = new BASEntities();

            List<Auth_Feature> _Auth_Feature_Lst = GetAllAuth_FeatureSubItemsByFeatureID(_FeatureID);

            var jsonResult = Json(_Auth_Feature_Lst, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        [HttpPost]
        public ActionResult jsonGetAuth_UserWithAuth_FeatureByFeatureID(int _FeatureID)
        {
            _BASEntities = new BASEntities();

            List<Auth_UserFeature> _Auth_UserFeature_Lst = GetAuth_UserWithAuth_FeatureByFeatureID(_FeatureID);

            var jsonResult = Json(_Auth_UserFeature_Lst, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        [HttpPost]
        public ActionResult DelAuth_FeatureByFeatureID(int _FeatureID)
        {
            string strResults = string.Empty;

            List<Auth_Feature> _Auth_Feature_Lst = GetAllAuth_FeatureSubItemsByFeatureID(_FeatureID);
            if(_Auth_Feature_Lst.Count > 0)
            {
                return Json(strResults, JsonRequestBehavior.AllowGet);
            }

            List<Auth_UserFeature> _Auth_UserFeature_Lst = GetAuth_UserWithAuth_FeatureByFeatureID(_FeatureID);
            if (_Auth_UserFeature_Lst.Count > 0)
            {
                return Json(strResults, JsonRequestBehavior.AllowGet);
            }

            _BASEntities = new BASEntities();

            Auth_Feature _Auth_Feature = _BASEntities.Auth_Feature.Where(x => x.FeatureID == _FeatureID).FirstOrDefault();

            if (_Auth_Feature != null)
            {
                _BASEntities.Auth_Feature.Remove(_Auth_Feature);
                _BASEntities.SaveChanges();
                strResults = "OK";
            }

            var jsonResult = Json(strResults, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public List<Auth_Feature> GetAllAuth_FeatureSubItemsByFeatureID(int _FeatureID)
        {
            _BASEntities = new BASEntities();

            List<Auth_Feature> _Auth_Feature_Lst = _BASEntities.Auth_Feature.Where(x => x.ParentFeatureID == _FeatureID).ToList();

            return _Auth_Feature_Lst;
        }

        public List<Auth_UserFeature> GetAuth_UserWithAuth_FeatureByFeatureID(int _FeatureID)
        {
            _BASEntities = new BASEntities();

            List<Auth_UserFeature> _Auth_UserFeature_Lst = _BASEntities.Auth_UserFeature.Where(x => x.FeatureID == _FeatureID).ToList();

            return _Auth_UserFeature_Lst;
        }





    }
}