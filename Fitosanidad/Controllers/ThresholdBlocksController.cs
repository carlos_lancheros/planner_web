﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;

namespace Fitosanidad.Controllers
{
    public class ThresholdBlocksController : BaseController
    {
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        SIEMBRASEntities siembrasEntities = null;
        // GET: ThresholdBlocks
        [Authorize]
        public ActionResult Index()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var ActualYear = DateTime.Today.Year;
            string currentWeek = BaseController.GetWeek(DateTime.Now).ToString("00");
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            SelectList lstWeek = null;
            DateTime strStartDate = DateTime.Now.AddYears(-1);
            DateTime strEndDate = DateTime.Now.AddMonths(1);

            var aListWeek = siembras_PLANNEREntities.sp_GetWeekList(strStartDate, strEndDate).ToList();

            lstWeek = new SelectList(aListWeek, "YEARWEEK", "YEARWEEK".Trim().ToUpper(), DateTime.Now.Year.ToString("0000") + currentWeek.ToString());
            ViewData["listWeek"] = lstWeek;

            ////Datos para dropDown de semanas
            //var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio >= ActualYear - 1);
            //SelectList listWeek = new SelectList((from s in Week
            //                                      select new
            //                                      {
            //                                          ID = s.ID,
            //                                          Nombre = s.Anio + "-" + s.Nombre
            //                                      }),
            //                "Id",
            //                "Nombre",
            //                null);
            //ViewData["listWeek"] = listWeek;

            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().OrderBy(x => x.Name).ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            var product_Lst = siembras_PLANNEREntities.fn_getAllActiveProducts().OrderBy(x => x.Name).ToList();
            SelectList productList = new SelectList(product_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["productList"] = productList;

            var listBT = siembras_PLANNEREntities.BAS_Pests.OrderBy(a => a.ID).OrderBy(x => x.Name);
            SelectList listaBT = new SelectList((from s in listBT
                                                 select new
                                                 {
                                                     Id = s.ID,
                                                     Name = s.Name + " - " + s.Abrr
                                                 }),
                            "Id",
                            "Name",
                            null);
            ViewData["listaBT"] = listaBT;

            return View();
        }

        [HttpPost]
        public ActionResult ProductByBlock(int BlockId)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var ProductByBlock = siembras_PLANNEREntities.fn_getProductByBlock(BlockId).ToList();
            SelectList ProductByBlockList = new SelectList(ProductByBlock, "PlantProductID", "PlantProductName");
            return Json(ProductByBlockList);
        }

        public JsonResult DrawReport(int WeekID, int WeekIDF, int GreeHouseID, int ProductID, int PestID)
        {
            siembrasEntities = new SIEMBRASEntities();
            var DataTotal = siembrasEntities.CargaInformacionGraficaRegistroFitosanitarioGeneralPlagaCompleto(WeekID, GreeHouseID, PestID, ProductID, WeekIDF).ToList();

            var Bloques = siembrasEntities.CargaInformacionGraficaRegistroFitosanitarioGeneralPlagaCompleto(WeekID, GreeHouseID, PestID, ProductID, WeekIDF).Distinct().ToList();

            var Semanas = siembrasEntities.CargaInformacionGraficaRegistroFitosanitarioGeneralPlagaCompleto(WeekID, GreeHouseID, PestID, ProductID, WeekIDF).Select(x => x.Semana).Distinct().ToList();

            var Data = new
            {
                DataTotal = DataTotal,
                Bloques = Bloques,
                Semanas = Semanas
            };

            return Json(Data, JsonRequestBehavior.AllowGet);
        }
    }
}