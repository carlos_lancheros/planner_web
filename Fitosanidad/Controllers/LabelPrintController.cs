﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using Microsoft.Win32.SafeHandles;
using Newtonsoft.Json;
using Planner.Model;
using System.Globalization;


namespace Fitosanidad.Controllers
{
    [Authorize]
    public class LabelPrintController : BaseController
    {
        HORUS_CTRL_NOVEDADESEntities _Horus_Ctrl_NovedadesEntities = null;
        public ActionResult Index()
        {
            _Horus_Ctrl_NovedadesEntities = new HORUS_CTRL_NOVEDADESEntities();
            string currentWeek = GetWeek(DateTime.Now);

            SelectList lstCentrosOperacion = null;
            //var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1 && x.Nombre.Contains("POST")).OrderBy(x => x.Nombre);
            var aList = _Horus_Ctrl_NovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);         


            lstCentrosOperacion = new SelectList(aList, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCentrosOperacion"] = lstCentrosOperacion;

            SelectList lstCentrosCosto = null;
            var aListc = _Horus_Ctrl_NovedadesEntities.CentrosCosto.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
            lstCentrosCosto = new SelectList(aListc, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCentrosCosto"] = lstCentrosCosto;

            //Week
            SelectList lstWeek = null;
            var aListWeek = _Horus_Ctrl_NovedadesEntities.Semanas.Where(n => n.Anio >= 2019).Select(x => new { x.ID, Name = x.Anio + " - " + x.Nombre }).ToList();
            // var intSelected = aListWeek.Find(x => x.Name == currentWeek);
            lstWeek = new SelectList(aListWeek, "ID", "Name".Trim().ToUpper());
            ViewData["lstWeek"] = lstWeek;


            var lstPrinter = new List<SelectListItem>
            {
                new SelectListItem()
                {
                    Text = "DATAMAX",
                    Value = "1"
                },
                new SelectListItem()
                {
                    Text = "DATAMAX BQT",
                    Value = "2"
                },
                new SelectListItem()
                {
                    Text = "ZEBRA",
                    Value = "3"
                },
                new SelectListItem()
                {
                    Text = "ZEBRA POST ALMER",
                    Value = "4"
                },
                new SelectListItem()
                {
                    Text = "ZEBRA BQT",
                    Value = "5"
                }
            };

            ViewData["lstPrinter"] = lstPrinter;

            showCollaborator(lstCentrosOperacion.First().Value, lstCentrosCosto.First().Value);

            return View();
        }

        public ActionResult showCollaborator(string CentrosOperacionID, string CentrosCostoID )
        {
            var a = Convert.ToInt32(CentrosOperacionID);
            var b = Convert.ToInt32(CentrosCostoID);
            _Horus_Ctrl_NovedadesEntities = new HORUS_CTRL_NOVEDADESEntities();


            if(b == 0)
            {
                var LabelPrint_List = _Horus_Ctrl_NovedadesEntities.Empleados.Where(x => x.FlagActivo == 1 && x.IDCentrosOperacion == a).OrderBy(x => x.Nombre).ToList();
                ViewBag.LabelPrint_List = LabelPrint_List;
            }
            else
            {
                var LabelPrint_List = _Horus_Ctrl_NovedadesEntities.Empleados.Where(x => x.FlagActivo == 1 && x.IDCentrosOperacion == a && x.IDCentrosCosto == b).OrderBy(x => x.Nombre).ToList();
                ViewBag.LabelPrint_List = LabelPrint_List;
            }                                  

            return PartialView("LabelPrint_List");
        }


        [DllImport("kernel32.dll", SetLastError = true)]
        static extern SafeFileHandle CreateFile(string lpFileName, FileAccess dwDesiredAccess,
        uint dwShareMode, IntPtr lpSecurityAttributes, FileMode dwCreationDisposition,
        uint dwFlagsAndAttributes, IntPtr hTemplateFile);

        public ActionResult GenerateCode(string Quantity, string Week, string[] ArrayCodes, string[] ArrNames, string[] ArrSurNames, string LabelUnit, int Printer, int NameEnable)

        {
            string command = "";
            int LinePrint = 0;
            string ColaboratorName = "";
            string ColaboratorSurname = "";
            if (Printer == 1) //datamax
            {
                for (int cant = 0; cant < ArrayCodes.Count(); cant++)
                {
                    int cont = 1;
                    int numero = Convert.ToInt32(ArrayCodes[cant]);

                    var Code = numero.ToString("D6");
                    ColaboratorName = ArrSurNames[cant].Substring(0, 7) + "." + ArrNames[cant].Substring(0, 3);

                    if (NameEnable == 1)
                    {
                        command += "L D11 H15\n";
                        command += "131300000100010" + ArrSurNames[cant] + ArrNames[cant] + "\n";
                        command += "E L\n";
                    }
                    for (int total = 0; total < Convert.ToInt32(Quantity); total++)
                    {

                        if (cont == 1)
                        {
                            command += "L D11 H15\n";
                            command += "121200000350010" + Week + "\n";
                            command += "131100000150010" + LabelUnit + "\n";
                            command += "121100000000010" + ColaboratorName + "\n";
                            command += "121080000430087" + Code + "\n";
                            command += "1W1D44000000000852HM," + Code + "\n";
                            LinePrint = 0;
                        }
                        if (cont == 2)
                        {
                            command += "121200000350145" + Week + "\n";
                            command += "131100000150145" + LabelUnit + "\n";
                            command += "121100000000145" + ColaboratorName + "\n";
                            command += "121080000430220" + Code + "\n";
                            command += "1W1D44000000002152HM," + Code + "\n";
                            LinePrint = 0;
                        }
                        if (cont == 3)
                        {
                            cont = 0;
                            command += "121200000350280" + Week + "\n";
                            command += "131100000150280" + LabelUnit + "\n";
                            command += "121100000000280" + ColaboratorName + "\n";
                            command += "121080000430355" + Code + "\n";
                            command += "1W1D44000000003502HM," + Code + "\n";

                            command += "E L\n";
                            LinePrint = 1;
                        }
                        cont++;
                    }
                    if (LinePrint == 0)
                    {
                        command += "E L\n";
                    }
                }
            }
            if (Printer == 2) //Datamax BQT
            {
                for (int cant = 0; cant < ArrayCodes.Count(); cant++)
                {
                    int cont = 1;
                    int numero = Convert.ToInt32(ArrayCodes[cant]);

                    var Code = numero.ToString("D6");
                    ColaboratorName = ArrSurNames[cant].Substring(0, 7) + "." + ArrNames[cant].Substring(0, 3);

                    if (NameEnable == 1)
                    {
                        command += "L D11 H15\n";
                        command += "131300000100010" + ArrSurNames[cant] + ArrNames[cant] + "\n";
                        command += "E L\n";
                    }
                    for (int total = 0; total < Convert.ToInt32(Quantity); total++)
                    {

                        if (cont == 1)
                        {
                            command += "L D11 H15\n";
                            command += "121200000350001" + Week + "\n";
                            command += "131100000150001" + LabelUnit + "\n";
                            command += "121100000000001" + ColaboratorName + "\n";
                            command += "121080000430075" + Code + "\n";
                            command += "1W1D44000000000712HM," + Code + "\n";
                            LinePrint = 0;
                        }
                        if (cont == 2)
                        {
                            command += "121200000350135" + Week + "\n";
                            command += "131100000150136" + LabelUnit + "\n";
                            command += "121100000000137" + ColaboratorName + "\n";
                            command += "121080000430210" + Code + "\n";
                            command += "1W1D44000000002052HM," + Code + "\n";
                            LinePrint = 0;
                        }
                        if (cont == 3)
                        {
                            cont = 0;
                            command += "121200000350270" + Week + "\n";
                            command += "131100000150271" + LabelUnit + "\n";
                            command += "121100000000272" + ColaboratorName + "\n";
                            command += "121080000430346" + Code + "\n";
                            command += "1W1D44000000003402HM," + Code + "\n";

                            command += "E L\n";
                            LinePrint = 1;
                        }
                        cont++;
                    }
                    if (LinePrint == 0)
                    {
                        command += "E L\n";
                    }
                }
            }
            if (Printer == 3) //ZEBRA
            {
                for (int cant = 0; cant < ArrayCodes.Count(); cant++)
                {
                    int cont = 1;
                    int numero = Convert.ToInt32(ArrayCodes[cant]);

                    //ColaboratorName = ArrSurnames[cant].Substring(0, 9) + " " + ArrNames[cant].Substring(0, 3);
                    ColaboratorName = ArrSurNames[cant];
                    ColaboratorSurname = ArrNames[cant];
                    var Code = numero.ToString("D6");

                    if (NameEnable == 1)
                    {
                        command += "^XA ^BY1,1,10\n";
                        command += "^FO50,30^A0N50,60^FD" + ArrNames[cant] + " " + ArrSurNames[cant] + "^FS\n";
                        command += "^XZ\n";
                    }

                    for (int total = 0; total < Convert.ToInt32(Quantity); total++)
                    {
                        if (cont == 1)
                        {
                            command += "^XA ^BY1,1,10\n";
                            command += "^FO170,14^BQN,10,4,N,N^FV   " + Code + "^FS\n";
                            command += "^FO180,0^A0N50,20^FD" + Code + "^FS\n";
                            command += "^FO10,10^A0N50,30^FD" + Week + "^FS\n";
                            command += "^FO10,40^A0N50,30^FD" + LabelUnit + "^FS\n";
                            command += "^FO10,70^A0N50,14^FD" + ColaboratorSurname + "^FS\n";
                            command += "^FO10,90^A0N50,14^FD" + ColaboratorName + "^FS\n";
                            LinePrint = 0;
                        }
                        if (cont == 2)
                        {
                            command += "^FO440,14^BQN,10,4,N,N^FV   " + Code + "^FS\n";
                            command += "^FO450,0^A0N50,20^FD" + Code + "^FS\n";
                            command += "^FO285,10^A0N50,30^FD" + Week + "^FS\n";
                            command += "^FO285,40^A0N50,30^FD" + LabelUnit + "^FS\n";
                            command += "^FO285,70^A0N50,14^FD" + ColaboratorSurname + "^FS\n";
                            command += "^FO285,90^A0N50,14^FD" + ColaboratorName + "^FS\n";
                            LinePrint = 0;
                        }

                        if (cont == 3)
                        {
                            cont = 0;
                            command += "^FO710,14^BQN,10,4,N,N^FV   " + Code + "^FS\n";
                            command += "^FO720,0^A0N50,20^FD" + Code + "^FS\n";
                            command += "^FO555,10^A0N50,30^FD" + Week + "^FS\n";
                            command += "^FO555,40^A0N50,30^FD" + LabelUnit + "^FS\n";
                            command += "^FO555,70^A0N50,14^FD" + ColaboratorSurname + "^FS\n";
                            command += "^FO555,90^A0N50,14^FD" + ColaboratorName + "^FS\n";
                            command += "^XZ\n";
                            LinePrint = 1;
                        }
                        cont++;
                    }
                    if (LinePrint == 0)
                    {
                        command += "^XZ\n";
                    }
                }
            }
            if (Printer == 4) //Zebra PC SENDERO
            {
                for (int cant = 0; cant < ArrayCodes.Count(); cant++)
                {
                    int cont = 1;
                    int numero = Convert.ToInt32(ArrayCodes[cant]);

                    var Code = numero.ToString("D6");

                    command += "^XA ^BY1,1,10\n";
                    command += "^FO50,30^A0N50,60^FD" + ArrNames[cant] + "^FS\n";
                    command += "^XZ\n";

                    for (int total = 0; total < Convert.ToInt32(Quantity); total++)
                    {
                        if (cont == 1)
                        {
                            command += "^XA ^BY1,1,10\n";
                            command += "^FO160,3^BQN,10,4,N,N^FV  " + Code + "^FS\n";
                            command += "^FO30,15^A0N50,30^FD" + Week + "^FS\n";
                            command += "^FO30,45^A0N50,30^FD" + LabelUnit + "^FS\n";
                            command += "^FO30,75^A0N50,30^FD" + Code + "^FS\n";
                            LinePrint = 0;
                        }
                        if (cont == 2)
                        {
                            command += "^FO440,3^BQN,10,4,N,N^FV  " + Code + "^FS\n";
                            command += "^FO310,15^A0N50,30^FD" + Week + "^FS\n";
                            command += "^FO310,45^A0N50,30^FD" + LabelUnit + "^FS\n";
                            command += "^FO310,75^A0N50,30^FD" + Code + "^FS\n";
                            LinePrint = 0;
                        }
                        if (cont == 3)
                        {
                            cont = 0;
                            command += "^FO710,3^BQN,10,4,N,N^FV  " + Code + "^FS\n";
                            command += "^FO580,15^A0N50,30^FD" + Week + "^FS\n";
                            command += "^FO580,45^A0N50,30^FD" + LabelUnit + "^FS\n";
                            command += "^FO580,75^A0N50,30^FD" + Code + "^FS\n";
                            command += "^XZ\n";
                            LinePrint = 0;
                        }
                        cont++;
                    }
                    if (LinePrint == 0)
                    {
                        command += "^XZ\n";
                    }

                }
            }
            if (Printer == 5) //ZEBRA BQT
            {
                for (int cant = 0; cant < ArrayCodes.Count(); cant++)
                {
                    int cont = 1;
                    int numero = Convert.ToInt32(ArrayCodes[cant]);

                    var Code = numero.ToString("D6");

                    command += "^XA ^BY1,1,10\n";
                    command += "^FO50,30^A0N50,60^FD" + ArrNames[cant] + "^FS\n";
                    command += "^XZ\n";

                    for (int total = 0; total < Convert.ToInt32(Quantity); total++)
                    {
                        if (cont == 1)
                        {
                            command += "^XA ^BY1,1,10\n";
                            command += "^FO150,3^BQN,10,4,N,N^FV  " + Code + "^FS\n";
                            command += "^FO10,15^A0N50,20^FD" + Week + "^FS\n";
                            command += "^FO10,45^A0N50,20^FD" + LabelUnit + "^FS\n";
                            command += "^FO10,75^A0N50,30^FD" + Code + "^FS\n";
                            LinePrint = 0;
                        }
                        if (cont == 2)
                        {
                            command += "^FO430,3^BQN,10,4,N,N^FV  " + Code + "^FS\n";
                            command += "^FO290,15^A0N50,20^FD" + Week + "^FS\n";
                            command += "^FO290,45^A0N50,20^FD" + LabelUnit + "^FS\n";
                            command += "^FO290,75^A0N50,30^FD" + Code + "^FS\n";
                            LinePrint = 0;
                        }
                        if (cont == 3)
                        {
                            cont = 0;
                            command += "^FO700,3^BQN,10,4,N,N^FV  " + Code + "^FS\n";
                            command += "^FO560,15^A0N50,20^FD" + Week + "^FS\n";
                            command += "^FO560,45^A0N50,20^FD" + LabelUnit + "^FS\n";
                            command += "^FO560,75^A0N50,30^FD" + Code + "^FS\n";
                            command += "^XZ\n";
                            LinePrint = 0;
                        }
                        cont++;
                    }
                    if (LinePrint == 0)
                    {
                        command += "^XZ\n";
                    }

                }
            }


            var NombreArchivo = "print.txt";
            string filePath = HelperController.VerifyExportEnvironment(NombreArchivo);


            //using (StreamWriter writer = System.IO.File.CreateText(filePath))
            //{
            //    writer.WriteLine(command);
            //    writer.Close();
            //}

            StreamWriter sw = new StreamWriter(filePath);
            sw.WriteLine(command);
            sw.Close();
            return Json(NombreArchivo);

        }

        public static string GetWeek(DateTime time)
        {
            int Week = 0;

            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            Week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            return time.Year.ToString("0000") + "-" + Week.ToString();
        }

        public ActionResult Download(string nombre)
        {
            string filePath = HelperController.VerifyExportEnvironment("");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath + nombre);
            System.IO.File.Delete(filePath + nombre);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombre);
        }
    }
}