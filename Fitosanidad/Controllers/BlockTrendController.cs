﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;


namespace Fitosanidad.Controllers
{
    public class BlockTrendController : BaseController
    {
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        SIEMBRASEntities siembrasEntities = null;
        // GET: BlockTrend
        public ActionResult Index()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().OrderBy(x => x.Name).ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            var product_Lst = siembras_PLANNEREntities.fn_getAllActiveProducts().OrderBy(x => x.Name).ToList();
            SelectList productList = new SelectList(product_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["productList"] = productList;

            var listBT = siembras_PLANNEREntities.BAS_Pests.OrderBy(a => a.ID).OrderBy(x => x.Name);
            SelectList listaBT = new SelectList((from s in listBT
                                                 select new
                                                 {
                                                     Id = s.ID,
                                                     Name = s.Name + " - " + s.Abrr
                                                 }),
                            "Id",
                            "Name",
                            null);
            ViewData["listaBT"] = listaBT;

            return View();
        }

        public List<BlockTrendResultModel> Results(BlockTrendModel d)
        {
            SqlConnection entityConnection = (SqlConnection)siembrasEntities.Database.Connection;
            SqlConnection cnn = entityConnection;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "InformeTendenciaBloque";
        
            SqlParameter prmIDUnidadesNegocios = new SqlParameter("@prmIDUnidadesNegocios", SqlDbType.Int);
            SqlParameter prmBlanco = new SqlParameter("@prmBlanco", SqlDbType.Int);
            SqlParameter prmClasesProductos = new SqlParameter("@prmClasesProductos", SqlDbType.Int);

            prmIDUnidadesNegocios.Value = d.prmIDUnidadesNegocios;
            prmBlanco.Value = d.prmBlanco;
            prmClasesProductos.Value = d.prmClasesProductos;

            cmd.Parameters.Add(prmIDUnidadesNegocios);
            cmd.Parameters.Add(prmBlanco);
            cmd.Parameters.Add(prmClasesProductos);

            cnn.Open();

            List<BlockTrendResultModel> ResultList = new List<BlockTrendResultModel>();

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    BlockTrendResultModel result = new BlockTrendResultModel();

                    result.IDUnidadesNegocios_Nombre = Convert.ToString(dr[0]);
                    result.IDInvernaderos_Codigo = Convert.ToString(dr[1]);
                    result.IDClasesProductos_Nombre = Convert.ToString(dr[2]);
                    result.Semana = Convert.ToString(dr[3]);
                    result.IDPlagas_Nombre = Convert.ToString(dr[4]);
                    result.PorcIncidencia = Convert.ToDecimal(dr[5]);
                    result.umbral = Convert.ToInt32(dr[7]);

                    ResultList.Add(result);
                }
                dr.Close();
            }

            cnn.Close();
            //var Results = ResultList.Select(m => m.IDInvernaderos_Codigo).Distinct().ToList();
            return ResultList;

        }

        [HttpPost]
        public JsonResult CantidadBloques(BlockTrendModel d)
        {
            siembrasEntities = new SIEMBRASEntities();

            List<BlockTrendResultModel> BlockTrendResultModel = Results(d);

            var Traedatos_sp = Results(d).OrderBy(m => m.IDInvernaderos_Codigo).Select(m => m.IDInvernaderos_Codigo).Distinct().ToList();

            return Json(Traedatos_sp, JsonRequestBehavior.AllowGet);
        }
    }
}