﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planner.Model;
using Resources;

using System.Globalization;

using System.Data;
using System.Data.SqlClient;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class Auth_PermissionsController : BaseController
    {
        BASEntities _BASEntities = null;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexGrid()
        {
            try
            {
                return Json(GetAllAuth_Permissions(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        public List<Auth_Permissions> GetAllAuth_Permissions()
        {
            _BASEntities = new BASEntities();
            List<Auth_Permissions> Results = _BASEntities.Auth_Permissions.ToList();

            return Results;
        }

    }
}