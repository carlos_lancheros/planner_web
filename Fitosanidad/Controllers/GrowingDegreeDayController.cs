﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planner.Model;
using Resources;
using Newtonsoft.Json;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class GrowingDegreeDayController : BaseController
    {
        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;

        SelectList lstBusinessUnit = null;
        SelectList lstGreenhouse = null;

        int intBusinessUnitId;
        int intGreenhouseId;

        public ActionResult GrowingDegreeDays()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            getBusinessUnitList();

            intBusinessUnitId = Int32.Parse(lstBusinessUnit.FirstOrDefault().Value);

            getGreenhouseList(intBusinessUnitId);

            intGreenhouseId = Int32.Parse(lstGreenhouse.FirstOrDefault().Value);

            return View();
        }

        public ActionResult getBusinessUnitList()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var listBusinessUnit = siembras_PLANNEREntities.fn_getBusinessUnitsSensor().Where(n => n.BusinessUnitID != 0);
            lstBusinessUnit = new SelectList(listBusinessUnit, "BusinessUnitID", "Name".Trim().ToUpper());
            ViewData["lstBusinessUnit"] = lstBusinessUnit;

            return Json(lstBusinessUnit, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getGreenhouseList(int intBusinessUnitId)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var listGreenhouse = siembras_PLANNEREntities.fn_getGreenHouseSensorInstall(intBusinessUnitId).OrderBy(a => a.Codigo);
            lstGreenhouse = new SelectList(listGreenhouse, "ID", "Codigo".Trim().ToUpper(), intBusinessUnitId);
            ViewData["lstGreenhouse"] = lstGreenhouse;

            return Json(lstGreenhouse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getGrowingDegreeDaysTable(int? intBusinessUnitId, int? intGreenhouseId, DateTime startDate, DateTime endDate)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var Result = siembras_PLANNEREntities.fn_getGDegreeDaybyParams(intBusinessUnitId, intGreenhouseId, startDate, endDate).OrderBy(a => a.Date);
            ViewBag.dataGdd = Result.ToList();

            return Json(Result.ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult getGrowingDegreeDaysGraphic(int? intBusinessUnitId, int? intGreenhouseId, DateTime? startDate, DateTime? endDate)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var Result = siembras_PLANNEREntities.fn_getTotalGrowingDegreeDays(intBusinessUnitId, intGreenhouseId, startDate, endDate).OrderBy(a => a.Date);
            ViewBag.DailyGdd = Result.ToList();

            return Json(Result.ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult getGrowingDegreeDaysTotal(int? intBusinessUnitId, int? intGreenhouseId, DateTime? startDate, DateTime? endDate)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var Result = siembras_PLANNEREntities.sp_GetTotalGDegreeDaybyParams(intBusinessUnitId, intGreenhouseId, startDate, endDate).FirstOrDefault();
            ViewData["totalGdd"] = Result;

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
    }
}