﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planner.Model;
using Resources;

using System.Globalization;

using System.Data;
using System.Data.SqlClient;

using RestSharp;
using System.Web.Script.Serialization;
using System.Web.Hosting;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Configuration;
using System.Data.EntityClient;

using iTextSharp.tool.xml;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class OrdersController : BaseController
    {
        public ActionResult Confirm()
        {
            try
            {
                List<SelectListItem> lstTipoCarga = new List<SelectListItem>();
                lstTipoCarga.Add(new SelectListItem() { Text = Resources.Resources.FechaDeEnvioDelCultivo, Value = "0" });
                lstTipoCarga.Add(new SelectListItem() { Text = Resources.Resources.FechaDeEnvioDeMiami, Value = "1" });
                ViewData["lstTipoCarga"] = new SelectList(lstTipoCarga, "Value", "Text");

                List<SelectListItem> lstOrders = new List<SelectListItem>();
                lstOrders.Add(new SelectListItem() { Text = Resources.Resources.Todo, Value = "1" });
                lstOrders.Add(new SelectListItem() { Text = Resources.Resources.ParaProduccion, Value = "3" });
                lstOrders.Add(new SelectListItem() { Text = Resources.Resources.ProyeccionALargoPlazo, Value = "2" });
                ViewData["lstOrders"] = new SelectList(lstOrders, "Value", "Text");

                return View();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public ActionResult Detail(int ID)
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public void Detail2Pdf(int DetailID, int ID)
        {
            try
            {
                DataTable tblDataTable = new DataTable();
                Document pdfDoc = new Document();
                Font fntContent = new Font();
                string[,] strColumns = new string[3, 3];
                strColumns[0, 0] = "ID";
                strColumns[0, 1] = "ID";
                strColumns[0, 2] = "26;152;166,0,0";

                strColumns[1, 0] = "ProductoRamo";
                strColumns[1, 1] = "Producto Ramo";
                strColumns[1, 2] = "26;152;166,1,1";

                strColumns[2, 0] = "UpcBarcodeBox";
                strColumns[2, 1] = "UPC";
                strColumns[2, 2] = "26;152;166,2,2";

                MemoryStream memStream = new MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memStream);
                pdfDoc.Open();

                int intColTot = 0;
                int intRecByPage = 50;

                string cnnString = ConfigurationManager.ConnectionStrings["NewSafexEntities"].ConnectionString;
                EntityConnectionStringBuilder e = new EntityConnectionStringBuilder(cnnString);
                string ProviderConnectionString = e.ProviderConnectionString;

                SqlConnection entityConnection = new SqlConnection(ProviderConnectionString);
                SqlConnection cnn = entityConnection;

                cnn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cnn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "PrintOrdenesIFURecetaUPC";

                    cmd.Parameters.Add(new SqlParameter("@prmIDSeccion", SqlDbType.Int));
                    cmd.Parameters.Add(new SqlParameter("@prmIDOrdenesEnsambleOriginalesDetalles", SqlDbType.Int));

                    cmd.Parameters["@prmIDSeccion"].Value = 1;
                    cmd.Parameters["@prmIDOrdenesEnsambleOriginalesDetalles"].Value = 6744332;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(tblDataTable);
                    }

                    intColTot = 3;
                }
                cnn.Close();

                HelperController.DataTable2Pdf(tblDataTable, pdfDoc, fntContent, intColTot, intRecByPage, strColumns);

                pdfDoc.NewPage();


                pdfDoc.Close();
                writer.Close();

                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=PO_" + DetailID.ToString() + "_" + DateTime.Now.ToString("MMddhhss") + ".pdf");
                Response.ContentType = "application/pdf";
                Response.OutputStream.Write(memStream.GetBuffer(), 0, memStream.GetBuffer().Length);
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
                Response.Close();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public ActionResult UPC2Pdf(int DetailID)
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public ActionResult Verify()
        {
            try
            {
                List<Dictionary<string, object>> AssociatedFarms_Lst = GetAssociatedFarms(-1, -1);

                List<string> AssociatedFarmsLst = new List<string>();

                foreach (var Farm in AssociatedFarms_Lst)
                {
                    if (!AssociatedFarmsLst.Contains(Farm["Farm"].ToString()))
                    {
                        AssociatedFarmsLst.Add(Farm["Farm"].ToString());
                    }
                }

                AssociatedFarmsLst.Sort();

                SelectList FincasAsociadas_Lst = new SelectList(AssociatedFarmsLst);
                ViewData["FincasAsociadas_Lst"] = FincasAsociadas_Lst;

                return View();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult VerifyGetData(int prmCodigoCliente, int prmCodigoFinca)
        {
            try
            {
                List<Dictionary<string, object>> lstResult = GetAssociatedFarms(prmCodigoCliente, prmCodigoFinca);

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public ActionResult Pending()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult PendingGetData()
        {
            try
            {
                List<Dictionary<string, object>> lstResult = GetVerifyGeneralesOrdersWithoutFirm();

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public ActionResult ToCancel()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult ToCancelGetData(string[] prmIDFarm)
        {
            try
            {
                List<Dictionary<string, object>> lstResult = GetShowOrderDetailToCancel(prmIDFarm);

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public List<Dictionary<string, object>> GetShowOrderDetailToCancel(string[] prmIDFarm)
        {
            try
            {
                string strFarms = (string.Join(",", prmIDFarm) == string.Empty ? null : string.Join(",", prmIDFarm));

                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetShowOrderDetailToCancel.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetShowOrderDetailToCancel", Method.GET);

                request.AddParameter("prmIDFarm", strFarms);


                request.AddHeader("Content-Type", "application/json");

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                return lstResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Dictionary<string, object>> GetVerifyGeneralesOrdersWithoutFirm()
        {
            try
            {
                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetVerifyGeneralesOrdersWithoutFirm.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetVerifyGeneralesOrdersWithoutFirm", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                return lstResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Dictionary<string, object>> GetAssociatedFarms(int? prmCodigoCliente, int? prmCodigoFinca)
        {
            try
            {
                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetLoadAssociatedFarms.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/GetLoadAssociatedFarms", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                if (prmCodigoCliente != null)
                {
                    request.AddParameter("prmCodigoCliente", prmCodigoCliente);
                }
                if (prmCodigoFinca != null)
                {
                    request.AddParameter("prmCodigoFinca", prmCodigoFinca);
                }

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                return lstResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult JsonGetAssociatedFarms(int? prmCodigoCliente, int? prmCodigoFinca)
        {
            try
            {
                List<Dictionary<string, object>> AssociatedFarms_Lst = GetAssociatedFarms(prmCodigoCliente, prmCodigoFinca);

                return this.Json(AssociatedFarms_Lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Dictionary<string, object>> GetLoadDetailsOrders(Guid prmIDCabezas, int prmFarm, int prmFarmOrder)
        {
            try
            {
                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetLoadDetailsOrders.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/GetLoadDetailsOrders", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmIDCabezas", prmIDCabezas);
                request.AddParameter("prmFarm", prmFarm);
                request.AddParameter("prmFarmOrder", prmFarmOrder);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                return lstResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult JsonGetLoadDetailsOrders(Guid prmIDCabezas, int prmFarm, int prmFarmOrder)
        {
            try
            {
                List<Dictionary<string, object>> DetailsOrders_Lst = GetLoadDetailsOrders(prmIDCabezas, prmFarm, prmFarmOrder);

                return this.Json(DetailsOrders_Lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult Export(string ExportData)
        {
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                StringReader reader = new StringReader(ExportData);
                Document PdfFile = new Document(PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(PdfFile, stream);
                PdfFile.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, PdfFile, reader);
                PdfFile.Close();
                return File(stream.ToArray(), "application/pdf", "ExportData.pdf");
            }
        }

        public ActionResult Bunch(int DetailID, int ID)
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public ActionResult Stem(int DetailID, int BunchID, int ID)
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult CargarOrdenesSyncFincas(int? prmIDSeccion = null, string[] prmIDFarm = null, string prmFInicial = null, string prmFFinal = null, int? prmTipoCarga = null)
        {
            try
            {
                string strFarms = (string.Join(",", prmIDFarm) == string.Empty ? null : string.Join(",", prmIDFarm));

                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://getorderssyncfarms.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetOrdersSyncFarms", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmIDSeccion", prmIDSeccion);
                request.AddParameter("prmIDFarm", strFarms);
                request.AddParameter("prmFInicial", prmFInicial);
                request.AddParameter("prmFFinal", prmFFinal);
                request.AddParameter("prmTipoCarga", prmTipoCarga);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult GetOrderDetailById(int prmIDordenesFinca)
        {
            try
            {
                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetOrderDetailById.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetOrderDetailById", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmIDordenesFinca", prmIDordenesFinca);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult GetOrderDetailRecipe(int prmOrdenesEnsambleOriginalesDetalles, int prmIDOrdenesEnsambleOriginalesDetallesReceta)
        {
            try
            {
                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetOrderDetailRecipe.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetOrderDetailRecipe", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmOrdenesEnsambleOriginalesDetalles", prmOrdenesEnsambleOriginalesDetalles);
                request.AddParameter("prmIDOrdenesEnsambleOriginalesDetallesReceta", prmIDOrdenesEnsambleOriginalesDetallesReceta);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpGet]
        public FileContentResult Download(int prmIDSeccion, string[] prmIDFarm, string[] prmOrders)
        {
            try
            {
                DateTime dtFileName = DateTime.Now;

                string strFileName = "DO" + Session["usrUserID"].ToString() + dtFileName.ToString("MMddhhss") + ".txt";

                string strFarms = (string.Join(",", prmIDFarm) == string.Empty ? null : string.Join(",", prmIDFarm));
                string strOrders = (string.Join(",", prmOrders) == string.Empty ? null : string.Join(",", prmOrders));
                var strCsv = string.Empty;

                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetFileDownloadOrdersDetail.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("/api/NewSafex/GetFileDownloadOrdersDetail", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmIDSeccion", prmIDSeccion);
                request.AddParameter("prmIDFarm", strFarms);
                request.AddParameter("prmOrders", strOrders);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);

                    foreach (var row in lstResult)
                    {
                        foreach (var column in row)
                        {
                            if (column.Key != "ID")
                            {
                                strCsv = strCsv + column.Value + ";";
                            }
                        }
                        strCsv = strCsv.Substring(0, strCsv.Length - 1) + Environment.NewLine;
                    }
                }
                else
                {

                }

                return File(new UTF8Encoding().GetBytes(strCsv), "text/csv", strFileName);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public FileContentResult DownloadLabels(string[] prmOrders)
        {
            try
            {
                DateTime dtFileName = DateTime.Now;

                string strFileName = "LPF" + Session["usrUserID"].ToString() + dtFileName.ToString("MMddhhss") + ".txt";

                string strOrders = (string.Join(",", prmOrders) == string.Empty ? null : string.Join(",", prmOrders));
                var strCsv = string.Empty;

                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetFileDownloadOrdersDetail.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("/api/NewSafex/GetFileDownloadOrdersDetail", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmOrders", strOrders);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);

                    foreach (var row in lstResult)
                    {
                        foreach (var column in row)
                        {
                            if (column.Key != "ID")
                            {
                                strCsv = strCsv + column.Value + ";";
                            }
                        }
                        strCsv = strCsv.Substring(0, strCsv.Length - 1) + Environment.NewLine;
                    }
                }
                else
                {

                }

                return File(new UTF8Encoding().GetBytes(strCsv), "text/csv", strFileName);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public FileContentResult GetCsvFile()
        {
            return File(GetCsv(), "text/csv", "filename.csv");
        }

        public byte[] GetCsv()
        {
            var stream = new MemoryStream();
            var csvWriter = new StreamWriter(stream, Encoding.GetEncoding("shift-jis"));
            csvWriter.WriteLine(String.Format("{0},{1},{2},{3}", "aaa1", "bbb1", "ccc1", "ddd1"));
            csvWriter.WriteLine(String.Format("{0},{1},{2},{3}", "aaa2", "bbb2", "ccc2", "ddd2"));
            csvWriter.Flush();

            return stream.ToArray();
        }

        [HttpPost]
        public ActionResult GetUPCRecipe(int? prmIDSeccion = null, int? prmIDOrdenesEnsambleOriginalesDetalles = null)
        {
            try
            {
                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetUPCRecipe.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetUPCRecipe", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmIDSeccion", prmIDSeccion);
                request.AddParameter("prmIDOrdenesEnsambleOriginalesDetalles", prmIDOrdenesEnsambleOriginalesDetalles);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult SetBouquetRow(int prmIDDetalleOriginal)
        {
            try
            {
                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetCheckBoxTypeIsWetBox.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetCheckBoxTypeIsWetBox", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmIDDetalleOriginal", prmIDDetalleOriginal);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                if (lstResult.Count == 1 && Convert.ToInt32(lstResult.FirstOrDefault()["IsWetBox"]) == 1)
                {
                    lstResult = new List<Dictionary<string, object>>();

                    url = "http://PutUpdateConsolidatedFlagDetails.sendero.api";
                    restClient = new RestClient(url);
                    request = new RestRequest("api/NewSafex/PutUpdateConsolidatedFlagDetails", Method.GET);

                    request.AddHeader("Content-Type", "application/json");

                    request.AddParameter("prmIDDetalles", prmIDDetalleOriginal);

                    response = restClient.Execute(request);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        serializer.MaxJsonLength = int.MaxValue;
                        lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                    }
                    else
                    {

                    }
                }
                else
                {

                }

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult GetVerifyPermissionsPinWeb(int prmAppUserWeb)
        {
            try
            {
                int lstResult = 0;

                var url = "http://GetVerifyPermissionsPinWeb.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetVerifyPermissionsPinWeb", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmAppUserWeb", prmAppUserWeb);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = Convert.ToInt32(response.Content);
                }
                else
                {

                }

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult GetVerifyGeneralPermits(int prmAppUserWeb)
        {
            try
            {
                int lstResult = 0;

                var url = "http://GetVerifyGeneralPermits.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetVerifyGeneralPermits", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmAppUserWeb", prmAppUserWeb);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = Convert.ToInt32(response.Content);
                }
                else
                {

                }

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult GetWebGuideKey()
        {
            try
            {
                int lstResult = 0;

                var url = "http://GetWebGuideKey.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetWebGuideKey", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = Convert.ToInt32(response.Content);
                }
                else
                {

                }

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public ActionResult Pin()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public ActionResult UpcPhotos()
        {
            try
            {
                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetAllFarms.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/GetAllFarms", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                List<SelectListItem> lstFarms = new List<SelectListItem>();

                foreach (Dictionary<string, object> row in lstResult)
                {
                    bool llFlagActivo = false;
                    string intId = string.Empty;
                    string strName = string.Empty;

                    foreach (string column in row.Keys)
                    {
                        if (column == "ID")
                        {
                            intId = row[column].ToString(); ;
                        }
                        if (column == "Nombre")
                        {
                            strName = row[column].ToString().ToUpper();
                        }
                        if (column == "FlagActivo" && Convert.ToInt32(row[column]) == 1)
                        {
                            llFlagActivo = true;
                        }
                    }

                    if (llFlagActivo && !intId.Trim().Equals("-1") && !intId.Trim().Equals("632"))
                    {
                        lstFarms.Add(new SelectListItem() { Text = strName, Value = intId });
                    }
                }

                ViewData["lstFarms"] = new SelectList(lstFarms.OrderBy(x => x.Text), "Value", "Text");

                return View();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult GetUPCPhotosInformation(DateTime? prmFechaIni = null, DateTime? prmFechaFin = null, int? prmIDFarm = null)
        {
            try
            {
                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetUPCPhotosInformation.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetUPCPhotosInformation", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                DateTime datFInicial = Convert.ToDateTime(prmFechaIni).Date;
                DateTime datFFinal = Convert.ToDateTime(prmFechaFin).Date.AddDays(1).AddMilliseconds(-1);

                request.AddParameter("prmFechaIni", datFInicial.Month + "-" + datFInicial.Day + "-" + datFInicial.Year);
                request.AddParameter("prmFechaFin", datFFinal.Month + "-" + datFFinal.Day + "-" + datFFinal.Year);
                request.AddParameter("prmIDFarm", prmIDFarm);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult GetInformationToReviewUPC(int? prmIDOrdenesEnsambleOriginalesDetalles = null)
        {
            try
            {
                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetInformationToReviewUPC.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetInformationToReviewUPC", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmIDOrdenesEnsambleOriginalesDetalles", prmIDOrdenesEnsambleOriginalesDetalles);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult GetUPCPhoto(int? prmIDOrdenesEnsmableOriginalesDetalles = null)
        {
            try
            {
                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetUPCPhoto.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetUPCPhoto", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmIDOrdenesEnsmableOriginalesDetalles", prmIDOrdenesEnsmableOriginalesDetalles);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult GetUPCPhotoFile(string strPhotoFileName)
        {
            try
            {
                strPhotoFileName = strPhotoFileName.Trim();

                HelperController.DownloadFile("174.143.52.110", "831425-admin", "B0m/B6sj@i", "AplicacionesWeb/FlowareWeb3.0/ArchivosFotos", "", strPhotoFileName);

                var jsonResult = Json(string.Empty, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                var jsonResult = Json(ex.Message.ToString(), JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
        }

        [HttpPost]
        public ActionResult GetUPCTypeFile(string strUPCTypeFileName)
        {
            try
            {
                strUPCTypeFileName = strUPCTypeFileName.Trim();

                HelperController.DownloadFile("174.143.52.110", "831425-admin", "B0m/B6sj@i", "AplicacionesWeb/FlowareWeb3.0/UPC", "", strUPCTypeFileName);

                var jsonResult = Json(string.Empty, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                var jsonResult = Json(ex.Message.ToString(), JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
        }

        public ActionResult BoxLabel()
        {
            try
            {
                List<SelectListItem> lstTipoCarga = new List<SelectListItem>();
                lstTipoCarga.Add(new SelectListItem() { Text = Resources.Resources.FechaDeEntrega, Value = "0" });
                lstTipoCarga.Add(new SelectListItem() { Text = Resources.Resources.FechaDeEnvioDeMiami, Value = "1" });
                lstTipoCarga.Add(new SelectListItem() { Text = Resources.Resources.FechaDeEnvio, Value = "2" });
                ViewData["lstTipoCarga"] = new SelectList(lstTipoCarga, "Value", "Text");

                List<SelectListItem> lstLabelType = new List<SelectListItem>();
                lstLabelType.Add(new SelectListItem() { Text = Resources.Resources.EstandarYEspecial, Value = "0" });
                lstLabelType.Add(new SelectListItem() { Text = Resources.Resources.SoloEstandar, Value = "1" });
                lstLabelType.Add(new SelectListItem() { Text = Resources.Resources.SoloEspecial, Value = "2" });
                ViewData["lstLabelType"] = new SelectList(lstLabelType, "Value", "Text");

                List<SelectListItem> lstPrinter = new List<SelectListItem>();
                lstPrinter.Add(new SelectListItem() { Text = "DATAMAX", Value = "0" });
                lstPrinter.Add(new SelectListItem() { Text = "ZEBRA", Value = "1" });
                ViewData["lstPrinter"] = new SelectList(lstPrinter, "Value", "Text");

                string[] prmIDFarm = new string[2];
                int intIDFarm = 0;
                var lstAuth_UserFarm = Session["usrAuth_UserFarm"] as List<Auth_UserFarm>;
                foreach (Auth_UserFarm _Auth_UserFarm in lstAuth_UserFarm)
                {
                    prmIDFarm[intIDFarm] = _Auth_UserFarm.FarmID.ToString();
                    intIDFarm++;
                }

                List<Dictionary<string, object>> listOrdersToPrint = GetOrdersToPrint(prmIDFarm, DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("yyyyMMdd"), Convert.ToInt32(lstTipoCarga.FirstOrDefault().Value), 0);
                List<SelectListItem> lstOrdersToPrint = new List<SelectListItem>();
                foreach (Dictionary<string, object> row in listOrdersToPrint)
                {
                    string intId = string.Empty;
                    string strName = string.Empty;

                    foreach (string column in row.Keys)
                    {
                        if (column == "ID")
                        {
                            intId = row[column].ToString(); ;
                        }
                        if (column == "Clientecodigo")
                        {
                            strName = row[column].ToString().ToUpper();
                        }
                    }

                    lstOrdersToPrint.Add(new SelectListItem() { Text = strName, Value = intId });
                }
                ViewData["lstOrdersToPrint"] = new SelectList(lstOrdersToPrint, "Value", "Text");

                return View();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult BoxLabelGetData(string[] prmIDFarm = null, string prmFInicial = null, string prmFFinal = null, int? prmTipoCarga = null, int? prmOrder = null)
        {
            try
            {
                List<Dictionary<string, object>> lstResult = GetOrdersToPrint(prmIDFarm, prmFInicial, prmFFinal, prmTipoCarga, prmOrder);
                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public ActionResult UPCLabel()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult UPCGetData()
        {
            try
            {
                List<Dictionary<string, object>> lstResult = GetVerifyGeneralesOrdersWithoutFirm();

                var jsonResult = Json(lstResult, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public List<Dictionary<string, object>> GetOrdersToPrint(string[] prmIDFarm = null, string prmFInicial = null, string prmFFinal = null, int? prmTipoCarga = null, int? prmOrder = null)
        {
            try
            {
                string strFarms = (string.Join(",", prmIDFarm) == string.Empty ? null : string.Join(",", prmIDFarm));

                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetOrdersToPrint.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetOrdersToPrint", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmIDFarm", strFarms);
                request.AddParameter("prmFInicial", prmFInicial);
                request.AddParameter("prmFFinal", prmFFinal);
                request.AddParameter("prmTipoCarga", prmTipoCarga);
                request.AddParameter("prmOrder", prmOrder);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                return lstResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public JsonResult JsonGetOrdersToPrint(string[] prmIDFarm = null, string prmFInicial = null, string prmFFinal = null, int? prmTipoCarga = null, int? prmOrder = null)
        {
            try
            {
                List<Dictionary<string, object>> GetOrdersToPrint_Lst = GetOrdersToPrint(prmIDFarm, prmFInicial, prmFFinal, prmTipoCarga, prmOrder);

                return this.Json(GetOrdersToPrint_Lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Dictionary<string, object>> GetDetailOrdersToPrint(int prmIDOrdenesFincas)
        {
            try
            {
                List<Dictionary<string, object>> lstResult = new List<Dictionary<string, object>>();

                var url = "http://GetDetailOrdersToPrint.sendero.api";
                RestClient restClient = new RestClient(url);
                RestRequest request = new RestRequest("api/NewSafex/GetDetailOrdersToPrint", Method.GET);

                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("prmIDOrdenesFincas", prmIDOrdenesFincas);

                var response = restClient.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;
                    lstResult = serializer.Deserialize<List<Dictionary<string, object>>>(response.Content);
                }
                else
                {

                }

                return lstResult;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        public JsonResult JsonGetDetailOrdersToPrint(int prmIDOrdenesFincas)
        {
            try
            {
                List<Dictionary<string, object>> GetDetailOrdersToPrint_Lst = GetDetailOrdersToPrint(prmIDOrdenesFincas);

                return this.Json(GetDetailOrdersToPrint_Lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}