﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Globalization;

namespace Fitosanidad
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            const string defautlRouteUrl = "{controller}/{action}/{id}";
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            RouteValueDictionary defaultRouteValueDictionary = new RouteValueDictionary(
                    new { controller = "Home", action = "Index", id = UrlParameter.Optional });
            Route defaultRoute = new Route(defautlRouteUrl,
                    defaultRouteValueDictionary, new MvcRouteHandler());
            routes.Add("DefaultGlobalised",
                        new GlobalizedRoute(defaultRoute.Url, defaultRoute.Defaults));
            routes.Add("Default", new Route(defautlRouteUrl,
                        defaultRouteValueDictionary, new MvcRouteHandler()));
        }
    }
}
