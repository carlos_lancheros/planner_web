﻿using System.Web;
using System.Web.Optimization;

namespace Fitosanidad
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-2.1.3.js",
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui.js",
                "~/Scripts/blockui-master/jquery.blockUI.js",
                "~/Scripts/DataTables/media/js/jquery.dataTables.min.js",
                "~/Scripts/DataTables/media/js/dataTables.bootstrap.js",
                //"~/Scripts/datatables.min.js",
                "~/Scripts/jquery.table2excel.js",
                "~/Scripts/Select2.js",
                "~/Scripts/Site.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/jquery-ui.css",
                "~/Content/bootstrap.css",
                "~/Content/Select2.css",
                "~/Content/Site.css",
                "~/admin-lte/css/AdminLTE.css",
                "~/admin-lte/css/skins/skin-green-light.css",
                "~/Content/Datatables/media/css/dataTables.bootstrap.css"
                //"~/Content/datatables.css"
                ));

            bundles.Add(new ScriptBundle("~/admin-lte/js").Include(
             "~/admin-lte/js/adminlte.js"));

            //bundles.Add(new ScriptBundle("~/admin-lte/js").Include(
            // "~/admin-lte/js/app.js",
            // "~/admin-lte/plugins/fastclick/fastclick.js"
            // ));
        }
    }
}
